﻿namespace SGMS
{
    partial class Purchases
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.invNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invpaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unpaidinv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalInv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invuserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PayBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ViewInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PrintInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.TodaySells = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.invNumSearch = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.bPrint = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.tYear = new System.Windows.Forms.ComboBox();
            this.tMonth = new System.Windows.Forms.ComboBox();
            this.tDay = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.fYear = new System.Windows.Forms.ComboBox();
            this.fMonth = new System.Windows.Forms.ComboBox();
            this.fDay = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ViewBTN = new Bunifu.Framework.UI.BunifuTileButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.invVIEW = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.printLast = new Bunifu.Framework.UI.BunifuFlatButton();
            this.unPaid = new System.Windows.Forms.Label();
            this.AddIn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.paidCash = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.Label();
            this.dataGridView1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Sprice = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.newSeller = new Bunifu.Framework.UI.BunifuFlatButton();
            this.newbTN = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDel = new Bunifu.Framework.UI.BunifuFlatButton();
            this.sellerName = new System.Windows.Forms.ComboBox();
            this.bPrice = new System.Windows.Forms.TextBox();
            this.iCount = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnAdd = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label11 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.itemBox = new System.Windows.Forms.ComboBox();
            this.itemBarcode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.isBarcode = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.newItem = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.RWait = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.reP = new Bunifu.Framework.UI.BunifuFlatButton();
            this.edP = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel12.SuspendLayout();
            this.bunifuCards4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.bunifuCards2.SuspendLayout();
            this.bunifuCards3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RWait)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 39);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(868, 494);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "المؤجلات";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invNum,
            this.invpaid,
            this.unpaidinv,
            this.totalInv,
            this.clientName,
            this.invuserName,
            this.invDate,
            this.invTime});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.DoubleBuffered = true;
            this.dataGridView3.EnableHeadersVisualStyles = false;
            this.dataGridView3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView3.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView3.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(862, 441);
            this.dataGridView3.TabIndex = 15;
            this.dataGridView3.SelectionChanged += new System.EventHandler(this.dataGridView3_SelectionChanged);
            // 
            // invNum
            // 
            this.invNum.FillWeight = 87.27693F;
            this.invNum.HeaderText = "فاتورة";
            this.invNum.Name = "invNum";
            this.invNum.ReadOnly = true;
            // 
            // invpaid
            // 
            this.invpaid.HeaderText = "المدفوع";
            this.invpaid.Name = "invpaid";
            this.invpaid.ReadOnly = true;
            // 
            // unpaidinv
            // 
            this.unpaidinv.HeaderText = "غير مدفوع";
            this.unpaidinv.Name = "unpaidinv";
            this.unpaidinv.ReadOnly = true;
            // 
            // totalInv
            // 
            this.totalInv.HeaderText = "إجمالي الفاتورة";
            this.totalInv.Name = "totalInv";
            this.totalInv.ReadOnly = true;
            // 
            // clientName
            // 
            this.clientName.HeaderText = "إسم العميل";
            this.clientName.Name = "clientName";
            this.clientName.ReadOnly = true;
            // 
            // invuserName
            // 
            this.invuserName.HeaderText = "إسم المستخدم";
            this.invuserName.Name = "invuserName";
            this.invuserName.ReadOnly = true;
            // 
            // invDate
            // 
            this.invDate.HeaderText = "تاريخ الفاتورة";
            this.invDate.Name = "invDate";
            this.invDate.ReadOnly = true;
            // 
            // invTime
            // 
            this.invTime.HeaderText = "وقت الفاتورة";
            this.invTime.Name = "invTime";
            this.invTime.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.PayBtn);
            this.panel3.Controls.Add(this.ViewInvByNum);
            this.panel3.Controls.Add(this.PrintInvByNum);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 444);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(862, 47);
            this.panel3.TabIndex = 0;
            // 
            // PayBtn
            // 
            this.PayBtn.Activecolor = System.Drawing.Color.Maroon;
            this.PayBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PayBtn.BackColor = System.Drawing.Color.Maroon;
            this.PayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PayBtn.BorderRadius = 7;
            this.PayBtn.ButtonText = "دفع المؤجل";
            this.PayBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayBtn.DisabledColor = System.Drawing.Color.White;
            this.PayBtn.Enabled = false;
            this.PayBtn.Font = new System.Drawing.Font("Cairo", 8F);
            this.PayBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.PayBtn.Iconimage = null;
            this.PayBtn.Iconimage_right = null;
            this.PayBtn.Iconimage_right_Selected = null;
            this.PayBtn.Iconimage_Selected = null;
            this.PayBtn.IconMarginLeft = 0;
            this.PayBtn.IconMarginRight = 0;
            this.PayBtn.IconRightVisible = false;
            this.PayBtn.IconRightZoom = 0D;
            this.PayBtn.IconVisible = false;
            this.PayBtn.IconZoom = 20D;
            this.PayBtn.IsTab = false;
            this.PayBtn.Location = new System.Drawing.Point(466, 9);
            this.PayBtn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PayBtn.Name = "PayBtn";
            this.PayBtn.Normalcolor = System.Drawing.Color.Maroon;
            this.PayBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PayBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.PayBtn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PayBtn.selected = false;
            this.PayBtn.Size = new System.Drawing.Size(119, 30);
            this.PayBtn.TabIndex = 45;
            this.PayBtn.Text = "دفع المؤجل";
            this.PayBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PayBtn.Textcolor = System.Drawing.Color.White;
            this.PayBtn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayBtn.Click += new System.EventHandler(this.PayBtn_Click);
            // 
            // ViewInvByNum
            // 
            this.ViewInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ViewInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ViewInvByNum.BorderRadius = 7;
            this.ViewInvByNum.ButtonText = "عرض الفاتورة";
            this.ViewInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewInvByNum.DisabledColor = System.Drawing.Color.Gray;
            this.ViewInvByNum.Enabled = false;
            this.ViewInvByNum.Font = new System.Drawing.Font("Cairo", 10F);
            this.ViewInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.ViewInvByNum.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.ViewInvByNum.Iconimage_right = null;
            this.ViewInvByNum.Iconimage_right_Selected = null;
            this.ViewInvByNum.Iconimage_Selected = null;
            this.ViewInvByNum.IconMarginLeft = 25;
            this.ViewInvByNum.IconMarginRight = 0;
            this.ViewInvByNum.IconRightVisible = false;
            this.ViewInvByNum.IconRightZoom = 0D;
            this.ViewInvByNum.IconVisible = false;
            this.ViewInvByNum.IconZoom = 30D;
            this.ViewInvByNum.IsTab = false;
            this.ViewInvByNum.Location = new System.Drawing.Point(725, 9);
            this.ViewInvByNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ViewInvByNum.Name = "ViewInvByNum";
            this.ViewInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.ViewInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.ViewInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ViewInvByNum.selected = false;
            this.ViewInvByNum.Size = new System.Drawing.Size(119, 30);
            this.ViewInvByNum.TabIndex = 43;
            this.ViewInvByNum.Text = "عرض الفاتورة";
            this.ViewInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ViewInvByNum.Textcolor = System.Drawing.Color.White;
            this.ViewInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewInvByNum.Click += new System.EventHandler(this.ViewInvByNum_Click);
            // 
            // PrintInvByNum
            // 
            this.PrintInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PrintInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PrintInvByNum.BorderRadius = 7;
            this.PrintInvByNum.ButtonText = "طباعة الفاتورة";
            this.PrintInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PrintInvByNum.DisabledColor = System.Drawing.Color.Gray;
            this.PrintInvByNum.Enabled = false;
            this.PrintInvByNum.Font = new System.Drawing.Font("Cairo", 8F);
            this.PrintInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.PrintInvByNum.Iconimage = null;
            this.PrintInvByNum.Iconimage_right = null;
            this.PrintInvByNum.Iconimage_right_Selected = null;
            this.PrintInvByNum.Iconimage_Selected = null;
            this.PrintInvByNum.IconMarginLeft = 0;
            this.PrintInvByNum.IconMarginRight = 0;
            this.PrintInvByNum.IconRightVisible = false;
            this.PrintInvByNum.IconRightZoom = 0D;
            this.PrintInvByNum.IconVisible = false;
            this.PrintInvByNum.IconZoom = 20D;
            this.PrintInvByNum.IsTab = false;
            this.PrintInvByNum.Location = new System.Drawing.Point(595, 9);
            this.PrintInvByNum.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PrintInvByNum.Name = "PrintInvByNum";
            this.PrintInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.OnHovercolor = System.Drawing.Color.Green;
            this.PrintInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.PrintInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PrintInvByNum.selected = false;
            this.PrintInvByNum.Size = new System.Drawing.Size(119, 30);
            this.PrintInvByNum.TabIndex = 42;
            this.PrintInvByNum.Text = "طباعة الفاتورة";
            this.PrintInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrintInvByNum.Textcolor = System.Drawing.Color.White;
            this.PrintInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintInvByNum.Click += new System.EventHandler(this.PrintInvByNum_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(343, 47);
            this.panel4.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Cairo", 12F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(29, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 27);
            this.label3.TabIndex = 39;
            this.label3.Text = "0";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 7F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label4.Location = new System.Drawing.Point(213, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 19);
            this.label4.TabIndex = 38;
            this.label4.Text = "إجمالي المؤجلات";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(19, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(279, 35);
            this.label2.TabIndex = 40;
            this.label2.Text = " ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Location = new System.Drawing.Point(4, 39);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(868, 494);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "سجل المشتريات";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn15,
            this.Column2});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.DoubleBuffered = true;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView2.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView2.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView2.Location = new System.Drawing.Point(3, 102);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(862, 349);
            this.dataGridView2.TabIndex = 7;
            this.dataGridView2.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView2_RowsAdded);
            this.dataGridView2.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView2_RowsRemoved);
            this.dataGridView2.SelectionChanged += new System.EventHandler(this.dataGridView2_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn5.FillWeight = 58.56418F;
            this.dataGridViewTextBoxColumn5.HeaderText = "فاتورة";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn1.FillWeight = 220.2111F;
            this.dataGridViewTextBoxColumn1.HeaderText = "إسم المنتج";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn2.FillWeight = 57.05908F;
            this.dataGridViewTextBoxColumn2.HeaderText = "الكمية";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn3.FillWeight = 91.37062F;
            this.dataGridViewTextBoxColumn3.HeaderText = "سعر الشراء";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn4.FillWeight = 70.96779F;
            this.dataGridViewTextBoxColumn4.HeaderText = "إجمالي";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn6.FillWeight = 112.2738F;
            this.dataGridViewTextBoxColumn6.HeaderText = "المورد";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn7.FillWeight = 119.1419F;
            this.dataGridViewTextBoxColumn7.HeaderText = " المستخدم";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn15.FillWeight = 74.04188F;
            this.dataGridViewTextBoxColumn15.HeaderText = "الوقت";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 96.37024F;
            this.Column2.HeaderText = "التاريخ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(862, 99);
            this.panel2.TabIndex = 6;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.bunifuCards4);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(173, 99);
            this.panel12.TabIndex = 1;
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.BackColor = System.Drawing.Color.White;
            this.bunifuCards4.BorderRadius = 5;
            this.bunifuCards4.BottomSahddow = false;
            this.bunifuCards4.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards4.Controls.Add(this.TodaySells);
            this.bunifuCards4.Controls.Add(this.label1);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(17, 13);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = false;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(137, 75);
            this.bunifuCards4.TabIndex = 37;
            // 
            // TodaySells
            // 
            this.TodaySells.Font = new System.Drawing.Font("Cairo", 12F);
            this.TodaySells.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.TodaySells.Location = new System.Drawing.Point(20, 30);
            this.TodaySells.Name = "TodaySells";
            this.TodaySells.Size = new System.Drawing.Size(96, 36);
            this.TodaySells.TabIndex = 39;
            this.TodaySells.Text = "0";
            this.TodaySells.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(14, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 19);
            this.label1.TabIndex = 38;
            this.label1.Text = "إجمالي مشترايات الفترة";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.invNumSearch);
            this.panel11.Controls.Add(this.checkBox1);
            this.panel11.Controls.Add(this.bPrint);
            this.panel11.Controls.Add(this.bunifuCards2);
            this.panel11.Controls.Add(this.bunifuCards3);
            this.panel11.Controls.Add(this.ViewBTN);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(179, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(683, 99);
            this.panel11.TabIndex = 0;
            // 
            // invNumSearch
            // 
            this.invNumSearch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.invNumSearch.Enabled = false;
            this.invNumSearch.Location = new System.Drawing.Point(23, 56);
            this.invNumSearch.Name = "invNumSearch";
            this.invNumSearch.Size = new System.Drawing.Size(78, 32);
            this.invNumSearch.TabIndex = 44;
            this.invNumSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.invNumSearch.TextChanged += new System.EventHandler(this.invNumSearch_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Cairo", 8F);
            this.checkBox1.Location = new System.Drawing.Point(109, 61);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 24);
            this.checkBox1.TabIndex = 43;
            this.checkBox1.Text = "رقم الفاتورة";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // bPrint
            // 
            this.bPrint.BackColor = System.Drawing.Color.Green;
            this.bPrint.color = System.Drawing.Color.Green;
            this.bPrint.colorActive = System.Drawing.Color.DarkGreen;
            this.bPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bPrint.Enabled = false;
            this.bPrint.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.bPrint.ForeColor = System.Drawing.Color.White;
            this.bPrint.Image = global::SGMS.Properties.Resources.print;
            this.bPrint.ImagePosition = 10;
            this.bPrint.ImageZoom = 25;
            this.bPrint.LabelPosition = 0;
            this.bPrint.LabelText = "";
            this.bPrint.Location = new System.Drawing.Point(23, 12);
            this.bPrint.Margin = new System.Windows.Forms.Padding(6);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(78, 40);
            this.bPrint.TabIndex = 37;
            this.bPrint.Click += new System.EventHandler(this.bPrint_Click);
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.White;
            this.bunifuCards2.BorderRadius = 5;
            this.bunifuCards2.BottomSahddow = false;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards2.Controls.Add(this.tYear);
            this.bunifuCards2.Controls.Add(this.tMonth);
            this.bunifuCards2.Controls.Add(this.tDay);
            this.bunifuCards2.Controls.Add(this.label15);
            this.bunifuCards2.LeftSahddow = false;
            this.bunifuCards2.Location = new System.Drawing.Point(207, 13);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = false;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(224, 75);
            this.bunifuCards2.TabIndex = 36;
            // 
            // tYear
            // 
            this.tYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tYear.Font = new System.Drawing.Font("Cairo", 8F);
            this.tYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tYear.FormattingEnabled = true;
            this.tYear.Items.AddRange(new object[] {
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.tYear.Location = new System.Drawing.Point(15, 33);
            this.tYear.Name = "tYear";
            this.tYear.Size = new System.Drawing.Size(68, 28);
            this.tYear.TabIndex = 43;
            // 
            // tMonth
            // 
            this.tMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tMonth.Font = new System.Drawing.Font("Cairo", 8F);
            this.tMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tMonth.FormattingEnabled = true;
            this.tMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.tMonth.Location = new System.Drawing.Point(89, 33);
            this.tMonth.Name = "tMonth";
            this.tMonth.Size = new System.Drawing.Size(66, 28);
            this.tMonth.TabIndex = 42;
            // 
            // tDay
            // 
            this.tDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tDay.Font = new System.Drawing.Font("Cairo", 8F);
            this.tDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tDay.FormattingEnabled = true;
            this.tDay.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.tDay.Location = new System.Drawing.Point(161, 33);
            this.tDay.Name = "tDay";
            this.tDay.Size = new System.Drawing.Size(48, 28);
            this.tDay.TabIndex = 41;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cairo", 7F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label15.Location = new System.Drawing.Point(126, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 19);
            this.label15.TabIndex = 38;
            this.label15.Text = "تحديد تاريخ النهاية";
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.White;
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = false;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards3.Controls.Add(this.fYear);
            this.bunifuCards3.Controls.Add(this.fMonth);
            this.bunifuCards3.Controls.Add(this.fDay);
            this.bunifuCards3.Controls.Add(this.label14);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(442, 13);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = false;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(224, 75);
            this.bunifuCards3.TabIndex = 35;
            // 
            // fYear
            // 
            this.fYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fYear.Font = new System.Drawing.Font("Cairo", 8F);
            this.fYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fYear.FormattingEnabled = true;
            this.fYear.Items.AddRange(new object[] {
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.fYear.Location = new System.Drawing.Point(15, 33);
            this.fYear.Name = "fYear";
            this.fYear.Size = new System.Drawing.Size(68, 28);
            this.fYear.TabIndex = 40;
            // 
            // fMonth
            // 
            this.fMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fMonth.Font = new System.Drawing.Font("Cairo", 8F);
            this.fMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fMonth.FormattingEnabled = true;
            this.fMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.fMonth.Location = new System.Drawing.Point(89, 33);
            this.fMonth.Name = "fMonth";
            this.fMonth.Size = new System.Drawing.Size(66, 28);
            this.fMonth.TabIndex = 39;
            // 
            // fDay
            // 
            this.fDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fDay.Font = new System.Drawing.Font("Cairo", 8F);
            this.fDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fDay.FormattingEnabled = true;
            this.fDay.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.fDay.Location = new System.Drawing.Point(161, 33);
            this.fDay.Name = "fDay";
            this.fDay.Size = new System.Drawing.Size(48, 28);
            this.fDay.TabIndex = 38;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Cairo", 7F);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(131, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 19);
            this.label14.TabIndex = 37;
            this.label14.Text = "تحديد تاريخ البداية";
            // 
            // ViewBTN
            // 
            this.ViewBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewBTN.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewBTN.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.ViewBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewBTN.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.ViewBTN.ForeColor = System.Drawing.Color.White;
            this.ViewBTN.Image = global::SGMS.Properties.Resources.search_52px;
            this.ViewBTN.ImagePosition = 10;
            this.ViewBTN.ImageZoom = 25;
            this.ViewBTN.LabelPosition = 0;
            this.ViewBTN.LabelText = "";
            this.ViewBTN.Location = new System.Drawing.Point(113, 12);
            this.ViewBTN.Margin = new System.Windows.Forms.Padding(6);
            this.ViewBTN.Name = "ViewBTN";
            this.ViewBTN.Size = new System.Drawing.Size(78, 40);
            this.ViewBTN.TabIndex = 34;
            this.ViewBTN.Click += new System.EventHandler(this.ViewBTN_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel7.Controls.Add(this.invVIEW);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(3, 451);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(862, 40);
            this.panel7.TabIndex = 8;
            // 
            // invVIEW
            // 
            this.invVIEW.Activecolor = System.Drawing.Color.White;
            this.invVIEW.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.invVIEW.BackColor = System.Drawing.Color.White;
            this.invVIEW.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.invVIEW.BorderRadius = 7;
            this.invVIEW.ButtonText = "عرض الفاتورة";
            this.invVIEW.Cursor = System.Windows.Forms.Cursors.Hand;
            this.invVIEW.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.invVIEW.Enabled = false;
            this.invVIEW.Font = new System.Drawing.Font("Cairo", 10F);
            this.invVIEW.ForeColor = System.Drawing.Color.White;
            this.invVIEW.Iconcolor = System.Drawing.Color.Transparent;
            this.invVIEW.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.invVIEW.Iconimage_right = null;
            this.invVIEW.Iconimage_right_Selected = null;
            this.invVIEW.Iconimage_Selected = null;
            this.invVIEW.IconMarginLeft = 25;
            this.invVIEW.IconMarginRight = 0;
            this.invVIEW.IconRightVisible = false;
            this.invVIEW.IconRightZoom = 0D;
            this.invVIEW.IconVisible = false;
            this.invVIEW.IconZoom = 30D;
            this.invVIEW.IsTab = false;
            this.invVIEW.Location = new System.Drawing.Point(16, 4);
            this.invVIEW.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.invVIEW.Name = "invVIEW";
            this.invVIEW.Normalcolor = System.Drawing.Color.White;
            this.invVIEW.OnHovercolor = System.Drawing.Color.White;
            this.invVIEW.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.invVIEW.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.invVIEW.selected = false;
            this.invVIEW.Size = new System.Drawing.Size(179, 33);
            this.invVIEW.TabIndex = 44;
            this.invVIEW.Text = "عرض الفاتورة";
            this.invVIEW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.invVIEW.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.invVIEW.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invVIEW.Click += new System.EventHandler(this.invVIEW_Click_1);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(868, 494);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "المشتريات";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.printLast);
            this.panel5.Controls.Add(this.unPaid);
            this.panel5.Controls.Add(this.AddIn);
            this.panel5.Controls.Add(this.paidCash);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.Total);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(3, 452);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(862, 39);
            this.panel5.TabIndex = 3;
            // 
            // printLast
            // 
            this.printLast.Activecolor = System.Drawing.Color.Maroon;
            this.printLast.BackColor = System.Drawing.Color.Maroon;
            this.printLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.printLast.BorderRadius = 7;
            this.printLast.ButtonText = "طباعة الفاتورة الاخيرة";
            this.printLast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.printLast.DisabledColor = System.Drawing.Color.White;
            this.printLast.Font = new System.Drawing.Font("Cairo", 8F);
            this.printLast.Iconcolor = System.Drawing.Color.Transparent;
            this.printLast.Iconimage = null;
            this.printLast.Iconimage_right = null;
            this.printLast.Iconimage_right_Selected = null;
            this.printLast.Iconimage_Selected = null;
            this.printLast.IconMarginLeft = 0;
            this.printLast.IconMarginRight = 0;
            this.printLast.IconRightVisible = false;
            this.printLast.IconRightZoom = 0D;
            this.printLast.IconVisible = false;
            this.printLast.IconZoom = 20D;
            this.printLast.IsTab = false;
            this.printLast.Location = new System.Drawing.Point(107, 9);
            this.printLast.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.printLast.Name = "printLast";
            this.printLast.Normalcolor = System.Drawing.Color.Maroon;
            this.printLast.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.printLast.OnHoverTextColor = System.Drawing.Color.White;
            this.printLast.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.printLast.selected = false;
            this.printLast.Size = new System.Drawing.Size(141, 23);
            this.printLast.TabIndex = 45;
            this.printLast.Text = "طباعة الفاتورة الاخيرة";
            this.printLast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.printLast.Textcolor = System.Drawing.Color.White;
            this.printLast.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printLast.Click += new System.EventHandler(this.printLast_Click);
            // 
            // unPaid
            // 
            this.unPaid.Font = new System.Drawing.Font("Cairo", 8F);
            this.unPaid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.unPaid.Location = new System.Drawing.Point(267, 8);
            this.unPaid.Name = "unPaid";
            this.unPaid.Size = new System.Drawing.Size(67, 25);
            this.unPaid.TabIndex = 39;
            this.unPaid.Text = "0";
            this.unPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddIn
            // 
            this.AddIn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddIn.BorderRadius = 7;
            this.AddIn.ButtonText = "حفظ";
            this.AddIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddIn.DisabledColor = System.Drawing.Color.Gray;
            this.AddIn.Enabled = false;
            this.AddIn.Font = new System.Drawing.Font("Cairo", 10F);
            this.AddIn.Iconcolor = System.Drawing.Color.Transparent;
            this.AddIn.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.AddIn.Iconimage_right = null;
            this.AddIn.Iconimage_right_Selected = null;
            this.AddIn.Iconimage_Selected = null;
            this.AddIn.IconMarginLeft = 0;
            this.AddIn.IconMarginRight = 0;
            this.AddIn.IconRightVisible = false;
            this.AddIn.IconRightZoom = 0D;
            this.AddIn.IconVisible = false;
            this.AddIn.IconZoom = 25D;
            this.AddIn.IsTab = false;
            this.AddIn.Location = new System.Drawing.Point(13, 5);
            this.AddIn.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.AddIn.Name = "AddIn";
            this.AddIn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.AddIn.OnHoverTextColor = System.Drawing.Color.White;
            this.AddIn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.AddIn.selected = false;
            this.AddIn.Size = new System.Drawing.Size(88, 30);
            this.AddIn.TabIndex = 41;
            this.AddIn.Text = "حفظ";
            this.AddIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AddIn.Textcolor = System.Drawing.Color.White;
            this.AddIn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddIn.Click += new System.EventHandler(this.AddIn_Click);
            // 
            // paidCash
            // 
            this.paidCash.Location = new System.Drawing.Point(429, 4);
            this.paidCash.Name = "paidCash";
            this.paidCash.Size = new System.Drawing.Size(113, 32);
            this.paidCash.TabIndex = 43;
            this.paidCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.paidCash.EnabledChanged += new System.EventHandler(this.paidCash_EnabledChanged);
            this.paidCash.TextChanged += new System.EventHandler(this.paidCash_TextChanged);
            this.paidCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlynumwithsinglepoint);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cairo", 7F);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label16.Location = new System.Drawing.Point(550, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 19);
            this.label16.TabIndex = 38;
            this.label16.Text = "المبلغ المدفوع";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cairo", 7F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label17.Location = new System.Drawing.Point(346, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 19);
            this.label17.TabIndex = 38;
            this.label17.Text = "المبلغ المتبقي";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cairo", 7F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label9.Location = new System.Drawing.Point(695, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 19);
            this.label9.TabIndex = 35;
            this.label9.Text = "الإجمالي";
            // 
            // Total
            // 
            this.Total.Font = new System.Drawing.Font("Cairo", 8F);
            this.Total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.Total.Location = new System.Drawing.Point(632, 8);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(53, 24);
            this.Total.TabIndex = 37;
            this.Total.Text = "0";
            this.Total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BarCode,
            this.Column5,
            this.count,
            this.BuyPrice,
            this.TotalBuy,
            this.SePrice});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.DoubleBuffered = true;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(3, 96);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(862, 395);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // BarCode
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BarCode.DefaultCellStyle = dataGridViewCellStyle18;
            this.BarCode.FillWeight = 58.50328F;
            this.BarCode.HeaderText = "باركود";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "إسم المنتج";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // count
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.count.DefaultCellStyle = dataGridViewCellStyle19;
            this.count.FillWeight = 56.99974F;
            this.count.HeaderText = "الكمية";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            // 
            // BuyPrice
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuyPrice.DefaultCellStyle = dataGridViewCellStyle20;
            this.BuyPrice.FillWeight = 78.06802F;
            this.BuyPrice.HeaderText = "السعر";
            this.BuyPrice.Name = "BuyPrice";
            this.BuyPrice.ReadOnly = true;
            // 
            // TotalBuy
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TotalBuy.DefaultCellStyle = dataGridViewCellStyle21;
            this.TotalBuy.FillWeight = 101.4645F;
            this.TotalBuy.HeaderText = "الإجمالي";
            this.TotalBuy.Name = "TotalBuy";
            this.TotalBuy.ReadOnly = true;
            // 
            // SePrice
            // 
            this.SePrice.HeaderText = "سعر البيع";
            this.SePrice.Name = "SePrice";
            this.SePrice.ReadOnly = true;
            this.SePrice.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Sprice);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.newSeller);
            this.panel1.Controls.Add(this.newbTN);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnDel);
            this.panel1.Controls.Add(this.sellerName);
            this.panel1.Controls.Add(this.bPrice);
            this.panel1.Controls.Add(this.iCount);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 49);
            this.panel1.TabIndex = 0;
            // 
            // Sprice
            // 
            this.Sprice.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Sprice.Location = new System.Drawing.Point(556, 8);
            this.Sprice.Name = "Sprice";
            this.Sprice.Size = new System.Drawing.Size(41, 32);
            this.Sprice.TabIndex = 47;
            this.Sprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Cairo", 7F);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label13.Location = new System.Drawing.Point(603, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 19);
            this.label13.TabIndex = 46;
            this.label13.Text = "سعر البيع";
            // 
            // newSeller
            // 
            this.newSeller.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newSeller.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newSeller.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newSeller.BorderRadius = 7;
            this.newSeller.ButtonText = "مورد جديد";
            this.newSeller.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newSeller.DisabledColor = System.Drawing.Color.Gray;
            this.newSeller.Font = new System.Drawing.Font("Cairo", 8F);
            this.newSeller.Iconcolor = System.Drawing.Color.Transparent;
            this.newSeller.Iconimage = null;
            this.newSeller.Iconimage_right = null;
            this.newSeller.Iconimage_right_Selected = null;
            this.newSeller.Iconimage_Selected = null;
            this.newSeller.IconMarginLeft = 0;
            this.newSeller.IconMarginRight = 0;
            this.newSeller.IconRightVisible = false;
            this.newSeller.IconRightZoom = 0D;
            this.newSeller.IconVisible = false;
            this.newSeller.IconZoom = 20D;
            this.newSeller.IsTab = false;
            this.newSeller.Location = new System.Drawing.Point(16, 12);
            this.newSeller.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.newSeller.Name = "newSeller";
            this.newSeller.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newSeller.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.newSeller.OnHoverTextColor = System.Drawing.Color.White;
            this.newSeller.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.newSeller.selected = false;
            this.newSeller.Size = new System.Drawing.Size(94, 25);
            this.newSeller.TabIndex = 41;
            this.newSeller.Text = "مورد جديد";
            this.newSeller.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newSeller.Textcolor = System.Drawing.Color.White;
            this.newSeller.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newSeller.Click += new System.EventHandler(this.newSeller_Click);
            // 
            // newbTN
            // 
            this.newbTN.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newbTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newbTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newbTN.BorderRadius = 7;
            this.newbTN.ButtonText = "";
            this.newbTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newbTN.DisabledColor = System.Drawing.Color.Gray;
            this.newbTN.Font = new System.Drawing.Font("Cairo", 8F);
            this.newbTN.Iconcolor = System.Drawing.Color.Transparent;
            this.newbTN.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.newbTN.Iconimage_right = null;
            this.newbTN.Iconimage_right_Selected = null;
            this.newbTN.Iconimage_Selected = null;
            this.newbTN.IconMarginLeft = 0;
            this.newbTN.IconMarginRight = 0;
            this.newbTN.IconRightVisible = false;
            this.newbTN.IconRightZoom = 0D;
            this.newbTN.IconVisible = true;
            this.newbTN.IconZoom = 30D;
            this.newbTN.IsTab = false;
            this.newbTN.Location = new System.Drawing.Point(87, 12);
            this.newbTN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.newbTN.Name = "newbTN";
            this.newbTN.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newbTN.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.newbTN.OnHoverTextColor = System.Drawing.Color.White;
            this.newbTN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.newbTN.selected = false;
            this.newbTN.Size = new System.Drawing.Size(34, 25);
            this.newbTN.TabIndex = 45;
            this.newbTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newbTN.Textcolor = System.Drawing.Color.White;
            this.newbTN.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newbTN.Visible = false;
            this.newbTN.Click += new System.EventHandler(this.newbTN_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 7F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label6.Location = new System.Drawing.Point(312, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 19);
            this.label6.TabIndex = 39;
            this.label6.Text = "المورد";
            // 
            // btnDel
            // 
            this.btnDel.Activecolor = System.Drawing.Color.Maroon;
            this.btnDel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDel.BackColor = System.Drawing.Color.Maroon;
            this.btnDel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDel.BorderRadius = 7;
            this.btnDel.ButtonText = "حذف";
            this.btnDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDel.DisabledColor = System.Drawing.Color.White;
            this.btnDel.Font = new System.Drawing.Font("Cairo", 8F);
            this.btnDel.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDel.Iconimage = null;
            this.btnDel.Iconimage_right = null;
            this.btnDel.Iconimage_right_Selected = null;
            this.btnDel.Iconimage_Selected = null;
            this.btnDel.IconMarginLeft = 0;
            this.btnDel.IconMarginRight = 0;
            this.btnDel.IconRightVisible = false;
            this.btnDel.IconRightZoom = 0D;
            this.btnDel.IconVisible = false;
            this.btnDel.IconZoom = 20D;
            this.btnDel.IsTab = false;
            this.btnDel.Location = new System.Drawing.Point(392, 13);
            this.btnDel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnDel.Name = "btnDel";
            this.btnDel.Normalcolor = System.Drawing.Color.Maroon;
            this.btnDel.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDel.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDel.selected = false;
            this.btnDel.Size = new System.Drawing.Size(65, 23);
            this.btnDel.TabIndex = 44;
            this.btnDel.Text = "حذف";
            this.btnDel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnDel.Textcolor = System.Drawing.Color.White;
            this.btnDel.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // sellerName
            // 
            this.sellerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.sellerName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.sellerName.FormattingEnabled = true;
            this.sellerName.Location = new System.Drawing.Point(127, 8);
            this.sellerName.Name = "sellerName";
            this.sellerName.Size = new System.Drawing.Size(180, 32);
            this.sellerName.TabIndex = 42;
            this.sellerName.SelectedIndexChanged += new System.EventHandler(this.sellerName_SelectedIndexChanged);
            // 
            // bPrice
            // 
            this.bPrice.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.bPrice.Location = new System.Drawing.Point(661, 8);
            this.bPrice.Name = "bPrice";
            this.bPrice.Size = new System.Drawing.Size(41, 32);
            this.bPrice.TabIndex = 43;
            this.bPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // iCount
            // 
            this.iCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.iCount.Location = new System.Drawing.Point(769, 8);
            this.iCount.Name = "iCount";
            this.iCount.Size = new System.Drawing.Size(41, 32);
            this.iCount.TabIndex = 43;
            this.iCount.Text = "1";
            this.iCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iCount.TextChanged += new System.EventHandler(this.iCount_TextChanged);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Cairo", 7F);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label12.Location = new System.Drawing.Point(708, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 19);
            this.label12.TabIndex = 39;
            this.label12.Text = "سعر الشراء";
            // 
            // btnAdd
            // 
            this.btnAdd.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdd.BorderRadius = 7;
            this.btnAdd.ButtonText = "";
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.DisabledColor = System.Drawing.Color.Gray;
            this.btnAdd.Font = new System.Drawing.Font("Cairo", 10F);
            this.btnAdd.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAdd.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.btnAdd.Iconimage_right = null;
            this.btnAdd.Iconimage_right_Selected = null;
            this.btnAdd.Iconimage_Selected = null;
            this.btnAdd.IconMarginLeft = 25;
            this.btnAdd.IconMarginRight = 0;
            this.btnAdd.IconRightVisible = false;
            this.btnAdd.IconRightZoom = 0D;
            this.btnAdd.IconVisible = true;
            this.btnAdd.IconZoom = 30D;
            this.btnAdd.IsTab = false;
            this.btnAdd.Location = new System.Drawing.Point(471, 3);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btnAdd.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.btnAdd.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAdd.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAdd.selected = false;
            this.btnAdd.Size = new System.Drawing.Size(70, 43);
            this.btnAdd.TabIndex = 41;
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAdd.Textcolor = System.Drawing.Color.White;
            this.btnAdd.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Cairo", 7F);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label11.Location = new System.Drawing.Point(812, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 19);
            this.label11.TabIndex = 39;
            this.label11.Text = "الكمية";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.itemBox);
            this.panel6.Controls.Add(this.itemBarcode);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.isBarcode);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.newItem);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(862, 44);
            this.panel6.TabIndex = 4;
            // 
            // itemBox
            // 
            this.itemBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.itemBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.itemBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.itemBox.FormattingEnabled = true;
            this.itemBox.Location = new System.Drawing.Point(478, 5);
            this.itemBox.Name = "itemBox";
            this.itemBox.Size = new System.Drawing.Size(162, 32);
            this.itemBox.TabIndex = 43;
            this.itemBox.SelectedIndexChanged += new System.EventHandler(this.itemBox_SelectedIndexChanged);
            this.itemBox.SelectedValueChanged += new System.EventHandler(this.itemBox_SelectedIndexChanged);
            this.itemBox.TextChanged += new System.EventHandler(this.itemBox_SelectedIndexChanged);
            // 
            // itemBarcode
            // 
            this.itemBarcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.itemBarcode.Enabled = false;
            this.itemBarcode.Location = new System.Drawing.Point(646, 5);
            this.itemBarcode.Name = "itemBarcode";
            this.itemBarcode.Size = new System.Drawing.Size(99, 32);
            this.itemBarcode.TabIndex = 43;
            this.itemBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.itemBarcode.TextChanged += new System.EventHandler(this.itemBarcode_TextChanged);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Cairo", 8F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label7.Location = new System.Drawing.Point(19, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 26);
            this.label7.TabIndex = 36;
            this.label7.Text = "0";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // isBarcode
            // 
            this.isBarcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.isBarcode.AutoSize = true;
            this.isBarcode.Font = new System.Drawing.Font("Cairo", 8F);
            this.isBarcode.Location = new System.Drawing.Point(751, 10);
            this.isBarcode.Name = "isBarcode";
            this.isBarcode.Size = new System.Drawing.Size(55, 24);
            this.isBarcode.TabIndex = 42;
            this.isBarcode.Text = "باركود";
            this.isBarcode.UseVisualStyleBackColor = true;
            this.isBarcode.CheckedChanged += new System.EventHandler(this.isBarcode_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cairo", 7F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(127, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 19);
            this.label8.TabIndex = 34;
            this.label8.Text = "رقم الفاتورة";
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(12, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(185, 35);
            this.label5.TabIndex = 37;
            this.label5.Text = " ";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Cairo", 7F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label10.Location = new System.Drawing.Point(813, 12);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 19);
            this.label10.TabIndex = 39;
            this.label10.Text = "المنتج";
            // 
            // newItem
            // 
            this.newItem.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newItem.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.newItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newItem.BorderRadius = 7;
            this.newItem.ButtonText = "منتج جديد";
            this.newItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newItem.DisabledColor = System.Drawing.Color.Gray;
            this.newItem.Font = new System.Drawing.Font("Cairo", 8F);
            this.newItem.Iconcolor = System.Drawing.Color.Transparent;
            this.newItem.Iconimage = null;
            this.newItem.Iconimage_right = null;
            this.newItem.Iconimage_right_Selected = null;
            this.newItem.Iconimage_Selected = null;
            this.newItem.IconMarginLeft = 0;
            this.newItem.IconMarginRight = 0;
            this.newItem.IconRightVisible = false;
            this.newItem.IconRightZoom = 0D;
            this.newItem.IconVisible = false;
            this.newItem.IconZoom = 20D;
            this.newItem.IsTab = false;
            this.newItem.Location = new System.Drawing.Point(381, 10);
            this.newItem.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.newItem.Name = "newItem";
            this.newItem.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newItem.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.newItem.OnHoverTextColor = System.Drawing.Color.White;
            this.newItem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.newItem.selected = false;
            this.newItem.Size = new System.Drawing.Size(86, 23);
            this.newItem.TabIndex = 41;
            this.newItem.Text = "منتج جديد";
            this.newItem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newItem.Textcolor = System.Drawing.Color.White;
            this.newItem.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newItem.Click += new System.EventHandler(this.newItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(70, 35);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(876, 537);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.RWait);
            this.tabPage4.Controls.Add(this.panel8);
            this.tabPage4.Location = new System.Drawing.Point(4, 39);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(868, 494);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "المرتجعات التالفة";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // RWait
            // 
            this.RWait.AllowUserToAddRows = false;
            this.RWait.AllowUserToDeleteRows = false;
            this.RWait.AllowUserToResizeColumns = false;
            this.RWait.AllowUserToResizeRows = false;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.RWait.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.RWait.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RWait.BackgroundColor = System.Drawing.Color.White;
            this.RWait.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RWait.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RWait.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.RWait.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RWait.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RWait.DefaultCellStyle = dataGridViewCellStyle26;
            this.RWait.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RWait.DoubleBuffered = true;
            this.RWait.EnableHeadersVisualStyles = false;
            this.RWait.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.RWait.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.RWait.HeaderForeColor = System.Drawing.Color.White;
            this.RWait.Location = new System.Drawing.Point(3, 3);
            this.RWait.MultiSelect = false;
            this.RWait.Name = "RWait";
            this.RWait.ReadOnly = true;
            this.RWait.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RWait.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.RWait.RowHeadersVisible = false;
            this.RWait.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RWait.Size = new System.Drawing.Size(862, 441);
            this.RWait.TabIndex = 17;
            this.RWait.SelectionChanged += new System.EventHandler(this.RWait_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "id";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.FillWeight = 87.27693F;
            this.dataGridViewTextBoxColumn8.HeaderText = "باركود المنتج";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "إسم المنتج";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "الكمية";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "سعر الشراء";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "سبب الإرتجاع";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "تاريخ الإرتجاع";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(3, 444);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(862, 47);
            this.panel8.TabIndex = 16;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.reP);
            this.panel9.Controls.Add(this.edP);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(343, 47);
            this.panel9.TabIndex = 0;
            // 
            // reP
            // 
            this.reP.Activecolor = System.Drawing.Color.Maroon;
            this.reP.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.reP.BackColor = System.Drawing.Color.Maroon;
            this.reP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.reP.BorderRadius = 7;
            this.reP.ButtonText = "إرتجاع الكمية";
            this.reP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reP.DisabledColor = System.Drawing.Color.White;
            this.reP.Enabled = false;
            this.reP.Font = new System.Drawing.Font("Cairo", 8F);
            this.reP.Iconcolor = System.Drawing.Color.Transparent;
            this.reP.Iconimage = null;
            this.reP.Iconimage_right = null;
            this.reP.Iconimage_right_Selected = null;
            this.reP.Iconimage_Selected = null;
            this.reP.IconMarginLeft = 0;
            this.reP.IconMarginRight = 0;
            this.reP.IconRightVisible = false;
            this.reP.IconRightZoom = 0D;
            this.reP.IconVisible = false;
            this.reP.IconZoom = 20D;
            this.reP.IsTab = false;
            this.reP.Location = new System.Drawing.Point(46, 10);
            this.reP.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.reP.Name = "reP";
            this.reP.Normalcolor = System.Drawing.Color.Maroon;
            this.reP.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.reP.OnHoverTextColor = System.Drawing.Color.White;
            this.reP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.reP.selected = false;
            this.reP.Size = new System.Drawing.Size(119, 30);
            this.reP.TabIndex = 45;
            this.reP.Text = "إرتجاع الكمية";
            this.reP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reP.Textcolor = System.Drawing.Color.White;
            this.reP.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reP.Click += new System.EventHandler(this.reP_Click);
            // 
            // edP
            // 
            this.edP.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.edP.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.edP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.edP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.edP.BorderRadius = 7;
            this.edP.ButtonText = "إستبدال الكمية";
            this.edP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.edP.DisabledColor = System.Drawing.Color.Gray;
            this.edP.Enabled = false;
            this.edP.Font = new System.Drawing.Font("Cairo", 8F);
            this.edP.Iconcolor = System.Drawing.Color.Transparent;
            this.edP.Iconimage = null;
            this.edP.Iconimage_right = null;
            this.edP.Iconimage_right_Selected = null;
            this.edP.Iconimage_Selected = null;
            this.edP.IconMarginLeft = 0;
            this.edP.IconMarginRight = 0;
            this.edP.IconRightVisible = false;
            this.edP.IconRightZoom = 0D;
            this.edP.IconVisible = false;
            this.edP.IconZoom = 20D;
            this.edP.IsTab = false;
            this.edP.Location = new System.Drawing.Point(175, 10);
            this.edP.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.edP.Name = "edP";
            this.edP.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.edP.OnHovercolor = System.Drawing.Color.Green;
            this.edP.OnHoverTextColor = System.Drawing.Color.White;
            this.edP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.edP.selected = false;
            this.edP.Size = new System.Drawing.Size(119, 30);
            this.edP.TabIndex = 42;
            this.edP.Text = "إستبدال الكمية";
            this.edP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.edP.Textcolor = System.Drawing.Color.White;
            this.edP.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edP.Click += new System.EventHandler(this.edP_Click);
            // 
            // Purchases
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl1);
            this.Name = "Purchases";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(876, 537);
            this.Load += new System.EventHandler(this.Home_Load);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards4.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.bunifuCards2.ResumeLayout(false);
            this.bunifuCards2.PerformLayout();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RWait)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Total;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabControl tabControl1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel12;
        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private System.Windows.Forms.Label TodaySells;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel11;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private System.Windows.Forms.ComboBox tYear;
        private System.Windows.Forms.ComboBox tMonth;
        private System.Windows.Forms.ComboBox tDay;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private System.Windows.Forms.ComboBox fYear;
        private System.Windows.Forms.ComboBox fMonth;
        private System.Windows.Forms.ComboBox fDay;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuTileButton ViewBTN;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private Bunifu.Framework.UI.BunifuFlatButton AddIn;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuFlatButton newSeller;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bPrice;
        private System.Windows.Forms.TextBox iCount;
        private System.Windows.Forms.TextBox itemBarcode;
        private System.Windows.Forms.CheckBox isBarcode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Bunifu.Framework.UI.BunifuFlatButton newItem;
        private Bunifu.Framework.UI.BunifuFlatButton btnAdd;
        private System.Windows.Forms.Label unPaid;
        private System.Windows.Forms.TextBox paidCash;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox sellerName;
        private System.Windows.Forms.ComboBox itemBox;
        private Bunifu.Framework.UI.BunifuFlatButton btnDel;
        private Bunifu.Framework.UI.BunifuTileButton bPrint;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox invNumSearch;
        private Bunifu.Framework.UI.BunifuFlatButton PrintInvByNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuFlatButton ViewInvByNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn invNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn invpaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn unpaidinv;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalInv;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn invuserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn invDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn invTime;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuFlatButton printLast;
        private Bunifu.Framework.UI.BunifuFlatButton newbTN;
        private Bunifu.Framework.UI.BunifuFlatButton PayBtn;
        private System.Windows.Forms.Panel panel7;
        private Bunifu.Framework.UI.BunifuFlatButton invVIEW;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn SePrice;
        private System.Windows.Forms.TabPage tabPage4;
        private Bunifu.Framework.UI.BunifuCustomDataGrid RWait;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.Panel panel8;
        private Bunifu.Framework.UI.BunifuFlatButton reP;
        private Bunifu.Framework.UI.BunifuFlatButton edP;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox Sprice;
    }
}
