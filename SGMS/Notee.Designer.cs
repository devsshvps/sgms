﻿namespace SGMS
{
    partial class Notee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Notee));
            this.panel3 = new System.Windows.Forms.Panel();
            this.StoreLogo = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.aboutBTN = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AMin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AClose = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.addName = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreLogo)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.StoreLogo);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 32);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(472, 116);
            this.panel3.TabIndex = 41;
            // 
            // StoreLogo
            // 
            this.StoreLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StoreLogo.Location = new System.Drawing.Point(347, 22);
            this.StoreLogo.Name = "StoreLogo";
            this.StoreLogo.Size = new System.Drawing.Size(99, 76);
            this.StoreLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.StoreLogo.TabIndex = 0;
            this.StoreLogo.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cairo", 30F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(63, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(258, 75);
            this.label7.TabIndex = 42;
            this.label7.Text = "ملاحظة إدارية";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.aboutBTN);
            this.panel1.Controls.Add(this.AMin);
            this.panel1.Controls.Add(this.AClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 32);
            this.panel1.TabIndex = 40;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(38, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 32);
            this.label1.TabIndex = 32;
            this.label1.Text = "ملاحظة إدارية";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::SGMS.Properties.Resources.DB_01;
            this.pictureBox2.Location = new System.Drawing.Point(13, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 32);
            this.label2.TabIndex = 30;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // aboutBTN
            // 
            this.aboutBTN.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.aboutBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.aboutBTN.BorderRadius = 0;
            this.aboutBTN.ButtonText = "";
            this.aboutBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutBTN.DisabledColor = System.Drawing.Color.Gray;
            this.aboutBTN.Dock = System.Windows.Forms.DockStyle.Right;
            this.aboutBTN.Iconcolor = System.Drawing.Color.Transparent;
            this.aboutBTN.Iconimage = global::SGMS.Properties.Resources.info_512px;
            this.aboutBTN.Iconimage_right = null;
            this.aboutBTN.Iconimage_right_Selected = null;
            this.aboutBTN.Iconimage_Selected = null;
            this.aboutBTN.IconMarginLeft = 0;
            this.aboutBTN.IconMarginRight = 0;
            this.aboutBTN.IconRightVisible = true;
            this.aboutBTN.IconRightZoom = 0D;
            this.aboutBTN.IconVisible = true;
            this.aboutBTN.IconZoom = 40D;
            this.aboutBTN.IsTab = false;
            this.aboutBTN.Location = new System.Drawing.Point(361, 0);
            this.aboutBTN.Name = "aboutBTN";
            this.aboutBTN.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.aboutBTN.OnHoverTextColor = System.Drawing.Color.White;
            this.aboutBTN.selected = false;
            this.aboutBTN.Size = new System.Drawing.Size(37, 32);
            this.aboutBTN.TabIndex = 29;
            this.aboutBTN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutBTN.Textcolor = System.Drawing.Color.White;
            this.aboutBTN.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.aboutBTN.Click += new System.EventHandler(this.aboutBTN_Click);
            // 
            // AMin
            // 
            this.AMin.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AMin.BorderRadius = 0;
            this.AMin.ButtonText = "";
            this.AMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AMin.DisabledColor = System.Drawing.Color.Gray;
            this.AMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.AMin.Iconcolor = System.Drawing.Color.Transparent;
            this.AMin.Iconimage = global::SGMS.Properties.Resources._1_03;
            this.AMin.Iconimage_right = null;
            this.AMin.Iconimage_right_Selected = null;
            this.AMin.Iconimage_Selected = null;
            this.AMin.IconMarginLeft = 0;
            this.AMin.IconMarginRight = 0;
            this.AMin.IconRightVisible = true;
            this.AMin.IconRightZoom = 0D;
            this.AMin.IconVisible = true;
            this.AMin.IconZoom = 40D;
            this.AMin.IsTab = false;
            this.AMin.Location = new System.Drawing.Point(398, 0);
            this.AMin.Name = "AMin";
            this.AMin.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.AMin.OnHoverTextColor = System.Drawing.Color.White;
            this.AMin.selected = false;
            this.AMin.Size = new System.Drawing.Size(37, 32);
            this.AMin.TabIndex = 28;
            this.AMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AMin.Textcolor = System.Drawing.Color.White;
            this.AMin.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AMin.Click += new System.EventHandler(this.AMin_Click);
            // 
            // AClose
            // 
            this.AClose.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AClose.BorderRadius = 0;
            this.AClose.ButtonText = "";
            this.AClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AClose.DisabledColor = System.Drawing.Color.Gray;
            this.AClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.AClose.Iconcolor = System.Drawing.Color.Transparent;
            this.AClose.Iconimage = global::SGMS.Properties.Resources._1_02;
            this.AClose.Iconimage_right = null;
            this.AClose.Iconimage_right_Selected = null;
            this.AClose.Iconimage_Selected = null;
            this.AClose.IconMarginLeft = 0;
            this.AClose.IconMarginRight = 0;
            this.AClose.IconRightVisible = true;
            this.AClose.IconRightZoom = 0D;
            this.AClose.IconVisible = true;
            this.AClose.IconZoom = 40D;
            this.AClose.IsTab = false;
            this.AClose.Location = new System.Drawing.Point(435, 0);
            this.AClose.Name = "AClose";
            this.AClose.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.OnHovercolor = System.Drawing.Color.Red;
            this.AClose.OnHoverTextColor = System.Drawing.Color.White;
            this.AClose.selected = false;
            this.AClose.Size = new System.Drawing.Size(37, 32);
            this.AClose.TabIndex = 27;
            this.AClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AClose.Textcolor = System.Drawing.Color.White;
            this.AClose.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AClose.Click += new System.EventHandler(this.AClose_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Cairo", 9F);
            this.label3.Location = new System.Drawing.Point(17, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(437, 288);
            this.label3.TabIndex = 42;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.addName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 442);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 61);
            this.panel2.TabIndex = 61;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Cairo", 8F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(294, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 26);
            this.label5.TabIndex = 63;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Cairo", 8F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(200, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 26);
            this.label4.TabIndex = 62;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // addName
            // 
            this.addName.Font = new System.Drawing.Font("Cairo", 8F);
            this.addName.ForeColor = System.Drawing.Color.White;
            this.addName.Location = new System.Drawing.Point(26, 17);
            this.addName.Name = "addName";
            this.addName.Size = new System.Drawing.Size(153, 26);
            this.addName.TabIndex = 61;
            this.addName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Cairo", 8F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(26, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 26);
            this.label9.TabIndex = 61;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Cairo", 8F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(200, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 26);
            this.label8.TabIndex = 62;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Cairo", 8F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(294, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 26);
            this.label6.TabIndex = 63;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(460, 148);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(12, 294);
            this.panel4.TabIndex = 62;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 148);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(12, 294);
            this.panel5.TabIndex = 63;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Cairo", 8F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(294, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 26);
            this.label10.TabIndex = 63;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Cairo", 8F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(200, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 26);
            this.label11.TabIndex = 62;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Cairo", 8F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(26, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(153, 26);
            this.label12.TabIndex = 61;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Notee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(472, 503);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Notee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ملاحظة إدارية";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Login_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreLogo)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox StoreLogo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuFlatButton aboutBTN;
        private Bunifu.Framework.UI.BunifuFlatButton AMin;
        private Bunifu.Framework.UI.BunifuFlatButton AClose;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label addName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}