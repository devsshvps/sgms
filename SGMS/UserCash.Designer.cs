﻿namespace SGMS
{
    partial class UserCash
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.casee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cash = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.spiMonth = new Bunifu.Framework.UI.BunifuTileButton();
            this.label4 = new System.Windows.Forms.Label();
            this.totalmonth = new System.Windows.Forms.Label();
            this.bunifuTileButton1 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton2 = new Bunifu.Framework.UI.BunifuTileButton();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.UserNam = new System.Windows.Forms.ComboBox();
            this.PayP = new Bunifu.Framework.UI.BunifuTileButton();
            this.AddNewP = new Bunifu.Framework.UI.BunifuTileButton();
            this.viewP = new Bunifu.Framework.UI.BunifuTileButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddNewAdmin = new Bunifu.Framework.UI.BunifuTileButton();
            this.viewAdmin = new Bunifu.Framework.UI.BunifuTileButton();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 39);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(868, 494);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "الشركاء";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cairo", 8F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(344, 393);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 20);
            this.label7.TabIndex = 42;
            this.label7.Text = "سيتم تحديثة في الإصدار 1.06.2020";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 14F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label6.Location = new System.Drawing.Point(274, 354);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(305, 36);
            this.label6.TabIndex = 41;
            this.label6.Text = "نأسف تم تعليق نظام الشركاء للتعديل";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::SGMS.Properties.Resources.Warning_icon;
            this.pictureBox1.Location = new System.Drawing.Point(289, 98);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(274, 278);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(868, 494);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "المصاريف";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.PDatagride);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(862, 488);
            this.panel3.TabIndex = 1;
            // 
            // PDatagride
            // 
            this.PDatagride.AllowUserToAddRows = false;
            this.PDatagride.AllowUserToDeleteRows = false;
            this.PDatagride.AllowUserToResizeColumns = false;
            this.PDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            this.PDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.PDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PDatagride.BackgroundColor = System.Drawing.Color.White;
            this.PDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Cairo", 8F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.PDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.casee,
            this.cash,
            this.Column2,
            this.Column1,
            this.reason,
            this.User,
            this.Date,
            this.time,
            this.cid,
            this.rowid});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Cairo", 8F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PDatagride.DefaultCellStyle = dataGridViewCellStyle9;
            this.PDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PDatagride.DoubleBuffered = true;
            this.PDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.PDatagride.EnableHeadersVisualStyles = false;
            this.PDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.PDatagride.Location = new System.Drawing.Point(0, 125);
            this.PDatagride.MultiSelect = false;
            this.PDatagride.Name = "PDatagride";
            this.PDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Cairo", 8F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.PDatagride.RowHeadersVisible = false;
            this.PDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PDatagride.Size = new System.Drawing.Size(862, 363);
            this.PDatagride.TabIndex = 10;
            this.PDatagride.SelectionChanged += new System.EventHandler(this.PDatagride_SelectionChanged);
            // 
            // casee
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.casee.DefaultCellStyle = dataGridViewCellStyle8;
            this.casee.FillWeight = 87.79237F;
            this.casee.HeaderText = "النوع";
            this.casee.Name = "casee";
            this.casee.ReadOnly = true;
            // 
            // cash
            // 
            this.cash.FillWeight = 54.0521F;
            this.cash.HeaderText = "المبلغ";
            this.cash.Name = "cash";
            this.cash.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "المبلغ المصروف";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "المستفيد";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // reason
            // 
            this.reason.FillWeight = 210.5565F;
            this.reason.HeaderText = "السبب";
            this.reason.Name = "reason";
            this.reason.ReadOnly = true;
            // 
            // User
            // 
            this.User.FillWeight = 74.70072F;
            this.User.HeaderText = "منفذ العملية";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.FillWeight = 74.70072F;
            this.Date.HeaderText = "التاريخ";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // time
            // 
            this.time.FillWeight = 56.70084F;
            this.time.HeaderText = "الوقت";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // cid
            // 
            this.cid.HeaderText = "id";
            this.cid.Name = "cid";
            this.cid.ReadOnly = true;
            this.cid.Visible = false;
            // 
            // rowid
            // 
            this.rowid.HeaderText = "rowid";
            this.rowid.Name = "rowid";
            this.rowid.ReadOnly = true;
            this.rowid.Visible = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.panel5);
            this.panel7.Controls.Add(this.panel18);
            this.panel7.Controls.Add(this.panel16);
            this.panel7.Controls.Add(this.panel15);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(862, 125);
            this.panel7.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 8F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(106, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 46;
            this.label3.Text = "ملخص مصاريف الشهر الحالي";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.spiMonth);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.totalmonth);
            this.panel5.Controls.Add(this.bunifuTileButton1);
            this.panel5.Controls.Add(this.bunifuTileButton2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 40);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(329, 85);
            this.panel5.TabIndex = 45;
            // 
            // spiMonth
            // 
            this.spiMonth.BackColor = System.Drawing.Color.Maroon;
            this.spiMonth.color = System.Drawing.Color.Maroon;
            this.spiMonth.colorActive = System.Drawing.Color.Red;
            this.spiMonth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.spiMonth.Font = new System.Drawing.Font("Cairo", 8F);
            this.spiMonth.ForeColor = System.Drawing.Color.White;
            this.spiMonth.Image = global::SGMS.Properties.Resources.visible_60px;
            this.spiMonth.ImagePosition = 8;
            this.spiMonth.ImageZoom = 22;
            this.spiMonth.LabelPosition = 25;
            this.spiMonth.LabelText = "بحث محدد في المصروفات";
            this.spiMonth.Location = new System.Drawing.Point(11, 47);
            this.spiMonth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spiMonth.Name = "spiMonth";
            this.spiMonth.Size = new System.Drawing.Size(155, 30);
            this.spiMonth.TabIndex = 52;
            this.spiMonth.Click += new System.EventHandler(this.spiMonth_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Cairo", 8F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label4.Location = new System.Drawing.Point(175, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 20);
            this.label4.TabIndex = 50;
            this.label4.Text = "إجمالي مصروفات الشهر الحالي";
            // 
            // totalmonth
            // 
            this.totalmonth.BackColor = System.Drawing.Color.White;
            this.totalmonth.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.totalmonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.totalmonth.Location = new System.Drawing.Point(174, 37);
            this.totalmonth.Name = "totalmonth";
            this.totalmonth.Size = new System.Drawing.Size(151, 40);
            this.totalmonth.TabIndex = 51;
            this.totalmonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuTileButton1
            // 
            this.bunifuTileButton1.BackColor = System.Drawing.Color.Navy;
            this.bunifuTileButton1.color = System.Drawing.Color.Navy;
            this.bunifuTileButton1.colorActive = System.Drawing.Color.Blue;
            this.bunifuTileButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton1.Font = new System.Drawing.Font("Cairo", 8F);
            this.bunifuTileButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton1.Image = global::SGMS.Properties.Resources.print_60px;
            this.bunifuTileButton1.ImagePosition = 8;
            this.bunifuTileButton1.ImageZoom = 22;
            this.bunifuTileButton1.LabelPosition = 25;
            this.bunifuTileButton1.LabelText = "طباعة";
            this.bunifuTileButton1.Location = new System.Drawing.Point(11, 10);
            this.bunifuTileButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bunifuTileButton1.Name = "bunifuTileButton1";
            this.bunifuTileButton1.Size = new System.Drawing.Size(75, 31);
            this.bunifuTileButton1.TabIndex = 49;
            this.bunifuTileButton1.Click += new System.EventHandler(this.bunifuTileButton1_Click);
            // 
            // bunifuTileButton2
            // 
            this.bunifuTileButton2.BackColor = System.Drawing.Color.CadetBlue;
            this.bunifuTileButton2.color = System.Drawing.Color.CadetBlue;
            this.bunifuTileButton2.colorActive = System.Drawing.Color.Teal;
            this.bunifuTileButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton2.Font = new System.Drawing.Font("Cairo", 8F);
            this.bunifuTileButton2.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton2.Image = global::SGMS.Properties.Resources.visible_60px;
            this.bunifuTileButton2.ImagePosition = 8;
            this.bunifuTileButton2.ImageZoom = 22;
            this.bunifuTileButton2.LabelPosition = 25;
            this.bunifuTileButton2.LabelText = "عرض";
            this.bunifuTileButton2.Location = new System.Drawing.Point(91, 10);
            this.bunifuTileButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bunifuTileButton2.Name = "bunifuTileButton2";
            this.bunifuTileButton2.Size = new System.Drawing.Size(75, 31);
            this.bunifuTileButton2.TabIndex = 48;
            this.bunifuTileButton2.Click += new System.EventHandler(this.bunifuTileButton2_Click);
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel4);
            this.panel18.Controls.Add(this.label1);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(329, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(268, 125);
            this.panel18.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.UserNam);
            this.panel4.Controls.Add(this.PayP);
            this.panel4.Controls.Add(this.AddNewP);
            this.panel4.Controls.Add(this.viewP);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 40);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(268, 85);
            this.panel4.TabIndex = 45;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label5.Location = new System.Drawing.Point(187, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 14);
            this.label5.TabIndex = 52;
            this.label5.Text = "مصاريف مستخدم";
            // 
            // UserNam
            // 
            this.UserNam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.UserNam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.UserNam.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.UserNam.FormattingEnabled = true;
            this.UserNam.Location = new System.Drawing.Point(16, 50);
            this.UserNam.Name = "UserNam";
            this.UserNam.Size = new System.Drawing.Size(165, 22);
            this.UserNam.TabIndex = 48;
            this.UserNam.SelectedIndexChanged += new System.EventHandler(this.UserNam_SelectedIndexChanged);
            // 
            // PayP
            // 
            this.PayP.BackColor = System.Drawing.Color.Maroon;
            this.PayP.color = System.Drawing.Color.Maroon;
            this.PayP.colorActive = System.Drawing.Color.Red;
            this.PayP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayP.Enabled = false;
            this.PayP.Font = new System.Drawing.Font("Cairo", 8F);
            this.PayP.ForeColor = System.Drawing.Color.White;
            this.PayP.Image = global::SGMS.Properties.Resources.visible_60px;
            this.PayP.ImagePosition = 8;
            this.PayP.ImageZoom = 22;
            this.PayP.LabelPosition = 25;
            this.PayP.LabelText = "دفع";
            this.PayP.Location = new System.Drawing.Point(16, 10);
            this.PayP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PayP.Name = "PayP";
            this.PayP.Size = new System.Drawing.Size(75, 30);
            this.PayP.TabIndex = 47;
            this.PayP.Click += new System.EventHandler(this.PayP_Click);
            // 
            // AddNewP
            // 
            this.AddNewP.BackColor = System.Drawing.Color.DarkGreen;
            this.AddNewP.color = System.Drawing.Color.DarkGreen;
            this.AddNewP.colorActive = System.Drawing.Color.Green;
            this.AddNewP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddNewP.Font = new System.Drawing.Font("Cairo", 8F);
            this.AddNewP.ForeColor = System.Drawing.Color.White;
            this.AddNewP.Image = global::SGMS.Properties.Resources.plus_480px;
            this.AddNewP.ImagePosition = 8;
            this.AddNewP.ImageZoom = 22;
            this.AddNewP.LabelPosition = 25;
            this.AddNewP.LabelText = "إضافة";
            this.AddNewP.Location = new System.Drawing.Point(180, 10);
            this.AddNewP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddNewP.Name = "AddNewP";
            this.AddNewP.Size = new System.Drawing.Size(75, 30);
            this.AddNewP.TabIndex = 46;
            this.AddNewP.Click += new System.EventHandler(this.AddNewP_Click);
            // 
            // viewP
            // 
            this.viewP.BackColor = System.Drawing.Color.CadetBlue;
            this.viewP.color = System.Drawing.Color.CadetBlue;
            this.viewP.colorActive = System.Drawing.Color.Teal;
            this.viewP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.viewP.Font = new System.Drawing.Font("Cairo", 8F);
            this.viewP.ForeColor = System.Drawing.Color.White;
            this.viewP.Image = global::SGMS.Properties.Resources.visible_60px;
            this.viewP.ImagePosition = 8;
            this.viewP.ImageZoom = 22;
            this.viewP.LabelPosition = 25;
            this.viewP.LabelText = "عرض";
            this.viewP.Location = new System.Drawing.Point(98, 10);
            this.viewP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.viewP.Name = "viewP";
            this.viewP.Size = new System.Drawing.Size(75, 30);
            this.viewP.TabIndex = 45;
            this.viewP.Click += new System.EventHandler(this.viewP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 8F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(92, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "المصاريف الشخصية";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.panel2);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel16.Location = new System.Drawing.Point(597, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(34, 125);
            this.panel16.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(34, 85);
            this.panel2.TabIndex = 44;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.panel1);
            this.panel15.Controls.Add(this.label2);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel15.Location = new System.Drawing.Point(631, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(231, 125);
            this.panel15.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.AddNewAdmin);
            this.panel1.Controls.Add(this.viewAdmin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 85);
            this.panel1.TabIndex = 43;
            // 
            // AddNewAdmin
            // 
            this.AddNewAdmin.BackColor = System.Drawing.Color.DarkGreen;
            this.AddNewAdmin.color = System.Drawing.Color.DarkGreen;
            this.AddNewAdmin.colorActive = System.Drawing.Color.Green;
            this.AddNewAdmin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddNewAdmin.Font = new System.Drawing.Font("Cairo", 8F);
            this.AddNewAdmin.ForeColor = System.Drawing.Color.White;
            this.AddNewAdmin.Image = global::SGMS.Properties.Resources.plus_480px;
            this.AddNewAdmin.ImagePosition = 8;
            this.AddNewAdmin.ImageZoom = 18;
            this.AddNewAdmin.LabelPosition = 25;
            this.AddNewAdmin.LabelText = "إضافة مصروف";
            this.AddNewAdmin.Location = new System.Drawing.Point(117, 16);
            this.AddNewAdmin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddNewAdmin.Name = "AddNewAdmin";
            this.AddNewAdmin.Size = new System.Drawing.Size(107, 52);
            this.AddNewAdmin.TabIndex = 44;
            this.AddNewAdmin.Click += new System.EventHandler(this.AddNewAdmin_Click);
            // 
            // viewAdmin
            // 
            this.viewAdmin.BackColor = System.Drawing.Color.CadetBlue;
            this.viewAdmin.color = System.Drawing.Color.CadetBlue;
            this.viewAdmin.colorActive = System.Drawing.Color.Teal;
            this.viewAdmin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.viewAdmin.Font = new System.Drawing.Font("Cairo", 8F);
            this.viewAdmin.ForeColor = System.Drawing.Color.White;
            this.viewAdmin.Image = global::SGMS.Properties.Resources.visible_60px;
            this.viewAdmin.ImagePosition = 8;
            this.viewAdmin.ImageZoom = 18;
            this.viewAdmin.LabelPosition = 25;
            this.viewAdmin.LabelText = "عرض المصروفات";
            this.viewAdmin.Location = new System.Drawing.Point(5, 16);
            this.viewAdmin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.viewAdmin.Name = "viewAdmin";
            this.viewAdmin.Size = new System.Drawing.Size(107, 52);
            this.viewAdmin.TabIndex = 43;
            this.viewAdmin.Click += new System.EventHandler(this.viewAdmin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 8F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(72, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 20);
            this.label2.TabIndex = 40;
            this.label2.Text = "المصاريف الإدارية";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Cairo", 8F);
            this.tabControl1.ItemSize = new System.Drawing.Size(70, 35);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(876, 537);
            this.tabControl1.TabIndex = 1;
            // 
            // UserCash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl1);
            this.Name = "UserCash";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(876, 537);
            this.Load += new System.EventHandler(this.Home_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuCustomDataGrid PDatagride;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuTileButton AddNewP;
        private Bunifu.Framework.UI.BunifuTileButton viewP;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuTileButton AddNewAdmin;
        private Bunifu.Framework.UI.BunifuTileButton viewAdmin;
        private Bunifu.Framework.UI.BunifuTileButton PayP;
        private System.Windows.Forms.Panel panel5;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label totalmonth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox UserNam;
        private System.Windows.Forms.DataGridViewTextBoxColumn casee;
        private System.Windows.Forms.DataGridViewTextBoxColumn cash;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn reason;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn cid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowid;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuTileButton spiMonth;
    }
}
