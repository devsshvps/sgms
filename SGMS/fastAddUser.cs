﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class FastAddUser : MetroFramework.Forms.MetroForm
    {
        DataTable x1;
        DataTable x2;
        public FastAddUser()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
        }

        private void add_Click(object sender, EventArgs e)
        {

            var uName = name.Text;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@Name", uName);
            x1 = DBConnection.SqliteCommand("SELECT *  FROM Clients where Name = @Name", dic);
            if (x1.Rows.Count > 0)
            {
                MessageBox.Show("يوجد مورد بهذا الإسم" + " " + ":" + " " + uName);
                name.SelectAll();
                name.Focus();
                return;
            }
            var uPhone = phone.Text;
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("@Phone", uPhone);
            x2 = DBConnection.SqliteCommand("SELECT *  FROM Clients where Phone = @Phone", dic2);
            if (x2.Rows.Count > 0)
            {
                MessageBox.Show("يوجد مورد بنفس رقم الهاتف" + " " + ":" + " " + uPhone);
                phone.SelectAll();
                phone.Focus();
                return;
            }

            if (x1.Rows.Count <= 0 && x2.Rows.Count <= 0)
            {
                AddToClients();
            }
        }

        private void name_TextChanged(object sender, EventArgs e)
        {
            if (name.TextLength > 0 && phone.TextLength > 0)
            {
                add.Enabled = true;
            }
            else
            {
                add.Enabled = false;
            }
        }

        private void AddToClients()
        {
            try
            {

                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@Name", name.Text);
                addC.Add("@Phone", phone.Text);
                var addToclients = DBConnection.SqliteCommand("INSERT  INTO `Clients`(`Name`, `Phone`) values(@Name,@Phone)", addC);
                cMessgeBox mess = new cMessgeBox("تمت إضافة المورد بنجاح", "done", "c",1000);
                mess.ShowDialog();
                this.Close();
            }
            catch
            {
            }
        }
    }
}
