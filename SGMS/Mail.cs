﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;

namespace SGMS
{
    public partial class Mail : UserControl
    {
        int readMSG;
        int MSGID;
        public Mail()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            loadInbox();
            loadOutbox();
            LoadUsers();
        }

        private void loadInbox()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", Login.cid);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Mail where toID =@id", dic).ConvertMail();
            if (check == null) return;

            foreach (Models.Mail item in check)
            {

                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(inboxview);
                row.Cells[0].Value = item.subject;
                row.Cells[1].Value = item.fromName;
                row.Cells[2].Value = item.m_time.ToString("t");
                row.Cells[3].Value = item.m_date.ToString("yyyy-MM-dd");
                row.Cells[4].Value = item.Message;
                row.Cells[5].Value = item.id;
                row.Cells[6].Value = item.m;
                inboxview.Rows.Add(row);
            }
        }

        private void inboxview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int RowNo;
                RowNo = e.RowIndex;
                Mssg.Text = inboxview.Rows[RowNo].Cells[4].Value.ToString();
                MSGID = int.Parse(inboxview.Rows[RowNo].Cells[5].Value.ToString());
                readMSG = int.Parse(inboxview.Rows[RowNo].Cells[6].Value.ToString());
                if (readMSG == 1)
                {
                    Dictionary<string, object> dico = new Dictionary<string, object>();
                    dico.Add("@m", 0);
                    dico.Add("@id", MSGID);
                    var MakeAsRead = DBConnection.SqliteCommand("update Mail Set  m = @m where id =@id", dico);
                }
            }
            catch { }
        }

        private void loadOutbox()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", Login.cid);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Mail where fromID =@id", dic).ConvertMail();
            if (check == null) return;

            foreach (Models.Mail item in check)
            {

                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(outview);
                row.Cells[0].Value = item.subject;
                row.Cells[1].Value = item.toName;
                row.Cells[2].Value = item.m_time.ToString("t");
                row.Cells[3].Value = item.m_date.ToString("yyyy-MM-dd");
                row.Cells[4].Value = item.Message;
                outview.Rows.Add(row);
            }
        }

        private void outview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int RowNo;
                RowNo = e.RowIndex;

                OutMssg.Text = outview.Rows[RowNo].Cells[4].Value.ToString();
            }
            catch { }

        }

        private static List<User> users = new List<User>();
        private void LoadUsers()
        {

            Dictionary<string, object> dics = new Dictionary<string, object>();
            dics.Add("@r", 1);
            var rIt = DBConnection.SqliteCommand("SELECT *  FROM Login", dics);
            if (rIt == null) return;
            if (rIt.Rows.Count > 0)
            {
                ClientName1.DataSource = rIt;
                ClientName1.DisplayMember = "fullname";
                ClientName1.ValueMember = "uid";
            }

        }

        private void CheckData()
        {
            if (ClientName1.SelectedValue == null)
            {
                MessageBox.Show("برجاء إختيار إسم المرسل اليه");
            }else if (SubjectM.Text == "")
            {
                MessageBox.Show("برجاء كتابة عنوان للرسالة");
            }else if (Masse.Text == "")
            {
                MessageBox.Show("برجاء كتابة نص الرسالة");
            }
            else
            {
                SendMail();
                MessageBox.Show("تم إرسال الرسالة بنجاح");
                SubjectM.Text = "";
                Masse.Text = "";
            }
        }
        private void SendMail()
        {

                var id = int.Parse(ClientName1.SelectedValue.ToString());
                var toName = ClientName1.Text.ToString();
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@subject", SubjectM.Text.ToString());
                addC.Add("@Message", Masse.Text.ToString());
                addC.Add("@fromID", Login.cid);
                addC.Add("@fromName", Login.cnn);
                addC.Add("@toID", id);
                addC.Add("@toName", toName);
                addC.Add("@m_date", DateTime.Now.ToString("yyyy-MM-dd"));
                addC.Add("@m_time", DateTime.Now.ToString("HH:mm:ss"));
                var addToItems = DBConnection.SqliteCommand("INSERT  INTO `Mail`(`subject`, `Message`, `fromID`, `fromName`, `toID`,`toName`,`m_date`,`m_time`) values(@subject,@Message,@fromID,@fromName,@toID,@toName,@m_date,@m_time)", addC);
        }

        private void Send_Click(object sender, EventArgs e)
        {
            CheckData();
        }
    }
}
