﻿namespace SGMS
{
    partial class PayThePurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel5 = new System.Windows.Forms.Panel();
            this.ViewInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.unPaid = new System.Windows.Forms.Label();
            this.AddIn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label17 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InvNum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.invDate = new System.Windows.Forms.Label();
            this.invClient = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Aunpaid = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AToolbar = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.NeedPay = new System.Windows.Forms.TextBox();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.ViewInvByNum);
            this.panel5.Controls.Add(this.unPaid);
            this.panel5.Controls.Add(this.AddIn);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(20, 338);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(551, 39);
            this.panel5.TabIndex = 42;
            // 
            // ViewInvByNum
            // 
            this.ViewInvByNum.Activecolor = System.Drawing.Color.DodgerBlue;
            this.ViewInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ViewInvByNum.BackColor = System.Drawing.Color.DodgerBlue;
            this.ViewInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ViewInvByNum.BorderRadius = 7;
            this.ViewInvByNum.ButtonText = "عرض الفاتورة";
            this.ViewInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewInvByNum.DisabledColor = System.Drawing.Color.Gray;
            this.ViewInvByNum.Font = new System.Drawing.Font("Cairo", 10F);
            this.ViewInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.ViewInvByNum.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.ViewInvByNum.Iconimage_right = null;
            this.ViewInvByNum.Iconimage_right_Selected = null;
            this.ViewInvByNum.Iconimage_Selected = null;
            this.ViewInvByNum.IconMarginLeft = 25;
            this.ViewInvByNum.IconMarginRight = 0;
            this.ViewInvByNum.IconRightVisible = false;
            this.ViewInvByNum.IconRightZoom = 0D;
            this.ViewInvByNum.IconVisible = false;
            this.ViewInvByNum.IconZoom = 30D;
            this.ViewInvByNum.IsTab = false;
            this.ViewInvByNum.Location = new System.Drawing.Point(120, 5);
            this.ViewInvByNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ViewInvByNum.Name = "ViewInvByNum";
            this.ViewInvByNum.Normalcolor = System.Drawing.Color.DodgerBlue;
            this.ViewInvByNum.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ViewInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.ViewInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ViewInvByNum.selected = false;
            this.ViewInvByNum.Size = new System.Drawing.Size(119, 30);
            this.ViewInvByNum.TabIndex = 58;
            this.ViewInvByNum.Text = "عرض الفاتورة";
            this.ViewInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ViewInvByNum.Textcolor = System.Drawing.Color.White;
            this.ViewInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewInvByNum.Click += new System.EventHandler(this.ViewInvByNum_Click);
            // 
            // unPaid
            // 
            this.unPaid.Font = new System.Drawing.Font("Cairo", 8F);
            this.unPaid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.unPaid.Location = new System.Drawing.Point(346, 8);
            this.unPaid.Name = "unPaid";
            this.unPaid.Size = new System.Drawing.Size(67, 25);
            this.unPaid.TabIndex = 39;
            this.unPaid.Text = "0";
            this.unPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddIn
            // 
            this.AddIn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddIn.BorderRadius = 7;
            this.AddIn.ButtonText = "دفع";
            this.AddIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddIn.DisabledColor = System.Drawing.Color.Gray;
            this.AddIn.Enabled = false;
            this.AddIn.Font = new System.Drawing.Font("Cairo", 10F);
            this.AddIn.Iconcolor = System.Drawing.Color.Transparent;
            this.AddIn.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.AddIn.Iconimage_right = null;
            this.AddIn.Iconimage_right_Selected = null;
            this.AddIn.Iconimage_Selected = null;
            this.AddIn.IconMarginLeft = 0;
            this.AddIn.IconMarginRight = 0;
            this.AddIn.IconRightVisible = false;
            this.AddIn.IconRightZoom = 0D;
            this.AddIn.IconVisible = false;
            this.AddIn.IconZoom = 25D;
            this.AddIn.IsTab = false;
            this.AddIn.Location = new System.Drawing.Point(13, 5);
            this.AddIn.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.AddIn.Name = "AddIn";
            this.AddIn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.AddIn.OnHoverTextColor = System.Drawing.Color.White;
            this.AddIn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.AddIn.selected = false;
            this.AddIn.Size = new System.Drawing.Size(88, 30);
            this.AddIn.TabIndex = 41;
            this.AddIn.Text = "دفع";
            this.AddIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AddIn.Textcolor = System.Drawing.Color.White;
            this.AddIn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddIn.Click += new System.EventHandler(this.AddIn_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cairo", 7F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label17.Location = new System.Drawing.Point(425, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 19);
            this.label17.TabIndex = 38;
            this.label17.Text = "المبلغ المتبقي";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 7F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(205, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 19);
            this.label3.TabIndex = 49;
            this.label3.Text = "تاريخ الفاتورة :";
            // 
            // InvNum
            // 
            this.InvNum.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InvNum.ForeColor = System.Drawing.Color.Black;
            this.InvNum.Location = new System.Drawing.Point(204, 19);
            this.InvNum.Name = "InvNum";
            this.InvNum.Size = new System.Drawing.Size(101, 26);
            this.InvNum.TabIndex = 47;
            this.InvNum.Text = "0";
            this.InvNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cairo", 7F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(312, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 19);
            this.label8.TabIndex = 46;
            this.label8.Text = "رقم الفاتورة";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(490, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 19);
            this.label1.TabIndex = 45;
            this.label1.Text = "إسم المورد :";
            // 
            // invDate
            // 
            this.invDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invDate.Font = new System.Drawing.Font("Cairo", 8F);
            this.invDate.ForeColor = System.Drawing.Color.Black;
            this.invDate.Location = new System.Drawing.Point(33, 100);
            this.invDate.Name = "invDate";
            this.invDate.Size = new System.Drawing.Size(166, 19);
            this.invDate.TabIndex = 51;
            // 
            // invClient
            // 
            this.invClient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invClient.Font = new System.Drawing.Font("Cairo", 8F);
            this.invClient.ForeColor = System.Drawing.Color.Black;
            this.invClient.Location = new System.Drawing.Point(325, 100);
            this.invClient.Name = "invClient";
            this.invClient.Size = new System.Drawing.Size(159, 19);
            this.invClient.TabIndex = 50;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label5.Location = new System.Drawing.Point(45, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 136);
            this.label5.TabIndex = 52;
            this.label5.Text = " ";
            // 
            // Aunpaid
            // 
            this.Aunpaid.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Aunpaid.ForeColor = System.Drawing.Color.Black;
            this.Aunpaid.Location = new System.Drawing.Point(59, 176);
            this.Aunpaid.Name = "Aunpaid";
            this.Aunpaid.Size = new System.Drawing.Size(106, 109);
            this.Aunpaid.TabIndex = 53;
            this.Aunpaid.Text = "0";
            this.Aunpaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(72, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 23);
            this.label4.TabIndex = 54;
            this.label4.Text = "المبلغ المؤجل";
            // 
            // AToolbar
            // 
            this.AToolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AToolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.AToolbar.Location = new System.Drawing.Point(20, 60);
            this.AToolbar.Name = "AToolbar";
            this.AToolbar.Size = new System.Drawing.Size(551, 29);
            this.AToolbar.TabIndex = 55;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(328, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 23);
            this.label6.TabIndex = 56;
            this.label6.Text = "المبلغ المراد دفعة";
            // 
            // NeedPay
            // 
            this.NeedPay.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.NeedPay.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NeedPay.Location = new System.Drawing.Point(295, 215);
            this.NeedPay.Name = "NeedPay";
            this.NeedPay.Size = new System.Drawing.Size(167, 37);
            this.NeedPay.TabIndex = 57;
            this.NeedPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NeedPay.TextChanged += new System.EventHandler(this.NeedPay_TextChanged);
            this.NeedPay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlynumwithsinglepoint);
            // 
            // PayThePurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 397);
            this.Controls.Add(this.NeedPay);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.AToolbar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Aunpaid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.invDate);
            this.Controls.Add(this.invClient);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.InvNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel5);
            this.Name = "PayThePurchase";
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label unPaid;
        private Bunifu.Framework.UI.BunifuFlatButton AddIn;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label InvNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label invDate;
        private System.Windows.Forms.Label invClient;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Aunpaid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel AToolbar;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuFlatButton ViewInvByNum;
        private System.Windows.Forms.TextBox NeedPay;
    }
}