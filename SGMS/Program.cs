﻿using Newtonsoft.Json;
using SGMS.Data;
using SGMS.Models;
using SGMS.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    static class Program
    {
        public static string FTP = "@silver-eg.com";
        public static string FTPS = "s4.asurahosting.com";
        public static string f;
        private static List<UpdateVersion> UpdateApp = new List<UpdateVersion>();
       public static string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            CheckUpdate();
            if (args == null || args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Splash());
            }
            else
            {
                f = args[0];
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Splash(f));
            }

        }
        private static async void CheckUpdate()
        {
            if (await API.IsConnected() == true)
            {
                var checkUPDATE = API.u("update");
                UpdateApp = JsonConvert.DeserializeObject<List<UpdateVersion>>(await checkUPDATE);
                var x = UpdateApp.Find(i => i.id == 1);
                if (version != x.v)
                {
                    DialogResult dialogResult = MessageBox.Show("يوجد تحديث جديد للبرنامج الإصدار" + " " + Login.arNumber(x.v), "تحديث جديد", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        var url = x.url;
                        var filename = x.filename;
                        var version = x.v;
                        System.Diagnostics.Process.Start("updater.exe", url + " " + filename + " " + version);
                        Application.Exit();
                    }
                    else if (dialogResult == DialogResult.No)
                    {

                    }
                }
            }
            else
            {

            }
        }

    }
}
