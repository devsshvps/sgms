﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SGMS
{
    public partial class Sales : UserControl
    {
        string InvViews = "";
        int InvType;
        double inunpaid1, invtotal1;
        double invpaid1;

        public Sales()
        {
            InitializeComponent();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.R))
            {
                if (Login.r5 == "1")
                {
                    ReturnS rs = new ReturnS();
                    rs.FormClosed += NewInvoiceClosed;
                    rs.ShowDialog();
                }

            }
            else if (keyData == Keys.F1)
            {
                if (Login.r3 == "1")
                {
                    NewInvoice();
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Roles();
            fDay.SelectedIndex = 0;
            tDay.SelectedIndex = 30;
            fMonth.SelectedItem = DateTime.Now.ToString("MM");
            tMonth.SelectedItem = DateTime.Now.ToString("MM");
            fYear.SelectedItem = DateTime.Now.ToString("yyyy");
            tYear.SelectedItem = DateTime.Now.ToString("yyyy");
            LoadToday();
            LoadUnPaidInv();
        }

        private void Roles()
        {
            if (Login.r2 == "0")
            {
                tabControl1.TabPages.Remove(tabPage1);
            }

            if (Login.r3 == "0")
            {
                NewInv.Visible = false;
            }

            if (Login.r5 == "0")
            {
                Return.Visible = false;
            }

            if (Login.r6 == "0")
            {
                tabControl1.TabPages.Remove(tabPage2);
            }

            if (Login.r7 == "0")
            {
                tabControl1.TabPages.Remove(tabPage3);
            }

        }


        // مبيعات اليوم
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {
                        InvViews = row.Cells[0].Value.ToString();
                        InvType = int.Parse(row.Cells[6].Value.ToString());
                        ViewInv.Enabled = true;
                        PrintInv.Enabled = true;
                    }
                    else
                    {
                        InvViews = "";
                        InvType = 0;
                        ViewInv.Enabled = false;
                        PrintInv.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void ViewInv_Click(object sender, EventArgs e)
        {
            ViewInvoice();
        }
        private void Return_Click(object sender, EventArgs e)
        {
            ReturnS Newnow = new ReturnS();
            Newnow.FormClosed += NewInvoiceClosed;
            Newnow.ShowDialog();
        }
        private void PrintInv_Click(object sender, EventArgs e)
        {
            PrintInvoice();
        }
        private void NewInv_Click(object sender, EventArgs e)
        {
            NewInvoice();
        }
        private void PrintInvoice()
        {
            string TextCash = "";
            try
            {
                if (InvType == 0)
                {
                    try
                    {
                        string inv = InvViews;
                        Dictionary<string, object> dica = new Dictionary<string, object>();
                        dica.Add("@num", inv);
                        dica.Add("@Type", InvType);
                        var check = DBConnection.SqliteCommand("SELECT * FROM Sells where Num = @num", dica);
                        var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS WHERE invNum = @num AND Tax = @Type", dica).ConvertCashSales();
                        if (invCash == null) return;
                        if (check == null) return;
                        if (check.Rows.Count > 0)
                        {
                            foreach (CashInvS item in invCash)
                            {
                                inunpaid1 = item.unpaid;
                                var invtotal13 = item.totalInv;
                                invpaid1 = invtotal13 - inunpaid1;
                            }
                            string d1 = DateTime.Now.ToString("dd");
                            string m1 = DateTime.Now.ToString("MM");
                            string y1 = DateTime.Now.ToString("yyyy");
                            string ti1 = DateTime.Now.ToString("t");
                            string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                            string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                            string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                            Print2S printPurch = new Print2S();
                            printPurch.SetDataSource(check);
                            printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                            printPurch.SetParameterValue("invsName", "فاتورة مبيعات");
                            printPurch.SetParameterValue("paid", invpaid1);
                            printPurch.SetParameterValue("unpaid", inunpaid1);
                            printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                            printPurch.SetParameterValue("name", Dashboard.invCoN);
                            printPurch.SetParameterValue("slug", Dashboard.invCoS);
                            printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                            printPurch.SetParameterValue("address", Dashboard.invCoA);
                            printPurch.PrintToPrinter(1, false, 0, 0);
                        }
                    }
                    catch { }

                }
                else
                {
                    double isX = 0;
                    var TaxNum = "";
                    var CoRecord = "";
                    int nIsx = 0;
                    string inv = InvViews;
                    try
                    {
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic.Add("@r", inv);
                        dic.Add("@Type", InvType);
                        var check = DBConnection.SqliteCommand("SELECT * FROM TaxSells where Num=@r", dic);
                        var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS WHERE invNum = @r AND Tax = @Type", dic).ConvertCashSales();
                        var Taxin = DBConnection.SqliteCommand("SELECT  *  FROM TaxInfo where id = 1", dic).ConvertTaxInfo();
                        foreach (TaxInfo TaxX in Taxin)
                        {
                            TaxNum = TaxX.TaxNum;
                            CoRecord = TaxX.CoRecord;
                        }
                        if (invCash == null) return;
                        if (check == null) return;
                        if (check.Rows.Count > 0)
                        {
                            for (int i = 0; i < check.Rows.Count; i++)
                            {
                                nIsx = int.Parse(check.Rows[i]["IsTax"].ToString());
                            }
                            foreach (CashInvS item in invCash)
                            {
                                inunpaid1 = item.unpaid;
                                invtotal1 = item.totalInv;
                                invpaid1 = invtotal1 - inunpaid1;
                                isX = item.totalInv;
                                TextCash = arTotal(isX, "جنيه", "قرش");
                            }

                            string d1 = DateTime.Now.ToString("dd");
                            string m1 = DateTime.Now.ToString("MM");
                            string y1 = DateTime.Now.ToString("yyyy");
                            string ti1 = DateTime.Now.ToString("t");
                            string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                            string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                            string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                            Print2STax printPurch = new Print2STax();
                            printPurch.SetDataSource(check);
                            printPurch.SetParameterValue("invsName", "فاتورة ضريبية");
                            printPurch.SetParameterValue("TaxNum", TaxNum);
                            printPurch.SetParameterValue("CoRecord", CoRecord);
                            if (nIsx == 1)
                            {
                                printPurch.SetParameterValue("isTax", "٪" + " " + Login.arNumber("1"));
                            }
                            else
                            {
                                printPurch.SetParameterValue("isTax", "لا يوجد");
                            }
                            printPurch.SetParameterValue("arTotal", "فقط" + " " + TextCash + " " + "لا غير");
                            printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                            printPurch.SetParameterValue("paid", invpaid1);
                            printPurch.SetParameterValue("unpaid", inunpaid1);
                            printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                            printPurch.SetParameterValue("name", Dashboard.invCoN);
                            printPurch.SetParameterValue("slug", Dashboard.invCoS);
                            printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                            printPurch.SetParameterValue("address", Dashboard.invCoA);
                            printPurch.PrintToPrinter(1, false, 0, 0);
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void NewInvoice()
        {
            invTypes Newnow = new invTypes();
            Newnow.FormClosed += NewInvoiceClosed;
            Newnow.ShowDialog();
        }
        void NewInvoiceClosed(object sender, FormClosedEventArgs e)
        {
            LoadToday();
            LoadUnPaidInv();
        }
        private void ViewInvoice()
        {
            if (InvViews != "")
            {
                SalesInvP viewSaleinv = new SalesInvP(InvViews, InvType);
                viewSaleinv.ShowDialog();
            }
            else
            {
                cMessgeBox mess = new cMessgeBox("برجاء إختيار فاتورة للعرض", "error", "p", 1500);
                mess.ShowDialog();
            }
        }
        private void LoadToday()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var fdate = DateTime.Today.ToString("yyyy-MM-dd 00:00:00");
                var tdate = DateTime.Today.ToString("yyyy-MM-dd 23:59:59");
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@fToday", fdate);
                dic.Add("@tToday", tdate);
                var result = DBConnection.SqliteCommand("Select * from CashInvS where invDateTime BETWEEN @fToday AND @tToday", dic).ConvertCashSales();
                if (result == null) return;
                foreach (CashInvS item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item.invNum;
                    row.Cells[1].Value = item.totalInv;
                    row.Cells[2].Value = item.unpaid;
                    row.Cells[3].Value = item.clientName;
                    row.Cells[4].Value = item.userName;
                    row.Cells[5].Value = Login.arNumber(item.invDateTime.ToString("t"));
                    row.Cells[6].Value = item.Tax;
                    if (item.Tax == 1)
                    {
                        row.DefaultCellStyle.BackColor = Color.Beige;
                    }
                    dataGridView1.Rows.Add(row);
                    dataGridView1.Sort(dataGridView1.Columns[5], ListSortDirection.Descending);
                    dataGridView1.Rows[0].Selected = true;
                }

            }
            catch { }
            SumData();
        }
        private void SumData()
        {
            tIncome.Text = "0";
            uIncome.Text = "0";
            Pincome.Text = "0";
            try
            {
                foreach (DataGridViewRow item in dataGridView1.Rows)
                {
                    // إجمالي اليوم
                    int n = item.Index;
                    var TotalCome = (Double.Parse(tIncome.Text.ToString()) + Double.Parse(dataGridView1.Rows[n].Cells[1].Value.ToString())).ToString();
                    tIncome.Text = TotalCome;

                    // إجمالي الغير محصل
                    var TotalUnPaid = (Double.Parse(uIncome.Text.ToString()) + Double.Parse(dataGridView1.Rows[n].Cells[2].Value.ToString())).ToString();
                    uIncome.Text = TotalUnPaid;

                    // إجمالي المحصل
                    var TotalPaid = Double.Parse(TotalCome) - Double.Parse(TotalUnPaid);
                    Pincome.Text = TotalPaid.ToString();
                }
            }
            catch { }
        }

        // صفحة سجل المبيعات
        private void dataGridView2_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (dataGridView2.Rows.Count >= 1)
            {
                bPrint.Enabled = true;
            }
            else
            {
                bPrint.Enabled = false;
            }
        }
        private void dataGridView2_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dataGridView2.Rows.Count >= 1)
            {
                bPrint.Enabled = true;
            }
            else
            {
                bPrint.Enabled = false;
            }
        }
        private void ViewBTN_Click(object sender, EventArgs e)
        {
            try
            {
                TodaySells.Text = "0";
                ttEarn.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
                string fd = fDay.SelectedItem.ToString();
                string fm = fMonth.SelectedItem.ToString();
                string fy = fYear.SelectedItem.ToString();
                string td = tDay.SelectedItem.ToString();
                string tm = tMonth.SelectedItem.ToString();
                string ty = tYear.SelectedItem.ToString();
                string f = fy + "-" + fm + "-" + fd;
                string t = ty + "-" + tm + "-" + td;
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select * from Sells where ddate between '" + f + "' and '" + t + "'", dic).ConvertSales();
                if (result == null) return;
                foreach (Models.Sales item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView2);
                    row.Cells[0].Value = item.Num;
                    row.Cells[1].Value = item.itemname;
                    row.Cells[2].Value = item.count;
                    row.Cells[3].Value = item.itemprice;
                    row.Cells[4].Value = item.totalprice;
                    row.Cells[5].Value = item.sellfor;
                    row.Cells[6].Value = item.User;
                    row.Cells[7].Value = Login.arNumber(item.ttime.ToString("t"));
                    row.Cells[8].Value = item.ddate.ToString("yyyy-MM-dd");
                    row.Cells[9].Value = item.earn;
                    dataGridView2.Rows.Add(row);
                    dataGridView2.CurrentCell = null;
                }
                Calculate2();
            }
            catch { }
        }
        private void Calculate2()
        {

            TodaySells.Text = "0";
            ttEarn.Text = "0";
            btn.Visible = true;
            try
            {

                foreach (DataGridViewRow item in dataGridView2.Rows)
                {
                    int n = item.Index;
                    TodaySells.Text = (Double.Parse(TodaySells.Text.ToString())
                    + Double.Parse(dataGridView2.Rows[n].Cells[4].Value.ToString())).ToString();

                    ttEarn.Text = (Double.Parse(ttEarn.Text.ToString())
                    + Double.Parse(dataGridView2.Rows[n].Cells[9].Value.ToString())).ToString();
                }
            }
            catch { }

        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                fDay.Enabled = false;
                tDay.Enabled = false;
                fMonth.Enabled = false;
                tMonth.Enabled = false;
                fYear.Enabled = false;
                tYear.Enabled = false;
                ViewBTN.Enabled = false;
                invNumSearch.Enabled = true;
                TodaySells.Text = "0";
                ttEarn.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
            }
            else
            {
                fDay.Enabled = true;
                tDay.Enabled = true;
                fMonth.Enabled = true;
                tMonth.Enabled = true;
                fYear.Enabled = true;
                tYear.Enabled = true;
                ViewBTN.Enabled = true;
                invNumSearch.Enabled = false;
                TodaySells.Text = "0";
                ttEarn.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
            }
        }
        private void InvNumberSearching()
        {
            try
            {
                TodaySells.Text = "0";
                ttEarn.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
                string invN = invNumSearch.Text;
                invN = invN.TrimStart('0');
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("num", invN); ;
                var result = DBConnection.SqliteCommand("SELECT * FROM Sells where Num=@num", dic).ConvertSales();
                if (result == null) return;
                foreach (Models.Sales item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView2);
                    row.Cells[0].Value = item.Num;
                    row.Cells[1].Value = item.itemname;
                    row.Cells[2].Value = item.count;
                    row.Cells[3].Value = item.itemprice;
                    row.Cells[4].Value = item.totalprice;
                    row.Cells[5].Value = item.sellfor;
                    row.Cells[6].Value = item.User;
                    row.Cells[7].Value = Login.arNumber(item.ttime.ToString("t"));
                    row.Cells[8].Value = item.ddate.ToString("yyyy-MM-dd");
                    row.Cells[9].Value = item.earn;
                    dataGridView2.Rows.Add(row);
                    dataGridView2.CurrentCell = null;
                }
                Calculate2();
            }
            catch
            {

            }
        }
        private void invNumSearch_TextChanged(object sender, EventArgs e)
        {
            InvNumberSearching();
        }
        private void PrintDate()
        {
            string fd = fDay.SelectedItem.ToString();
            string fm = fMonth.SelectedItem.ToString();
            string fy = fYear.SelectedItem.ToString();
            string td = tDay.SelectedItem.ToString();
            string tm = tMonth.SelectedItem.ToString();
            string ty = tYear.SelectedItem.ToString();
            string f1 = Login.arNumber(fd) + " " + "-" + " " + Login.arDate(fm) + " " + "-" + " " + Login.arNumber(fy);
            string t1 = Login.arNumber(td) + " " + "-" + " " + Login.arDate(tm) + " " + "-" + " " + Login.arNumber(ty);
            string f = fy + "-" + fm + "-" + fd;
            string t = ty + "-" + tm + "-" + td;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة سجل المبيعات في الفترة من" + " " + f1 + " " + "حتي" + " " + t1, "طباعة سجل مبيعات", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("Select * from Sells where ddate between '" + f + "' and '" + t + "'", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        SpicialSalesprint printPurch = new SpicialSalesprint();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("dFrom", f1);
                        printPurch.SetParameterValue("dTo", t1);
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);
                    }

                }
                catch (Exception ex)
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void PrintInvNum()
        {
            string inv = invNumSearch.Text;
            int sNuM = int.Parse(inv.ToString());
            var finalnumber = "";
            if (sNuM >= 1 && sNuM < 10)
            {
                finalnumber = "000000" + sNuM.ToString();
            }
            else if (sNuM >= 10 && sNuM < 100)
            {
                finalnumber = "00000" + sNuM.ToString();
            }
            else if (sNuM >= 100 && sNuM < 1000)
            {
                finalnumber = "0000" + sNuM.ToString();
            }
            else if (sNuM >= 1000 && sNuM < 10000)
            {
                finalnumber = "000" + sNuM.ToString();
            }
            else if (sNuM >= 10000 && sNuM < 100000)
            {
                finalnumber = "00" + sNuM.ToString();
            }
            else if (sNuM >= 100000 && sNuM < 1000000)
            {
                finalnumber = "0" + sNuM.ToString();
            }
            else if (sNuM >= 1000000)
            {
                finalnumber = sNuM.ToString();
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("num", inv);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة فاتورة مبيعات رقم" + " " + ":" + " " + finalnumber, "طباعة فاتورة مبيعات", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT * FROM Sells where Num=@num", dic);
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@num AND Tax = 0", dic).ConvertCashSales();
                    if (invCash == null) return;
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        foreach (CashInvS item in invCash)
                        {
                            inunpaid1 = item.unpaid;
                            invtotal1 = item.totalInv;
                            invpaid1 = invtotal1 - inunpaid1;
                        }

                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2S printPurch = new Print2S();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("invsName", "فاتورة مبيعات");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("paid", invpaid1);
                        printPurch.SetParameterValue("unpaid", inunpaid1);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                        printPurch.SetParameterValue("address", Dashboard.invCoA);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void bPrint_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                PrintInvNum();
            }
            else
            {
                PrintDate();
            }
            checkBox1.Checked = false;
        }

        // صفحة الغير محصل

        private void LoadUnPaidInv()
        {
            try
            {
                dataGridView3.Rows.Clear();
                dataGridView3.Refresh();

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@unpaid", 0);
                var result = DBConnection.SqliteCommand("Select * from CashInvS where unpaid > @unpaid AND Tax = 0 ", dic).ConvertCashSales();
                if (result == null) return;
                foreach (CashInvS item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView3);
                    row.Cells[0].Value = item.invNum;
                    row.Cells[2].Value = item.unpaid;
                    row.Cells[3].Value = item.totalInv;
                    row.Cells[4].Value = item.clientName;
                    row.Cells[5].Value = item.userName;
                    row.Cells[6].Value = item.invDateTime.ToString("yyyy-MM-dd");
                    row.Cells[7].Value = item.invDateTime.ToString("t");
                    dataGridView3.Rows.Add(row);
                    dataGridView3.Sort(dataGridView3.Columns[0], ListSortDirection.Descending);
                    dataGridView3.Rows[0].Selected = true;
                }
                SumUnPaid();
            }
            catch { }
        }
        private void SumUnPaid()
        {
            try
            {
                foreach (DataGridViewRow ll in dataGridView3.Rows)
                {
                    int TP = ll.Index;
                    double u = Convert.ToDouble(dataGridView3.Rows[TP].Cells[2].Value);
                    double t = Convert.ToDouble(dataGridView3.Rows[TP].Cells[3].Value);
                    double pa;
                    pa = t - u;
                    dataGridView3.Rows[TP].Cells[1].Value = pa.ToString();
                }

                label3.Text = "0";
                foreach (DataGridViewRow item in dataGridView3.Rows)
                {
                    int n = item.Index;
                    var finalsum = (Double.Parse(label3.Text.ToString())
                    + Double.Parse(dataGridView3.Rows[n].Cells[2].Value.ToString())).ToString();
                    label3.Text = finalsum;
                }
            }
            catch { }
        }
        private void ViewInvByNum_Click(object sender, EventArgs e)
        {
            if (InvViews.Length > 0)
            {
                SalesInvP viewinv = new SalesInvP(InvViews, InvType);
                viewinv.ShowDialog();
            }
            else
            {
                MessageBox.Show("برجاء إختيار فاتورة للعرض");
            }
        }
        private void PrintInvByNum_Click(object sender, EventArgs e)
        {
            try
            {
                string inv = InvViews;
                int sNuM = int.Parse(inv.ToString());
                var finalnumber = "";
                if (sNuM >= 1 && sNuM < 10)
                {
                    finalnumber = "000000" + sNuM.ToString();
                }
                else if (sNuM >= 10 && sNuM < 100)
                {
                    finalnumber = "00000" + sNuM.ToString();
                }
                else if (sNuM >= 100 && sNuM < 1000)
                {
                    finalnumber = "0000" + sNuM.ToString();
                }
                else if (sNuM >= 1000 && sNuM < 10000)
                {
                    finalnumber = "000" + sNuM.ToString();
                }
                else if (sNuM >= 10000 && sNuM < 100000)
                {
                    finalnumber = "00" + sNuM.ToString();
                }
                else if (sNuM >= 100000 && sNuM < 1000000)
                {
                    finalnumber = "0" + sNuM.ToString();
                }
                else if (sNuM >= 1000000)
                {
                    finalnumber = sNuM.ToString();
                }
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("num", inv);
                DialogResult dialogResult = MessageBox.Show("هل تود طباعة فاتورة مبيعات رقم" + " " + ":" + " " + finalnumber, "طباعة فاتورة مبيعات", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    try
                    {
                        var result = DBConnection.SqliteCommand("SELECT * FROM Sells where Num=@num", dic);
                        var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@num AND Tax = 0", dic).ConvertCashSales();
                        if (invCash == null) return;
                        if (result == null) return;
                        if (result.Rows.Count > 0)
                        {
                            foreach (CashInvS item in invCash)
                            {
                                inunpaid1 = item.unpaid;
                                invtotal1 = item.totalInv;
                                invpaid1 = invtotal1 - inunpaid1;
                            }

                            string d1 = DateTime.Now.ToString("dd");
                            string m1 = DateTime.Now.ToString("MM");
                            string y1 = DateTime.Now.ToString("yyyy");
                            string ti1 = DateTime.Now.ToString("t");
                            string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                            string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                            string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                            Print2S printPurch = new Print2S();
                            printPurch.SetDataSource(result);
                            printPurch.SetParameterValue("invsName", "فاتورة مبيعات");
                            printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                            printPurch.SetParameterValue("paid", invpaid1);
                            printPurch.SetParameterValue("unpaid", inunpaid1);
                            printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                            printPurch.SetParameterValue("name", Dashboard.invCoN);
                            printPurch.SetParameterValue("slug", Dashboard.invCoS);
                            printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                            printPurch.SetParameterValue("address", Dashboard.invCoA);
                            printPurch.PrintToPrinter(1, false, 0, 0);

                        }

                    }
                    catch
                    {
                    }

                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
            catch
            {

            }
        }
        private void dataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView3.SelectedRows)
                {
                    if (dataGridView3.SelectedRows.Count > 0)
                    {
                        InvViews = row.Cells[0].Value.ToString();
                        ViewInvByNum.Enabled = true;
                        PrintInvByNum.Enabled = true;
                        PayBtn.Enabled = true;
                    }
                    else
                    {
                        InvViews = "";
                        ViewInvByNum.Enabled = false;
                        PrintInvByNum.Enabled = false;
                        PayBtn.Enabled = false;
                    }
                }
            }
            catch { }
        }
        void paynowClosed(object sender, FormClosedEventArgs e)
        {
            LoadUnPaidInv();
            LoadToday();
        }

        private void btn_Click(object sender, EventArgs e)
        {
            btn.Visible = false;
        }

        private void PayBtn_Click(object sender, EventArgs e)
        {
            PayTheSales paynow = new PayTheSales(InvViews);
            paynow.FormClosed += paynowClosed;
            paynow.ShowDialog();
        }

        private string arTotal(double Amount, string Curr, string SubCurr)
        {
            string Value = Math.Floor(Amount).ToString();
            string NewValue = null;
            string[] Part1 = new string[10] { "", "واحد ", "اثنان", "ثلاث", "أربعة", "خمسة", "ستة", "سبعة", "ثمانية", "تسعة" };
            string[] Part2 = new string[10] { "", "عشر", "عشرون", "ثلاثون", "أربعون", "خمسون", "ستون", "سبعون", "ثمانون", "تسعون" };
            string[] Part3 = new string[10] { "", "مئة", "مئتان", "ثلاثمائة ", "أربعمائة", "خمسمائة", "ستمائة", "سبعمائة", "ثمانمائة", "تسعمائة" };
            string[] Part4 = new string[10] { "", "ألف", "ألفان", "ثلاثة آلاف", "أربع آلاف", "خمسة آلاف", "ستة آلاف", "سبع آلاف", "ثمانية آلاف", "تسعة آلاف" };

            //---------------------------< ....... الملاييييييييييييين ........ >
            if (Value.Length == 7)
            {
                if (Value.Substring(0, 1) == "1")
                    NewValue = "مليون ";
                else if (Value.Substring(0, 1) == "2")
                    NewValue = "مليونان ";
                else
                    NewValue = Part1[int.Parse(Value.Substring(0, 1))] + " ملايين";

                Value = Value.Substring(1, Value.Length - 1);
            }
            //------------------------------------< ....... مئات الألوف....... >
            if (Value.Length == 6)
            {
                NewValue += " " + Part3[int.Parse(Value.Substring(0, 1))] + " و";
                Value = Value.Substring(1, Value.Length - 1);
            }
            //----------------------------------
            if (Value.Length == 5)
            {
                NewValue += " " + Part1[int.Parse(Value.Substring(1, 1))];
                NewValue += " و" + Part2[int.Parse(Value.Substring(0, 1))] + " ألفا و";
                Value = Value.Substring(1, Value.Length - 1);
                Value = Value.Substring(1, Value.Length - 1);
            }
            //---------------------------------
            if (Value.Length == 4)
            {
                NewValue += " " + Part4[int.Parse(Value.Substring(0, 1))];
                Value = Value.Substring(1, Value.Length - 1);
            }
            if (Value.Length == 3)
            {
                NewValue += " " + Part3[int.Parse(Value.Substring(0, 1))] + " و";
                Value = Value.Substring(1, Value.Length - 1);
            }
            //-----------------------------------
            if (Value.Length == 2)
            {
                if (Part2[int.Parse(Value.Substring(0, 1))] == "عشر")
                {
                    NewValue += Part1[int.Parse(Value.Substring(1, 1))];
                }
                else
                {
                    if (int.Parse(Value) == 20 || int.Parse(Value) == 30 || int.Parse(Value) == 40 || int.Parse(Value) == 50 || int.Parse(Value) == 60 || int.Parse(Value) == 70 || int.Parse(Value) == 80 || int.Parse(Value) == 90)
                    {
                        NewValue += Part1[int.Parse(Value.Substring(1, 1))];
                    }
                    else
                    {
                        NewValue += Part1[int.Parse(Value.Substring(1, 1))] + " و";
                    }

                }
                NewValue += " " + Part2[int.Parse(Value.Substring(0, 1))];
                Value = Value.Substring(1, Value.Length - 1);
                Value = Value.Substring(1, Value.Length - 1);
            }
            if (Value.Length == 1)
            {
                NewValue += " " + Part1[int.Parse(Value.Substring(0, 1))];
                Value = Value.Substring(1, Value.Length - 1);
            }


            NewValue += " " + Curr;
            //الكسر العشري
            string Real = Amount.ToString("");
            int Pos = Real.IndexOf('.', 0, Real.Length);
            if (Pos != -1)
            {
                Pos++;
                try
                {
                    string NewReal = (float.Parse(Real.Substring(Pos, Real.Length - Pos))).ToString();
                    if (NewReal.Length == 1)
                        NewValue += " و " + Part2[int.Parse(NewReal.Substring(0, 1))];
                    else
                    {
                        NewValue += " و " + Part1[int.Parse(NewReal.Substring(1, 1))];
                        NewValue += " و " + Part2[int.Parse(NewReal.Substring(0, 1))];
                    }
                    NewValue += " " + SubCurr;
                }
                catch { }
            }
            return NewValue;

        }

    }
}


