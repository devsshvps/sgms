﻿namespace SGMS
{
    partial class Sales
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.invNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invpaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unpaidinv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalInv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invuserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PayBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ViewInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PrintInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.btn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ttEarn = new System.Windows.Forms.Label();
            this.TodaySells = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.invNumSearch = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.bPrint = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.tYear = new System.Windows.Forms.ComboBox();
            this.tMonth = new System.Windows.Forms.ComboBox();
            this.tDay = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.fYear = new System.Windows.Forms.ComboBox();
            this.fMonth = new System.Windows.Forms.ComboBox();
            this.fDay = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ViewBTN = new Bunifu.Framework.UI.BunifuTileButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Inv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unPaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.NewInv = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PrintInv = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Return = new Bunifu.Framework.UI.BunifuFlatButton();
            this.ViewInv = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uIncome = new System.Windows.Forms.TextBox();
            this.Pincome = new System.Windows.Forms.TextBox();
            this.tIncome = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel12.SuspendLayout();
            this.bunifuCards4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.bunifuCards2.SuspendLayout();
            this.bunifuCards3.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 39);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(868, 494);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "غير محصل";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invNum,
            this.invpaid,
            this.unpaidinv,
            this.totalInv,
            this.clientName,
            this.invuserName,
            this.invDate,
            this.invTime});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.DoubleBuffered = true;
            this.dataGridView3.EnableHeadersVisualStyles = false;
            this.dataGridView3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView3.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView3.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(862, 441);
            this.dataGridView3.TabIndex = 15;
            this.dataGridView3.SelectionChanged += new System.EventHandler(this.dataGridView3_SelectionChanged);
            // 
            // invNum
            // 
            this.invNum.FillWeight = 87.27693F;
            this.invNum.HeaderText = "فاتورة";
            this.invNum.Name = "invNum";
            this.invNum.ReadOnly = true;
            // 
            // invpaid
            // 
            this.invpaid.HeaderText = "المدفوع";
            this.invpaid.Name = "invpaid";
            this.invpaid.ReadOnly = true;
            // 
            // unpaidinv
            // 
            this.unpaidinv.HeaderText = "غير مدفوع";
            this.unpaidinv.Name = "unpaidinv";
            this.unpaidinv.ReadOnly = true;
            // 
            // totalInv
            // 
            this.totalInv.HeaderText = "الإجمالي";
            this.totalInv.Name = "totalInv";
            this.totalInv.ReadOnly = true;
            // 
            // clientName
            // 
            this.clientName.HeaderText = "العميل";
            this.clientName.Name = "clientName";
            this.clientName.ReadOnly = true;
            // 
            // invuserName
            // 
            this.invuserName.HeaderText = "المستخدم";
            this.invuserName.Name = "invuserName";
            this.invuserName.ReadOnly = true;
            // 
            // invDate
            // 
            this.invDate.HeaderText = "تاريخ الفاتورة";
            this.invDate.Name = "invDate";
            this.invDate.ReadOnly = true;
            // 
            // invTime
            // 
            this.invTime.HeaderText = "وقت الفاتورة";
            this.invTime.Name = "invTime";
            this.invTime.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.PayBtn);
            this.panel3.Controls.Add(this.ViewInvByNum);
            this.panel3.Controls.Add(this.PrintInvByNum);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 444);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(862, 47);
            this.panel3.TabIndex = 0;
            // 
            // PayBtn
            // 
            this.PayBtn.Activecolor = System.Drawing.Color.Maroon;
            this.PayBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PayBtn.BackColor = System.Drawing.Color.Maroon;
            this.PayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PayBtn.BorderRadius = 7;
            this.PayBtn.ButtonText = "تحصيل المبلغ";
            this.PayBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayBtn.DisabledColor = System.Drawing.Color.White;
            this.PayBtn.Enabled = false;
            this.PayBtn.Font = new System.Drawing.Font("Cairo", 8F);
            this.PayBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.PayBtn.Iconimage = null;
            this.PayBtn.Iconimage_right = null;
            this.PayBtn.Iconimage_right_Selected = null;
            this.PayBtn.Iconimage_Selected = null;
            this.PayBtn.IconMarginLeft = 0;
            this.PayBtn.IconMarginRight = 0;
            this.PayBtn.IconRightVisible = false;
            this.PayBtn.IconRightZoom = 0D;
            this.PayBtn.IconVisible = false;
            this.PayBtn.IconZoom = 20D;
            this.PayBtn.IsTab = false;
            this.PayBtn.Location = new System.Drawing.Point(466, 9);
            this.PayBtn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PayBtn.Name = "PayBtn";
            this.PayBtn.Normalcolor = System.Drawing.Color.Maroon;
            this.PayBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PayBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.PayBtn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PayBtn.selected = false;
            this.PayBtn.Size = new System.Drawing.Size(119, 30);
            this.PayBtn.TabIndex = 45;
            this.PayBtn.Text = "تحصيل المبلغ";
            this.PayBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PayBtn.Textcolor = System.Drawing.Color.White;
            this.PayBtn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayBtn.Click += new System.EventHandler(this.PayBtn_Click);
            // 
            // ViewInvByNum
            // 
            this.ViewInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ViewInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ViewInvByNum.BorderRadius = 7;
            this.ViewInvByNum.ButtonText = "عرض الفاتورة";
            this.ViewInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewInvByNum.DisabledColor = System.Drawing.Color.Gray;
            this.ViewInvByNum.Enabled = false;
            this.ViewInvByNum.Font = new System.Drawing.Font("Cairo", 10F);
            this.ViewInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.ViewInvByNum.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.ViewInvByNum.Iconimage_right = null;
            this.ViewInvByNum.Iconimage_right_Selected = null;
            this.ViewInvByNum.Iconimage_Selected = null;
            this.ViewInvByNum.IconMarginLeft = 25;
            this.ViewInvByNum.IconMarginRight = 0;
            this.ViewInvByNum.IconRightVisible = false;
            this.ViewInvByNum.IconRightZoom = 0D;
            this.ViewInvByNum.IconVisible = false;
            this.ViewInvByNum.IconZoom = 30D;
            this.ViewInvByNum.IsTab = false;
            this.ViewInvByNum.Location = new System.Drawing.Point(725, 9);
            this.ViewInvByNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ViewInvByNum.Name = "ViewInvByNum";
            this.ViewInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.ViewInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.ViewInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ViewInvByNum.selected = false;
            this.ViewInvByNum.Size = new System.Drawing.Size(119, 30);
            this.ViewInvByNum.TabIndex = 43;
            this.ViewInvByNum.Text = "عرض الفاتورة";
            this.ViewInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ViewInvByNum.Textcolor = System.Drawing.Color.White;
            this.ViewInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewInvByNum.Click += new System.EventHandler(this.ViewInvByNum_Click);
            // 
            // PrintInvByNum
            // 
            this.PrintInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PrintInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PrintInvByNum.BorderRadius = 7;
            this.PrintInvByNum.ButtonText = "طباعة الفاتورة";
            this.PrintInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PrintInvByNum.DisabledColor = System.Drawing.Color.Gray;
            this.PrintInvByNum.Enabled = false;
            this.PrintInvByNum.Font = new System.Drawing.Font("Cairo", 8F);
            this.PrintInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.PrintInvByNum.Iconimage = null;
            this.PrintInvByNum.Iconimage_right = null;
            this.PrintInvByNum.Iconimage_right_Selected = null;
            this.PrintInvByNum.Iconimage_Selected = null;
            this.PrintInvByNum.IconMarginLeft = 0;
            this.PrintInvByNum.IconMarginRight = 0;
            this.PrintInvByNum.IconRightVisible = false;
            this.PrintInvByNum.IconRightZoom = 0D;
            this.PrintInvByNum.IconVisible = false;
            this.PrintInvByNum.IconZoom = 20D;
            this.PrintInvByNum.IsTab = false;
            this.PrintInvByNum.Location = new System.Drawing.Point(595, 9);
            this.PrintInvByNum.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PrintInvByNum.Name = "PrintInvByNum";
            this.PrintInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.OnHovercolor = System.Drawing.Color.Green;
            this.PrintInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.PrintInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PrintInvByNum.selected = false;
            this.PrintInvByNum.Size = new System.Drawing.Size(119, 30);
            this.PrintInvByNum.TabIndex = 42;
            this.PrintInvByNum.Text = "طباعة الفاتورة";
            this.PrintInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrintInvByNum.Textcolor = System.Drawing.Color.White;
            this.PrintInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintInvByNum.Click += new System.EventHandler(this.PrintInvByNum_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(343, 47);
            this.panel4.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Cairo", 12F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(29, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 27);
            this.label3.TabIndex = 39;
            this.label3.Text = "0";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 7F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label4.Location = new System.Drawing.Point(200, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 19);
            this.label4.TabIndex = 38;
            this.label4.Text = "إجمالي الغير محصل";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(19, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(279, 35);
            this.label2.TabIndex = 40;
            this.label2.Text = " ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 39);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(868, 494);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "سجل المبيعات";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn15,
            this.Column2,
            this.Column1});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.DoubleBuffered = true;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView2.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView2.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView2.Location = new System.Drawing.Point(3, 102);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(862, 389);
            this.dataGridView2.TabIndex = 7;
            this.dataGridView2.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView2_RowsAdded);
            this.dataGridView2.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView2_RowsRemoved);
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn5.FillWeight = 58.56418F;
            this.dataGridViewTextBoxColumn5.HeaderText = "فاتورة";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn1.FillWeight = 220.2111F;
            this.dataGridViewTextBoxColumn1.HeaderText = "إسم المنتج";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn2.FillWeight = 57.05908F;
            this.dataGridViewTextBoxColumn2.HeaderText = "الكمية";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn3.FillWeight = 91.37062F;
            this.dataGridViewTextBoxColumn3.HeaderText = "السعر";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn4.FillWeight = 70.96779F;
            this.dataGridViewTextBoxColumn4.HeaderText = "إجمالي";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn6.FillWeight = 112.2738F;
            this.dataGridViewTextBoxColumn6.HeaderText = "العميل";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn7.FillWeight = 119.1419F;
            this.dataGridViewTextBoxColumn7.HeaderText = " المستخدم";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn15.FillWeight = 74.04188F;
            this.dataGridViewTextBoxColumn15.HeaderText = "الوقت";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 96.37024F;
            this.Column2.HeaderText = "التاريخ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel12);
            this.panel2.Controls.Add(this.panel11);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(862, 99);
            this.panel2.TabIndex = 6;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.bunifuCards4);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(173, 99);
            this.panel12.TabIndex = 1;
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.BackColor = System.Drawing.Color.White;
            this.bunifuCards4.BorderRadius = 5;
            this.bunifuCards4.BottomSahddow = false;
            this.bunifuCards4.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards4.Controls.Add(this.btn);
            this.bunifuCards4.Controls.Add(this.ttEarn);
            this.bunifuCards4.Controls.Add(this.TodaySells);
            this.bunifuCards4.Controls.Add(this.label9);
            this.bunifuCards4.Controls.Add(this.label8);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(17, 13);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = false;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(137, 75);
            this.bunifuCards4.TabIndex = 37;
            // 
            // btn
            // 
            this.btn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.btn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn.BorderRadius = 7;
            this.btn.ButtonText = "عرض";
            this.btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn.DisabledColor = System.Drawing.Color.White;
            this.btn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn.Iconcolor = System.Drawing.Color.Transparent;
            this.btn.Iconimage = null;
            this.btn.Iconimage_right = null;
            this.btn.Iconimage_right_Selected = null;
            this.btn.Iconimage_Selected = null;
            this.btn.IconMarginLeft = 0;
            this.btn.IconMarginRight = 0;
            this.btn.IconRightVisible = false;
            this.btn.IconRightZoom = 0D;
            this.btn.IconVisible = false;
            this.btn.IconZoom = 20D;
            this.btn.IsTab = false;
            this.btn.Location = new System.Drawing.Point(12, 44);
            this.btn.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.btn.Name = "btn";
            this.btn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.btn.OnHoverTextColor = System.Drawing.Color.White;
            this.btn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn.selected = false;
            this.btn.Size = new System.Drawing.Size(54, 17);
            this.btn.TabIndex = 70;
            this.btn.Text = "عرض";
            this.btn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn.Textcolor = System.Drawing.Color.White;
            this.btn.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn.Click += new System.EventHandler(this.btn_Click);
            // 
            // ttEarn
            // 
            this.ttEarn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ttEarn.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.ttEarn.ForeColor = System.Drawing.Color.Maroon;
            this.ttEarn.Location = new System.Drawing.Point(12, 44);
            this.ttEarn.Name = "ttEarn";
            this.ttEarn.Size = new System.Drawing.Size(55, 14);
            this.ttEarn.TabIndex = 66;
            this.ttEarn.Text = "0";
            this.ttEarn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TodaySells
            // 
            this.TodaySells.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.TodaySells.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.TodaySells.ForeColor = System.Drawing.Color.Maroon;
            this.TodaySells.Location = new System.Drawing.Point(12, 22);
            this.TodaySells.Name = "TodaySells";
            this.TodaySells.Size = new System.Drawing.Size(55, 14);
            this.TodaySells.TabIndex = 66;
            this.TodaySells.Text = "0";
            this.TodaySells.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label9.Location = new System.Drawing.Point(70, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 14);
            this.label9.TabIndex = 66;
            this.label9.Text = "إجمالي الأرباح";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(69, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 14);
            this.label8.TabIndex = 66;
            this.label8.Text = "إجمالي مبيعات";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.invNumSearch);
            this.panel11.Controls.Add(this.checkBox1);
            this.panel11.Controls.Add(this.bPrint);
            this.panel11.Controls.Add(this.bunifuCards2);
            this.panel11.Controls.Add(this.bunifuCards3);
            this.panel11.Controls.Add(this.ViewBTN);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(179, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(683, 99);
            this.panel11.TabIndex = 0;
            // 
            // invNumSearch
            // 
            this.invNumSearch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.invNumSearch.Enabled = false;
            this.invNumSearch.Location = new System.Drawing.Point(23, 56);
            this.invNumSearch.Name = "invNumSearch";
            this.invNumSearch.Size = new System.Drawing.Size(78, 32);
            this.invNumSearch.TabIndex = 44;
            this.invNumSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.invNumSearch.TextChanged += new System.EventHandler(this.invNumSearch_TextChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Cairo", 8F);
            this.checkBox1.Location = new System.Drawing.Point(109, 61);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 24);
            this.checkBox1.TabIndex = 43;
            this.checkBox1.Text = "رقم الفاتورة";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // bPrint
            // 
            this.bPrint.BackColor = System.Drawing.Color.Green;
            this.bPrint.color = System.Drawing.Color.Green;
            this.bPrint.colorActive = System.Drawing.Color.DarkGreen;
            this.bPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bPrint.Enabled = false;
            this.bPrint.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.bPrint.ForeColor = System.Drawing.Color.White;
            this.bPrint.Image = global::SGMS.Properties.Resources.print;
            this.bPrint.ImagePosition = 10;
            this.bPrint.ImageZoom = 25;
            this.bPrint.LabelPosition = 0;
            this.bPrint.LabelText = "";
            this.bPrint.Location = new System.Drawing.Point(23, 12);
            this.bPrint.Margin = new System.Windows.Forms.Padding(6);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(78, 40);
            this.bPrint.TabIndex = 37;
            this.bPrint.Click += new System.EventHandler(this.bPrint_Click);
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.White;
            this.bunifuCards2.BorderRadius = 5;
            this.bunifuCards2.BottomSahddow = false;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards2.Controls.Add(this.tYear);
            this.bunifuCards2.Controls.Add(this.tMonth);
            this.bunifuCards2.Controls.Add(this.tDay);
            this.bunifuCards2.Controls.Add(this.label15);
            this.bunifuCards2.LeftSahddow = false;
            this.bunifuCards2.Location = new System.Drawing.Point(207, 13);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = false;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(224, 75);
            this.bunifuCards2.TabIndex = 36;
            // 
            // tYear
            // 
            this.tYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tYear.Font = new System.Drawing.Font("Cairo", 8F);
            this.tYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tYear.FormattingEnabled = true;
            this.tYear.Items.AddRange(new object[] {
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.tYear.Location = new System.Drawing.Point(15, 33);
            this.tYear.Name = "tYear";
            this.tYear.Size = new System.Drawing.Size(68, 28);
            this.tYear.TabIndex = 43;
            // 
            // tMonth
            // 
            this.tMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tMonth.Font = new System.Drawing.Font("Cairo", 8F);
            this.tMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tMonth.FormattingEnabled = true;
            this.tMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.tMonth.Location = new System.Drawing.Point(89, 33);
            this.tMonth.Name = "tMonth";
            this.tMonth.Size = new System.Drawing.Size(66, 28);
            this.tMonth.TabIndex = 42;
            // 
            // tDay
            // 
            this.tDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.tDay.Font = new System.Drawing.Font("Cairo", 8F);
            this.tDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tDay.FormattingEnabled = true;
            this.tDay.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.tDay.Location = new System.Drawing.Point(161, 33);
            this.tDay.Name = "tDay";
            this.tDay.Size = new System.Drawing.Size(48, 28);
            this.tDay.TabIndex = 41;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cairo", 7F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label15.Location = new System.Drawing.Point(126, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 19);
            this.label15.TabIndex = 38;
            this.label15.Text = "تحديد تاريخ النهاية";
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.White;
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = false;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards3.Controls.Add(this.fYear);
            this.bunifuCards3.Controls.Add(this.fMonth);
            this.bunifuCards3.Controls.Add(this.fDay);
            this.bunifuCards3.Controls.Add(this.label14);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(442, 13);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = false;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(224, 75);
            this.bunifuCards3.TabIndex = 35;
            // 
            // fYear
            // 
            this.fYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fYear.Font = new System.Drawing.Font("Cairo", 8F);
            this.fYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fYear.FormattingEnabled = true;
            this.fYear.Items.AddRange(new object[] {
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.fYear.Location = new System.Drawing.Point(15, 33);
            this.fYear.Name = "fYear";
            this.fYear.Size = new System.Drawing.Size(68, 28);
            this.fYear.TabIndex = 40;
            // 
            // fMonth
            // 
            this.fMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fMonth.Font = new System.Drawing.Font("Cairo", 8F);
            this.fMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fMonth.FormattingEnabled = true;
            this.fMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.fMonth.Location = new System.Drawing.Point(89, 33);
            this.fMonth.Name = "fMonth";
            this.fMonth.Size = new System.Drawing.Size(66, 28);
            this.fMonth.TabIndex = 39;
            // 
            // fDay
            // 
            this.fDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fDay.Font = new System.Drawing.Font("Cairo", 8F);
            this.fDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fDay.FormattingEnabled = true;
            this.fDay.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.fDay.Location = new System.Drawing.Point(161, 33);
            this.fDay.Name = "fDay";
            this.fDay.Size = new System.Drawing.Size(48, 28);
            this.fDay.TabIndex = 38;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Cairo", 7F);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(131, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 19);
            this.label14.TabIndex = 37;
            this.label14.Text = "تحديد تاريخ البداية";
            // 
            // ViewBTN
            // 
            this.ViewBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewBTN.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewBTN.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.ViewBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewBTN.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.ViewBTN.ForeColor = System.Drawing.Color.White;
            this.ViewBTN.Image = global::SGMS.Properties.Resources.search_52px;
            this.ViewBTN.ImagePosition = 10;
            this.ViewBTN.ImageZoom = 25;
            this.ViewBTN.LabelPosition = 0;
            this.ViewBTN.LabelText = "";
            this.ViewBTN.Location = new System.Drawing.Point(113, 12);
            this.ViewBTN.Margin = new System.Windows.Forms.Padding(6);
            this.ViewBTN.Name = "ViewBTN";
            this.ViewBTN.Size = new System.Drawing.Size(78, 40);
            this.ViewBTN.TabIndex = 34;
            this.ViewBTN.Click += new System.EventHandler(this.ViewBTN_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(868, 494);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "مبيعات اليوم";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Inv,
            this.Total,
            this.unPaid,
            this.Client,
            this.User,
            this.iTime,
            this.Tax});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.DoubleBuffered = true;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(3, 46);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(862, 445);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // Inv
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Inv.DefaultCellStyle = dataGridViewCellStyle18;
            this.Inv.FillWeight = 58.50328F;
            this.Inv.HeaderText = "رقم الفاتورة";
            this.Inv.Name = "Inv";
            this.Inv.ReadOnly = true;
            this.Inv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Total
            // 
            this.Total.FillWeight = 96.27002F;
            this.Total.HeaderText = "الإجمالي";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // unPaid
            // 
            this.unPaid.HeaderText = "المتبقي";
            this.unPaid.Name = "unPaid";
            this.unPaid.ReadOnly = true;
            this.unPaid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Client
            // 
            this.Client.HeaderText = "العميل";
            this.Client.Name = "Client";
            this.Client.ReadOnly = true;
            this.Client.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // User
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.User.DefaultCellStyle = dataGridViewCellStyle19;
            this.User.FillWeight = 56.99974F;
            this.User.HeaderText = "المستخدم";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            this.User.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // iTime
            // 
            this.iTime.FillWeight = 76.0958F;
            this.iTime.HeaderText = "الوقت";
            this.iTime.Name = "iTime";
            this.iTime.ReadOnly = true;
            this.iTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Tax
            // 
            this.Tax.HeaderText = "Tax";
            this.Tax.Name = "Tax";
            this.Tax.ReadOnly = true;
            this.Tax.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(862, 43);
            this.panel6.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.NewInv);
            this.panel1.Controls.Add(this.PrintInv);
            this.panel1.Controls.Add(this.Return);
            this.panel1.Controls.Add(this.ViewInv);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 44);
            this.panel1.TabIndex = 1;
            // 
            // NewInv
            // 
            this.NewInv.Activecolor = System.Drawing.Color.Maroon;
            this.NewInv.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.NewInv.BackColor = System.Drawing.Color.Maroon;
            this.NewInv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.NewInv.BorderRadius = 7;
            this.NewInv.ButtonText = "فاتورة جديدة";
            this.NewInv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NewInv.DisabledColor = System.Drawing.Color.Gray;
            this.NewInv.Font = new System.Drawing.Font("Cairo", 10F);
            this.NewInv.Iconcolor = System.Drawing.Color.Transparent;
            this.NewInv.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.NewInv.Iconimage_right = null;
            this.NewInv.Iconimage_right_Selected = null;
            this.NewInv.Iconimage_Selected = null;
            this.NewInv.IconMarginLeft = 25;
            this.NewInv.IconMarginRight = 0;
            this.NewInv.IconRightVisible = false;
            this.NewInv.IconRightZoom = 0D;
            this.NewInv.IconVisible = false;
            this.NewInv.IconZoom = 30D;
            this.NewInv.IsTab = false;
            this.NewInv.Location = new System.Drawing.Point(764, 7);
            this.NewInv.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.NewInv.Name = "NewInv";
            this.NewInv.Normalcolor = System.Drawing.Color.Maroon;
            this.NewInv.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.NewInv.OnHoverTextColor = System.Drawing.Color.White;
            this.NewInv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.NewInv.selected = false;
            this.NewInv.Size = new System.Drawing.Size(93, 30);
            this.NewInv.TabIndex = 50;
            this.NewInv.Text = "فاتورة جديدة";
            this.NewInv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.NewInv.Textcolor = System.Drawing.Color.White;
            this.NewInv.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewInv.Click += new System.EventHandler(this.NewInv_Click);
            // 
            // PrintInv
            // 
            this.PrintInv.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInv.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PrintInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PrintInv.BorderRadius = 7;
            this.PrintInv.ButtonText = "طباعة الفاتورة";
            this.PrintInv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PrintInv.DisabledColor = System.Drawing.Color.Gray;
            this.PrintInv.Enabled = false;
            this.PrintInv.Font = new System.Drawing.Font("Cairo", 8F);
            this.PrintInv.Iconcolor = System.Drawing.Color.Transparent;
            this.PrintInv.Iconimage = null;
            this.PrintInv.Iconimage_right = null;
            this.PrintInv.Iconimage_right_Selected = null;
            this.PrintInv.Iconimage_Selected = null;
            this.PrintInv.IconMarginLeft = 0;
            this.PrintInv.IconMarginRight = 0;
            this.PrintInv.IconRightVisible = false;
            this.PrintInv.IconRightZoom = 0D;
            this.PrintInv.IconVisible = false;
            this.PrintInv.IconZoom = 20D;
            this.PrintInv.IsTab = false;
            this.PrintInv.Location = new System.Drawing.Point(566, 7);
            this.PrintInv.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PrintInv.Name = "PrintInv";
            this.PrintInv.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInv.OnHovercolor = System.Drawing.Color.Green;
            this.PrintInv.OnHoverTextColor = System.Drawing.Color.White;
            this.PrintInv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PrintInv.selected = false;
            this.PrintInv.Size = new System.Drawing.Size(93, 30);
            this.PrintInv.TabIndex = 48;
            this.PrintInv.Text = "طباعة الفاتورة";
            this.PrintInv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrintInv.Textcolor = System.Drawing.Color.White;
            this.PrintInv.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintInv.Click += new System.EventHandler(this.PrintInv_Click);
            // 
            // Return
            // 
            this.Return.Activecolor = System.Drawing.Color.DarkBlue;
            this.Return.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Return.BackColor = System.Drawing.Color.Navy;
            this.Return.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Return.BorderRadius = 7;
            this.Return.ButtonText = "إرتجاع منتج";
            this.Return.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Return.DisabledColor = System.Drawing.Color.Gray;
            this.Return.Font = new System.Drawing.Font("Cairo", 8F);
            this.Return.Iconcolor = System.Drawing.Color.Transparent;
            this.Return.Iconimage = null;
            this.Return.Iconimage_right = null;
            this.Return.Iconimage_right_Selected = null;
            this.Return.Iconimage_Selected = null;
            this.Return.IconMarginLeft = 0;
            this.Return.IconMarginRight = 0;
            this.Return.IconRightVisible = false;
            this.Return.IconRightZoom = 0D;
            this.Return.IconVisible = false;
            this.Return.IconZoom = 20D;
            this.Return.IsTab = false;
            this.Return.Location = new System.Drawing.Point(467, 7);
            this.Return.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Return.Name = "Return";
            this.Return.Normalcolor = System.Drawing.Color.Navy;
            this.Return.OnHovercolor = System.Drawing.Color.DarkBlue;
            this.Return.OnHoverTextColor = System.Drawing.Color.White;
            this.Return.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Return.selected = false;
            this.Return.Size = new System.Drawing.Size(93, 30);
            this.Return.TabIndex = 51;
            this.Return.Text = "إرتجاع منتج";
            this.Return.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Return.Textcolor = System.Drawing.Color.White;
            this.Return.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Return.Click += new System.EventHandler(this.Return_Click);
            // 
            // ViewInv
            // 
            this.ViewInv.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInv.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ViewInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ViewInv.BorderRadius = 7;
            this.ViewInv.ButtonText = "عرض الفاتورة";
            this.ViewInv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewInv.DisabledColor = System.Drawing.Color.Gray;
            this.ViewInv.Font = new System.Drawing.Font("Cairo", 10F);
            this.ViewInv.Iconcolor = System.Drawing.Color.Transparent;
            this.ViewInv.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.ViewInv.Iconimage_right = null;
            this.ViewInv.Iconimage_right_Selected = null;
            this.ViewInv.Iconimage_Selected = null;
            this.ViewInv.IconMarginLeft = 25;
            this.ViewInv.IconMarginRight = 0;
            this.ViewInv.IconRightVisible = false;
            this.ViewInv.IconRightZoom = 0D;
            this.ViewInv.IconVisible = false;
            this.ViewInv.IconZoom = 30D;
            this.ViewInv.IsTab = false;
            this.ViewInv.Location = new System.Drawing.Point(665, 7);
            this.ViewInv.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ViewInv.Name = "ViewInv";
            this.ViewInv.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInv.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.ViewInv.OnHoverTextColor = System.Drawing.Color.White;
            this.ViewInv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ViewInv.selected = false;
            this.ViewInv.Size = new System.Drawing.Size(93, 30);
            this.ViewInv.TabIndex = 49;
            this.ViewInv.Text = "عرض الفاتورة";
            this.ViewInv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ViewInv.Textcolor = System.Drawing.Color.White;
            this.ViewInv.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewInv.Click += new System.EventHandler(this.ViewInv_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.uIncome);
            this.panel5.Controls.Add(this.Pincome);
            this.panel5.Controls.Add(this.tIncome);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(322, 44);
            this.panel5.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cairo", 7F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(32, -3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 19);
            this.label7.TabIndex = 39;
            this.label7.Text = "غير محصل";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 7F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(140, -3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 19);
            this.label6.TabIndex = 39;
            this.label6.Text = "المحصل";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cairo", 7F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(242, -3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 19);
            this.label5.TabIndex = 38;
            this.label5.Text = "الإجمالي";
            // 
            // uIncome
            // 
            this.uIncome.Enabled = false;
            this.uIncome.Location = new System.Drawing.Point(8, 12);
            this.uIncome.Name = "uIncome";
            this.uIncome.Size = new System.Drawing.Size(100, 32);
            this.uIncome.TabIndex = 2;
            this.uIncome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Pincome
            // 
            this.Pincome.Enabled = false;
            this.Pincome.Location = new System.Drawing.Point(111, 12);
            this.Pincome.Name = "Pincome";
            this.Pincome.Size = new System.Drawing.Size(100, 32);
            this.Pincome.TabIndex = 1;
            this.Pincome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tIncome
            // 
            this.tIncome.Enabled = false;
            this.tIncome.Location = new System.Drawing.Point(214, 12);
            this.tIncome.Name = "tIncome";
            this.tIncome.Size = new System.Drawing.Size(100, 32);
            this.tIncome.TabIndex = 0;
            this.tIncome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(70, 35);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(876, 537);
            this.tabControl1.TabIndex = 0;
            // 
            // Sales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl1);
            this.Name = "Sales";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(876, 537);
            this.Load += new System.EventHandler(this.Home_Load);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards4.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.bunifuCards2.ResumeLayout(false);
            this.bunifuCards2.PerformLayout();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards3.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel12;
        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private System.Windows.Forms.Panel panel11;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private System.Windows.Forms.ComboBox tYear;
        private System.Windows.Forms.ComboBox tMonth;
        private System.Windows.Forms.ComboBox tDay;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private System.Windows.Forms.ComboBox fYear;
        private System.Windows.Forms.ComboBox fMonth;
        private System.Windows.Forms.ComboBox fDay;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuTileButton ViewBTN;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private Bunifu.Framework.UI.BunifuTileButton bPrint;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox invNumSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuFlatButton ViewInvByNum;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuFlatButton PayBtn;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView1;
        private System.Windows.Forms.Panel panel6;
        private Bunifu.Framework.UI.BunifuFlatButton PrintInvByNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn invNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn invpaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn unpaidinv;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalInv;
        private System.Windows.Forms.DataGridViewTextBoxColumn clientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn invuserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn invDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn invTime;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuFlatButton NewInv;
        private Bunifu.Framework.UI.BunifuFlatButton PrintInv;
        private Bunifu.Framework.UI.BunifuFlatButton Return;
        private Bunifu.Framework.UI.BunifuFlatButton ViewInv;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox uIncome;
        private System.Windows.Forms.TextBox Pincome;
        private System.Windows.Forms.TextBox tIncome;
        private System.Windows.Forms.Label ttEarn;
        private System.Windows.Forms.Label TodaySells;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private Bunifu.Framework.UI.BunifuFlatButton btn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn unPaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Client;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
        private System.Windows.Forms.DataGridViewTextBoxColumn iTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tax;
    }
}
