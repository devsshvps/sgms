﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;

namespace SGMS
{
    public partial class PExpenses : UserControl
    {
        double OldSales, OldPu, OldUnPaidSales, OldUnPaidPu, TotalOldUsers, TotalOldAdmin;
        double NewSales, NewPu, NewUnPaidSales, NewUnPaidPu, TotalNewUsers, TotalNewAdmin;
        DateTime ACMONTH , oldMONTHF;
        private static List<Expense> PExpense = new List<Expense>();
        public PExpenses()
        {
            InitializeComponent();

            MonthYear.Text = Login.arDate(DateTime.Now.ToString("MM")) + " " + "لسنة" + " " + Login.arNumber(DateTime.Now.ToString("yyyy"));

        }
        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillPEList();
            ActiveDate();
            OldMounthData();
            ThisMounthData();
            sumFinal();
            FinalCheck();
            UnPaidPCheck();
            UnPaidSCheck();
        }
        private void FillPEList()
        {
            try
            {
                PExpense.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 0);
                var PE = DBConnection.SqliteCommand("SELECT * FROM PExpenses", dic);

                if (PE == null) return;
                if (PE.Rows.Count > 0)
                {
                    foreach (DataRow item in PE.Rows)
                    {
                        Expense e = new Expense();
                        e.id = int.Parse(item["id"].ToString());
                        e.UserID = int.Parse(item["UserID"].ToString());
                        e.UserName = item["UserName"].ToString();
                        e.Cash = double.Parse(item["Cash"].ToString());
                        e.TCash = double.Parse(item["TCash"].ToString());
                        e.Reason = item["Reason"].ToString();
                        e.LoginID = int.Parse(item["LoginID"].ToString());
                        e.LoginName = item["LoginName"].ToString();
                        e.r_Time = DateTime.Parse(item["r_Time"].ToString());
                        e.r_Date = DateTime.Parse(item["r_Date"].ToString());
                        e.r = int.Parse(item["r"].ToString());
                        PExpense.Add(e);
                    }
                }

            }
            catch { }
        }
        private void ActiveDate()
        {
            try
            {

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                var AC = DBConnection.SqliteCommand("SELECT ActiveDate FROM Activition where id = 1", dic);
                if (AC == null) return;
                if (AC.Rows.Count > 0)
                {
                    ACMONTH = DateTime.Parse(AC.Rows[0]["ActiveDate"].ToString());
                    oldMONTHF = DateTime.Parse(ACMONTH.AddMonths(+1).ToString("yyyy-MM-01"));

                }
            }
            catch { }
        }
        private void OldMounthData()
        {
            LoadOldSales();
            LoadOldPu();
            LoadOldUnPaidSales();
            LoadOldUnPaidPu();
            LoadOldPEadmins();
            LoadOldPEusers();
        }
        private void ThisMounthData()
        {
            LoadNewSales();
            LoadNewPu();
            LoadNewUnPaidSales();
            LoadNewUnPaidPu();
            LoadNewPEadmins();
            LoadNewPEusers();
        }

        private void LoadOldSales()
        {
            try
            {
                var m = DateTime.Now.AddMonths(-1).ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = ACMONTH.ToString("yyyy-MM-dd");
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(totalprice) totalprice from Sells where ddate between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    OldSales = Double.Parse(result.Rows[0]["totalprice"].ToString());
                }
            }
            catch
            {

            }

        }
        private void LoadOldPu()
        {
            try
            {
                var m = DateTime.Now.AddMonths(-1).ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = oldMONTHF.ToString("yyyy-MM-dd");
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(TotalBuy) TotalBuy from Purchases where Date_ct between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    OldPu = Double.Parse(result.Rows[0]["TotalBuy"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadOldUnPaidSales()
        {
            try
            {
                var m = DateTime.Now.AddMonths(-1).ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = ACMONTH.ToString("yyyy-MM-dd HH:mm:ss");
                string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(unpaid) unpaid from CashInvS where invDateTime between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    OldUnPaidSales = Double.Parse(result.Rows[0]["unpaid"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadOldUnPaidPu()
        {
            try
            {
                var m = DateTime.Now.AddMonths(-1).ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = oldMONTHF.ToString("yyyy-MM-dd HH:mm:ss");
                string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(unpaid) unpaid from CashInvD where invDateTime between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    OldUnPaidPu = Double.Parse(result.Rows[0]["unpaid"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadOldPEadmins()
        {
            try
            {
                var m = DateTime.Now.AddMonths(-1).ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = ACMONTH.ToString("yyyy-MM-dd");
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                dic.Add("r", 956135252);
                var result = DBConnection.SqliteCommand("Select sum(Cash) Cash from PExpenses where r_Date between '" + f + "' and '" + t + "' AND r = @r", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    TotalOldAdmin = Double.Parse(result.Rows[0]["Cash"].ToString());
                }
            }
            catch
            {

            }

        }
        private void LoadOldPEusers()
        {
            try
            {
                var m = DateTime.Now.AddMonths(-1).ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = ACMONTH.ToString("yyyy-MM-dd");
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                dic.Add("r", 956135253);
                var result = DBConnection.SqliteCommand("Select sum(Cash) Cash from PExpenses where r_Date between '" + f + "' and '" + t + "' AND r = @r", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    TotalOldUsers = Double.Parse(result.Rows[0]["Cash"].ToString());
                }
            }
            catch
            {

            }

        }


        private void btnDel_Click(object sender, EventArgs e)
        {
            ViewS viewS = new ViewS();
            viewS.ShowDialog();
        }
        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            ViewP viewP = new ViewP();
            viewP.ShowDialog();
        }
        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            ViewUP viewuP = new ViewUP();
            viewuP.ShowDialog();
        }
        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            ViewUS viewuS = new ViewUS();
            viewuS.ShowDialog();
        }
        private void bunifuFlatButton6_Click(object sender, EventArgs e)
        {
            ViewPaidS viewPS = new ViewPaidS();
            viewPS.ShowDialog();
        }
        private void bunifuFlatButton7_Click(object sender, EventArgs e)
        {
            ViewPaidP viewPP = new ViewPaidP();
            viewPP.ShowDialog();
        }
        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            ViewEX viewEX = new ViewEX(956135252);
            viewEX.ShowDialog();
        }
        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            ViewEX viewEX = new ViewEX(956135253);
            viewEX.ShowDialog();
        }

        private void LoadNewSales()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = y + "-" + m + "-" + "01";
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(totalprice) totalprice from Sells where ddate between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    TotalSell.Text = Login.arNumber(result.Rows[0]["totalprice"].ToString());
                    NewSales = Double.Parse(result.Rows[0]["totalprice"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewPu()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = y + "-" + m + "-" + "01";
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(TotalBuy) TotalBuy from Purchases where Date_ct between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    TotalPu.Text = Login.arNumber(result.Rows[0]["TotalBuy"].ToString());
                    NewPu = Double.Parse(result.Rows[0]["TotalBuy"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewUnPaidSales()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
                string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(unpaid) unpaid from CashInvS where invDateTime between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    UnPaidSell.Text = Login.arNumber(result.Rows[0]["unpaid"].ToString());
                    NewUnPaidSales = Double.Parse(result.Rows[0]["unpaid"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewUnPaidPu()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
                string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(unpaid) unpaid from CashInvD where invDateTime between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    UnPaidPu.Text = Login.arNumber(result.Rows[0]["unpaid"].ToString());
                    NewUnPaidPu = Double.Parse(result.Rows[0]["unpaid"].ToString());
                }

            }
            catch
            {

            }
        }
        private void LoadNewPEadmins()
        {
            var Padmin = PExpense.FindAll(i => i.r_Date.ToString("yyyy-MM") == DateTime.Now.ToString("yyyy-MM") && i.r == 956135252);
            TotalAdmin.Text = Login.arNumber(Padmin.Sum(i => i.Cash).ToString());
            TotalNewAdmin = Padmin.Sum(i => i.Cash);
        }
        private void LoadNewPEusers()
        {
            var Padmin = PExpense.FindAll(i => i.r_Date.ToString("yyyy-MM") == DateTime.Now.ToString("yyyy-MM") && i.r == 956135253);
            TotalUsers.Text = Login.arNumber(Padmin.Sum(i => i.Cash).ToString());
            TotalNewUsers = Padmin.Sum(i => i.Cash);
        }


        private void sumFinal()
        {
            //OLD
            var oSales = OldSales - OldUnPaidSales; // النتيجة الوارد
            var oPu = OldPu - OldUnPaidPu; // النتيجة الصادر
            var FinalOld = oSales - oPu - TotalOldUsers - TotalOldAdmin; // الوارد - الصادر
            //New
            var nSales = NewSales - NewUnPaidSales; // النتيجة الوارد
            var nPu = NewPu - NewUnPaidPu; // النتيجة الصادر
            var FinalNew = nSales - nPu - TotalNewUsers - TotalNewAdmin; // الوارد - الصادر

            //Final
            var thM = DateTime.Now.ToString("yyyy-MM");
            var oM = DateTime.Now.AddMonths(-1).ToString("yyyy-MM");
            if (thM == ACMONTH.ToString("yyyy-MM"))
            {
                var th = nSales - TotalNewUsers - TotalNewAdmin;
                FinalCash.Text = Login.arNumber(th.ToString());
                OldCash.Text = "0";
            }
            else if (ACMONTH.ToString("yyyy-MM") == oM)
            {
                var tlm = nSales - nPu - TotalNewUsers - TotalNewAdmin;
                var olm = oSales - TotalOldUsers - TotalOldAdmin;
                var fm = tlm + olm;
                FinalCash.Text = fm.ToString();
                OldCash.Text = olm.ToString();
            }
            else
            {
                var FinalFianl = FinalOld + FinalNew;
                FinalCash.Text = Login.arNumber(FinalFianl.ToString());
                OldCash.Text = FinalOld.ToString();
            }
        }
        private void UnPaidPCheck()
        {
            var m = DateTime.Now.ToString("MM");
            string y = DateTime.Now.ToString("yyyy");
            string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
            string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);
            try
            {

                var result = DBConnection.SqliteCommand("Select * from PaidPurchases where PaidDate between '" + f + "' and '" + t + "'", dic);
                if (result.Rows.Count > 0)
                {
                    bunifuFlatButton7.Visible = true;
                }
            }
            catch { }
        }
        private void UnPaidSCheck()
        {
            var m = DateTime.Now.ToString("MM");
            string y = DateTime.Now.ToString("yyyy");
            string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
            string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);
            try
            {
                var result = DBConnection.SqliteCommand("Select * from PaidSales where PaidDate between '" + f + "' and '" + t + "'", dic);
                if (result.Rows.Count > 0)
                {
                    bunifuFlatButton6.Visible = true;
                }
            }
            catch { }
        }
        private void FinalCheck()
        {
            if (string.IsNullOrEmpty(TotalSell.Text) || TotalSell.Text == "٠")
            {
                TotalSell.Text = "لا يوجد";
                btnDel.Visible = false;
                label14.Visible = false;
            }
            if (string.IsNullOrEmpty(TotalPu.Text) || TotalPu.Text == "٠")
            {
                TotalPu.Text = "لا يوجد";
                bunifuFlatButton1.Visible = false;
                label15.Visible = false;
            }
            if (string.IsNullOrEmpty(TotalAdmin.Text) || TotalAdmin.Text == "٠")
            {
                TotalAdmin.Text = "لا يوجد";
                bunifuFlatButton2.Visible = false;
                label16.Visible = false;
            }
            if (string.IsNullOrEmpty(TotalUsers.Text) || TotalUsers.Text == "٠")
            {
                TotalUsers.Text = "لا يوجد";
                bunifuFlatButton3.Visible = false;
                label17.Visible = false;
            }
            if (string.IsNullOrEmpty(UnPaidPu.Text) || UnPaidPu.Text == "٠")
            {
                UnPaidPu.Text = "لا يوجد";
                bunifuFlatButton4.Visible = false;
                label21.Visible = false;
            }
            if (string.IsNullOrEmpty(UnPaidSell.Text) || UnPaidSell.Text == "٠")
            {
                UnPaidSell.Text = "لا يوجد";
                bunifuFlatButton5.Visible = false;
                label24.Visible = false;
            }
            if (string.IsNullOrEmpty(OldCash.Text) || OldCash.Text == "0")
            {
                OldCash.Text = "لا يوجد";
                label13.Visible = false;
            }



        }
    }
}

