﻿namespace SGMS
{
    partial class EditItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BarCodeLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p1 = new System.Windows.Forms.PictureBox();
            this.p3 = new System.Windows.Forms.Label();
            this.p2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.iPrice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edit = new Bunifu.Framework.UI.BunifuFlatButton();
            this.iName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SGMS.Properties.Resources.Warning_icon;
            this.pictureBox1.Location = new System.Drawing.Point(205, 213);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 22);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 77;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label6.Location = new System.Drawing.Point(49, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 14);
            this.label6.TabIndex = 76;
            this.label6.Text = "برجاء التواصل مع إدارة البرنامج لتعديله";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(72, 212);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 14);
            this.label7.TabIndex = 75;
            this.label7.Text = "لا يمكنك تغيير باركود المنتج";
            // 
            // BarCodeLabel
            // 
            this.BarCodeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BarCodeLabel.Font = new System.Drawing.Font("Code EAN13", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.BarCodeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.BarCodeLabel.Location = new System.Drawing.Point(35, 117);
            this.BarCodeLabel.Name = "BarCodeLabel";
            this.BarCodeLabel.Size = new System.Drawing.Size(182, 69);
            this.BarCodeLabel.TabIndex = 74;
            this.BarCodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(94, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 19);
            this.label1.TabIndex = 73;
            this.label1.Text = "باركود المنتج";
            // 
            // p1
            // 
            this.p1.Image = global::SGMS.Properties.Resources.Warning_icon;
            this.p1.Location = new System.Drawing.Point(583, 166);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(22, 22);
            this.p1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.p1.TabIndex = 72;
            this.p1.TabStop = false;
            // 
            // p3
            // 
            this.p3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.p3.AutoSize = true;
            this.p3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.p3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.p3.Location = new System.Drawing.Point(323, 170);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(154, 14);
            this.p3.TabIndex = 71;
            this.p3.Text = "برجاء التواصل مع إدارة البرنامج لتعديله";
            // 
            // p2
            // 
            this.p2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.p2.AutoSize = true;
            this.p2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.p2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.p2.Location = new System.Drawing.Point(478, 170);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(101, 14);
            this.p2.TabIndex = 70;
            this.p2.Text = "لا يمكنك تغيير إسم  المنتج";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 7F);
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(615, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 19);
            this.label4.TabIndex = 69;
            this.label4.Text = "سعر البيع";
            // 
            // iPrice
            // 
            this.iPrice.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.iPrice.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iPrice.Location = new System.Drawing.Point(482, 209);
            this.iPrice.Name = "iPrice";
            this.iPrice.Size = new System.Drawing.Size(127, 37);
            this.iPrice.TabIndex = 68;
            this.iPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlynumwithsinglepoint);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 25F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label3.Location = new System.Drawing.Point(248, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(206, 63);
            this.label3.TabIndex = 67;
            this.label3.Text = "تعديل المنتج";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label2.Location = new System.Drawing.Point(615, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 19);
            this.label2.TabIndex = 66;
            this.label2.Text = "إسم المنتج";
            // 
            // edit
            // 
            this.edit.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.edit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.edit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.edit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.edit.BorderRadius = 7;
            this.edit.ButtonText = "تعديل";
            this.edit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.edit.DisabledColor = System.Drawing.Color.Gray;
            this.edit.Font = new System.Drawing.Font("Cairo", 8F);
            this.edit.Iconcolor = System.Drawing.Color.Transparent;
            this.edit.Iconimage = null;
            this.edit.Iconimage_right = null;
            this.edit.Iconimage_right_Selected = null;
            this.edit.Iconimage_Selected = null;
            this.edit.IconMarginLeft = 0;
            this.edit.IconMarginRight = 0;
            this.edit.IconRightVisible = false;
            this.edit.IconRightZoom = 0D;
            this.edit.IconVisible = false;
            this.edit.IconZoom = 20D;
            this.edit.IsTab = false;
            this.edit.Location = new System.Drawing.Point(316, 209);
            this.edit.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.edit.Name = "edit";
            this.edit.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.edit.OnHovercolor = System.Drawing.Color.Blue;
            this.edit.OnHoverTextColor = System.Drawing.Color.White;
            this.edit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.edit.selected = false;
            this.edit.Size = new System.Drawing.Size(127, 37);
            this.edit.TabIndex = 64;
            this.edit.Text = "تعديل";
            this.edit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.edit.Textcolor = System.Drawing.Color.White;
            this.edit.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // iName
            // 
            this.iName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.iName.Enabled = false;
            this.iName.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iName.Location = new System.Drawing.Point(316, 125);
            this.iName.Name = "iName";
            this.iName.Size = new System.Drawing.Size(293, 37);
            this.iName.TabIndex = 65;
            this.iName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EditItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 283);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BarCodeLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.p1);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.iPrice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edit);
            this.Controls.Add(this.iName);
            this.Name = "EditItem";
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Shown += new System.EventHandler(this.EditItem_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label BarCodeLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox p1;
        private System.Windows.Forms.Label p3;
        private System.Windows.Forms.Label p2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox iPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuFlatButton edit;
        private System.Windows.Forms.TextBox iName;
    }
}