﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using System.IO;
using System.Reflection;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.IO.Compression;
using System.Net.NetworkInformation;
using System.Diagnostics;

namespace SGMS
{
    public partial class Settings : UserControl
    {
        private Dashboard mn1 = null;
        string phonee, USR, PASS, SNM, FILEN,UR;
        string nStoreName, Pass;
        string nStoreLogo;
        string MediaName, nPrintLogo, nPrintCoName, nPrintCoSlu, nPrintCoAddress, nPrintCoPhone;
        string version;
        int CID;
        cMessgeBox cmbox;
        public Settings(Dashboard mn)
        {
            InitializeComponent();
            mn1 = mn;
            Roles();
            version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            label24.Text = Login.arNumber(String.Format("{0}", version));
            NowNotes.SelectionAlignment = HorizontalAlignment.Center;
            CheckNotes();
            checkInternet();
        }
        private static List<Setting> SettingP = new List<Setting>();
        private static List<User> UsersP = new List<User>();
        private static List<UpdateVersion> UpdateApp = new List<UpdateVersion>();
        private static List<DE> UDE = new List<DE>();

        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            checkFav();
            LoadPSetting();
            CheckMedia();
            LoadUsers();
            FindAll();
        }
        private void UpdateI()
        {
            mn1.LoadStoreInfo();
            SettingP.Clear();
            LoadPSetting();

        }

        private void Roles()
        {
            if (Login.r21 == "0")
            {
                tabControl1.TabPages.Remove(tabPage1);
            }

            if (Login.r22 == "0")
            {
                tabControl1.TabPages.Remove(tabPage2);
            }

            if (Login.r23 == "0")
            {
                tabControl1.TabPages.Remove(tabPage3);
            }
        }

        // إعدادات البرنامج العامة
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                dic.Add("@fav", 1);
                var FavBar = DBConnection.SqliteCommand("update FavBar Set Fav=@fav where id=@id", dic);
            }
            else
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                dic.Add("@fav", 2);
                var FavBar = DBConnection.SqliteCommand("update FavBar Set Fav=@fav where id=@id", dic);
            }
        }
        private void checkFav()
        {
            Dictionary<string, object> favPr = new Dictionary<string, object>();
            favPr.Add("@r", 1);
            var Favs = DBConnection.SqliteCommand("select * from FavBar where id = @r", favPr).ConvertFavBar();
            if (Favs.Count > 0)
            {
                int FavStatus = Favs[0].Fav;
                if (FavStatus == 1)
                {
                    checkBox1.Checked = true;
                }
                else
                {
                    checkBox1.Checked = false;
                }
            }

        }
        private void CheckMedia()
        {
            if (MediaName == "usrData/002.mp3")
            {
                button1.Text = "إختيار ملف";

            }
            else
            {
                button1.Text = "إستعادة";
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "إختيار ملف")
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Title = "أختيار الملف الموسيقي ";
                    dlg.Filter = "MP3 files (*.mp3)|*.mp3";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        var fileName = dlg.FileName;
                        System.IO.File.Copy(dlg.FileName, "usrData/MediaFile.mp3", true);

                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@MediaName", "usrData/MediaFile.mp3");
                        addC.Add("@id", 1);
                        var UpdateCu = DBConnection.SqliteCommand("update Settings Set MediaName = @MediaName where id=@id", addC);
                        UpdateI();
                        cmbox = new cMessgeBox("تم تحديث البيانات", "done", "p", 1000);
                        cmbox.ShowDialog();
                    }
                }
            }
            else
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@MediaName", "usrData/002.mp3");
                addC.Add("@id", 1);
                var UpdateCu = DBConnection.SqliteCommand("update Settings Set MediaName = @MediaName where id=@id", addC);
                UpdateI();
            }
        }

        // إعدادات البرنامج
        private void LoadPSetting()
        {

            try
            {
                SettingP.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id =@id", dic);
                if (result.Rows.Count > 0)
                {
                    foreach (DataRow item in result.Rows)
                    {
                        Setting c = new Setting();
                        c.id = int.Parse(item["id"].ToString());
                        c.StoreName = item["StoreName"].ToString();
                        c.StoreLogo = item["StoreLogo"].ToString();
                        c.MediaName = item["MediaName"].ToString();
                        c.PrintLogo = item["PrintLogo"].ToString();
                        c.PrintCoName = item["PrintCoName"].ToString();
                        c.PrintCoSlu = item["PrintCoSlu"].ToString();
                        c.PrintCoAddress = item["PrintCoAddress"].ToString();
                        c.PrintCoPhone = item["PrintCoPhone"].ToString();
                        SettingP.Add(c);
                    }
                }

                int nid = 1;
                var cup = SettingP.Find(i => i.id == nid);
                nStoreLogo = cup.StoreLogo.ToString();
                MediaName = cup.MediaName.ToString();
                nPrintLogo = cup.PrintLogo.ToString();

                nStoreName = cup.StoreName.ToString();
                pCoName.Text = nStoreName;

                nPrintCoName = cup.PrintCoName.ToString();
                textBox2.Text = nPrintCoName;

                nPrintCoSlu = cup.PrintCoSlu.ToString();
                textBox3.Text = nPrintCoSlu;

                nPrintCoAddress = cup.PrintCoAddress.ToString();
                textBox6.Text = nPrintCoAddress;

                nPrintCoPhone = cup.PrintCoPhone.ToString().Trim();
                var ccc = nPrintCoPhone.Split('-');
                textBox5.Text = ccc[0];
                textBox4.Text = ccc[1];
            }
            catch { }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> addC = new Dictionary<string, object>();
            addC.Add("@StoreName", pCoName.Text.ToString());
            addC.Add("@id", 1);
            var UpdateCu = DBConnection.SqliteCommand("update Settings Set StoreName = @StoreName where id=@id", addC);
            UpdateI();
            cmbox = new cMessgeBox("تم تحديث البيانات", "done", "p", 1000);
            cmbox.ShowDialog();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "أختيار شعار الشركة ";
                dlg.Filter = "png files (*.png)|*.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    var fileName = dlg.FileName;
                    System.IO.File.Copy(dlg.FileName, "usrData/usrLogo.png", true);


                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@StoreLogo", "usrData/usrLogo.png");
                    addC.Add("@id", 1);
                    var UpdateCu = DBConnection.SqliteCommand("update Settings Set StoreLogo = @StoreLogo where id=@id", addC);
                    UpdateI();
                    cmbox = new cMessgeBox("تم تحديث البيانات", "done", "p", 1000);
                    cmbox.ShowDialog();
                }
            }
        }

        // إعدادات الطباعة
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Title = "أختيار شعار الشركة للطباعة ";
                    dlg.Filter = "png files (*.png)|*.png";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(dlg.FileName);
                        Bitmap img = new Bitmap(dlg.FileName);


                        if (img.Width <= 201 && img.Height <= 201)
                        {
                            var fileName = dlg.FileName;
                            System.IO.File.Copy(dlg.FileName, "usrData/invLogo.png", true);
                            cmbox = new cMessgeBox("تم تحديث البيانات", "done", "p", 1000);
                            cmbox.ShowDialog();
                        }
                        else
                        {
                            MessageBox.Show("200x200 اقصي حجم صورة مسموح به هو");
                        }


                    }
                }
                UpdateI();
            }
            catch { }

        }
        private void button4_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(textBox5.Text))
            {
                phonee = textBox4.Text;
            }
            else if (string.IsNullOrWhiteSpace(textBox4.Text))
            {
                phonee = textBox5.Text;
            }
            else if (string.IsNullOrWhiteSpace(textBox5.Text) && string.IsNullOrWhiteSpace(textBox4.Text))
            {
                MessageBox.Show("لا يمكن ترك بيانات فارغة");
            }
            else if (!string.IsNullOrWhiteSpace(textBox5.Text) && !string.IsNullOrWhiteSpace(textBox4.Text))
            {
                phonee = textBox5.Text + " " + "-" + " " + textBox4.Text;
            }
            Dictionary<string, object> addC = new Dictionary<string, object>();
            addC.Add("@PrintCoName", textBox2.Text.ToString());
            addC.Add("@PrintCoSlu", textBox3.Text.ToString());
            addC.Add("@PrintCoAddress", textBox6.Text.ToString());
            addC.Add("@PrintCoPhone", phonee);
            addC.Add("@id", 1);
            var UpdateCu = DBConnection.SqliteCommand("update Settings Set PrintCoName = @PrintCoName, PrintCoSlu = @PrintCoSlu, PrintCoAddress = @PrintCoAddress, PrintCoPhone = @PrintCoPhone where id=@id", addC);
            UpdateI();
            cmbox = new cMessgeBox("تم تحديث البيانات", "done", "p", 1000);
            cmbox.ShowDialog();
        }




        private void checkInternet()
        {
            try
            {
                if (NetworkInterface.GetIsNetworkAvailable() &&
                    new Ping().Send(new IPAddress(new byte[] { 8, 8, 8, 8 }), 2000).Status == IPStatus.Success)
                {
                    NewVersion();
                    FillBackup();
                    label20.Visible = false;
                    comboBox1.Enabled = true;
                    button6.Enabled = true;
                    button9.Visible = false;
                }
            }
            catch { }

        }


        // إعدادات النسخة السحابية
        private async void FTPD()
        {
            try
            {
                SNMC();
                var USERD = API.u("user?" + "SN=" + SNM);
                if(await USERD == "error")
                {
                    cmbox = new cMessgeBox("حدث خطأ في جلب بيانات النسخة السحابية", "error", "p", 1000);
                    cmbox.ShowDialog();
                }
                else
                {
                    UDE = JsonConvert.DeserializeObject<List<DE>>(await USERD);
                    var x = UDE.Find(i => i.SN == SNM);
                    USR = x.USER;
                    PASS = x.FTP;
                }
            }
            catch { }
        }
        private void SNMC()
        {
            Dictionary<string, object> NCM = new Dictionary<string, object>();
            NCM.Add("@r", 1);
            var N = DBConnection.SqliteCommand("select * from Activition where id = @r", NCM);
            if (N.Rows.Count > 0)
            {
                SNM = N.Rows[0]["sn1"].ToString(); ;
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = "System.Silver";
                string newName = "Upload.Silver";
                string sourcePath = "";
                string targetPath = "";
                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                string destFile = System.IO.Path.Combine(targetPath, newName);
                System.IO.File.Copy(sourceFile, destFile, true);

                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcePath);

                    foreach (string s in files)
                    {
                        fileName = System.IO.Path.GetFileName(s);
                        destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }

                var fname = DateTime.Now.ToString("yyy-MM-dd") + "-" + DateTime.Now.ToString("HH") + "-" + DateTime.Now.ToString("mm") + "-" + DateTime.Now.ToString("ss");
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("{0}{1}", "ftp://" + Program.FTPS + "/", fname + ".Silver")));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(USR + Program.FTP, PASS);
                Stream ftpStream = request.GetRequestStream();
                FileStream fs = File.OpenRead("Upload.Silver");

                using (Stream fileStream = fs)
                {
                    fileStream.CopyTo(ftpStream);
                    fs.Close();
                    ftpStream.Close();
                    File.Delete("Upload.Silver");
                    cmbox = new cMessgeBox("تم إنشاء النسخة السحابية بنجاح", "done", "p", 1000);
                    cmbox.ShowDialog();
                    FillBackup();
                }
            }
            catch { }

        }
        private void FillBackup()
        {
            try
            {
                FTPD();
                comboBox1.Items.Clear();
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + Program.FTPS + "/");
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(USR + Program.FTP, PASS);

                comboBox1.BeginUpdate();
                try
                {
                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        while (!reader.EndOfStream)
                        {
                            comboBox1.Items.Add(reader.ReadLine());
                        }
                    }
                }
                finally
                {
                    comboBox1.EndUpdate();
                }
            }
            catch { }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "." || comboBox1.Text == "..")
            {
                FILEN = "";
                button7.Enabled = false;
            }
            else
            {
                FILEN = comboBox1.Text;
                button7.Enabled = true;
            }

        }
        private void button9_Click(object sender, EventArgs e)
        {
            checkInternet();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            var url = "ftp://" + Program.FTPS + "/";
            var USER = USR + Program.FTP;
            var PASSW = PASS;
            var FILN = comboBox1.Text;
            System.Diagnostics.Process.Start("RestoreDB.exe", url + " " + USER + " " + PASSW + " " + FILN);
            Application.Exit();
        }
        // التحديثات
        private async void NewVersion()
        {
            try
            {
                var checkUPDATE = API.u("update");
                UpdateApp = JsonConvert.DeserializeObject<List<UpdateVersion>>(await checkUPDATE);
                var x = UpdateApp.Find(i => i.id == 1);
                label26.Text = Login.arNumber(x.v);
                if (version != x.v)
                {
                    button8.Enabled = true;
                    button8.Visible = true;
                }
                else
                {
                    button8.Enabled = false;
                    button8.Visible = false;
                }
            }
            catch { }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            var x = UpdateApp.Find(i => i.id == 1);
            var url = x.url;
            var filename = x.filename;
            var version = x.v;
            Process.Start("updater.exe", url + " " + filename + " " + version);
            Application.Exit();
        }

        // المستخدمين
        public static string ConvertState(string x)
        {
            string text = x.Replace("1", "مستخدم فعال");
            return text.Replace("0", "مستخدم محظور");
        }
        private void LoadUsers()
        {
            try
            {
                UsersP.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var result = DBConnection.SqliteCommand("SELECT *  FROM Login", dic);
                if (result.Rows.Count > 0)
                {

                    CustomerN.DataSource = result;
                    CustomerN.DisplayMember = "fullname";
                    CustomerN.ValueMember = "uid";
                    CustomerN.SelectedIndex = -1;

                    foreach (DataRow item in result.Rows)
                    {
                        User u = new User();
                        u.uid = int.Parse(item["uid"].ToString());
                        u.username = item["username"].ToString();
                        u.fullname = item["fullname"].ToString();
                        u.password = item["password"].ToString();
                        u.Cash = double.Parse(item["Cash"].ToString());
                        u.BlockState = int.Parse(item["BlockState"].ToString());
                        UsersP.Add(u);
                    }
                }
                Loadme();
            }
            catch { }
        }
        private void Loadme()
        {
            var uu = UsersP.Find(u => u.uid == Login.cid);
            Pass = uu.password;
        }

        private void FindAll()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var ui = UsersP.FindAll(u => u.uid > 0);

                foreach (var uu in ui)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = uu.fullname;
                    row.Cells[1].Value = uu.username;
                    row.Cells[2].Value = uu.Cash;
                    row.Cells[3].Value = ConvertState(uu.BlockState.ToString());
                    row.Cells[4].Value = uu.uid;
                    dataGridView1.Rows.Add(row);
                }
            }
            catch { }
        }



        private void CustomerByP_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(CustomerByP.Text))
            {
                FindAll();
            }
            else
            {
                try
                {
                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    var uu = UsersP.Find(u => u.username == CustomerByP.Text);
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = uu.fullname;
                    row.Cells[1].Value = uu.username;
                    row.Cells[2].Value = uu.Cash;
                    row.Cells[3].Value = ConvertState(uu.BlockState.ToString());
                    row.Cells[4].Value = uu.uid;
                    dataGridView1.Rows.Add(row);

                }
                catch { }

            }
        }
        private void CustomerN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CustomerN.SelectedIndex == -1)
            {
                FindAll();
            }
            else
            {
                try
                {
                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    var us = int.Parse(CustomerN.SelectedValue.ToString());
                    var uu = UsersP.Find(u => u.uid == us);
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = uu.fullname;
                    row.Cells[1].Value = uu.username;
                    row.Cells[2].Value = uu.Cash;
                    row.Cells[3].Value = ConvertState(uu.BlockState.ToString());
                    row.Cells[4].Value = uu.uid;
                    dataGridView1.Rows.Add(row);
                }
                catch { }

            }
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {
                        CID = int.Parse(row.Cells[4].Value.ToString());
                        editC.Enabled = true;
                        BlockU.Enabled = true;
                    }
                    else
                    {
                        CID = 0;
                        editC.Enabled = false;
                        BlockU.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void BlockU_Click(object sender, EventArgs e)
        {
            string value = "";
            if (cRole.InputBox("تأكيد صلاحياتك", "ادخل كلمة المرور الخاصة بك", ref value) == DialogResult.OK)
            {
                if (value == Pass)
                {
                    try
                    {
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@BlockState", 0);
                        addC.Add("@Blocker", Login.cnn);
                        addC.Add("@uid", CID);
                        var UpdateCu = DBConnection.SqliteCommand("update Login Set BlockState = @BlockState, Blocker = @Blocker where uid=@uid", addC);
                        MessageBox.Show("تم حظر المستخدم بنجاح");
                        LoadUsers();
                        FindAll();
                    }
                    catch { }
                }
                else
                {
                    MessageBox.Show("كلمة المرور خاطئة");
                }
            }
        }
        private void editC_Click(object sender, EventArgs e)
        {
            string value = "";
            if (cRole.InputBox("تأكيد صلاحياتك", "ادخل كلمة المرور الخاصة بك", ref value) == DialogResult.OK)
            {
                if (value == Pass)
                {
                    EditLogin editU = new EditLogin(CID);
                    editU.FormClosed += AddUClosed;
                    editU.ShowDialog();
                }
                else
                {
                    MessageBox.Show("كلمة المرور خاطئة");
                }
            }
        }
        private void AddNew_Click(object sender, EventArgs e)
        {
            string value = "";
            if (cRole.InputBox("تأكيد صلاحياتك", "ادخل كلمة المرور الخاصة بك", ref value) == DialogResult.OK)
            {
                if (value == Pass)
                {
                    AddLogin addU = new AddLogin();
                    addU.FormClosed += AddUClosed;
                    addU.ShowDialog();
                }
                else
                {
                    MessageBox.Show("كلمة المرور خاطئة");
                }
            }
        }
        void AddUClosed(object sender, FormClosedEventArgs e)
        {
            LoadUsers();
            FindAll();
        }

        // الملاحظات الإدارية
        private void CheckNotes()
        {
            Dictionary<string, object> prams = new Dictionary<string, object>();
            prams.Add("@r", 1);
            var Note = DBConnection.SqliteCommand("select * from AdminNotes where r=@r", prams).ConvertNotes();
            if (Note.Count > 0)
            {
                int idNote = Note[0].id;
                string aNote = Note[0].Note;
                string aUser = Note[0].User;
                NowNotes.Text = aNote;
                addName.Text = aUser;
                if (idNote == 0)
                {

                    ShowHide.LabelText = "عرض الملاحظة";
                    ShowHide.color = Color.DarkGreen;
                    ShowHide.BackColor = Color.DarkGreen;
                    ShowHide.colorActive = Color.Green;
                    ShowHide.Image = Properties.Resources.visible_60px;
                }
                else if (idNote == 1)
                {
                    ShowHide.LabelText = "إخفاء الملاحظة";
                    ShowHide.color = Color.Maroon;
                    ShowHide.BackColor = Color.Maroon;
                    ShowHide.colorActive = Color.FromArgb(192, 0, 0);
                    ShowHide.Image = global::SGMS.Properties.Resources.shutdown_52px;
                }

            }
        }
        private void ShowHide_Click(object sender, EventArgs e)
        {
            if (ShowHide.LabelText == "عرض الملاحظة")
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                var HIDE = DBConnection.SqliteCommand("update AdminNotes Set  id = @id where r = 1", dic);
                cMessgeBox mess = new cMessgeBox("تم إعادة عرض الملاحظة الإدارية بنجاح", "done", "p", 1000);
                mess.ShowDialog();
                mn1.isNotes();
                ((Label)mn1.Controls.Find("AdminNotes", true)[0]).Visible = true;
                CheckNotes();
            }
            else if (ShowHide.LabelText == "إخفاء الملاحظة")
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 0);
                var HIDE = DBConnection.SqliteCommand("update AdminNotes Set  id = @id where r = 1", dic);
                cMessgeBox mess = new cMessgeBox("تم إخفاء الملاحظة الإدارية بنجاح", "done", "p", 1000);
                mess.ShowDialog();
                ((Label)mn1.Controls.Find("AdminNotes", true)[0]).Visible = false;
                CheckNotes();
            }

        }
        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            dic.Add("@Note", NowNotes.Text);
            dic.Add("@User", Login.cnn.ToString());
            dic.Add("@ct_at", DateTime.Now.ToString("yyyy-MM-dd"));
            dic.Add("@ti_at", DateTime.Now.ToString("HH:mm:ss"));
            var EDIT = DBConnection.SqliteCommand("update AdminNotes Set id =@id, Note = @Note , User=@User, ct_at=@ct_at, ti_at=@ti_at where r = 1", dic);
            cMessgeBox mess = new cMessgeBox("تم تعديل الملاحظة الإدارية بنجاح", "done", "p", 1000);
            mess.ShowDialog();
            CheckNotes();
            mn1.isNotes();
            ((Label)mn1.Controls.Find("AdminNotes", true)[0]).Visible = true;
        }
    }
}







