﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;

namespace SGMS
{
    public partial class FastAddItem : Form
    {
        DataTable x1;
        DataTable x2;
        string Barrcode, Check12Digits;

        public FastAddItem()
        {
            InitializeComponent();
            GenerateBarCode();

        }

        private void insertBtn_Click(object sender, EventArgs e)
        {

            var BrCode = itemBarcode.Text;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@BarCode", BrCode);
            x1 = DBConnection.SqliteCommand("SELECT *  FROM Items where BarCode = @BarCode", dic);
            if (x1.Rows.Count > 0)
            {
                MessageBox.Show("يوجد منتج مطابق لهذا الباركود تم تحديثة برجاء الإضافة مره اخري " + " " + ":" + " " + BrCode);
                GenerateBarCode();
                return;
            }
            var IName = itemName.Text;
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2.Add("@Name", IName);
            x2 = DBConnection.SqliteCommand("SELECT *  FROM Items where Name = @Name", dic2);
            if (x2.Rows.Count > 0)
            {
                MessageBox.Show("يوجد منتج مطابق لهذا الإسم" + " " + ":" + " " + IName);
                itemName.SelectAll();
                itemName.Focus();
                return;
            }

            if (x1.Rows.Count <= 0 && x2.Rows.Count <= 0)
            {
                //
                if (ItemsDatagride.Rows.Count > 0)
                {

                    foreach (DataGridViewRow drow in ItemsDatagride.Rows)
                    {

                        if (Convert.ToString(drow.Cells[0].Value) == itemBarcode.Text)
                        {
                            MessageBox.Show("يوجد منتج مطابق لهذا الباركود تم تحديثة برجاء الإضافة مره اخري" + " " + ":" + " " + BrCode + " " + "في قائمة إضافة المنتجات الحالية");
                            GenerateBarCode();
                            return;
                        }
                        else if (Convert.ToString(drow.Cells[1].Value) == itemName.Text)
                        {
                            MessageBox.Show("يوجد منتج مطابق لهذا الإسم" + " " + ":" + " " + IName + " " + "في قائمة إضافة المنتجات الحالية");
                            itemName.SelectAll();
                            itemName.Focus();
                            return;
                        }
                    }
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(ItemsDatagride);
                    row.Cells[0].Value = itemBarcode.Text;
                    row.Cells[1].Value = itemName.Text;
                    ItemsDatagride.Rows.Add(row);
                    ItemsDatagride.CurrentCell = null;
                    GenerateBarCode();
                    itemName.Text = "";
                    itemName.Focus();
                }
                else if (ItemsDatagride.Rows.Count <= 0)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(ItemsDatagride);
                    row.Cells[0].Value = itemBarcode.Text;
                    row.Cells[1].Value = itemName.Text;
                    ItemsDatagride.Rows.Add(row);
                    ItemsDatagride.CurrentCell = null;
                    GenerateBarCode();
                    itemName.Text = "";
                    itemName.Focus();
                }
            }
        }
        private void addFastItems_Click(object sender, EventArgs e)
        {
            AddToItems();
        }
        private void AddToItems()
        {
            try
            {
                for (int i = 0; i < ItemsDatagride.Rows.Count; i++)
                {
                    var BarCode = ItemsDatagride.Rows[i].Cells["BarCode11"].Value.ToString();
                    var iName = ItemsDatagride.Rows[i].Cells["ItemiName"].Value.ToString();
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@BarCode", BarCode);
                    addC.Add("@Name", iName);
                    var addToItems = DBConnection.SqliteCommand("INSERT  INTO `Items`(`BarCode`, `Name`) values(@BarCode,@Name)", addC);

                }
                cMessgeBox mess = new cMessgeBox("تمت إضافة المنتجات بنجاح", "done", "p", 1000);
                mess.ShowDialog();
                this.Close();
            }
            catch
            {
            }
        }

        private void itemBarcode_TextChanged(object sender, EventArgs e)
        {
            if (itemBarcode.TextLength > 0 && itemName.TextLength > 0)
            {
                insertBtn.Enabled = true;
            }
            else
            {
                insertBtn.Enabled = false;
            }
        }

        private void ItemsDatagride_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (ItemsDatagride.Rows.Count >= 1)
            {
                addFastItems.Enabled = true;
                btnDel.Enabled = true;
            }
            else
            {
                addFastItems.Enabled = false;
                btnDel.Enabled = false;
            }
        }

        private void ItemsDatagride_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (ItemsDatagride.Rows.Count >= 1)
            {
                addFastItems.Enabled = true;
                btnDel.Enabled = true;
            }
            else
            {
                addFastItems.Enabled = false;
                btnDel.Enabled = false;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (ItemsDatagride.SelectedRows.Count >= 1)
            {
                foreach (DataGridViewRow row in ItemsDatagride.SelectedRows)
                {
                    ItemsDatagride.Rows.RemoveAt(row.Index);
                }
            }
            else
            {
                MessageBox.Show("برجاء تحديد عنصر للحذف");
            }
        }
        private void GenerateBarCode()
        {
            itemBarcode.Text = GetNextInt64().ToString();
            Check12Digits = itemBarcode.Text.PadRight(12, '0');
            Barrcode = EAN13Class.EAN13(Check12Digits);
            itemBarcode.Text = Check12Digits;
            if (!String.Equals(EAN13Class.Barcode13Digits, "") || (EAN13Class.Barcode13Digits != ""))
            {
                itemBarcode.Text = EAN13Class.Barcode13Digits.ToString();
            }
        }
        public long GetNextInt64()
        {
            var bytes = new byte[sizeof(Int64)];
            RNGCryptoServiceProvider Gen = new RNGCryptoServiceProvider();
            Gen.GetBytes(bytes);

            long random = BitConverter.ToInt64(bytes, 0);

            //Remove any possible negative generator numbers and shorten the generated number to 12-digits
            string pos = random.ToString().Replace("-", "").Substring(0, 12);
            return Convert.ToInt64(pos);
        }

    }
}
