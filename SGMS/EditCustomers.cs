﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;

namespace SGMS
{

    public partial class EditCustomers : MetroFramework.Forms.MetroForm
    {
        string cid;
        string cname;
        string cphone;
        string caddress;
        cMessgeBox cmbox;
        public EditCustomers(string xid, string xname, string xphone, string xaddress)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            cid = xid;
            cname = xname;
            cphone = xphone;
            caddress = xaddress;
            LoadList();

        }
        private static List<Customer> CList = new List<Customer>();
        private void LoadList()
        {
            CList.Clear();
            try
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 1);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT * FROM  Customers where id > @r", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    for (int i = 0; i < rIt.Rows.Count; i++)
                    {
                        var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                        var _Name = rIt.Rows[i]["Name"].ToString();
                        var _Phone = rIt.Rows[i]["Phone"].ToString();
                        var _Cash = rIt.Rows[i]["Cash"].ToString();
                        var _Address = rIt.Rows[i]["Address"].ToString();
                        var p = new Customer
                        {
                            id = _id,
                            Name = _Name,
                            Phone = _Phone,
                            Address = _Address,
                            Cash = Double.Parse(_Cash),
                        };
                        CList.Add(p);
                    }

                }
                FillData();
            }
            catch { }
        }
        private void updateclient()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@Name", name.Text);
                addC.Add("@phone", phone.Text);
                addC.Add("@Address", Address.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير البيانات بسبب :" + " " + tReason.Text);
                addC.Add("@From1", cphone);
                addC.Add("@To1", phone.Text);
                addC.Add("@From2", cname);
                addC.Add("@To2", name.Text);
                addC.Add("@From3", caddress);
                addC.Add("@To3", Address.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdatePU = DBConnection.SqliteCommand("update Sells Set sellfor = @Name where sellforid=@cID", addC);
                var UpdatePP = DBConnection.SqliteCommand("update PaidSales Set ClientName = @Name where ClientID=@cID", addC);
                var UpdateCI = DBConnection.SqliteCommand("update CashInvS Set clientName = @Name where clientID=@cID", addC);
                var UpdateCL = DBConnection.SqliteCommand("update Customers Set Name = @Name , Phone = @phone , Address = @Address where id=@cID", addC);
                var insertHistory1 = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From1,@To1,@EditDate)", addC);
                var insertHistory2 = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From2,@To2,@EditDate)", addC);
                var insertHistory3 = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From3,@To3,@EditDate)", addC);

                CList.Clear();
            }
            catch { }
        }
        private void updatename()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@Name", name.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير الإسم بسبب :" + " " + tReason.Text);
                addC.Add("@From", cname);
                addC.Add("@To", name.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdatePU = DBConnection.SqliteCommand("update Sells Set sellfor = @Name where sellforid=@cID", addC);
                var UpdatePP = DBConnection.SqliteCommand("update PaidSales Set ClientName = @Name where ClientID=@cID", addC);
                var UpdateCI = DBConnection.SqliteCommand("update CashInvS Set clientName = @Name where clientID=@cID", addC);
                var UpdateCL = DBConnection.SqliteCommand("update Customers Set Name = @Name where id=@cID", addC);
                var insertHistory = DBConnection.SqliteCommand("INSERT  INTO `EditCUHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From,@To,@EditDate)", addC);
                CList.Clear();
            }
            catch { }
        }
        private void updatephone()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@phone", phone.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير الرقم بسبب :" + " " + tReason.Text);
                addC.Add("@From", cphone);
                addC.Add("@To", phone.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdateCu = DBConnection.SqliteCommand("update Customers Set Phone = @phone where id=@cID", addC);
                var insertHistory = DBConnection.SqliteCommand("INSERT  INTO `EditCUHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From,@To,@EditDate)", addC);
                CList.Clear();
            }
            catch { }

        }
        private void updatephoneaddress()
        {
            updatephone();
            updateaddress();
        }
        private void updatephonename()
        {
            updatephone();
            updatename();
        }
        private void updatenameaddress()
        {
            updatename();
            updateaddress();
        }
        private void updateaddress()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@Address", Address.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير العنوان بسبب :" + " " + tReason.Text);
                addC.Add("@From", caddress);
                addC.Add("@To", Address.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdateCu = DBConnection.SqliteCommand("update Customers Set Address = @Address where id=@cID", addC);
                var insertHistory = DBConnection.SqliteCommand("INSERT  INTO `EditCUHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From,@To,@EditDate)", addC);
                CList.Clear();
            }
            catch { }

        }
        private void add_Click(object sender, EventArgs e)
        {
            if (name.Text == cname && phone.Text == cphone && Address.Text == caddress)
            {
                cmbox = new cMessgeBox("لم تقوم بتغيير اي بيانات للعميل", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else if (string.IsNullOrWhiteSpace(name.Text) || string.IsNullOrWhiteSpace(phone.Text) || string.IsNullOrWhiteSpace(tReason.Text) || string.IsNullOrWhiteSpace(Address.Text))
            {
                cmbox = new cMessgeBox("لا يمكنك ترك بيانات فارغة", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else if (name.Text == cname && Address.Text == caddress && phone.Text != cphone)
            {
                try
                {
                    var phone1 = phone.Text.ToString();
                    var cup = CList.Find(i => i.Phone == phone1);
                    if (cup == null)
                    {
                        updatephone();
                        cmbox = new cMessgeBox("تم تعديل رقم العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        var cpn = cup.Name;
                        var cppp = cup.Phone;
                        cmbox = new cMessgeBox("العميل" + " " + cpn + " " + "مسجل بنفس الرقم", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                }
                catch
                {

                }
            }
            else if (phone.Text == cphone && Address.Text == caddress && name.Text != cname)
            {
                try
                {
                    var Name1 = name.Text.ToString();
                    var cup = CList.Find(i => i.Name == Name1);

                    if (cup == null)
                    {
                        updatename();
                        cmbox = new cMessgeBox("تم تعديل إسم العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();

                    }
                    else
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بالفعل", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                }
                catch { }
            }
            else if (phone.Text == cphone && Address.Text != caddress && name.Text == cname)
            {
                try
                {
                    var Name1 = Address.Text.ToString();
                    var cup = CList.Find(i => i.Address == Name1);

                    if (cup == null)
                    {
                        updateaddress();
                        cmbox = new cMessgeBox("تم تعديل عنوان العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();

                    }
                    else
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بهذا العنوان", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                }
                catch { }
            }

            else if (phone.Text != cphone && Address.Text != caddress && name.Text == cname)
            {
                try
                {
                    var Address2 = Address.Text.ToString();
                    var Phone2 = phone.Text.ToString();
                    var cup = CList.Find(i => i.Address == Address2);
                    var cup2 = CList.Find(i => i.Phone == Phone2);
                    if (cup != null)
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بهذا العنوان", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else if(cup2 != null)
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بنفس الرقم", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else
                    {
                        updatephoneaddress();
                        cmbox = new cMessgeBox("تم تعديل عنوان ورقم العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                }
                catch { }
            }
            else if (phone.Text == cphone && Address.Text != caddress && name.Text != cname)
            {
                try
                {
                    var Address2 = Address.Text.ToString();
                    var Name2 = name.Text.ToString();
                    var cup = CList.Find(i => i.Address == Address2);
                    var cup2 = CList.Find(i => i.Name == Name2);
                    if (cup != null)
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بهذا العنوان", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else if (cup2 != null)
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بالفعل", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else
                    {
                        updatenameaddress();
                        cmbox = new cMessgeBox("تم تعديل إسم و عنوان العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                }
                catch { }
            }
            else if (phone.Text != cphone && Address.Text == caddress && name.Text != cname)
            {
                try
                {
                    var Name2 = name.Text.ToString();
                    var Phone2 = phone.Text.ToString();
                    var cup = CList.Find(i => i.Name == Name2);
                    var cup2 = CList.Find(i => i.Phone == Phone2);
                    if (cup != null)
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بالفعل", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else if (cup2 != null)
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + cpn + " " + "مسجل بنفس الرقم", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else
                    {
                        updatephonename();
                        cmbox = new cMessgeBox("تم تعديل إسم ورقم العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                }
                catch { }
            }

            else
            {
                try
                {
                    var name1 = name.Text.ToString();
                    var phone1 = phone.Text.ToString();
                    var address1 = Address.Text.ToString();
                    var cup = CList.Find(i => i.Name == name1);
                    var cpp = CList.Find(i => i.Phone == phone1);
                    var cpa = CList.Find(i => i.Address == address1);
                    if (cup != null)
                    {
                        cmbox = new cMessgeBox("يوجد عميل بإسم" + " " + name1 + " " + "مسجل بالفعل", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else if (cpp != null)
                    {
                        var cpn = cpp.Name;
                        cmbox = new cMessgeBox("العميل" + " " + cpn + " " + "مسجل بنفس الرقم", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else if (cpa != null)
                    {
                        var cpn = cpp.Name;
                        cmbox = new cMessgeBox("العميل" + " " + cpn + " " + "مسجل بنفس العنوان", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else
                    {
                        updateclient();
                        cmbox = new cMessgeBox("تم تعديل بيانات العميل بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                }
                catch { }

            }
        }
        private void FillData()
        {
            name.Text = cname;
            phone.Text = cphone;
            Address.Text = caddress;
            var tid = int.Parse(cid);
            var ccid = CList.Find(i => i.id == tid);
            var cashe = ccid.Cash;
            if (cashe > 0)
            {
                name.Enabled = false;
                p1.Visible = true;
                p2.Visible = true;
                p3.Visible = true;
            }
            else
            {
                name.Enabled = true;
                p1.Visible = false;
                p2.Visible = false;
                p3.Visible = false;
            }
        }
    }
}

