﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class ViewClientInvs : MetroFramework.Forms.MetroForm
    {
        string ClientID;
        string ClientName;
        string ClientPhone;
        string InvNum;
        double cashed;
        double inunpaid1;
        double invpaid1;
        public ViewClientInvs(string ID, string Name, string Phone)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            ClientID = ID;
            ClientPhone = Phone;
            ClientName = Name;

            LoadUnPaided();
        }

        private static List<CashInvD> CPinv = new List<CashInvD>();
        private void LoadUnPaided()
        {
            invClient.Text = ClientName;
            label4.Text = ClientPhone;
            CPinv.Clear();
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();

            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", ClientID);
                var cIDCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where clientID=@id", dic).ConvertCashInvoice();
                if (cIDCash == null) return;

                foreach (CashInvD item in cIDCash)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item.invNum;
                    row.Cells[1].Value = Double.Parse(item.totalInv.ToString()) - Double.Parse(item.unpaid.ToString());
                    row.Cells[2].Value = item.unpaid;
                    row.Cells[3].Value = item.totalInv;
                    row.Cells[4].Value = item.invDateTime.ToString("yyyy-MM-dd");
                    row.Cells[5].Value = item.invDateTime.ToString("t");
                    dataGridView1.Rows.Add(row);

                    int _id = item.id;
                    int _invNum = item.invNum;
                    double _unpaid = item.unpaid;
                    double _totalInv = item.totalInv;
                    int _clientID = item.clientID;
                    int _userID = item.userID;
                    var _clientName = item.clientName;
                    var _userName = item.userName;
                    var _invDateTime = item.invDateTime;
                    var p = new CashInvD
                    {
                        id = _id,
                        invNum = _invNum,
                        unpaid = _unpaid,
                        totalInv = _totalInv,
                        clientID = _clientID,
                        userID = _userID,
                        clientName = _clientName,
                        userName = _userName,
                        invDateTime = _invDateTime,
                    };
                    CPinv.Add(p);
                }
                sumMethod();
            }

            catch
            {
            }
        }

        private void viUnpaid_Click(object sender, EventArgs e)
        {
            if (viUnpaid.color == Color.Maroon || viUnpaid.BackColor == Color.Maroon)
            {
                try
                {
                    label5.Text = "عرض كل فواتير العميل";
                    viUnpaid.color = Color.DarkBlue;
                    viUnpaid.BackColor = Color.DarkBlue;
                    viUnpaid.colorActive = Color.DarkBlue;
                    label5.ForeColor = Color.DarkBlue;


                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    double unP = 0;
                    var cup = CPinv.Find(i => i.unpaid > unP);

                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = cup.invNum;
                    row.Cells[1].Value = Double.Parse(cup.totalInv.ToString()) - Double.Parse(cup.unpaid.ToString());
                    row.Cells[2].Value = cup.unpaid;
                    row.Cells[3].Value = cup.totalInv;
                    row.Cells[4].Value = cup.invDateTime.ToString("yyyy-MM-dd");
                    row.Cells[5].Value = cup.invDateTime.ToString("t");
                    dataGridView1.Rows.Add(row);
                }
                catch
                {

                }
            }
            else
            {
                try
                {
                    label5.Text = "مؤجلات المورد فقط";
                    viUnpaid.color = Color.Maroon;
                    viUnpaid.BackColor = Color.Maroon;
                    label5.ForeColor = Color.Maroon;
                    viUnpaid.colorActive = Color.Firebrick;
                    dataGridView1.Rows.Clear();
                    dataGridView1.Refresh();
                    LoadUnPaided();
                }
                catch
                {

                }
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                cashed = 0;
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {
                        
                        InvNum = row.Cells[0].Value.ToString();
                        cashed = Double.Parse(row.Cells[2].Value.ToString());

                        PrintInvByNum.Enabled = true;
                        ViewInvByNum.Enabled = true;
                        if (cashed > 0)
                        {
                            PayBtn.Enabled = true;
                        }
                        else
                        {
                            PayBtn.Enabled = false;
                        }

                    }
                    else
                    {
                        InvNum = "";
                        PayBtn.Enabled = false;
                        PrintInvByNum.Enabled = false;
                        ViewInvByNum.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void sumMethod()
        {
            Total.Text = "0";
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                int n = item.Index;
                var finalsum = (Double.Parse(Total.Text.ToString())
                + Double.Parse(dataGridView1.Rows[n].Cells[2].Value.ToString())).ToString();
                Total.Text = finalsum;
            }
        }

        private void PrintInvByNum_Click(object sender, EventArgs e)
        {
            PrintInv();
        }

        private void ViewInvByNum_Click(object sender, EventArgs e)
        {
            ViewInvoicePage viewinv = new ViewInvoicePage(InvNum);
            viewinv.ShowDialog();
        }

        private void PayBtn_Click(object sender, EventArgs e)
        {
            PayThePurchase paynow = new PayThePurchase(InvNum);
            paynow.FormClosed += paynowClosed;
            paynow.ShowDialog();
        }
        private void PrintInv()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@r", InvNum);
            var check = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@r", dic);
            var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@r", dic).ConvertCashInvoice();
            if (invCash == null) return;
            if (check == null) return;
            if (check.Rows.Count > 0)
            {
                foreach (CashInvD item in invCash)
                {
                    inunpaid1 = item.unpaid;
                    var invtotal13 = item.totalInv;
                    invpaid1 = invtotal13 - inunpaid1;
                }
                string d1 = DateTime.Now.ToString("dd");
                string m1 = DateTime.Now.ToString("MM");
                string y1 = DateTime.Now.ToString("yyyy");
                string ti1 = DateTime.Now.ToString("t");
                string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                Print2P printPurch = new Print2P();
                printPurch.SetDataSource(check);
                printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                printPurch.SetParameterValue("invsName", "فاتورة مشتريات");
                printPurch.SetParameterValue("paid", invpaid1);
                printPurch.SetParameterValue("unpaid", inunpaid1);
                printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                printPurch.SetParameterValue("name", Dashboard.invCoN);
                printPurch.SetParameterValue("slug", Dashboard.invCoS);
                printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                printPurch.SetParameterValue("address", Dashboard.invCoA);
                printPurch.PrintToPrinter(1, false, 0, 0);
            }
        }
        void paynowClosed(object sender, FormClosedEventArgs e)
        {
            LoadUnPaided();
        }

    }
}

