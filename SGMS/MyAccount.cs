﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;

namespace SGMS
{
    public partial class MyAccount : UserControl
    {
        DataTable x1;
        public MyAccount()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            int today = int.Parse(DateTime.Now.ToString("dd"));
            this.Dock = DockStyle.Fill;
            fDay.SelectedIndex = today;
            fMonth.SelectedItem = DateTime.Now.ToString("MM");
            fYear.SelectedItem = DateTime.Now.ToString("yyyy");
        }

        private void insertNotice()
        {
            if (checkBox1.Checked)
            {
                string fd = fDay.SelectedItem.ToString();
                string fm = fMonth.SelectedItem.ToString();
                string fy = fYear.SelectedItem.ToString();
                string f = fy + "-" + fm + "-" + fd;
                int rd = int.Parse(fd) - 1;
                string rem = fy + "-" + fm + "-" + rd;
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@UserID", Login.cid);
                addC.Add("@remNotice", richTextBox1.Text);
                addC.Add("@ct_date", DateTime.Now.ToString("yyyy-MM-dd"));
                addC.Add("@ct_time", DateTime.Now.ToString("HH:mm:ss"));
                addC.Add("@RemDate", rem);
                addC.Add("@showDate", f);
                var addNotice = DBConnection.SqliteCommand("INSERT  INTO `Notice`(`UserID`, `remNotice`, `ct_date`, `ct_time`, `RemDate`,`showDate`) values(@UserID,@remNotice,@ct_date,@ct_time,@RemDate,@showDate)", addC);

            }
            else
            {
                string fd = fDay.SelectedItem.ToString();
                string fm = fMonth.SelectedItem.ToString();
                string fy = fYear.SelectedItem.ToString();
                string f = fy + "-" + fm + "-" + fd;
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@UserID", Login.cid);
                addC.Add("@remNotice", richTextBox1.Text);
                addC.Add("@ct_date", DateTime.Now.ToString("yyyy-MM-dd"));
                addC.Add("@ct_time", DateTime.Now.ToString("HH:mm:ss"));
                addC.Add("@RemDate", f);
                addC.Add("@showDate", f);
                var addNotice = DBConnection.SqliteCommand("INSERT  INTO `Notice`(`UserID`, `remNotice`, `ct_date`, `ct_time`, `RemDate`,`showDate`) values(@UserID,@remNotice,@ct_date,@ct_time,@RemDate,@showDate)", addC);

            }

        }

        private void AddIn_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "" || richTextBox1.Text == "أكتب مفكرتك هنا")
            {
                MessageBox.Show("برجاء كتابة نص المفكرة");
            }
            else
            {
                insertNotice();
                MessageBox.Show("تم إضافة المفكرة بنجاح");
            }

        }

        private void richTextBox1_Enter(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "أكتب مفكرتك هنا")
            {
                richTextBox1.Text = "";
            }
        }

        private void richTextBox1_Leave(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "")
            {
                richTextBox1.Text = "أكتب مفكرتك هنا";
            }
        }

        private void bunifuMaterialTextbox1_OnValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (bunifuMaterialTextbox1.Text == Login.uPassword)
                {
                    panel8.Visible = true;
                    panel10.Visible = false;
                    FillAcc();
                }
                else
                {
                    panel8.Visible = false;
                    panel10.Visible = true;
                }
            }
            catch { }
        }


        private void FillAcc()
        {
            try
            {
                Dictionary<string, object> prams = new Dictionary<string, object>();
                prams.Add("@id", Login.cid);
                var result = DBConnection.SqliteCommand("select * from Login where uid=@id", prams).ConvertUser();


                if (result.Count > 0)
                {
                    UsrFN.Text = result[0].fullname.ToString();
                    UsrN.Text = result[0].username.ToString();
                    UsrPass.Text = result[0].password.ToString();
                    label6.Text = result[0].Cash.ToString();
                }
            }
            catch { }
        }
        private void updateAcc()
        {
            try
            {
                if (UsrN.Text == Login.uNam)
                {
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@password", UsrPass.Text.ToString());
                    addC.Add("@id", Login.cid);
                    var UpdateNotice = DBConnection.SqliteCommand("update Login Set  password = @password where uid=@id", addC);
                    MessageBox.Show("تم تعديل كلمة المرور بنجاح");
                    FillAcc();
                }
                else
                {
                    var unam = UsrN.Text;
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@username", unam);
                    x1 = DBConnection.SqliteCommand("SELECT *  FROM Login where username = @username", dic);
                    if (x1.Rows.Count > 0)
                    {
                        MessageBox.Show("يوجد إسم مستخدم مطابق لهذا الإسم" + " " + ":" + " " + unam);
                        return;
                    }
                    else
                    {
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@username", UsrN.Text.ToString());
                        addC.Add("@password", UsrPass.Text.ToString());
                        addC.Add("@id", Login.cid);
                        var UpdateNotice = DBConnection.SqliteCommand("update Login Set username = @username , password = @password where uid=@id", addC);
                        MessageBox.Show("تم تعديل بياناتك بنجاح");
                        FillAcc();
                    }
                }
            }
            catch { }
            }
        private void Send_Click(object sender, EventArgs e)
        {
            updateAcc();
        }

        private void bunifuMaterialTextbox1_Enter(object sender, EventArgs e)
        {
            bunifuMaterialTextbox1.isPassword = true;
        }
    }
}

