﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class cMessgeBox : MetroFramework.Forms.MetroForm
    {
        private System.Windows.Forms.Timer tmr = new System.Windows.Forms.Timer();
        int ttime;
        public cMessgeBox(string theMessage, string status, string page, int time)
        {
            InitializeComponent();
            ttime = time;
            if (status == "done")
            {
                if (page == "c")
                {
                    message.Text = theMessage;
                    this.Style = MetroFramework.MetroColorStyle.Blue;
                    message.ForeColor = Color.RoyalBlue;
                    pictureBox1.Image = Properties.Resources.user_add;

                    while (message.Width < System.Windows.Forms.TextRenderer.MeasureText(message.Text,
                    new Font(message.Font.FontFamily, message.Font.Size, message.Font.Style)).Width)
                    {
                        message.Font = new Font(message.Font.FontFamily, message.Font.Size - 0.5f, message.Font.Style);
                    }
                }
                if (page == "p")
                {
                    message.Text = theMessage;
                    this.Style = MetroFramework.MetroColorStyle.Green;
                    message.ForeColor = Color.Green;
                    pictureBox1.Image = Properties.Resources.success;
                    while (message.Width < System.Windows.Forms.TextRenderer.MeasureText(message.Text,
                    new Font(message.Font.FontFamily, message.Font.Size, message.Font.Style)).Width)
                    {
                        message.Font = new Font(message.Font.FontFamily, message.Font.Size - 0.5f, message.Font.Style);
                    }
                }
                if (page == "e")
                {
                    message.Text = theMessage;
                    this.Style = MetroFramework.MetroColorStyle.Green;
                    message.ForeColor = Color.Green;
                    pictureBox1.Image = Properties.Resources.editC;
                    while (message.Width < System.Windows.Forms.TextRenderer.MeasureText(message.Text,
                    new Font(message.Font.FontFamily, message.Font.Size, message.Font.Style)).Width)
                    {
                        message.Font = new Font(message.Font.FontFamily, message.Font.Size - 0.5f, message.Font.Style);
                    }
                }

            }
            else if (status == "error")
            {
                message.Text = theMessage;
                message.ForeColor = Color.Red;
                pictureBox1.Image = Properties.Resources.error;
                this.Style = MetroFramework.MetroColorStyle.Red;
                while (message.Width < System.Windows.Forms.TextRenderer.MeasureText(message.Text,
                new Font(message.Font.FontFamily, message.Font.Size, message.Font.Style)).Width)
                {
                    message.Font = new Font(message.Font.FontFamily, message.Font.Size - 0.5f, message.Font.Style);
                }
            }

        }

        private void cMessgeBox_Shown(object sender, EventArgs e)
        {
            tmr.Start();
            tmr.Interval = ttime;
            tmr.Tick += tmr_Tick;
        }
        void tmr_Tick(object sender, EventArgs e)

        {
            this.Close();
        }
    }
}
