﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using System.Collections;

namespace SGMS
{
    public partial class Clients : UserControl
    {
        string CLID;
        string ClName;
        string ClPhone;
        string ClAddress;
        public Clients()
        {

            InitializeComponent();
            LoadAll();

        }
        private void PurchaseClients_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadAll();
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAll();
        }
        private static List<Customer> CustomerP = new List<Customer>();
        private void AddNew_Click(object sender, EventArgs e)
        {
            FastAddCustomer fastuser = new FastAddCustomer();
            fastuser.FormClosed += FastAddUserClosed;
            fastuser.ShowDialog();
        }
        void FastAddUserClosed(object sender, FormClosedEventArgs e)
        {
            LoadAll();
        }
        private void LoadAll()
        {
            try
            {
                CustomerP.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                CustomerByP.Text = "";

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@r", "1");
                var result = DBConnection.SqliteCommand("SELECT DISTINCT * FROM Customers where id > @r", dic).ConvertCustomers();
                if (result == null) return;
                if (result.Count > 0)
                    foreach (Customer item in result) 
                    {
                        CustomerN.DataSource = result;
                        CustomerN.DisplayMember = "Name";
                        CustomerN.ValueMember = "id";
                        CustomerN.SelectedIndex = -1;

                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView1);
                        row.Cells[0].Value = item.Name;
                        row.Cells[1].Value = item.Phone;
                        row.Cells[2].Value = item.Address;
                        row.Cells[3].Value = item.Cash;
                        row.Cells[4].Value = item.id;
                        dataGridView1.Rows.Add(row);

                        int _id = item.id;
                        string _Name = item.Name;
                        var _Phone = item.Phone;
                        string _Address = item.Address;
                        double _Cash = item.Cash;
                        var p = new Customer
                        {
                            id = _id,
                            Name = _Name,
                            Phone = _Phone,
                            Address = _Address,
                            Cash = _Cash,
                        };
                        CustomerP.Add(p);
                    }

            }
            catch
            {

            }
        }
        private void CustomerByPhone()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var phone = CustomerByP.Text.ToString();
                var cup = CustomerP.Find(i => i.Phone == phone);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = cup.Name;
                row.Cells[1].Value = cup.Phone;
                row.Cells[2].Value = cup.Address;
                row.Cells[3].Value = cup.Cash;
                row.Cells[4].Value = cup.id;
                dataGridView1.Rows.Add(row);

            }
            catch
            {

            }
        }
        private void CustomerByName()
        {
            try
            {

                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var id = int.Parse(CustomerN.SelectedValue.ToString());
                var cup = CustomerP.Find(i => i.id == id);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = cup.Name;
                row.Cells[1].Value = cup.Phone;
                row.Cells[2].Value = cup.Address;
                row.Cells[3].Value = cup.Cash;
                row.Cells[4].Value = cup.id;
                dataGridView1.Rows.Add(row);
            }
            catch
            {

            }
        }
        private void CustomerN_SelectedIndexChanged(object sender, EventArgs e)
        {
            CustomerByName();
        }
        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            LoadAll();
        }
        private void CustomerByCash()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var item = CustomerP.FindAll(i => i.Cash > 0);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item1.Name;
                    row.Cells[1].Value = item1.Phone;
                    row.Cells[2].Value = item1.Address;
                    row.Cells[3].Value = item1.Cash;
                    row.Cells[4].Value = item1.id;
                    dataGridView1.Rows.Add(row);
                }

            }
            catch
            {

            }
        }
        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            CustomerByCash();
        }
        private void CustomerByP_TextChanged_1(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(CustomerByP.Text))
            {
                LoadAll();
            }
            else
            {
                CustomerByPhone();
            }
        }
        private void viewUnPaid_Click(object sender, EventArgs e)
        {
            ViewCustomerInvs viewallinv = new ViewCustomerInvs(CLID, ClName, ClPhone);
            viewallinv.FormClosed += ViewClientClosed;
            viewallinv.ShowDialog();

        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {
                        CLID = row.Cells[4].Value.ToString();
                        ClName = row.Cells[0].Value.ToString();
                        ClPhone = row.Cells[1].Value.ToString();
                        ClAddress = row.Cells[2].Value.ToString();
                        viewUnPaid.Enabled = true;
                        editC.Enabled = true;
                    }
                    else
                    {
                        CLID = "";
                        ClName = "";
                        ClPhone = "";
                        ClAddress = "";
                        viewUnPaid.Enabled = false;
                        editC.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void editC_Click(object sender, EventArgs e)
        {
            EditCustomers EditClients = new EditCustomers(CLID, ClName, ClPhone, ClAddress);
            EditClients.FormClosed += EditClientsClosed;
            EditClients.ShowDialog();
        }
        void ViewClientClosed(object sender, FormClosedEventArgs e)
        {
            LoadAll();
        }
        void EditClientsClosed(object sender, FormClosedEventArgs e)
        {
            LoadAll();
        }

    }
}
