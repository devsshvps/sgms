﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class Login : Form
    {
        #region fileds

        public static string cnn = "";
        public static string invLogo = "";
        public static string logopath = "";
        public static string invCoN = "";
        public static string invCoS = "";
        public static string invCoA = "";
        public static string invCoP = "";
        public static string r1 = "";
        public static string r2 = "";
        public static string r3 = "";
        public static string r4 = "";
        public static string r5 = "";
        public static string r6 = "";
        public static string r7 = "";
        public static string r8 = "";
        public static string r9 = "";
        public static string r10 = "";
        public static string r11 = "";
        public static string r12 = "";
        public static string r13 = "";
        public static string r14 = "";
        public static string r15 = "";
        public static string r16 = "";
        public static string r17 = "";
        public static string r18 = "";
        public static string r19 = "";
        public static string r20 = "";
        public static string r21 = "";
        public static string r22 = "";
        public static string r23 = "";
        public static string r24 = "";
        public static string r25 = "";
        public static string r26 = "";
        public static string r27 = "";
        public static string r28 = "";
        public static string r29 = "";
        public static string r30 = "";
        public static string uPassword = "";
        public static string uNam = "";
        public static int cid = 0;
        public static int BlockState = 0;
        public static string Blocker = "";
        public static double Cash = 0;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        #endregion
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public Login()
        {
            InitializeComponent();

        }
        private void Login_Load(object sender, EventArgs e)
        {
            LoadStoreInfo();
        }

        private void AClose_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void AMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void aboutBTN_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void tab3_Click(object sender, EventArgs e)
        {
            CheckLogin();
        }

        private void _Start()
        {
            if (BlockState == 0)
            {
                MessageBox.Show("نأسف تم حظر هذا الحساب بواسطة" + " " + ":" + " " + Blocker);
                Application.Exit();
            }
            else
            {
                try
                {
                    Application.EnableVisualStyles();
                    Application.Run(new Dashboard());
                }
                catch { }
            }
        }

        private void CheckLogin()
        {
            if (this.usr.Text == "إسم المستخدم" || this.usr.Text == "")
            {
                cMessgeBox cMessge = new cMessgeBox("برجاء إدخال إسم المستخدم", "error", "p", 1500);
                cMessge.ShowDialog();
                //Bunifu.Snackbar.Show(this, "برجاء إدخال إسم المستخدم", 3000, Snackbar.Views.SnackbarDesigner.MessageTypes.Error);
                return;

            }
            else if (this.pass.Text == "كلمة المرور" || this.pass.Text == "")
            {
                cMessgeBox cMessge = new cMessgeBox("برجاء إدخال كلمة المرور", "error", "p", 1500);
                cMessge.ShowDialog();
                //Bunifu.Snackbar.Show(this, "برجاء إدخال كلمة المرور", 3000, Snackbar.Views.SnackbarDesigner.MessageTypes.Error);
                return;
            }

            Dictionary<string, object> prams = new Dictionary<string, object>();
            prams.Add("@username", usr.Text.ToString());
            prams.Add("@password", pass.Text.ToString());
            var result = DBConnection.SqliteCommand("select * from Login where username=@username and password=@password", prams).ConvertUser();


            if (result.Count > 0)
            {
                cnn = result[0].fullname;
                uPassword = result[0].password.ToString();
                uNam = result[0].username.ToString();
                cid = result[0].uid;
                Cash = result[0].Cash;
                r1 = result[0].role1.ToString();
                r2 = result[0].role2.ToString();
                r3 = result[0].role3.ToString();
                r4 = result[0].role4.ToString();
                r5 = result[0].role5.ToString();
                r6 = result[0].role6.ToString();
                r7 = result[0].role7.ToString();
                r8 = result[0].role8.ToString();
                r9 = result[0].role9.ToString();
                r10 = result[0].role10.ToString();
                r11 = result[0].role11.ToString();
                r12 = result[0].role12.ToString();
                r13 = result[0].role13.ToString();
                r14 = result[0].role14.ToString();
                r15 = result[0].role15.ToString();
                r16 = result[0].role16.ToString();
                r17 = result[0].role17.ToString();
                r18 = result[0].role18.ToString();
                r19 = result[0].role19.ToString();
                r20 = result[0].role20.ToString();
                r21 = result[0].role21.ToString();
                r22 = result[0].role22.ToString();
                r23 = result[0].role23.ToString();
                r24 = result[0].role24.ToString();
                r25 = result[0].role25.ToString();
                r26 = result[0].role26.ToString();
                r27 = result[0].role27.ToString();
                r28 = result[0].role28.ToString();
                r29 = result[0].role29.ToString();
                r30 = result[0].role30.ToString();
                BlockState = result[0].BlockState;
                Blocker = result[0].Blocker.ToString();
               
                Thread str = new Thread(_Start);
                str.SetApartmentState(ApartmentState.STA);
                str.Start();
                Close(); 
            }
            else
            {
                cMessgeBox cMessge = new cMessgeBox("خطاء في بيانات تسجيل الدخول", "error", "p", 1500);
                cMessge.ShowDialog();
                return;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                CheckLogin();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void pass_Enter(object sender, EventArgs e)
        {
            if (pass.Text == "كلمة المرور")
            {
                pass.Text = "";
                pass.isPassword = true;
            }
        }

        private void pass_Leave(object sender, EventArgs e)
        {
            if (pass.Text == "")
            {
                pass.Text = "كلمة المرور";
                pass.isPassword = false;
            }
        }

        private void usr_Leave(object sender, EventArgs e)
        {
            if (usr.Text == "")
            {
                usr.Text = "إسم المستخدم";
            }
        }

        private void usr_Enter(object sender, EventArgs e)
        {
            if (usr.Text == "إسم المستخدم")
            {
                usr.Text = "";
            }
        }
        private static List<Setting> SettingP = new List<Setting>();
        private void LoadStoreInfo()
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id <=@id", dic);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var _id = int.Parse(result.Rows[i]["id"].ToString());
                var _StoreLogo = result.Rows[i]["StoreLogo"].ToString();
                var _PrintLogo = result.Rows[i]["PrintLogo"].ToString();
                var _PrintCoName = result.Rows[i]["PrintCoName"].ToString();
                var _PrintCoSlu = result.Rows[i]["PrintCoSlu"].ToString();
                var _PrintCoAddress = result.Rows[i]["PrintCoAddress"].ToString();
                var _PrintCoPhone = result.Rows[i]["PrintCoPhone"].ToString();
                var p = new Setting
                {
                    id = _id,
                    StoreLogo = _StoreLogo,
                    PrintLogo = _PrintLogo,
                    PrintCoName = _PrintCoName,
                    PrintCoSlu = _PrintCoSlu,
                    PrintCoAddress = _PrintCoAddress,
                    PrintCoPhone = _PrintCoPhone,
                };
                SettingP.Add(p);
            }

            int nid = 1;
            var cup = SettingP.Find(i => i.id == nid);
            var nStoreLogo = cup.StoreLogo.ToString();
            StoreLogo.ImageLocation = nStoreLogo;
            invLogo = cup.PrintLogo;
            logopath = Application.StartupPath + "//" + invLogo;
            invCoN = cup.PrintCoName;
            invCoS = cup.PrintCoSlu;
            invCoA = cup.PrintCoAddress;
            invCoP = cup.PrintCoPhone;
        }

        public static string arNumber(string x)
        {
            string text = x.Replace("0", "٠");
            text = text.Replace("1", "١");
            text = text.Replace("2", "٢");
            text = text.Replace("3", "٣");
            text = text.Replace("4", "٤");
            text = text.Replace("5", "٥");
            text = text.Replace("6", "٦");
            text = text.Replace("7", "٧");
            text = text.Replace("8", "٨");
            text = text.Replace("PM", "مساءً");
            text = text.Replace("AM", "صباحاً");
            return text.Replace("9", "٩");
        }
        public static string arDate(string x)
        {
            string text = x.Replace("01", "يناير");
            text = text.Replace("02", "فبراير");
            text = text.Replace("03", "مارس");
            text = text.Replace("04", "ابريل");
            text = text.Replace("05", "مايو");
            text = text.Replace("06", "يونيو");
            text = text.Replace("07", "يوليو");
            text = text.Replace("08", "اغسطس");
            text = text.Replace("09", "سبتمبر");
            text = text.Replace("10", "اكتوبر");
            text = text.Replace("11", "نوفمبر");
            return text.Replace("12", "ديسمبر");
        }

    }
}

