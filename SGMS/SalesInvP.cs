﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class SalesInvP : Form
    {
        string InvNNN;
        double inunpaid1;
        double invtotal1;
        double invpaid1;
        string finalnumber;
        int isTax;
        string TaxNum, CoRecord;

        public SalesInvP(String invNumber, int invType)
        {
            InitializeComponent();
            InvNNN = invNumber;
            isTax = invType;
            LoadInv();
            if (isTax == 1)
            {
                this.Text = "عرض فاتورة ضريبية رقم" + " " + ":" + " " + Login.arNumber(finalnumber);
            }
            else
            {
                this.Text = "عرض لفاتورة المبيعات رقم" + " " + ":" + " " + Login.arNumber(finalnumber);
            }

        }

        private void PrintInvByNum_Click(object sender, EventArgs e)
        {
            PrintInv();
        }

        private void PrintInv()
        {
            if (isTax == 0)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@r", InvNNN);
                var check = DBConnection.SqliteCommand("SELECT * FROM Sells where Num=@r", dic);
                var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@r AND Tax = 0", dic).ConvertCashSales();
                if (invCash == null) return;
                if (check == null) return;
                if (check.Rows.Count > 0)
                {
                    foreach (CashInvS item in invCash)
                    {
                        inunpaid1 = item.unpaid;
                        var invtotal13 = item.totalInv;
                        invpaid1 = invtotal13 - inunpaid1;
                    }
                    string d1 = DateTime.Now.ToString("dd");
                    string m1 = DateTime.Now.ToString("MM");
                    string y1 = DateTime.Now.ToString("yyyy");
                    string ti1 = DateTime.Now.ToString("t");
                    string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                    string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                    string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                    Print2S printPurch = new Print2S();
                    printPurch.SetDataSource(check);
                    printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                    printPurch.SetParameterValue("invsName", "فاتورة مبيعات");
                    printPurch.SetParameterValue("paid", invpaid1);
                    printPurch.SetParameterValue("unpaid", inunpaid1);
                    printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                    printPurch.SetParameterValue("name", Dashboard.invCoN);
                    printPurch.SetParameterValue("slug", Dashboard.invCoS);
                    printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                    printPurch.SetParameterValue("address", Dashboard.invCoA);
                    printPurch.PrintToPrinter(1, false, 0, 0);
                }
            }
            else
            {
                double isX = 0;
                int nIsx = 0;
                try
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@r", InvNNN);
                    var check = DBConnection.SqliteCommand("SELECT * FROM TaxSells where Num=@r", dic);
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@r AND Tax = 1", dic).ConvertCashSales();
                    var Taxin = DBConnection.SqliteCommand("SELECT  *  FROM TaxInfo where id = 1", dic).ConvertTaxInfo();
                    foreach (TaxInfo TaxX in Taxin)
                    {
                        TaxNum = TaxX.TaxNum;
                        CoRecord = TaxX.CoRecord;
                    }
                    if (invCash == null) return;
                    if (check == null) return;
                    if (check.Rows.Count > 0)
                    {
                        for (int i = 0; i < check.Rows.Count; i++)
                        {
                            nIsx = int.Parse(check.Rows[i]["IsTax"].ToString());
                        }
                        foreach (CashInvS item in invCash)
                        {
                            inunpaid1 = item.unpaid;
                            invtotal1 = item.totalInv;
                            isX = item.totalInv;
                            invpaid1 = invtotal1 - inunpaid1;
                        }

                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2STax printPurch = new Print2STax();
                        printPurch.SetDataSource(check);
                        printPurch.SetParameterValue("invsName", "فاتورة ضريبية");
                        printPurch.SetParameterValue("TaxNum", TaxNum);
                        printPurch.SetParameterValue("CoRecord", CoRecord);
                        if (nIsx == 1)
                        {
                            printPurch.SetParameterValue("isTax", "٪" + Login.arNumber("1"));
                        }
                        else
                        {
                            printPurch.SetParameterValue("isTax", "لا يوجد");
                        }
                        printPurch.SetParameterValue("arTotal", "فقط" + " " + arTotal(invtotal1, "جنيه", "قرش") + " " + "لا غير");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("paid", invpaid1);
                        printPurch.SetParameterValue("unpaid", inunpaid1);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                        printPurch.SetParameterValue("address", Dashboard.invCoA);
                        printPurch.PrintToPrinter(1, false, 0, 0);
                    }

                }
                catch
                {
                }
            }
        }
        private void LoadInv()
        {

            try
            {
                string inv = InvNNN;
                int sNuM = int.Parse(inv.ToString());

                if (sNuM >= 1 && sNuM < 10)
                {
                    finalnumber = "000000" + sNuM.ToString();
                }
                else if (sNuM >= 10 && sNuM < 100)
                {
                    finalnumber = "00000" + sNuM.ToString();
                }
                else if (sNuM >= 100 && sNuM < 1000)
                {
                    finalnumber = "0000" + sNuM.ToString();
                }
                else if (sNuM >= 1000 && sNuM < 10000)
                {
                    finalnumber = "000" + sNuM.ToString();
                }
                else if (sNuM >= 10000 && sNuM < 100000)
                {
                    finalnumber = "00" + sNuM.ToString();
                }
                else if (sNuM >= 100000 && sNuM < 1000000)
                {
                    finalnumber = "0" + sNuM.ToString();
                }
                else if (sNuM >= 1000000)
                {
                    finalnumber = sNuM.ToString();
                }
                InvNum.Text = Login.arNumber(finalnumber);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("num", inv);
                dic.Add("@Type", isTax);

                if (isTax == 0)
                {
                    var result = DBConnection.SqliteCommand("SELECT * FROM Sells where Num=@num", dic).ConvertSales();
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@num AND Tax = @Type", dic).ConvertCashSales();
                    if (invCash == null) return;
                    if (result == null) return;

                    foreach (CashInvS item in invCash)
                    {
                        inunpaid1 = item.unpaid;
                        invtotal1 = item.totalInv;
                        invpaid1 = invtotal1 - inunpaid1;
                    }

                    foreach (Models.Sales item in result)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(invDatagride);
                        row.Cells[0].Value = item.itemname;
                        row.Cells[1].Value = item.count;
                        row.Cells[2].Value = item.itemprice;
                        row.Cells[3].Value = item.totalprice;
                        invDatagride.Rows.Add(row);
                        invDatagride.CurrentCell = null;
                        invDate.Text = item.ddate.ToString("yyyy-MM-dd");
                        invClient.Text = item.sellfor;
                    }
                    Total.Text = invtotal1.ToString();
                    Unpaid.Text = inunpaid1.ToString();
                    Paid.Text = invpaid1.ToString();
                }
                else
                {
                    var result = DBConnection.SqliteCommand("SELECT * FROM TaxSells where Num=@num", dic).ConvertSales();
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@num AND Tax = @Type", dic).ConvertCashSales();
                    if (invCash == null) return;
                    if (result == null) return;

                    foreach (CashInvS item in invCash)
                    {
                        inunpaid1 = item.unpaid;
                        invtotal1 = item.totalInv;
                        invpaid1 = invtotal1 - inunpaid1;
                    }

                    foreach (Models.Sales item in result)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(invDatagride);
                        row.Cells[0].Value = item.itemname;
                        row.Cells[1].Value = item.count;
                        row.Cells[2].Value = item.itemprice;
                        row.Cells[3].Value = item.totalprice;
                        invDatagride.Rows.Add(row);
                        invDatagride.CurrentCell = null;
                        invDate.Text = item.ddate.ToString("yyyy-MM-dd");
                        invClient.Text = item.sellfor;
                    }
                    Total.Text = invtotal1.ToString();
                    Unpaid.Text = inunpaid1.ToString();
                    Paid.Text = invpaid1.ToString();
                }

            }
            catch
            {
            }
        }

        private string arTotal(double Amount, string Curr, string SubCurr)
        {
            string Value = Math.Floor(Amount).ToString();
            string NewValue = null;
            string[] Part1 = new string[10] { "", "واحد ", "اثنان", "ثلاث", "أربعة", "خمسة", "ستة", "سبعة", "ثمانية", "تسعة" };
            string[] Part2 = new string[10] { "", "عشر", "عشرون", "ثلاثون", "أربعون", "خمسون", "ستون", "سبعون", "ثمانون", "تسعون" };
            string[] Part3 = new string[10] { "", "مئة", "مئتان", "ثلاثمائة ", "أربعمائة", "خمسمائة", "ستمائة", "سبعمائة", "ثمانمائة", "تسعمائة" };
            string[] Part4 = new string[10] { "", "ألف", "ألفان", "ثلاثة آلاف", "أربع آلاف", "خمسة آلاف", "ستة آلاف", "سبع آلاف", "ثمانية آلاف", "تسعة آلاف" };

            //---------------------------< ....... الملاييييييييييييين ........ >
            if (Value.Length == 7)
            {
                if (Value.Substring(0, 1) == "1")
                    NewValue = "مليون ";
                else if (Value.Substring(0, 1) == "2")
                    NewValue = "مليونان ";
                else
                    NewValue = Part1[int.Parse(Value.Substring(0, 1))] + " ملايين";

                Value = Value.Substring(1, Value.Length - 1);
            }
            //------------------------------------< ....... مئات الألوف....... >
            if (Value.Length == 6)
            {
                NewValue += " " + Part3[int.Parse(Value.Substring(0, 1))] + " و";
                Value = Value.Substring(1, Value.Length - 1);
            }
            //----------------------------------
            if (Value.Length == 5)
            {
                NewValue += " " + Part1[int.Parse(Value.Substring(1, 1))];
                NewValue += " و" + Part2[int.Parse(Value.Substring(0, 1))] + " ألفا و";
                Value = Value.Substring(1, Value.Length - 1);
                Value = Value.Substring(1, Value.Length - 1);
            }
            //---------------------------------
            if (Value.Length == 4)
            {
                NewValue += " " + Part4[int.Parse(Value.Substring(0, 1))];
                Value = Value.Substring(1, Value.Length - 1);
            }
            if (Value.Length == 3)
            {
                NewValue += " " + Part3[int.Parse(Value.Substring(0, 1))] + " و";
                Value = Value.Substring(1, Value.Length - 1);
            }
            //-----------------------------------
            if (Value.Length == 2)
            {
                if (Part2[int.Parse(Value.Substring(0, 1))] == "عشر")
                {
                    NewValue += Part1[int.Parse(Value.Substring(1, 1))];
                }
                else
                {
                    if (int.Parse(Value) == 20 || int.Parse(Value) == 30 || int.Parse(Value) == 40 || int.Parse(Value) == 50 || int.Parse(Value) == 60 || int.Parse(Value) == 70 || int.Parse(Value) == 80 || int.Parse(Value) == 90)
                    {
                        NewValue += Part1[int.Parse(Value.Substring(1, 1))];
                    }
                    else
                    {
                        NewValue += Part1[int.Parse(Value.Substring(1, 1))] + " و";
                    }

                }
                NewValue += " " + Part2[int.Parse(Value.Substring(0, 1))];
                Value = Value.Substring(1, Value.Length - 1);
                Value = Value.Substring(1, Value.Length - 1);
            }
            if (Value.Length == 1)
            {
                NewValue += " " + Part1[int.Parse(Value.Substring(0, 1))];
                Value = Value.Substring(1, Value.Length - 1);
            }


            NewValue += " " + Curr;
            //الكسر العشري
            string Real = Amount.ToString("");
            int Pos = Real.IndexOf('.', 0, Real.Length);
            if (Pos != -1)
            {
                Pos++;
                try
                {
                    string NewReal = (float.Parse(Real.Substring(Pos, Real.Length - Pos))).ToString();
                    if (NewReal.Length == 1)
                        NewValue += " و " + Part2[int.Parse(NewReal.Substring(0, 1))];
                    else
                    {
                        NewValue += " و " + Part1[int.Parse(NewReal.Substring(1, 1))];
                        NewValue += " و " + Part2[int.Parse(NewReal.Substring(0, 1))];
                    }
                    NewValue += " " + SubCurr;
                }
                catch { }
            }
            return NewValue;

        }

    }
}

