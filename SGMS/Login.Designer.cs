﻿namespace SGMS
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.panel3 = new System.Windows.Forms.Panel();
            this.StoreLogo = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.aboutBTN = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AMin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AClose = new Bunifu.Framework.UI.BunifuFlatButton();
            this.usr = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tab3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pass = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreLogo)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.StoreLogo);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 32);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(472, 116);
            this.panel3.TabIndex = 41;
            // 
            // StoreLogo
            // 
            this.StoreLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StoreLogo.Location = new System.Drawing.Point(347, 22);
            this.StoreLogo.Name = "StoreLogo";
            this.StoreLogo.Size = new System.Drawing.Size(99, 76);
            this.StoreLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.StoreLogo.TabIndex = 0;
            this.StoreLogo.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cairo", 30F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(63, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(267, 75);
            this.label7.TabIndex = 42;
            this.label7.Text = "تسجيل الدخول";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.aboutBTN);
            this.panel1.Controls.Add(this.AMin);
            this.panel1.Controls.Add(this.AClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 32);
            this.panel1.TabIndex = 40;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(38, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 32);
            this.label1.TabIndex = 32;
            this.label1.Text = "تسجيل الدخول";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::SGMS.Properties.Resources.DB_01;
            this.pictureBox2.Location = new System.Drawing.Point(13, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 32);
            this.label2.TabIndex = 30;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // aboutBTN
            // 
            this.aboutBTN.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.aboutBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.aboutBTN.BorderRadius = 0;
            this.aboutBTN.ButtonText = "";
            this.aboutBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutBTN.DisabledColor = System.Drawing.Color.Gray;
            this.aboutBTN.Dock = System.Windows.Forms.DockStyle.Right;
            this.aboutBTN.Iconcolor = System.Drawing.Color.Transparent;
            this.aboutBTN.Iconimage = global::SGMS.Properties.Resources.info_512px;
            this.aboutBTN.Iconimage_right = null;
            this.aboutBTN.Iconimage_right_Selected = null;
            this.aboutBTN.Iconimage_Selected = null;
            this.aboutBTN.IconMarginLeft = 0;
            this.aboutBTN.IconMarginRight = 0;
            this.aboutBTN.IconRightVisible = true;
            this.aboutBTN.IconRightZoom = 0D;
            this.aboutBTN.IconVisible = true;
            this.aboutBTN.IconZoom = 40D;
            this.aboutBTN.IsTab = false;
            this.aboutBTN.Location = new System.Drawing.Point(361, 0);
            this.aboutBTN.Name = "aboutBTN";
            this.aboutBTN.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.aboutBTN.OnHoverTextColor = System.Drawing.Color.White;
            this.aboutBTN.selected = false;
            this.aboutBTN.Size = new System.Drawing.Size(37, 32);
            this.aboutBTN.TabIndex = 29;
            this.aboutBTN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutBTN.Textcolor = System.Drawing.Color.White;
            this.aboutBTN.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.aboutBTN.Click += new System.EventHandler(this.aboutBTN_Click);
            // 
            // AMin
            // 
            this.AMin.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AMin.BorderRadius = 0;
            this.AMin.ButtonText = "";
            this.AMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AMin.DisabledColor = System.Drawing.Color.Gray;
            this.AMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.AMin.Iconcolor = System.Drawing.Color.Transparent;
            this.AMin.Iconimage = global::SGMS.Properties.Resources._1_03;
            this.AMin.Iconimage_right = null;
            this.AMin.Iconimage_right_Selected = null;
            this.AMin.Iconimage_Selected = null;
            this.AMin.IconMarginLeft = 0;
            this.AMin.IconMarginRight = 0;
            this.AMin.IconRightVisible = true;
            this.AMin.IconRightZoom = 0D;
            this.AMin.IconVisible = true;
            this.AMin.IconZoom = 40D;
            this.AMin.IsTab = false;
            this.AMin.Location = new System.Drawing.Point(398, 0);
            this.AMin.Name = "AMin";
            this.AMin.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.AMin.OnHoverTextColor = System.Drawing.Color.White;
            this.AMin.selected = false;
            this.AMin.Size = new System.Drawing.Size(37, 32);
            this.AMin.TabIndex = 28;
            this.AMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AMin.Textcolor = System.Drawing.Color.White;
            this.AMin.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AMin.Click += new System.EventHandler(this.AMin_Click);
            // 
            // AClose
            // 
            this.AClose.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AClose.BorderRadius = 0;
            this.AClose.ButtonText = "";
            this.AClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AClose.DisabledColor = System.Drawing.Color.Gray;
            this.AClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.AClose.Iconcolor = System.Drawing.Color.Transparent;
            this.AClose.Iconimage = global::SGMS.Properties.Resources._1_02;
            this.AClose.Iconimage_right = null;
            this.AClose.Iconimage_right_Selected = null;
            this.AClose.Iconimage_Selected = null;
            this.AClose.IconMarginLeft = 0;
            this.AClose.IconMarginRight = 0;
            this.AClose.IconRightVisible = true;
            this.AClose.IconRightZoom = 0D;
            this.AClose.IconVisible = true;
            this.AClose.IconZoom = 40D;
            this.AClose.IsTab = false;
            this.AClose.Location = new System.Drawing.Point(435, 0);
            this.AClose.Name = "AClose";
            this.AClose.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.OnHovercolor = System.Drawing.Color.Red;
            this.AClose.OnHoverTextColor = System.Drawing.Color.White;
            this.AClose.selected = false;
            this.AClose.Size = new System.Drawing.Size(37, 32);
            this.AClose.TabIndex = 27;
            this.AClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AClose.Textcolor = System.Drawing.Color.White;
            this.AClose.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AClose.Click += new System.EventHandler(this.AClose_Click);
            // 
            // usr
            // 
            this.usr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.usr.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.usr.Font = new System.Drawing.Font("Cairo", 10F);
            this.usr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.usr.HintForeColor = System.Drawing.Color.Empty;
            this.usr.HintText = "";
            this.usr.isPassword = false;
            this.usr.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(109)))), ((int)(((byte)(124)))));
            this.usr.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.usr.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(99)))), ((int)(((byte)(124)))));
            this.usr.LineThickness = 5;
            this.usr.Location = new System.Drawing.Point(98, 197);
            this.usr.Margin = new System.Windows.Forms.Padding(4);
            this.usr.Name = "usr";
            this.usr.Size = new System.Drawing.Size(273, 48);
            this.usr.TabIndex = 42;
            this.usr.Text = "إسم المستخدم";
            this.usr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.usr.Enter += new System.EventHandler(this.usr_Enter);
            this.usr.Leave += new System.EventHandler(this.usr_Leave);
            // 
            // tab3
            // 
            this.tab3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.tab3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tab3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab3.BorderRadius = 0;
            this.tab3.ButtonText = "تسجيل الدخول";
            this.tab3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tab3.DisabledColor = System.Drawing.Color.Gray;
            this.tab3.Iconcolor = System.Drawing.Color.Transparent;
            this.tab3.Iconimage = global::SGMS.Properties.Resources.contacts_filled_100px;
            this.tab3.Iconimage_right = null;
            this.tab3.Iconimage_right_Selected = null;
            this.tab3.Iconimage_Selected = null;
            this.tab3.IconMarginLeft = 0;
            this.tab3.IconMarginRight = 0;
            this.tab3.IconRightVisible = true;
            this.tab3.IconRightZoom = 0D;
            this.tab3.IconVisible = true;
            this.tab3.IconZoom = 40D;
            this.tab3.IsTab = false;
            this.tab3.Location = new System.Drawing.Point(150, 382);
            this.tab3.Name = "tab3";
            this.tab3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab3.OnHoverTextColor = System.Drawing.Color.White;
            this.tab3.selected = false;
            this.tab3.Size = new System.Drawing.Size(168, 48);
            this.tab3.TabIndex = 44;
            this.tab3.Tag = "2";
            this.tab3.Text = "تسجيل الدخول";
            this.tab3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab3.Textcolor = System.Drawing.Color.White;
            this.tab3.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab3.Click += new System.EventHandler(this.tab3_Click);
            // 
            // pass
            // 
            this.pass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.pass.Font = new System.Drawing.Font("Cairo", 10F);
            this.pass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.pass.HintForeColor = System.Drawing.Color.Empty;
            this.pass.HintText = "";
            this.pass.isPassword = false;
            this.pass.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(109)))), ((int)(((byte)(124)))));
            this.pass.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.pass.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(99)))), ((int)(((byte)(124)))));
            this.pass.LineThickness = 4;
            this.pass.Location = new System.Drawing.Point(98, 274);
            this.pass.Margin = new System.Windows.Forms.Padding(4);
            this.pass.Name = "pass";
            this.pass.Size = new System.Drawing.Size(273, 45);
            this.pass.TabIndex = 46;
            this.pass.Text = "كلمة المرور";
            this.pass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.pass.Enter += new System.EventHandler(this.pass_Enter);
            this.pass.Leave += new System.EventHandler(this.pass_Leave);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(472, 503);
            this.Controls.Add(this.pass);
            this.Controls.Add(this.tab3);
            this.Controls.Add(this.usr);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل الدخول";
            this.Load += new System.EventHandler(this.Login_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreLogo)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox StoreLogo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuFlatButton aboutBTN;
        private Bunifu.Framework.UI.BunifuFlatButton AMin;
        private Bunifu.Framework.UI.BunifuFlatButton AClose;
        private Bunifu.Framework.UI.BunifuMaterialTextbox usr;
        private Bunifu.Framework.UI.BunifuFlatButton tab3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox pass;
    }
}