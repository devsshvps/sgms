﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using SGMS.Print;

namespace SGMS
{
    public partial class Reports : UserControl
    {
        int favBAR;
        public Reports()
        {
            InitializeComponent();
            Roles();

        }
        private static List<Item> ItemP = new List<Item>();

        private static List<EditCLHistory> EditCL = new List<EditCLHistory>();

        private static List<EditCUHistory> EditCU = new List<EditCUHistory>();

        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillList();
            FavBar();
            ViewCUgistory();
            ViewCLgistory();
        }
        private void FavBar()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM FavBar where id =@id", dic).ConvertFavBar();
            if (check == null) return;

            foreach (FavBar item in check)
            {
                favBAR = item.Fav;
                if (favBAR == 1)
                {
                    itemsBarcodes.Focus();
                    itemsBarcodes.Select();
                }
            }
        }

        private void Roles()
        {
            if (Login.r12 == "0")
            {
                tabControl1.TabPages.Remove(tabPage1);
            }

            if (Login.r13 == "0")
            {
                tabControl1.TabPages.Remove(tabPage2);
            }

            if (Login.r14 == "0")
            {
                tabControl1.TabPages.Remove(tabPage3);
            }
        }

        // تقارير الأصناف
        private void FillList()
        {
            try
            {
                ItemP.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();

                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 1);
                var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where r =@r", dics).ConvertItems();
                if (result == null) return;
                if (result.Count > 0)
                {
                    itemsNames.DataSource = result;
                    itemsNames.DisplayMember = "Name";
                    itemsNames.ValueMember = "id";

                    foreach (Item item in result)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView1);
                        row.Cells[0].Value = item.BarCode;
                        row.Cells[1].Value = item.Name;
                        row.Cells[2].Value = item.Count;
                        row.Cells[3].Value = item.ItemPrice;
                        row.Cells[4].Value = item.SellPrice;
                        row.Cells[5].Value = item.TotalBuy;
                        row.Cells[6].Value = item.TotalSell;
                        row.Cells[7].Value = item.Earn;
                        row.Cells[8].Value = item.NumOfSales;
                        row.Cells[9].Value = item.NumOfPurc;
                        dataGridView1.Rows.Add(row);

                        var _id = item.id;
                        var _Barcode = item.BarCode;
                        var _name = item.Name;
                        var _price = item.SellPrice;
                        var _count = item.Count;
                        var _TB = item.TotalBuy;
                        var _TS = item.TotalSell;
                        var _E = item.Earn;
                        var _N = item.NumOfSales;
                        var _P = item.NumOfPurc;
                        var p = new Item
                        {
                            id = _id,
                            BarCode = _Barcode,
                            Name = _name,
                            Count = _count,
                            TotalBuy = _TB,
                            ItemPrice = _price,
                            TotalSell = _TS,
                            NumOfSales = _N,
                            NumOfPurc = _P,
                            Earn = _E
                        };
                        ItemP.Add(p);
                    }
                    if (dataGridView1.Rows.Count > 0)
                    {
                        btnPrint.Enabled = true;
                    }
                    else
                    {
                        btnPrint.Enabled = false;
                    }
                }
                btnPrint.LabelText = "طباعة الكل";
                btnPrint.color = Color.FromArgb(18, 119, 165);
                btnPrint.BackColor = Color.FromArgb(18, 119, 165);
                btnPrint.colorActive = Color.FromArgb(18, 119, 200);
            }
            catch { }
        }
        private void AllBTN_Click(object sender, EventArgs e)
        {
            FillList();
        }
        private void NoStock()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var item = ItemP.FindAll(i => i.Count <= 0);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item1.BarCode;
                    row.Cells[1].Value = item1.Name;
                    row.Cells[2].Value = item1.Count;
                    row.Cells[3].Value = item1.ItemPrice;
                    row.Cells[4].Value = item1.SellPrice;
                    row.Cells[5].Value = item1.TotalBuy;
                    row.Cells[6].Value = item1.TotalSell;
                    row.Cells[7].Value = item1.Earn;
                    row.Cells[8].Value = item1.NumOfSales;
                    row.Cells[9].Value = item1.NumOfPurc;
                    dataGridView1.Rows.Add(row);
                }
                btnPrint.LabelText = "طباعة المنتجات التي نفذت";
                btnPrint.color = Color.FromArgb(192, 0, 0);
                btnPrint.BackColor = Color.FromArgb(192, 0, 0);
                btnPrint.colorActive = Color.FromArgb(192, 64, 0);
            }
            catch { }
            if (dataGridView1.Rows.Count > 0)
            {
                btnPrint.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
            }
        }
        private void noStockBTN_Click(object sender, EventArgs e)
        {
            NoStock();
        }
        private void SmallStock()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var item = ItemP.FindAll(i => i.Count <= 10 && i.Count > 0);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item1.BarCode;
                    row.Cells[1].Value = item1.Name;
                    row.Cells[2].Value = item1.Count;
                    row.Cells[3].Value = item1.ItemPrice;
                    row.Cells[4].Value = item1.SellPrice;
                    row.Cells[5].Value = item1.TotalBuy;
                    row.Cells[6].Value = item1.TotalSell;
                    row.Cells[7].Value = item1.Earn;
                    row.Cells[8].Value = item1.NumOfSales;
                    row.Cells[9].Value = item1.NumOfPurc;
                    dataGridView1.Rows.Add(row);
                }
                btnPrint.LabelText = "طباعة المنتجات التي اوشكت علي النفاذ";
                btnPrint.color = Color.Orange;
                btnPrint.BackColor = Color.Orange;
                btnPrint.colorActive = Color.OrangeRed;
            }
            catch { }
            if (dataGridView1.Rows.Count > 0)
            {
                btnPrint.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
            }
        }
        private void StockBTN_Click(object sender, EventArgs e)
        {
            SmallStock();
        }
        private void ItemByName()
        {
            try
            {

                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var id = int.Parse(itemsNames.SelectedValue.ToString());
                var cup = ItemP.Find(i => i.id == id);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = cup.BarCode;
                row.Cells[1].Value = cup.Name;
                row.Cells[2].Value = cup.Count;
                row.Cells[3].Value = cup.ItemPrice;
                row.Cells[4].Value = cup.SellPrice;
                row.Cells[5].Value = cup.TotalBuy;
                row.Cells[6].Value = cup.TotalSell;
                row.Cells[7].Value = cup.Earn;
                row.Cells[8].Value = cup.NumOfSales;
                row.Cells[9].Value = cup.NumOfPurc;
                dataGridView1.Rows.Add(row);

            }
            catch
            {

            }
            if (dataGridView1.Rows.Count > 0)
            {
                btnPrint.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
            }
            btnPrint.LabelText = "طباعة المنتج المحدد بالإسم";
            btnPrint.color = Color.DarkGreen;
            btnPrint.BackColor = Color.DarkGreen;
            btnPrint.colorActive = Color.Green;
        }
        private void ItemByBarCode()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var barcodd = itemsBarcodes.Text;
                var cup = ItemP.Find(i => i.BarCode == barcodd);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = cup.BarCode;
                row.Cells[1].Value = cup.Name;
                row.Cells[2].Value = cup.Count;
                row.Cells[3].Value = cup.ItemPrice;
                row.Cells[4].Value = cup.SellPrice;
                row.Cells[5].Value = cup.TotalBuy;
                row.Cells[6].Value = cup.TotalSell;
                row.Cells[7].Value = cup.Earn;
                row.Cells[8].Value = cup.NumOfSales;
                row.Cells[9].Value = cup.NumOfPurc;
                dataGridView1.Rows.Add(row);

            }
            catch
            {

            }
            if (dataGridView1.Rows.Count > 0)
            {
                btnPrint.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
            }
            btnPrint.LabelText = "طباعة المنتج المحدد بالباركود";
            btnPrint.color = Color.DarkGreen;
            btnPrint.BackColor = Color.DarkGreen;
            btnPrint.colorActive = Color.Green;
        }
        private void itemsBarcodes_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(itemsBarcodes.Text))
            {
                ItemByBarCode();
            }
            else
            {
                FillList();
            }
        }
        private void itemsNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemByName();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (btnPrint.LabelText == "طباعة الكل")
            {
                PrintAll();
            }
            else if (btnPrint.LabelText == "طباعة المنتجات التي نفذت")
            {
                PrintNoStock();
            }
            else if (btnPrint.LabelText == "طباعة المنتجات التي اوشكت علي النفاذ")
            {
                PrintSmallStock();
            }
            else if (btnPrint.LabelText == "طباعة المنتج المحدد بالباركود")
            {
                PrintSpecialBarCode();
            }
            else if (btnPrint.LabelText == "طباعة المنتج المحدد بالإسم")
            {
                PrintSpecialItem();
            }
        }
        private void PrintAll()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("num", 1);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة تقرير جميع الأصناف", "طباعة تقرير الأصناف", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT * FROM Items where r=@num", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة التقرير يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2Item printPurch = new Print2Item();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("Type", "تقرير جميع الأصناف");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void PrintNoStock()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("Count", 0);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة تقرير الأصناف التي نفذت", "طباعة تقرير الأصناف", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where Count =@Count", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة التقرير يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2Item printPurch = new Print2Item();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("Type", "تقرير الأصناف التي نفذت");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void PrintSmallStock()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@Count", 10);
            dic.Add("@No", 0);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة تقرير الأصناف التي اوشكت علي النفاذ", "طباعة تقرير الأصناف", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where Count <=@Count and Count >@No", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة التقرير يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2Item printPurch = new Print2Item();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("Type", "تقرير الأصناف التي اوشكت علي النفاذ");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void PrintSpecialItem()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("id", itemsNames.SelectedValue);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة تقرير الصنف المحدد ؟", "طباعة تقرير الأصناف", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where id =@id", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة التقرير يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2Item printPurch = new Print2Item();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("Type", "تقرير صنف محدد");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void PrintSpecialBarCode()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("BarCode", itemsBarcodes.Text);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة تقرير الصنف المحدد ؟", "طباعة تقرير الأصناف", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where BarCode =@BarCode", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة التقرير يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2Item printPurch = new Print2Item();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("Type", "تقرير صنف محدد");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }


        // تقارير تعديل العملاء
        private void ViewCUgistory()
        {
            try
            {
                bunifuCustomDataGrid2.Rows.Clear();
                bunifuCustomDataGrid2.Refresh();
                EditCU.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 0);
                var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM EditCUHistory where id >@r", dics).ConvertEditCUHistory();
                if (result == null) return;
                if (result.Count > 0)
                {
                    foreach (EditCUHistory item in result)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(bunifuCustomDataGrid2);
                        row.Cells[0].Value = item.EditDate.ToString("yyyy-MM-dd");
                        row.Cells[1].Value = item.EditDate.ToString("t");
                        row.Cells[2].Value = item.From;
                        row.Cells[3].Value = item.To;
                        row.Cells[4].Value = item.Reason;
                        row.Cells[5].Value = item.UserName;
                        row.Cells[6].Value = item.id;
                        bunifuCustomDataGrid2.Rows.Add(row);
                        bunifuCustomDataGrid2.Sort(bunifuCustomDataGrid2.Columns[6], ListSortDirection.Descending);
                        bunifuCustomDataGrid2.ClearSelection();

                        var _Date = item.EditDate;
                        var _From = item.From;
                        var _To = item.To;
                        var _Reason = item.Reason;
                        var _UserName = item.UserName;
                        var _id = item.id;
                        var p = new EditCUHistory
                        {
                            id = _id,
                            EditDate = _Date,
                            From = _From,
                            To = _To,
                            Reason = _Reason,
                            UserName = _UserName
                        };
                        EditCU.Add(p);
                    }
                }
            }
            catch { }
        }

        // تقارير تعديل الموردين
        private void ViewCLgistory()
        {
            try
            {
                EditCL.Clear();
                bunifuCustomDataGrid1.Rows.Clear();
                bunifuCustomDataGrid1.Refresh();

                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 0);
                var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM EditCLHistory where id >@r", dics).ConvertEditCLHistory();
                if (result == null) return;
                if (result.Count > 0)
                {
                    foreach (EditCLHistory item in result)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(bunifuCustomDataGrid1);
                        row.Cells[0].Value = item.EditDate.ToString("yyyy-MM-dd");
                        row.Cells[1].Value = item.EditDate.ToString("t");
                        row.Cells[2].Value = item.From;
                        row.Cells[3].Value = item.To;
                        row.Cells[4].Value = item.Reason;
                        row.Cells[5].Value = item.UserName;
                        row.Cells[6].Value = item.id;
                        bunifuCustomDataGrid1.Rows.Add(row);
                        bunifuCustomDataGrid1.Sort(bunifuCustomDataGrid1.Columns[6], ListSortDirection.Descending);
                        bunifuCustomDataGrid1.ClearSelection();

                        var _Date = item.EditDate;
                        var _From = item.From;
                        var _To = item.To;
                        var _Reason = item.Reason;
                        var _UserName = item.UserName;
                        var _id = item.id;
                        var p = new EditCLHistory
                        {
                            id = _id,
                            EditDate = _Date,
                            From = _From,
                            To = _To,
                            Reason = _Reason,
                            UserName = _UserName
                        };
                        EditCL.Add(p);

                    }
                }
            }
            catch { }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(textBox2.Text))
                {

                    bunifuCustomDataGrid2.Rows.Clear();
                    bunifuCustomDataGrid2.Refresh();

                    var f = textBox2.Text.ToString();
                    var cup = EditCU.Find(i => i.From == f);

                    if(cup != null)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(bunifuCustomDataGrid2);
                        row.Cells[0].Value = cup.EditDate.ToString("yyyy-MM-dd");
                        row.Cells[1].Value = cup.EditDate.ToString("t");
                        row.Cells[2].Value = cup.From;
                        row.Cells[3].Value = cup.To;
                        row.Cells[4].Value = cup.Reason;
                        row.Cells[5].Value = cup.UserName;
                        row.Cells[6].Value = cup.id;
                        bunifuCustomDataGrid2.Rows.Add(row);
                    }

                    var t = textBox2.Text.ToString();
                    var cup2 = EditCU.Find(i => i.To == t);

                    if(cup2 != null)
                    {
                        DataGridViewRow row2 = new DataGridViewRow();
                        row2.CreateCells(bunifuCustomDataGrid2);
                        row2.Cells[0].Value = cup2.EditDate.ToString("yyyy-MM-dd");
                        row2.Cells[1].Value = cup2.EditDate.ToString("t");
                        row2.Cells[2].Value = cup2.From;
                        row2.Cells[3].Value = cup2.To;
                        row2.Cells[4].Value = cup2.Reason;
                        row2.Cells[5].Value = cup2.UserName;
                        row2.Cells[6].Value = cup2.id;
                        bunifuCustomDataGrid2.Rows.Add(row2);
                    }
                    bunifuCustomDataGrid2.Sort(bunifuCustomDataGrid2.Columns[6], ListSortDirection.Descending);
                    bunifuCustomDataGrid2.ClearSelection();

                }
                else
                {
                    ViewCUgistory();
                }
            }
            catch { }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(textBox1.Text))
                {

                    bunifuCustomDataGrid1.Rows.Clear();
                    bunifuCustomDataGrid1.Refresh();

                    var f = textBox1.Text.ToString();
                    var cup = EditCL.Find(i => i.From == f);
                    if (cup != null)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(bunifuCustomDataGrid1);
                        row.Cells[0].Value = cup.EditDate.ToString("yyyy-MM-dd");
                        row.Cells[1].Value = cup.EditDate.ToString("t");
                        row.Cells[2].Value = cup.From;
                        row.Cells[3].Value = cup.To;
                        row.Cells[4].Value = cup.Reason;
                        row.Cells[5].Value = cup.UserName;
                        row.Cells[6].Value = cup.id;
                        bunifuCustomDataGrid1.Rows.Add(row);
                    }

                    var t = textBox1.Text.ToString();
                    var cup2 = EditCL.Find(i => i.To == t);

                    if (cup2 != null)
                    {
                        DataGridViewRow row2 = new DataGridViewRow();
                        row2.CreateCells(bunifuCustomDataGrid1);
                        row2.Cells[0].Value = cup2.EditDate.ToString("yyyy-MM-dd");
                        row2.Cells[1].Value = cup2.EditDate.ToString("t");
                        row2.Cells[2].Value = cup2.From;
                        row2.Cells[3].Value = cup2.To;
                        row2.Cells[4].Value = cup2.Reason;
                        row2.Cells[5].Value = cup2.UserName;
                        row2.Cells[6].Value = cup2.id;
                        bunifuCustomDataGrid1.Rows.Add(row2);
                    }
                    bunifuCustomDataGrid1.Sort(bunifuCustomDataGrid1.Columns[6], ListSortDirection.Descending);
                    bunifuCustomDataGrid1.ClearSelection();


                }
                else
                {
                    ViewCLgistory();
                }
            }
            catch { }
        }
    }
}

