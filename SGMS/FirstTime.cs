﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class FirstTime : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public FirstTime()
        {
            InitializeComponent();

        }
        private void Login_Load(object sender, EventArgs e)
        {
            LoadStoreInfo();
        }

        private void AMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void aboutBTN_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        private static List<Setting> SettingP = new List<Setting>();

        private void LoadStoreInfo()
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id <=@id", dic);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var _id = int.Parse(result.Rows[i]["id"].ToString());
                var _StoreLogo = result.Rows[i]["StoreLogo"].ToString();
                var p = new Setting
                {
                    id = _id,
                    StoreLogo = _StoreLogo,
                };
                SettingP.Add(p);
            }

            int nid = 1;
            var cup = SettingP.Find(i => i.id == nid);
            var nStoreLogo = cup.StoreLogo.ToString();

            StoreLogo.ImageLocation = nStoreLogo;

        }

        private void BTNcheck()
        {
            if (bunifuMaterialTextbox1.Text == "" || bunifuMaterialTextbox1.Text == "الإسم بالكامل" || bunifuMaterialTextbox2.Text == "" ||
                bunifuMaterialTextbox2.Text == "إسم المستخدم" || bunifuMaterialTextbox3.Text == "" || bunifuMaterialTextbox3.Text == "كلمة المرور")
            {
                tab3.Enabled = false;
            }
            else
            {
                tab3.Enabled = true;
            }
        }

        private void bunifuMaterialTextbox1_OnValueChanged(object sender, EventArgs e)
        {
            BTNcheck();
        }

        private void bunifuMaterialTextbox2_OnValueChanged(object sender, EventArgs e)
        {
            BTNcheck();
        }

        private void bunifuMaterialTextbox3_OnValueChanged(object sender, EventArgs e)
        {
            BTNcheck();
        }

        private void tab3_Click(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox2.Text == "" || bunifuMaterialTextbox2.Text == "إسم المستخدم")
            {
                MessageBox.Show("برجاء كتابة إسم المستخدم");
                return;
            }
            else if (bunifuMaterialTextbox1.Text == "" || bunifuMaterialTextbox1.Text == "الإسم بالكامل")
            {
                MessageBox.Show("برجاء كتابة الإسم بالكامل");
                return;
            }
            else if (bunifuMaterialTextbox3.Text == "" || bunifuMaterialTextbox3.Text == "كلمة المرور")
            {
                MessageBox.Show("برجاء كتابة كلمة المرور");
                return;
            }
            else
            {
                var username = bunifuMaterialTextbox2.Text.ToString();
                var password = bunifuMaterialTextbox3.Text.ToString();
                var fullname = bunifuMaterialTextbox1.Text.ToString();
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@username", username);
                addC.Add("@password", password);
                addC.Add("@fullname", fullname);
                addC.Add("@role1", 1);
                addC.Add("@role2", 1);
                addC.Add("@role3", 1);
                addC.Add("@role4", 1);
                addC.Add("@role5", 1);
                addC.Add("@role6", 1);
                addC.Add("@role7", 1);
                addC.Add("@role8", 1);
                addC.Add("@role9", 1);
                addC.Add("@role10", 1);
                addC.Add("@role11", 1);
                addC.Add("@role12", 1);
                addC.Add("@role13", 1);
                addC.Add("@role14", 1);
                addC.Add("@role15", 1);
                addC.Add("@role16", 1);
                addC.Add("@role17", 1);
                addC.Add("@role18", 1);
                addC.Add("@role19", 1);
                addC.Add("@role20", 1);
                addC.Add("@role21", 1);
                addC.Add("@role22", 1);
                addC.Add("@role23", 1);
                addC.Add("@role24", 1);
                addC.Add("@role25", 1);
                addC.Add("@role26", 1);
                addC.Add("@role27", 1);
                addC.Add("@role28", 1);
                addC.Add("@role29", 1);
                addC.Add("@role30", 1);
                var addNotice = DBConnection.SqliteCommand("INSERT  INTO `Login`(`username`, `password`, `fullname`, `role1`, `role2`,`role3`, `role4`, `role5`,`role6`, `role7`, `role8`,`role9`, `role10`, `role11`,`role12`, `role13`, `role14`,`role15`, `role16`, `role17`,`role18`, `role19`, `role20`,`role21`, `role22`, `role23`, `role24`, `role25`, `role26`, `role27`, `role28`, `role29`, `role30`) values(@username,@password,@fullname,@role1,@role2,@role3,@role4,@role5,@role6,@role7,@role8,@role9,@role10,@role11,@role12,@role13,@role14,@role15,@role16,@role17,@role18,@role19,@role20,@role21,@role22,@role23,@role24,@role25,@role26,@role27,@role28,@role29,@role30)", addC);
                MessageBox.Show("تم حفظ البيانات جاري إعادة تشغيل البرنامج");
                Application.Restart();
            }
        }

        private void bunifuMaterialTextbox1_Enter(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox1.Text == "الإسم بالكامل")
            {
                bunifuMaterialTextbox1.Text = "";
            }
        }

        private void bunifuMaterialTextbox1_Leave(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox1.Text == "")
            {
                bunifuMaterialTextbox1.Text = "الإسم بالكامل";
            }
        }

        private void bunifuMaterialTextbox2_Enter(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox2.Text == "إسم المستخدم")
            {
                bunifuMaterialTextbox2.Text = "";
            }
        }

        private void bunifuMaterialTextbox2_Leave(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox2.Text == "")
            {
                bunifuMaterialTextbox2.Text = "إسم المستخدم";
            }
        }

        private void bunifuMaterialTextbox3_Enter(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox3.Text == "كلمة المرور")
            {
                bunifuMaterialTextbox3.Text = "";
            }
        }

        private void bunifuMaterialTextbox3_Leave(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox3.Text == "")
            {
                bunifuMaterialTextbox3.Text = "كلمة المرور";
            }
        }

    }
}

