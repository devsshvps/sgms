using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using SGMS.Print;

namespace SGMS
{

    public partial class UserCash : UserControl
    {
        int ccid, ppid;
        cMessgeBox cmbox;
        double uc;
        public UserCash()
        {
            InitializeComponent();
            Roles();

        }
        private static List<Expense> EX = new List<Expense>();
        private static List<User> UserP = new List<User>();
        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillList();
            FillLoginList();
            LoadMonth();
            UserNam.SelectedIndex = -1;
        }

        private void Roles()
        {

            if (Login.r19 == "0")
            {
                tabControl1.TabPages.Remove(tabPage1);
            }

            if (Login.r20 == "0")
            {
                tabControl1.TabPages.Remove(tabPage2);
            }
        }
        public static string ConvertR(string x)
        {
            string text = x.Replace("956135252", "����� �����");
            text = text.Replace("956135253", "����� ����");
            text = text.Replace("administrator", "-");
            return text.Replace("956135251", "����� �����");
        }
        private void FillList()
        {
            try
            {
                EX.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var result = DBConnection.SqliteCommand("Select * from PExpenses", dic).ConvertExpenses();
                if (result == null) return;
                foreach (Expense item in result)
                {
                    Expense e = new Expense();
                    e.id = item.id;
                    e.UserID = item.UserID;
                    e.UserName = item.UserName;
                    e.Cash = item.Cash;
                    e.TCash = item.TCash;
                    e.Reason = item.Reason;
                    e.LoginID = item.LoginID;
                    e.LoginName = item.LoginName;
                    e.r_Time = item.r_Time;
                    e.r_Date = item.r_Date;
                    e.r = item.r;
                    EX.Add(e);


                }
            }
            catch { }

        }
        private void FillLoginList()
        {
            try
            {
                UserP.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@BlockState", 1);
                var rIt = DBConnection.SqliteCommand("SELECT uid,fullname,Cash  FROM Login where BlockState =@BlockState", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    UserNam.DataSource = rIt;
                    UserNam.DisplayMember = "fullname";
                    UserNam.ValueMember = "uid";


                    for (int i = 0; i < rIt.Rows.Count; i++)
                    {
                        var _id = int.Parse(rIt.Rows[i]["uid"].ToString());
                        var _fullname = rIt.Rows[i]["fullname"].ToString();
                        var _Cash = Double.Parse(rIt.Rows[i]["Cash"].ToString());
                        var p = new User
                        {
                            uid = _id,
                            fullname = _fullname,
                            Cash = _Cash
                        };
                        UserP.Add(p);
                    }
                }
            }
            catch { }

        }
        private void LoadMonth()
        {
            try
            {
                PDatagride.Rows.Clear();
                PDatagride.Refresh();
                var nmonth = DateTime.Now.ToString("yyyy-MM");
                var thism = EX.FindAll(i => i.r_Date.ToString("yyyy-MM") == nmonth);
                double Msum = thism.Sum(i => i.Cash);
                totalmonth.Text = Login.arNumber(Msum.ToString());
                foreach (var item in thism)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = ConvertR(item.r.ToString());
                    row.Cells[1].Value = item.Cash;
                    row.Cells[2].Value = item.TCash;
                    row.Cells[3].Value = ConvertR(item.LoginName);
                    row.Cells[4].Value = item.Reason;
                    row.Cells[5].Value = item.UserName;
                    row.Cells[6].Value = Login.arNumber(item.r_Date.ToString("dd")) + " " +
                        Login.arDate(item.r_Date.ToString("MM")) + " " +
                        Login.arNumber(item.r_Date.ToString("yyyy"));
                    row.Cells[7].Value = Login.arNumber(item.r_Time.ToString("t"));
                    row.Cells[8].Value = item.LoginID;
                    row.Cells[9].Value = item.id;

                    if (item.r == 956135252)
                    {
                        row.Cells[2].Value = "-";
                    }
                    else
                    {
                        row.Cells[2].Value = item.TCash;
                    }
                    if (item.r == 956135251)
                    {
                        row.DefaultCellStyle.Font = new Font("Cairo", 8F, FontStyle.Strikeout);
                    }
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[1], ListSortDirection.Descending);

                }
            }
            catch { }
        }
        private void viewAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                PDatagride.Rows.Clear();
                PDatagride.Refresh();
                var thism = EX.FindAll(i => i.r == 956135252);
                foreach (var item in thism)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = ConvertR(item.r.ToString());
                    row.Cells[1].Value = item.Cash;
                    row.Cells[2].Value = "-";
                    row.Cells[3].Value = "-";
                    row.Cells[4].Value = item.Reason;
                    row.Cells[5].Value = item.UserName;
                    row.Cells[6].Value = Login.arNumber(item.r_Date.ToString("dd")) + " " +
                        Login.arDate(item.r_Date.ToString("MM")) + " " +
                        Login.arNumber(item.r_Date.ToString("yyyy"));
                    row.Cells[7].Value = Login.arNumber(item.r_Time.ToString("t"));
                    row.Cells[8].Value = item.LoginID;
                    row.Cells[9].Value = item.id;
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[1], ListSortDirection.Descending);

                }
            }
            catch { }
        }
        private void viewP_Click(object sender, EventArgs e)
        {
            try
            {
                PDatagride.Rows.Clear();
                PDatagride.Refresh();
                var thism = EX.FindAll(i => i.r == 956135253);
                foreach (var item in thism)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = ConvertR(item.r.ToString());
                    row.Cells[1].Value = item.Cash;
                    row.Cells[2].Value = item.TCash;
                    row.Cells[3].Value = ConvertR(item.LoginName);
                    row.Cells[4].Value = item.Reason;
                    row.Cells[5].Value = item.UserName;
                    row.Cells[6].Value = Login.arNumber(item.r_Date.ToString("dd")) + " " +
                        Login.arDate(item.r_Date.ToString("MM")) + " " +
                        Login.arNumber(item.r_Date.ToString("yyyy"));
                    row.Cells[7].Value = Login.arNumber(item.r_Time.ToString("t"));
                    row.Cells[8].Value = item.LoginID;
                    row.Cells[9].Value = item.id;
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[1], ListSortDirection.Descending);

                }
            }
            catch { }
        }
        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            LoadMonth();
        }
        private void PayP_Click(object sender, EventArgs e)
        {
            if (ccid == 0)
            {
                cmbox = new cMessgeBox("�� ���� ��� ����� �����", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else
            {
                if (uc > 0)
                {
                    PayPE pay = new PayPE(ccid, ppid);
                    pay.FormClosed += payClosed;
                    pay.ShowDialog();
                }
                else
                {
                    cmbox = new cMessgeBox("������� ����� ������", "error", "", 2000);
                    cmbox.ShowDialog();
                }
            }
        }
        private void PDatagride_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in PDatagride.SelectedRows)
                {
                    if (PDatagride.SelectedRows.Count > 0)
                    {
                        ccid = int.Parse(row.Cells[8].Value.ToString());
                        ppid = int.Parse(row.Cells[9].Value.ToString());
                        uc = Double.Parse(row.Cells[1].Value.ToString());
                        PayP.Enabled = true;
                    }
                    else
                    {
                        ccid = 0;
                        ppid = 0;
                        uc = 0;
                        PayP.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            try
            {
                var nmonth = DateTime.Now.ToString("yyyy-MM");
                var f = nmonth + "-01";
                var t = nmonth + "-31";
                var thism = EX.FindAll(i => i.r_Date.ToString("yyyy-MM") == nmonth);

                if (thism.Count > 0)
                {

                    var f1 = "���" + " " + Login.arDate(DateTime.Now.ToString("MM")) + " " + "����" + " " + Login.arNumber(DateTime.Now.ToString("yyyy"));
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    var df = f;
                    var dt = t;

                    var result = DBConnection.SqliteCommand("Select * from PExpenses where r_Date between '" + df + "' and '" + dt + "'", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        Print2PE printPurch = new Print2PE();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("dFrom", f1);
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);
                        cmbox = new cMessgeBox("�� ����� ������� �����", "done", "p", 1000);
                        cmbox.ShowDialog();
                    }


                }
                else
                {
                    cmbox = new cMessgeBox("�� ���� ������ ��� ����� ��������", "error", "", 2000);
                    cmbox.ShowDialog();
                }
            }
            catch { }

        }
        private void AddNewAdmin_Click(object sender, EventArgs e)
        {
            AddAdminEX AddEX = new AddAdminEX();
            AddEX.FormClosed += payClosed;
            AddEX.ShowDialog();
        }
        private void AddNewP_Click(object sender, EventArgs e)
        {
            AddUsernEX AddEX = new AddUsernEX();
            AddEX.FormClosed += payClosed;
            AddEX.ShowDialog();
        }
        private void UserNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserNam.SelectedIndex == -1)
            {
                LoadMonth();
            }
            else
            {
                try
                {
                    PDatagride.Rows.Clear();
                    PDatagride.Refresh();
                    int uud = int.Parse(UserNam.SelectedValue.ToString());
                    var uuui = EX.FindAll(i => i.LoginID == uud);

                    foreach (var item in uuui)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(PDatagride);
                        row.Cells[0].Value = ConvertR(item.r.ToString());
                        row.Cells[1].Value = item.Cash;
                        row.Cells[2].Value = item.TCash;
                        row.Cells[3].Value = ConvertR(item.LoginName);
                        row.Cells[4].Value = item.Reason;
                        row.Cells[5].Value = item.UserName;
                        row.Cells[6].Value = Login.arNumber(item.r_Date.ToString("dd")) + " " +
                            Login.arDate(item.r_Date.ToString("MM")) + " " +
                            Login.arNumber(item.r_Date.ToString("yyyy"));
                        row.Cells[7].Value = Login.arNumber(item.r_Time.ToString("t"));
                        row.Cells[8].Value = item.LoginID;
                        row.Cells[9].Value = item.id;

                        if (item.r == 956135251)
                        {
                            row.DefaultCellStyle.Font = new Font("Cairo", 8F, FontStyle.Strikeout);
                        }

                        PDatagride.Rows.Add(row);
                        PDatagride.Sort(PDatagride.Columns[1], ListSortDirection.Descending);

                    }
                }
                catch { }

            }

        }

        private void spiMonth_Click(object sender, EventArgs e)
        {
            SPIEX SPIEx = new SPIEX();
            SPIEx.ShowDialog();
        }

        void payClosed(object sender, FormClosedEventArgs e)
        {
            FillList();
            LoadMonth();
            FillLoginList();
        }

    }
}