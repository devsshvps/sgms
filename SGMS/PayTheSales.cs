﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class PayTheSales : MetroFramework.Forms.MetroForm
    {
        string InvNNN;
        double uni;
        int cid;
        string finalnumber;
        double clientcash;
        double CashInvDUnpaid;
        string printid;
        public PayTheSales(string InvNN)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            InvNNN = InvNN;
            LoadPay();
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
        private void AddIn_Click(object sender, EventArgs e)
        {
            double cin = Double.Parse(NeedPay.Text);
            if (cin > 0)
            {
                NeedPay.Enabled = false;
                InsertPay();
                updateClient();
                updateCashInvD();
                DialogResult dialogResult = MessageBox.Show("هل تود طباعة إيصال إستلام نقدي ؟", "طباعة إيصيال إستلام", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    try
                    {

                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic.Add("@num", printid);
                        var result = DBConnection.SqliteCommand("SELECT * FROM PaidSales where id=@num", dic);
                        if (result == null) return;
                        if (result.Rows.Count > 0)
                        {
                            string d1 = DateTime.Now.ToString("dd");
                            string m1 = DateTime.Now.ToString("MM");
                            string y1 = DateTime.Now.ToString("yyyy");
                            string ti1 = DateTime.Now.ToString("t");
                            string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                            string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                            string PrintDateTime = "بتاريخ" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                            payInvoiceSales Payinva = new payInvoiceSales();
                            Payinva.SetDataSource(result);
                            Payinva.SetParameterValue("invsName", "إيصال إستلام نقدي");
                            Payinva.SetParameterValue("logoUrl", Dashboard.logopath);
                            Payinva.SetParameterValue("PrintDateTime", PrintDateTime);
                            Payinva.SetParameterValue("name", Dashboard.invCoN);
                            Payinva.SetParameterValue("slug", Dashboard.invCoS);
                            Payinva.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                            Payinva.SetParameterValue("address", Dashboard.invCoA);
                            Payinva.PrintToPrinter(2, false, 0, 0);

                        }

                    }
                    catch
                    {
                    }
                }
                else if (dialogResult == DialogResult.No)
                {

                }
                cMessgeBox box = new cMessgeBox("تمت عملية دفع الغير محصل بنجاح", "done", "p",1000);
                box.ShowDialog();
                this.Close();
            }
            else
            {
                cMessgeBox box = new cMessgeBox("فشلت العملية لعدم تحديد مبلغ", "error", "p",3000);
                box.ShowDialog();
            }

        }
        private void InsertPay()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@InvNumber", InvNNN);
                addC.Add("@Paid", NeedPay.Text);
                addC.Add("@UnPaid", unPaid.Text);
                addC.Add("@ClientID", cid);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientName", invClient.Text);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@PaidDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var addToclients = DBConnection.SqliteCommand("INSERT  INTO `PaidSales`(`InvNumber`, `Paid`, `UnPaid`, `ClientID`, `UserID`, `ClientName`, `UserName`, `PaidDate`) values(@InvNumber,@Paid,@UnPaid,@ClientID,@UserID,@ClientName,@UserName,@PaidDate)", addC);
                getID();

            }
            catch
            {
            }
        }
        private void LoadPay()
        {
            try
            {
                string inv = InvNNN;
                int sNuM = int.Parse(inv.ToString());

                if (sNuM >= 1 && sNuM < 10)
                {
                    finalnumber = "000000" + sNuM.ToString();
                }
                else if (sNuM >= 10 && sNuM < 100)
                {
                    finalnumber = "00000" + sNuM.ToString();
                }
                else if (sNuM >= 100 && sNuM < 1000)
                {
                    finalnumber = "0000" + sNuM.ToString();
                }
                else if (sNuM >= 1000 && sNuM < 10000)
                {
                    finalnumber = "000" + sNuM.ToString();
                }
                else if (sNuM >= 10000 && sNuM < 100000)
                {
                    finalnumber = "00" + sNuM.ToString();
                }
                else if (sNuM >= 100000 && sNuM < 1000000)
                {
                    finalnumber = "0" + sNuM.ToString();
                }
                else if (sNuM >= 1000000)
                {
                    finalnumber = sNuM.ToString();
                }
                InvNum.Text = Login.arNumber(finalnumber);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("num", inv);
                var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@num AND Tax = 0", dic).ConvertCashSales();
                if (invCash == null) return;

                foreach (CashInvS item in invCash)
                {
                    uni = item.unpaid;
                    cid = item.clientID;
                    Aunpaid.Text = item.unpaid.ToString();
                    invClient.Text = item.clientName;
                    invDate.Text = item.invDateTime.ToString("yyyy-MM-dd");
                    NeedPay.Text = item.unpaid.ToString();
                }
            }

            catch
            {
            }
        }
        private void Calculate()
        {
            try
            {
                double needpay = Double.Parse(NeedPay.Text.ToString());
                double unpa = uni - needpay;
                unPaid.Text = unpa.ToString();
            }
            catch
            {

            }
        }
        private void NeedPay_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double needpay = 0;

                if (!string.IsNullOrWhiteSpace(NeedPay.Text))
                {
                    needpay = Double.Parse(NeedPay.Text.ToString());
                    if (needpay > uni)
                    {
                        MessageBox.Show("المبلغ المدفوع أكبر من المبلغ الغير محصل");
                        unPaid.Text = uni.ToString();
                        NeedPay.Text = uni.ToString();

                    }
                    else
                    {
                        AddIn.Enabled = true;
                        Calculate();
                    }
                }
                else
                {
                    unPaid.Text = uni.ToString();
                    AddIn.Enabled = false;
                }
            }
            catch
            {
            }
        }
        private void updateCashInvD()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@invNum", InvNNN);
                var Ccl = DBConnection.SqliteCommand("SELECT *  FROM CashInvS where invNum =@invNum AND Tax = 0", dic);
                if (Ccl == null) return;
                if (Ccl.Rows.Count > 0)
                {
                    for (int i = 0; i < Ccl.Rows.Count; i++)
                    {
                        CashInvDUnpaid = Double.Parse(Ccl.Rows[i]["unpaid"].ToString());
                    }
                    double newunpaid = CashInvDUnpaid - Double.Parse(NeedPay.Text);
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@invNum", InvNNN);
                    addC.Add("@newunpaid", newunpaid);
                    var UpdateCu = DBConnection.SqliteCommand("update CashInvS Set unpaid = @newunpaid where invNum=@invNum AND Tax = 0", addC);
                }

            }
            catch
            {
            }
        }
        private void updateClient()
        {

            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", cid);
                var Ccl = DBConnection.SqliteCommand("SELECT *  FROM Customers where id =@id", dic);
                if (Ccl == null) return;
                if (Ccl.Rows.Count > 0)
                {
                    for (int i = 0; i < Ccl.Rows.Count; i++)
                    {
                        clientcash = Double.Parse(Ccl.Rows[i]["Cash"].ToString());
                    }
                    double newclientcash = clientcash - Double.Parse(NeedPay.Text);
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@cID", cid);
                    addC.Add("@Cash", newclientcash);
                    var UpdateCu = DBConnection.SqliteCommand("update Customers Set Cash = @Cash where id=@cID", addC);
                }

            }
            catch
            {
            }
        }
        private void ViewInvByNum_Click(object sender, EventArgs e)
        {
            SalesInvP viewinv = new SalesInvP(InvNNN,0);
            viewinv.ShowDialog();

        }
        private void getID()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            dic.Add("@r", 1);
            var check = DBConnection.SqliteCommand("SELECT MAX(id) FROM PaidSales", dic);
            if (check == null) return;
            if (check.Rows.Count > 0)
            {
                printid = check.Rows[0]["MAX(id)"].ToString();
            }    
        }
    }
}

