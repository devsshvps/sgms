﻿using Newtonsoft.Json;
using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace SGMS
{
    public partial class SNACCT : Form
    {
        string UserSerial;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        private static List<DE> UDE = new List<DE>();
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public SNACCT()
        {
            InitializeComponent();
        }
        private void AMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void AClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void AToolbar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void aboutBTN_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void tab3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(UName.Text) || string.IsNullOrWhiteSpace(SNAC.Text))
            {
                cMessgeBox cMessge = new cMessgeBox("برجاء ملئ جميع البيانات", "error", "p", 1000);
                cMessge.ShowDialog();
            }
            else
            {
                CHECKAC();
            }
        }

        private void SNACCT_Load(object sender, EventArgs e)
        {
            GetProcessorId();
        }
        private async void CHECKAC()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Activition where id =@id", dic).ConvertSN();
            if (check == null) return;

            foreach (SN item in check)
            {
                if (item.sn1 == "0" && item.PC == "0" && item.PC1 == "0" && item.HDD == "0" && item.HDD1 == "0")
                {
                    string PCN = GetProcessorId().ToString();
                    string HDDN = identifier("Win32_DiskDrive", "SerialNumber");
                    UserSerial = this.convSN(SNAC.Text);
                    var Username = UName.Text;
                    var USERD = API.u("SNActive?" + "SN=" + UserSerial + "&" + "USER=" + Username);
                    if (await USERD == "error")
                    {
                        
                        //
                        HttpResponseMessage response = null;
                        using (var client = new HttpClient())
                        {
                            string json = new JavaScriptSerializer().Serialize(new
                            {
                                SN = UserSerial,
                                USER = Username,
                                PC = PCN,
                                HDD = HDDN
                            });

                            response = client.PostAsync(
                             DBConnection.URL + "error?",
                              new StringContent(json, Encoding.UTF8, "application/json")).Result;
                        }
                        //
                        MessageBox.Show("برجاء التأكد من بيانات التفعيل");
                    }
                    else
                    {
                        UDE = JsonConvert.DeserializeObject<List<DE>>(await USERD);
                        var x = UDE.Find(i => i.SN == UserSerial);
                        var userPC = x.PC;
                        var userPC1 = x.PC1;
                        var UserHDD = x.HDD;
                        var UserHDD1 = x.HDD1;

                        if (userPC == "0" && userPC1 == "0" && UserHDD == "0" && UserHDD1 == "0")
                        {
                            HttpResponseMessage response = null;
                            using (var client = new HttpClient())
                            {
                                string json = new JavaScriptSerializer().Serialize(new
                                {
                                    SN = UserSerial,
                                    PC = PCN,
                                    HDD = HDDN
                                });

                                response = client.PostAsync(
                                 DBConnection.URL + "First?",
                                  new StringContent(json, Encoding.UTF8, "application/json")).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    insertActive();
                                }
                                else
                                {
                                    //
                                    HttpResponseMessage response1 = null;
                                    using (var client1 = new HttpClient())
                                    {
                                        string json1 = new JavaScriptSerializer().Serialize(new
                                        {
                                            SN = UserSerial,
                                            USER = Username,
                                            PC = PCN,
                                            HDD = HDDN
                                        });

                                        response1 = client1.PostAsync(
                                         DBConnection.URL + "error?",
                                          new StringContent(json1, Encoding.UTF8, "application/json")).Result;
                                    }
                                    //
                                    MessageBox.Show("نأسف يوجد خطأ في تفعيل البرنامج برجاء التأكد من إتصالك بالإنترنت أو اتصل بالإدارة");
                                    Application.Exit();
                                }
                            }
                        }
                        else
                        {
                            //
                            HttpResponseMessage response = null;
                            using (var client = new HttpClient())
                            {
                                string json = new JavaScriptSerializer().Serialize(new
                                {
                                    SN = UserSerial,
                                    USER = Username,
                                    PC = PCN,
                                    HDD = HDDN
                                });

                                response = client.PostAsync(
                                 DBConnection.URL + "error?",
                                  new StringContent(json, Encoding.UTF8, "application/json")).Result;
                            }
                            //
                            MessageBox.Show("لا يمكن تفعيل البرنامج - تم إبلاغ إدارة البرنامج بهذا النشاط");
                            Application.Exit();
                        }
                    }
                }
                else
                {
                    //
                    HttpResponseMessage response = null;
                    using (var client = new HttpClient())
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            SN = UserSerial,
                            USER = UName.Text,
                            PC = GetProcessorId().ToString(),
                            HDD = identifier("Win32_DiskDrive", "SerialNumber")
                        });

                        response = client.PostAsync(
                         DBConnection.URL + "error?",
                          new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    }
                    //
                    MessageBox.Show("لا يمكن تفعيل البرنامج - تم إبلاغ إدارة البرنامج بهذا النشاط");
                    Close();
                }

            }
        }

        public static string GetProcessorId()
        {

            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();
            String Id = String.Empty;
            foreach (ManagementObject mo in moc)
            {

                Id = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return Id;

        }

        private string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        private void insertActive()
        {
            string PCN = GetProcessorId().ToString();
            string HDDN = identifier("Win32_DiskDrive", "SerialNumber");
            Dictionary<string, object> dico = new Dictionary<string, object>();
            dico.Add("@sn1", UserSerial);
            dico.Add("@pc", PCN);
            dico.Add("@hdd", HDDN);
            dico.Add("@id", 1);
            dico.Add("ActiveDate", DateTime.Now.ToString("yyyy-MM-dd"));
            var ACTAPP = DBConnection.SqliteCommand("update Activition Set sn1 = @sn1, PC = @pc, HDD =@hdd, ActiveDate =@ActiveDate where id =@id", dico);

            FirstTime FT = new FirstTime();
            FT.Show();
            this.Hide();
        }

        private string convSN(string x)
        {
            string text = x.Replace("A", "11");
            text = text.Replace("B", "12");
            text = text.Replace("C", "13");
            text = text.Replace("D", "14");
            text = text.Replace("E", "15");
            text = text.Replace("F", "16");
            text = text.Replace("G", "17");
            text = text.Replace("H", "18");
            text = text.Replace("I", "19");
            text = text.Replace("J", "20");
            text = text.Replace("K", "10");
            text = text.Replace("L", "30");
            text = text.Replace("a", "40");
            text = text.Replace("b", "45");
            text = text.Replace("v", "50");
            return text.Replace("z", "90");
        }
    }
}
