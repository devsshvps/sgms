﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using System.Collections;

namespace SGMS
{
    public partial class PurchaseClients : UserControl
    {
        string CLID;
        string ClName;
        string ClPhone;
        string EditID;
        string EditName;
        string EditPhone;
        public PurchaseClients()
        {

            InitializeComponent();
            LoadAll();

        }
        private void PurchaseClients_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadAll();
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAll();
        }
        private static List<Client> CustomerP = new List<Client>();
        private void AddNew_Click(object sender, EventArgs e)
        {
            FastAddUser fastuser = new FastAddUser();
            fastuser.FormClosed += FastAddUserClosed;
            fastuser.ShowDialog();
        }
        void FastAddUserClosed(object sender, FormClosedEventArgs e)
        {
            LoadAll();
        }
        private void LoadAll()
        {
            try
            {
                CustomerP.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                CustomerByP.Text = "";

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@r", "1");
                var result = DBConnection.SqliteCommand("SELECT DISTINCT * FROM Clients where id > @r", dic).ConvertClients();
                if (result == null) return;
                if (result.Count > 0)
                    foreach (Client item in result) 
                    {
                        CustomerN.DataSource = result;
                        CustomerN.DisplayMember = "Name";
                        CustomerN.ValueMember = "id";
                        CustomerN.SelectedIndex = -1;

                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView1);
                        row.Cells[0].Value = item.Name;
                        row.Cells[1].Value = item.Phone;
                        row.Cells[2].Value = item.Cash;
                        row.Cells[3].Value = item.id;
                        dataGridView1.Rows.Add(row);

                        int _id = item.id;
                        string _Name = item.Name;
                        var _Phone = item.Phone;
                        double _Cash = item.Cash;
                        var p = new Client
                        {
                            id = _id,
                            Name = _Name,
                            Phone = _Phone,
                            Cash = _Cash,
                        };
                        CustomerP.Add(p);
                    }

            }
            catch
            {

            }
        }
        private void CustomerByPhone()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var phone = CustomerByP.Text.ToString();
                var cup = CustomerP.Find(i => i.Phone == phone);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = cup.Name;
                row.Cells[1].Value = cup.Phone;
                row.Cells[2].Value = cup.Cash;
                row.Cells[3].Value = cup.id;
                dataGridView1.Rows.Add(row);

            }
            catch
            {

            }
        }
        private void CustomerByName()
        {
            try
            {

                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var id = int.Parse(CustomerN.SelectedValue.ToString());
                var cup = CustomerP.Find(i => i.id == id);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = cup.Name;
                row.Cells[1].Value = cup.Phone;
                row.Cells[2].Value = cup.Cash;
                row.Cells[3].Value = cup.id;
                dataGridView1.Rows.Add(row);
            }
            catch
            {

            }
        }
        private void CustomerN_SelectedIndexChanged(object sender, EventArgs e)
        {
            CustomerByName();
        }
        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            LoadAll();
        }
        private void CustomerByCash()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var item = CustomerP.FindAll(i => i.Cash > 0);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item1.Name;
                    row.Cells[1].Value = item1.Phone;
                    row.Cells[2].Value = item1.Cash;
                    row.Cells[3].Value = item1.id;
                    dataGridView1.Rows.Add(row);
                }

            }
            catch
            {

            }
        }
        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            CustomerByCash();
        }
        private void CustomerByP_TextChanged_1(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(CustomerByP.Text))
            {
                LoadAll();
            }
            else
            {
                CustomerByPhone();
            }
        }
        private void viewUnPaid_Click(object sender, EventArgs e)
        {
            ViewClientInvs viewallinv = new ViewClientInvs(CLID, ClName, ClPhone);
            viewallinv.FormClosed += ViewClientClosed;
            viewallinv.ShowDialog();

        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {
                        CLID = row.Cells[3].Value.ToString();
                        ClName = row.Cells[0].Value.ToString();
                        ClPhone = row.Cells[1].Value.ToString();
                        viewUnPaid.Enabled = true;
                        editC.Enabled = true;
                    }
                    else
                    {
                        CLID = "";
                        ClName = "";
                        ClPhone = "";
                        viewUnPaid.Enabled = false;
                        editC.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void editC_Click(object sender, EventArgs e)
        {
            EditClients EditClients = new EditClients(CLID, ClName, ClPhone);
            EditClients.FormClosed += EditClientsClosed;
            EditClients.ShowDialog();
        }
        void ViewClientClosed(object sender, FormClosedEventArgs e)
        {
            LoadAll();
        }
        void EditClientsClosed(object sender, FormClosedEventArgs e)
        {
            LoadAll();
        }

    }
}
