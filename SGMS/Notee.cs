﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class Notee : Form
    {
       
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public Notee()
        {
            InitializeComponent();

        }
        private void Login_Load(object sender, EventArgs e)
        {
            LoadStoreInfo();
            LoadNotes();
        }

        private void AClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void AMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void aboutBTN_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Hide();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private static List<Setting> SettingP = new List<Setting>();

        private void LoadStoreInfo()
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id <=@id", dic);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var _id = int.Parse(result.Rows[i]["id"].ToString());
                var _StoreLogo = result.Rows[i]["StoreLogo"].ToString();
                var p = new Setting
                {
                    id = _id,
                    StoreLogo = _StoreLogo,
                };
                SettingP.Add(p);
            }

            int nid = 1;
            var cup = SettingP.Find(i => i.id == nid);
            var nStoreLogo = cup.StoreLogo.ToString();

            StoreLogo.ImageLocation = nStoreLogo;

        }

        private void LoadNotes()
        {
            Dictionary<string, object> prams = new Dictionary<string, object>();
            prams.Add("@id", 1);
            var Note = DBConnection.SqliteCommand("select * from AdminNotes where id=@id", prams).ConvertNotes();
            if (Note.Count > 0)
            {
                string aNote = Note[0].Note;
                string aName = Note[0].User;
                DateTime ct_at = Note[0].ct_at;
                DateTime ti_at = Note[0].ti_at;
                label3.Text = aNote;
                addName.Text = aName;
                label5.Text = ct_at.ToString("yyyy-MM-dd");
                label4.Text = ti_at.ToString("t");


            }
            else
            {

            }
        }
    }
}

