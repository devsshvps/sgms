﻿using Newtonsoft.Json;
using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Management;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace SGMS
{
    public partial class Splash : Form
    {
        private System.Windows.Forms.Timer tmr = new System.Windows.Forms.Timer();
        string SNN,DeleteFile;
        string S = @"""""";

        private static List<DE> UDE = new List<DE>();
        private static List<Item> III = new List<Item>();

        public Splash()
        {
            InitializeComponent();
            var appVersion = Assembly.GetExecutingAssembly().GetName().Version;

            III = DB.SELECT<Item>("Items");
            if(III.Count>0)
            {
                var x = III.Find(s => s.id == 1);
                MessageBox.Show(x.Name);
            }
        }
        public Splash(string x)
        {
            InitializeComponent();
            var appVersion = Assembly.GetExecutingAssembly().GetName().Version;
            DeleteFile = x;
            InsertUpdate();
            DeleteUpdate();

        }
        private void InsertUpdate()
        {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@ver", "1.7.2020.0");
            //var u1 = DBConnection.SqliteCommand("ALTER TABLE Offers ADD r1 VARCHAR NOT NULL DEFAULT" + S, dic);
            //var u2 = DBConnection.SqliteCommand("ALTER TABLE Offers ADD r2 VARCHAR NOT NULL DEFAULT" + S, dic);
            //var u3 = DBConnection.SqliteCommand("ALTER TABLE Offers ADD r3 VARCHAR NOT NULL DEFAULT" + S, dic);
            //var u4 = DBConnection.SqliteCommand("ALTER TABLE Offers ADD r4 VARCHAR NOT NULL DEFAULT" + S, dic);
            //var u5 = DBConnection.SqliteCommand("ALTER TABLE Offers ADD r5 VARCHAR NOT NULL DEFAULT" + S, dic);
            //var u6 = DBConnection.SqliteCommand("ALTER TABLE Offers ADD p1 VARCHAR NOT NULL DEFAULT" + S, dic);

            var uP = DBConnection.SqliteCommand("UPDATE Activition SET ver = @ver WHERE id = 1", dic);
            var u1 = DBConnection.SqliteCommand("ALTER TABLE CashInvS ADD Tax INTEGER NOT NULL DEFAULT 0", dic);
            var u2 = DBConnection.SqliteCommand("CREATE TABLE TaxInfo(id INTEGER PRIMARY KEY,TaxNum VARCHAR NOT NULL,CoRecord VARCHAR NOT NULL)", dic);
            var u2x = DBConnection.SqliteCommand("INSERT  INTO `TaxInfo`(`id`,`TaxNum`, `CoRecord`) values(1,0,0)", dic);
            var u3 = DBConnection.SqliteCommand("CREATE TABLE TaxSells (id INTEGER PRIMARY KEY AUTOINCREMENT ,Num INTEGER, BarCode VARCHAR,itemname VARCHAR ,count DOUBLE, itemprice DOUBLE, buyprice DOUBLE, totalprice DOUBLE,sellforid  INTEGER CONSTRAINT [0] NOT NULL DEFAULT (0), sellfor VARCHAR CONSTRAINT نقدي NOT NULL DEFAULT نقدي, Userid INTEGER, User VARCHAR, ddate DATE CONSTRAINT [CURRENT_DATE] NOT NULL DEFAULT (CURRENT_DATE), ttime TIME DEFAULT (CURRENT_TIME) CONSTRAINT [CURRENT_TIME] NOT NULL, r INTEGER DEFAULT (1) CONSTRAINT [1] NOT NULL, earn DOUBLE, IsTax INTEGER DEFAULT (0) CONSTRAINT [0] NOT NULL)", dic);
            var u3x = DBConnection.SqliteCommand("INSERT  INTO `TaxSells`(`id`,`Num`,`earn`) values(1,0,0)", dic);

        }
        private void DeleteUpdate()
        {
            File.Delete(DeleteFile);
            string ExFolder = System.IO.Path.GetFileNameWithoutExtension(DeleteFile);
            var dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "/" + ExFolder);
            dir.Attributes = dir.Attributes & ~FileAttributes.ReadOnly;
            dir.Delete(true);
            cMessgeBox cMessge = new cMessgeBox("تم تحديث الإصدار بنجاح","done","p",1000);
            cMessge.ShowDialog();
            UpdateInfo Ui = new UpdateInfo();
            Ui.ShowDialog();
        }
        private void Splash_Load(object sender, EventArgs e)
        {
        }
        private void Splash_Shown(object sender, EventArgs e)
        {
            tmr.Start();
            tmr.Interval = 1000;
            tmr.Tick += tmr_Tick;
        }
        void tmr_Tick(object sender, EventArgs e)

        {
            GetProcessorId();
            CHECKAC();
        }
        private void Start()
        {

            tmr.Stop(); 
            try
            {
                Application.EnableVisualStyles();
                Application.Run(new Login());
            }
            catch(Exception ex)
            {
            }
        }
        private void Activition()
        {
            Application.EnableVisualStyles();
            Application.Run(new SNACCT());
        }
        private async void CHECKAC()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Activition where id =@id", dic).ConvertSN();
            if (check == null) return;

            foreach (SN item in check)
            {

                var pc = item.PC.ToString();
                var hhd = item.HDD.ToString();
                var pc1 = item.PC1.ToString();
                var hhd1 = item.HDD1.ToString();
                var cpuName = GetProcessorId();
                var hddName = identifier("Win32_DiskDrive", "SerialNumber");
                SNN = item.sn1;

                if ((pc == cpuName && hhd == hddName)|| (pc1 == cpuName && hhd1 == hddName))
                {
                    tmr.Stop();
                    this.Hide();
                    new Login().ShowDialog();

                }
                
                else if (pc == "0" && hhd == "0")
                {
                    tmr.Stop();
                    Thread _str = new Thread(Activition);
                    _str.SetApartmentState(ApartmentState.STA);
                    _str.Start();
                    Close();
                }
                else if (pc1 == "0" && hhd1 == "0")
                {
                    string PCN = GetProcessorId().ToString();
                    string HDDN = identifier("Win32_DiskDrive", "SerialNumber");
                    try
                    {
                        var USERD = API.u("user?" + "SN=" + SNN);
                        if(await USERD == "error")
                        {
                            HttpResponseMessage response = null;
                            using (var client = new HttpClient())
                            {
                                string json = new JavaScriptSerializer().Serialize(new
                                {
                                    SN = SNN,
                                    USER = "تجربة شاشة Splash",
                                    PC = GetProcessorId().ToString(),
                                    HDD = identifier("Win32_DiskDrive", "SerialNumber")
                                });

                                response = client.PostAsync(
                                 DBConnection.URL + "error?",
                                  new StringContent(json, Encoding.UTF8, "application/json")).Result;
                            }

                            MessageBox.Show("نأسف يوجد خطأ في تفعيل النسخة الثانوية للبرنامج برجاء التأكد من إتصالك بالإنترنت أو اتصل بالإدارة");
                            Application.Exit();
                        }
                        else
                        {
                            UDE = JsonConvert.DeserializeObject<List<DE>>(await USERD);
                            var x = UDE.Find(i => i.SN == SNN);
                            var PC1U = x.PC1;
                            var HDD1U = x.HDD1;
                            if (PC1U == "0" && HDD1U == "0")
                            {
                                HttpResponseMessage response = null;
                                //
                                using (var client = new HttpClient())
                                {
                                    string json = new JavaScriptSerializer().Serialize(new
                                    {
                                        SN = SNN,
                                        PC1 = cpuName,
                                        HDD1 = hddName
                                    });

                                    response = client.PostAsync(
                                     DBConnection.URL + "Second?",
                                      new StringContent(json, Encoding.UTF8, "application/json")).Result;
                                    if (response.IsSuccessStatusCode)
                                    {
                                        Dictionary<string, object> dico = new Dictionary<string, object>();
                                        dico.Add("@pc1", PCN);
                                        dico.Add("@hdd1", HDDN);
                                        dico.Add("@id", 1);
                                        var ACTAPP = DBConnection.SqliteCommand("update Activition Set  PC1 = @pc1, HDD1 =@hdd1  where id =@id", dico);

                                        tmr.Stop();
                                        cMessgeBox cMessge = new cMessgeBox("تم تفعيل النسخة الثانوية بنجاح", "done", "p", 1000);
                                        cMessge.ShowDialog();
                                        //tmr.Stop();
                                        //Thread _str = new Thread(Start);
                                        //  _str.SetApartmentState(ApartmentState.STA);
                                        //  _str.Start();
                                        //  Close();
                                        Start(); return;

                                    }
                                    else
                                    {
                                        HttpResponseMessage response1 = null;
                                        using (var client1 = new HttpClient())
                                        {
                                            string json1 = new JavaScriptSerializer().Serialize(new
                                            {
                                                SN = SNN,
                                                USER = "تجربة شاشة Splash",
                                                PC = GetProcessorId().ToString(),
                                                HDD = identifier("Win32_DiskDrive", "SerialNumber")
                                            });

                                            response1 = client1.PostAsync(
                                             DBConnection.URL + "error?",
                                              new StringContent(json1, Encoding.UTF8, "application/json")).Result;
                                        }

                                        tmr.Stop();
                                        MessageBox.Show("نأسف يوجد خطأ في تفعيل النسخة الثانوية للبرنامج برجاء التأكد من إتصالك بالإنترنت أو اتصل بالإدارة");
                                        Application.Exit();
                                    }
                                }
                            }else if (PC1U == PCN && HDD1U == HDDN)
                            {
                                Dictionary<string, object> dico = new Dictionary<string, object>();
                                dico.Add("@pc1", PCN);
                                dico.Add("@hdd1", HDDN);
                                dico.Add("@id", 1);
                                var ACTAPP = DBConnection.SqliteCommand("update Activition Set  PC1 = @pc1, HDD1 =@hdd1  where id =@id", dico);

                                //tmr.Stop();
                                //Thread _str = new Thread(Start);
                                //  _str.SetApartmentState(ApartmentState.STA);
                                //  _str.Start();
                                //  Close();
                                Start(); return;

                            }
                            else
                            {
                                HttpResponseMessage response = null;
                                using (var client = new HttpClient())
                                {
                                    string json = new JavaScriptSerializer().Serialize(new
                                    {
                                        SN = SNN,
                                        USER = "تجربة شاشة Splash جهاز ثانوي",
                                        PC = GetProcessorId().ToString(),
                                        HDD = identifier("Win32_DiskDrive", "SerialNumber")
                                    });

                                    response = client.PostAsync(
                                     DBConnection.URL + "error?",
                                      new StringContent(json, Encoding.UTF8, "application/json")).Result;
                                }
                                tmr.Stop();
                                MessageBox.Show("نأسف لا يمكنك إستخدام البرنامج برجاء التواصل مع الإدارة");
                                Application.Exit();
                            }
                        }
                    }
                    catch { }
                }
                else
                {
                    HttpResponseMessage response = null;
                    using (var client = new HttpClient())
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            SN = SNN,
                            USER = "تجربة شاشة Splash",
                            PC = GetProcessorId().ToString(),
                            HDD = identifier("Win32_DiskDrive", "SerialNumber")
                        });

                        response = client.PostAsync(
                         DBConnection.URL + "error?",
                          new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    }
                    tmr.Stop();
                    MessageBox.Show("نأسف لا يمكنك إستخدام التطبيق برجاء التواصل مع الإدارة");
                    Application.Exit();
                }
            }

        }
        public static String GetProcessorId()
        {

            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();
            String Id = String.Empty;
            foreach (ManagementObject mo in moc)
            {

                Id = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return Id;

        }
        private string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }
    }

}



