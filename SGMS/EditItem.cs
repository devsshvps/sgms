﻿using SGMS.BarCodeGE;
using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SGMS
{

    public partial class EditItem : MetroFramework.Forms.MetroForm
    {
        string BrCo, Check12Digits, BarCode;
        double SP;
        cMessgeBox cmbox;
        public EditItem(string BarCode1)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            BrCo = BarCode1;
            BarCode = BarCode1;
            LoadList();

        }
        private static List<Item> itemss = new List<Item>();
        private void edit_Click(object sender, EventArgs e)
        {

            if(double.Parse(iPrice.Text) == 0 || string.IsNullOrWhiteSpace(iPrice.Text))
            {
                cmbox = new cMessgeBox("لا يمكنك ترك سعر المنتج فارغ", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else if(double.Parse(iPrice.Text) == SP)
            {
                cmbox = new cMessgeBox("لم تقم بتغيير السعر للمنتج", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else
            {
                updatename();
                cmbox = new cMessgeBox("تم تعديل سعر المنتج بنجاح", "done", "p", 1000);
                cmbox.ShowDialog();
                this.Close();
            }
        }
        private void LoadList()
        {
            itemss.Clear();
            try
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 0);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT * FROM  Items where id > @r", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    for (int i = 0; i < rIt.Rows.Count; i++)
                    {
                        var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                        var _Name = rIt.Rows[i]["Name"].ToString();
                        var _BarCode = rIt.Rows[i]["BarCode"].ToString();
                        var _Count = rIt.Rows[i]["Count"].ToString();
                        var _SellPrice = rIt.Rows[i]["SellPrice"].ToString();
                        var p = new Item
                        {
                            id = _id,
                            Name = _Name,
                            BarCode = _BarCode,
                            Count = Double.Parse(_Count),
                            SellPrice = Double.Parse(_SellPrice)
                        };
                        itemss.Add(p);
                    }
                    FillData();
                }
            }
            catch { }
        }
        private void FillData()
        {
            var ccid = itemss.Find(i => i.BarCode == BarCode);
                var IN = ccid.Name;
                SP = ccid.SellPrice;
                string brCode = BrCo.Substring(0, (BrCo.Length - 1));
                Check12Digits = brCode.PadRight(12, '0');
                BrCo = EAN13Class.EAN13(Check12Digits);
                BarCodeLabel.Text = BrCo;
                iName.Text = IN;
                iPrice.Text = SP.ToString();


        }

        private void EditItem_Shown(object sender, EventArgs e)
        {
            var ccid = itemss.Find(i => i.BarCode == BarCode);
            if (ccid.Count <= 0)
            {
                cmbox = new cMessgeBox("لا يوجد كمية متوفرة من المنتج", "error", "p", 1500);
                cmbox.ShowDialog();
                Close();
            }
        }

        private void updatename()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@SellPrice", iPrice.Text);
                addC.Add("@BarCode", BarCode);
                var UpdatePr = DBConnection.SqliteCommand("update Items Set SellPrice = @SellPrice where BarCode=@BarCode", addC);
                itemss.Clear();
            }
            catch { }
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            System.Windows.Forms.TextBox txtDecimal = sender as System.Windows.Forms.TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
    }
}

