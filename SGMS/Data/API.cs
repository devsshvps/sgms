﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Data
{
    public static class API
    {
       static string readapi;
        public static async Task<string> u(string t)
        {
            HttpClient clint = new HttpClient();
            clint.BaseAddress = new Uri(DBConnection.URL);
            HttpResponseMessage response = clint.GetAsync(t).Result;
            if (response.IsSuccessStatusCode)
            {
                readapi = await response.Content.ReadAsStringAsync();
            }
            else
            {
                readapi = "error";
            }
            return readapi;
        }

        public static async Task<bool> IsConnected(string url = null)
        {
            try
            {

                var client = new HttpClient();
                if (string.IsNullOrEmpty(url))
                {
                    url = "http://google.com";
                }
                var result = await client.GetAsync(url);
                return result.IsSuccessStatusCode;

            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
