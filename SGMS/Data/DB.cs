﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS.Data
{
    public static class DB
    {
        // SELECT
        public static DataTable SELECT(string All, string Table)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "SELECT " + All + " FROM " + Table;
                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }
            // wait
        }
        public static DataTable SELECT(string All,string Table, string where)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "SELECT " + All + " From " + Table + " WHERE " + where;
                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }
        public static DataTable SELECT(string All,string Table, string Commands, Dictionary<string, string> prams)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "SELECT " + All + "From " + Table + " WHERE " + Commands;
                foreach (var item in prams)
                    cmd.Parameters.AddWithValue(item.Key, item.Value);

                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }

        public static DataTable SELECT(string commandText, Dictionary<string, object> prams)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = commandText;
                foreach (var item in prams)
                    cmd.Parameters.AddWithValue(item.Key, item.Value);
                DBConnection.DBOpen(con);


                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }
        public static DataTable SELECT(string commandText)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = commandText;
                DBConnection.DBOpen(con);


                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }

        public static List<T> SELECT<T>(string Table, string All = "*")
        {
            List<T> data = new List<T>();
            var xT = SELECT(All, Table);
            if (xT.Rows.Count > 0)
            {
                foreach (DataRow row in xT.Rows)
                {
                    T item = GetItem<T>(row);
                    data.Add(item);
                }
                return data;
            }
            else
            {
                return default; //دا الصح 
            }
        }
        public static List<T> SELECT<T>(string Table, string where, string All = "*")
        {
            List<T> data = new List<T>();
            var xT = SELECT(All,Table, where);
            if (xT.Rows.Count > 0)
            {
                foreach (DataRow row in xT.Rows)
                {
                    T item = GetItem<T>(row);
                    data.Add(item);
                }
                return data;
            }
            else
            {
                return null;
            }
        }
        public static List<T> SELECT<T>(string Table, string Command, Dictionary<string, string> prams, string All = "*")
        {
            List<T> data = new List<T>();
            var xT = SELECT(All,Table, Command, prams);
            if (xT.Rows.Count > 0)
            {
                foreach (DataRow row in xT.Rows)
                {
                    T item = GetItem<T>(row);
                    data.Add(item);
                }
                return data;
            }
            else
            {
                return null;
            }
        }


        // INSERT
        public static DataTable INSERT(string Table, string column, string value)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "INSERT INTO " + Table + " (" + column + ") " + " VALUES " + "(" + value + " )";
                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }
        public static DataTable INSERT(string Table, string column, string value, Dictionary<string, string> prams)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);

                cmd.CommandText = "INSERT INTO " + Table + " (" + column + " )" + " VALUES " + "(" + value + " )";

                foreach (var item in prams)
                    cmd.Parameters.AddWithValue(item.Key, item.Value);

                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }


        // UPDATE
        public static DataTable UPDATE(string Table, string Command, string where)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "UPDATE " + Table + " SET " + Command + " WHERE " + where;
                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }
        public static DataTable UPDATE(string Table, string Command, string where, Dictionary<string, string> prams)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "UPDATE " + Table + " SET " + Command + " WHERE " + where;
                foreach (var item in prams)
                    cmd.Parameters.AddWithValue(item.Key, item.Value);
                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }


        // DELETE
        public static DataTable DELETE(string Table, string where)
        {
            var con = DBConnection.connection();

            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "DELETE FROM " + Table + " WHERE " + where;
                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }
        public static DataTable DELETE(string Table, string Commands, Dictionary<string, string> prams)
        {
            var con = DBConnection.connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = "DELETE FROM " + Table + " WHERE " + Commands;
                foreach (var item in prams)
                    cmd.Parameters.AddWithValue(item.Key, item.Value);

                DBConnection.DBOpen(con);
                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBConnection.DBClose(con);
                return data;
            }
            catch
            {
                DBConnection.DBClose(con);
                return null;
            }

        }








        // References
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            PropertyInfo[] properties = temp.GetProperties();
            foreach (DataColumn column in dr.Table.Columns)
            {
                var prop = properties.FirstOrDefault(x => x.Name == column.ColumnName);
                if(column.DataType.Name==nameof(Int64) )
                    prop.SetValue(obj,int.Parse(dr[column.ColumnName].ToString()), null);
                else
                    prop.SetValue(obj, dr[column.ColumnName], null); 

            }
            return obj;
        }
    }
}

//مش مشكلةني مش عارف الموضوع جي منين بظبط بس كدا الكود بقي في تشك ودا مش لطيف لان كمية الداتا كتير 
// ما عشان كدا انا شيلت الفورتش اللي كانت في الكود اصلا 