﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Data
{
    public class DBConnection
    {
        private static string pewsss = "111710112020";
        private static string DBSe = "fi3201e:11ys45e3203.11i3201ve21";
        private static string DbSe = "I";
        public static string URL = "https://sgms.silver-eg.com/1.0/";
        public static SQLiteConnection connection()
        {
            try
            {
                var oneone = x12(pewsss);
                var onetow = x13(DBSe);
                var vs = x13(DbSe);
                string wecom = "FullUri = '"+onetow+"'; Version = '"+vs+"'; Password = '"+oneone+"'";
                var conn = wecom;
                return new SQLiteConnection(conn);

            }
            catch
            {
                return null;
            }
        }
        public static bool DBOpen(SQLiteConnection con)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                    return true;
                con.Open();
                return true;
            }
            catch (Exception)
            {

                return false;

            }

        }
        public static bool DBClose(SQLiteConnection con)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                    return true;

                con.Close();
                return true;
            }
            catch (Exception)
            {

                return false;

            }

        }
        public static DataTable SqliteCommand(string commandText, Dictionary<string, object> prams = null)
        {
            var con = connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = commandText;
                foreach (var item in prams)
                    cmd.Parameters.AddWithValue(item.Key, item.Value);
                DBOpen(con);


                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBClose(con);
                return data;
            }
            catch
            {
                DBClose(con);
                return null;
            }

        }
        public static DataTable SqliteCommand(string commandText)
        {
            var con = connection();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(con);
                cmd.CommandText = commandText;
                DBOpen(con);


                SQLiteDataAdapter SqlDataAdapter = new SQLiteDataAdapter(cmd);
                var data = new DataTable();
                SqlDataAdapter.Fill(data);
                DBClose(con);
                return data;
            }
            catch
            {
                DBClose(con);
                return null;
            }

        }
        public static string x12(string x)
        {
            string text = x.Replace("11", "S");
            text = text.Replace("B", "11");
            text = text.Replace("30", "C");
            text = text.Replace("D", "10");
            text = text.Replace("16", "E");
            text = text.Replace("X", "16");
            text = text.Replace("17", "G");
            text = text.Replace("10", "M");
            text = text.Replace("I", "19");
            text = text.Replace("21", "r");
            text = text.Replace("3203", "m");
            text = text.Replace("L", "30");
            text = text.Replace("a", "40");
            text = text.Replace("45", "t");
            text = text.Replace("3201", "l");
            return text.Replace("90", "u");
        }
        public static string x13(string x)
        {
            string text = x.Replace("11", "S");
            text = text.Replace("B", "11");
            text = text.Replace("30", "C");
            text = text.Replace("D", "10");
            text = text.Replace("16", "E");
            text = text.Replace("X", "16");
            text = text.Replace("17", "G");
            text = text.Replace("10", "M");
            text = text.Replace("I", "3");
            text = text.Replace("21", "r");
            text = text.Replace("3203", "m");
            text = text.Replace("L", "30");
            text = text.Replace("a", "40");
            text = text.Replace("45", "t");
            text = text.Replace("3201", "l");
            return text.Replace("90", "u");
        }


    }
}
