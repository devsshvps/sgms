﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;

namespace SGMS
{

    public partial class EditClients : MetroFramework.Forms.MetroForm
    {
        string cid;
        string cname;
        string cphone;
        cMessgeBox cmbox;
        public EditClients(string xid, string xname, string xphone)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            cid = xid;
            cname = xname;
            cphone = xphone;
            LoadList();

        }
        private static List<Client> CList = new List<Client>();
        private void LoadList()
        {
            CList.Clear();
            try
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 1);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT * FROM  Clients where id > @r", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    for (int i = 0; i < rIt.Rows.Count; i++)
                    {
                        var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                        var _Name = rIt.Rows[i]["Name"].ToString();
                        var _Phone = rIt.Rows[i]["Phone"].ToString();
                        var _Cash = rIt.Rows[i]["Cash"].ToString();
                        var p = new Client
                        {
                            id = _id,
                            Name = _Name,
                            Phone = _Phone,
                            Cash = Double.Parse(_Cash),
                        };
                        CList.Add(p);
                    }

                }
                FillData();
            }
            catch { }
        }
        private void updateclient()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@Name", name.Text);
                addC.Add("@phone", phone.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير البيانات بسبب :" + " " + tReason.Text);
                addC.Add("@From1", cphone);
                addC.Add("@To1", phone.Text);
                addC.Add("@From2", cname);
                addC.Add("@To2", name.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdatePU = DBConnection.SqliteCommand("update Purchases Set ClientName = @Name where Client=@cID", addC);
                var UpdatePP = DBConnection.SqliteCommand("update PaidPurchases Set ClientName = @Name where ClientID=@cID", addC);
                var UpdateCI = DBConnection.SqliteCommand("update CashInvD Set clientName = @Name where clientID=@cID", addC);
                var UpdateCL = DBConnection.SqliteCommand("update Clients Set Name = @Name , Phone = @phone where id=@cID", addC);
                var insertHistory1 = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From1,@To1,@EditDate)", addC);
                var insertHistory2 = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From2,@To2,@EditDate)", addC);

                CList.Clear();
            }
            catch { }
        }
        private void updatename()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@Name", name.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير الإسم بسبب :" + " " + tReason.Text);
                addC.Add("@From", cname);
                addC.Add("@To", name.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdatePU = DBConnection.SqliteCommand("update Purchases Set ClientName = @Name where Client=@cID", addC);
                var UpdatePP = DBConnection.SqliteCommand("update PaidPurchases Set ClientName = @Name where ClientID=@cID", addC);
                var UpdateCI = DBConnection.SqliteCommand("update CashInvD Set clientName = @Name where clientID=@cID", addC);
                var UpdateCL = DBConnection.SqliteCommand("update Clients Set Name = @Name where id=@cID", addC);
                var insertHistory = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From,@To,@EditDate)", addC);
                CList.Clear();
            }
            catch { }
        }
        private void updatephone()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cid);
                addC.Add("@phone", phone.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@ClientID", cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@Reason", "تم تغيير الرقم بسبب :" + " " + tReason.Text);
                addC.Add("@From", cphone);
                addC.Add("@To", phone.Text);
                addC.Add("@EditDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var UpdateCu = DBConnection.SqliteCommand("update Clients Set Phone = @phone where id=@cID", addC);
                var insertHistory = DBConnection.SqliteCommand("INSERT  INTO `EditCLHistory`(`UserID`, `ClientID`, `UserName`, `Reason`, `From`, `To`, `EditDate`) values(@UserID,@ClientID,@UserName,@Reason,@From,@To,@EditDate)", addC);
                CList.Clear();
            }
            catch { }

        }
        private void add_Click(object sender, EventArgs e)
        {
            if (name.Text == cname && phone.Text == cphone)
            {
                cmbox = new cMessgeBox("لم تقوم بتغيير اي بيانات للمورد", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else if (string.IsNullOrWhiteSpace(name.Text) || string.IsNullOrWhiteSpace(phone.Text) || string.IsNullOrWhiteSpace(tReason.Text))
            {
                cmbox = new cMessgeBox("لا يمكنك ترك بيانات فارغة", "error", "", 2000);
                cmbox.ShowDialog();
            }
            else if (name.Text == cname && phone.Text != cphone)
            {
                try
                {
                    var phone1 = phone.Text.ToString();
                    var cup = CList.Find(i => i.Phone == phone1);
                    if (cup == null)
                    {
                        updatephone();
                        cmbox = new cMessgeBox("تم تعديل رقم المورد بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        var cpn = cup.Name;
                        var cppp = cup.Phone;
                        cmbox = new cMessgeBox("المورد" + " " + cpn + " " + "مسجل بنفس الرقم", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                }
                catch
                {

                }
            }
            else if (phone.Text == cphone && name.Text != cname)
            {
                try
                {
                    var Name1 = name.Text.ToString();
                    var cup = CList.Find(i => i.Name == Name1);

                    if (cup == null)
                    {
                        updatename();
                        cmbox = new cMessgeBox("تم تعديل إسم المورد بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();

                    }
                    else
                    {
                        var cpn = cup.Name;
                        cmbox = new cMessgeBox("يوجد مورد بإسم" + " " + cpn + " " + "مسجل بالفعل", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                }
                catch { }
            }
            else
            {
                try
                {
                    var name1 = name.Text.ToString();
                    var phone1 = phone.Text.ToString();
                    var cup = CList.Find(i => i.Name == name1);
                    var cpp = CList.Find(i => i.Phone == phone1);
                    if (cup != null)
                    {
                        cmbox = new cMessgeBox("يوجد مورد بإسم" + " " + name1 + " " + "مسجل بالفعل", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else if (cpp != null)
                    {
                        var cpn = cpp.Name;
                        cmbox = new cMessgeBox("المورد" + " " + cpn + " " + "مسجل بنفس الرقم", "error", "", 2000);
                        cmbox.ShowDialog();
                    }
                    else
                    {
                        updateclient();
                        cmbox = new cMessgeBox("تم تعديل بيانات المورد بنجاح", "done", "e", 1000);
                        cmbox.ShowDialog();
                        this.Close();
                    }
                }
                catch { }

            }
        }
        private void FillData()
        {
            name.Text = cname;
            phone.Text = cphone;
            var tid = int.Parse(cid);
            var ccid = CList.Find(i => i.id == tid);
            var cashe = ccid.Cash;
            if (cashe > 0)
            {
                name.Enabled = false;
                p1.Visible = true;
                p2.Visible = true;
                p3.Visible = true;
            }
            else
            {
                name.Enabled = true;
                p1.Visible = false;
                p2.Visible = false;
                p3.Visible = false;
            }
        }
    }
}

