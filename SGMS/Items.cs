﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using SGMS.BarCodeGE;

namespace SGMS
{
    public partial class Items : UserControl
    {
        int favBAR;
        string BarCodeM;
        string Barrcode, Check12Digits, iName, SSPrice;
        double itemC;
        public Items()
        {
            InitializeComponent();
        }
        private void Items_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            ItemsData();
            LoadAll();
            FavBar();
        }

        private void FavBar()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM FavBar where id =@id", dic).ConvertFavBar();
            if (check == null) return;

            foreach (FavBar item in check)
            {
                favBAR = item.Fav;
                if (favBAR == 1)
                {
                    itemsBarcodes.Focus();
                    itemsBarcodes.Select();
                }
            }
        }

        //ملئ القائمة
        private static List<Item> ItemP = new List<Item>();
        private void ItemsData()
        {
            ItemP.Clear();
            Dictionary<string, object> dics = new Dictionary<string, object>();
            dics.Add("@r", 1);
            var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where r =@r", dics);
            if (rIt == null) return;
            if (rIt.Rows.Count > 0)
            {
                itemsNames.DataSource = rIt;
                itemsNames.DisplayMember = "Name";
                itemsNames.ValueMember = "id";



                for (int i = 0; i < rIt.Rows.Count; i++)
                {
                    var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                    var _Barcode = rIt.Rows[i]["BarCode"].ToString();
                    var _name = rIt.Rows[i]["Name"].ToString();
                    var _price = rIt.Rows[i]["SellPrice"].ToString();
                    var _count = rIt.Rows[i]["Count"].ToString();
                    var _TB = rIt.Rows[i]["TotalBuy"].ToString();
                    var p = new Item
                    {
                        id = _id,
                        BarCode = _Barcode,
                        Name = _name,
                        Count = Double.Parse(_count),
                        TotalBuy = Double.Parse(_TB),
                        ItemPrice = Double.Parse(_price)

                    };
                    ItemP.Add(p);
                }
            }
        }
        private void LoadAll()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@Count", "1000000");
            var result = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where Count <=@Count", dic).ConvertItems();
            if (result == null) return;

            foreach (Item item in result)
            {

                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = item.BarCode;
                row.Cells[1].Value = item.Name;
                row.Cells[2].Value = item.Count;
                row.Cells[3].Value = item.SellPrice;
                dataGridView1.Rows.Add(row);

            }

        }
        private void BarCodeItem()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var brcode = itemsBarcodes.Text;
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@BarCode", brcode);
                var result = DBConnection.SqliteCommand("SELECT  *  FROM Items where BarCode =@BarCode", dic).ConvertItems();
                if (result == null) return;

                foreach (Item item in result)
                {

                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item.BarCode;
                    row.Cells[1].Value = item.Name;
                    row.Cells[2].Value = item.Count;
                    row.Cells[3].Value = item.SellPrice;
                    dataGridView1.Rows.Add(row);

                }

            }
            catch
            {

            }
        }
        private void FindItem()
        {
            try
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                var id = int.Parse(itemsNames.SelectedValue.ToString());
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", id);
                var result = DBConnection.SqliteCommand("SELECT  *  FROM Items where id =@id", dic).ConvertItems();
                if (result == null) return;

                foreach (Item item in result)
                {

                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = item.BarCode;
                    row.Cells[1].Value = item.Name;
                    row.Cells[2].Value = item.Count;
                    row.Cells[3].Value = item.SellPrice;
                    dataGridView1.Rows.Add(row);
                }

            }
            catch
            {

            }
        }
        private void itemsNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            FindItem();
        }
        private void itemsBarcodes_TextChanged(object sender, EventArgs e)
        {
            if (itemsBarcodes.TextLength <= 0)
            {
                LoadAll();
            }
            else
            {
                BarCodeItem();
            }
        }


        // إضافة منتجات جديدة
        private void AddNew_Click(object sender, EventArgs e)
        {
            FastAddItem fastnewitem = new FastAddItem();
            fastnewitem.FormClosed += FastAddItemClosed;
            fastnewitem.ShowDialog();
        }
        void FastAddItemClosed(object sender, FormClosedEventArgs e)
        {
            ItemsData();
            LoadAll();
        }


        // تعديل المنتج
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    if (dataGridView1.SelectedRows.Count > 0)
                    {
                        BarCodeM = row.Cells[0].Value.ToString();
                        itemC = Double.Parse(row.Cells[3].Value.ToString());
                        editC.Enabled = true;
                    }
                    else
                    {
                        BarCodeM = "";
                        itemC = 0;
                        editC.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }
        private void editC_Click(object sender, EventArgs e)
        {
            EditItem EditItems = new EditItem(BarCodeM);
            EditItems.FormClosed += EditItemsClosed;
            EditItems.ShowDialog();
        }

        void EditItemsClosed(object sender, FormClosedEventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            LoadAll();
        }


        //طباعة الباركود
        private void PrintLabels_Click(object sender, EventArgs e)
        {
            int pn;
            var br = BarCodeM;
            if(itemC <= 0)
            {
                cMessgeBox cMessge = new cMessgeBox("لا يمكن طباعة الباركود لعدم وجود سعر بيع", "error", "p", 1500);
                cMessge.ShowDialog();
            }
            else
            {
                string value = "";
                if (Tmp.InputBox("طباعة ملصقات الباركود", "عدد الصفحات المراد طباعتها ؟ ، عدد الملصقات في كل صفحة 20 ملصق للمنتج", ref value) == DialogResult.OK)
                {
                    pn = int.Parse(value);
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@r", br);
                    var check = DBConnection.SqliteCommand("SELECT * FROM Items where BarCode=@r", dic).ConvertItems();
                    if (check == null) return;
                    if (check.Count > 0)
                    {
                        foreach (Item item1 in check)
                        {
                            BarCodes BarC = new BarCodes();
                            BarC.SetDataSource(check);
                            string brCode = item1.BarCode.Substring(0, (item1.BarCode.Length - 1));
                            iName = item1.Name;
                            SSPrice = item1.SellPrice.ToString();
                            Check12Digits = brCode.PadRight(12, '0');
                            Barrcode = EAN13Class.EAN13(Check12Digits);
                            BarC.SetParameterValue("Name", iName);
                            BarC.SetParameterValue("Barcode", Barrcode);
                            BarC.SetParameterValue("Sprice", "السعر" + " " + SSPrice);
                            BarC.PrintToPrinter(pn, false, 0, 0);
                        }

                    }


                }

            }
        }
    }
}
