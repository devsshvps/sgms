﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class ViewInvoicePage : Form
    {
        string InvNNN;
        double inunpaid1;
        double invtotal1;
        double invpaid1;
        string finalnumber;

        public ViewInvoicePage(String invNumber)
        {
            InitializeComponent();
            InvNNN = invNumber;
            LoadInv();
            this.Text = "عرض لفاتورة المشتريات رقم" + " " + ":" + " " + Login.arNumber(finalnumber);
        }

        private void PrintInvByNum_Click(object sender, EventArgs e)
        {
            PrintInv();
        }

        private void PrintInv()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@r", InvNNN);
            var check = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@r", dic);
            var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@r", dic).ConvertCashInvoice();
            if (invCash == null) return;
            if (check == null) return;
            if (check.Rows.Count > 0)
            {
                foreach (CashInvD item in invCash)
                {
                    inunpaid1 = item.unpaid;
                    var invtotal13 = item.totalInv;
                    invpaid1 = invtotal13 - inunpaid1;
                }
                string d1 = DateTime.Now.ToString("dd");
                string m1 = DateTime.Now.ToString("MM");
                string y1 = DateTime.Now.ToString("yyyy");
                string ti1 = DateTime.Now.ToString("t");
                string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                Print2P printPurch = new Print2P();
                printPurch.SetDataSource(check);
                printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                printPurch.SetParameterValue("invsName", "فاتورة مشتريات");
                printPurch.SetParameterValue("paid", invpaid1);
                printPurch.SetParameterValue("unpaid", inunpaid1);
                printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                printPurch.SetParameterValue("name", Dashboard.invCoN);
                printPurch.SetParameterValue("slug", Dashboard.invCoS);
                printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                printPurch.SetParameterValue("address", Dashboard.invCoA);
                printPurch.PrintToPrinter(1, false, 0, 0);
            }
        }
            private void LoadInv()
            {

                try
                {
                    string inv = InvNNN;
                    int sNuM = int.Parse(inv.ToString());

                    if (sNuM >= 1 && sNuM < 10)
                    {
                        finalnumber = "000000" + sNuM.ToString();
                    }
                    else if (sNuM >= 10 && sNuM < 100)
                    {
                        finalnumber = "00000" + sNuM.ToString();
                    }
                    else if (sNuM >= 100 && sNuM < 1000)
                    {
                        finalnumber = "0000" + sNuM.ToString();
                    }
                    else if (sNuM >= 1000 && sNuM < 10000)
                    {
                        finalnumber = "000" + sNuM.ToString();
                    }
                    else if (sNuM >= 10000 && sNuM < 100000)
                    {
                        finalnumber = "00" + sNuM.ToString();
                    }
                    else if (sNuM >= 100000 && sNuM < 1000000)
                    {
                        finalnumber = "0" + sNuM.ToString();
                    }
                    else if (sNuM >= 1000000)
                    {
                        finalnumber = sNuM.ToString();
                    }
                    InvNum.Text = Login.arNumber(finalnumber);
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("num", inv);
                    var result = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@num", dic).ConvertPurchases();
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@num", dic).ConvertCashInvoice();
                    if (invCash == null) return;
                    if (result == null) return;

                    foreach (CashInvD item in invCash)
                    {
                        inunpaid1 = item.unpaid;
                        invtotal1 = item.totalInv;
                        invpaid1 = invtotal1 - inunpaid1;
                    }

                    foreach (Purchase item in result)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(invDatagride);
                        row.Cells[0].Value = item.item;
                        row.Cells[1].Value = item.Count;
                        row.Cells[2].Value = item.ItemPrice;
                        row.Cells[3].Value = item.TotalBuy;
                        invDatagride.Rows.Add(row);
                        invDatagride.CurrentCell = null;
                        invDate.Text = item.Date_ct.ToString("yyyy-MM-dd");
                        invClient.Text = item.ClientName;
                    }
                    Total.Text = invtotal1.ToString();
                    Unpaid.Text = inunpaid1.ToString();
                    Paid.Text = invpaid1.ToString();
                }

                catch
                {
                }
            }
        }
    }

