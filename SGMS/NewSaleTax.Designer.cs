﻿namespace SGMS
{
    partial class NewSaleTax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewSaleTax));
            this.panel5 = new System.Windows.Forms.Panel();
            this.unPaid = new System.Windows.Forms.Label();
            this.AddIn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.paidCash = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.Label();
            this.dataGridView1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Earn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.ShowCount = new System.Windows.Forms.Label();
            this.Sprice = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.newSeller = new Bunifu.Framework.UI.BunifuFlatButton();
            this.newbTN = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDel = new Bunifu.Framework.UI.BunifuFlatButton();
            this.sellerName = new System.Windows.Forms.ComboBox();
            this.iCount = new System.Windows.Forms.TextBox();
            this.btnAdd = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bPrice = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.u1 = new System.Windows.Forms.Label();
            this.u2 = new System.Windows.Forms.Label();
            this.itemBarcode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.isBarcode = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.itemBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.isTax = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.isTax);
            this.panel5.Controls.Add(this.unPaid);
            this.panel5.Controls.Add(this.AddIn);
            this.panel5.Controls.Add(this.paidCash);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.Total);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(20, 641);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(860, 39);
            this.panel5.TabIndex = 7;
            // 
            // unPaid
            // 
            this.unPaid.Font = new System.Drawing.Font("Cairo", 8F);
            this.unPaid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.unPaid.Location = new System.Drawing.Point(222, 8);
            this.unPaid.Name = "unPaid";
            this.unPaid.Size = new System.Drawing.Size(67, 25);
            this.unPaid.TabIndex = 39;
            this.unPaid.Text = "0";
            this.unPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddIn
            // 
            this.AddIn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AddIn.BorderRadius = 7;
            this.AddIn.ButtonText = "حفظ";
            this.AddIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddIn.DisabledColor = System.Drawing.Color.Gray;
            this.AddIn.Enabled = false;
            this.AddIn.Font = new System.Drawing.Font("Cairo", 10F);
            this.AddIn.Iconcolor = System.Drawing.Color.Transparent;
            this.AddIn.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.AddIn.Iconimage_right = null;
            this.AddIn.Iconimage_right_Selected = null;
            this.AddIn.Iconimage_Selected = null;
            this.AddIn.IconMarginLeft = 0;
            this.AddIn.IconMarginRight = 0;
            this.AddIn.IconRightVisible = false;
            this.AddIn.IconRightZoom = 0D;
            this.AddIn.IconVisible = false;
            this.AddIn.IconZoom = 25D;
            this.AddIn.IsTab = false;
            this.AddIn.Location = new System.Drawing.Point(13, 5);
            this.AddIn.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.AddIn.Name = "AddIn";
            this.AddIn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.AddIn.OnHoverTextColor = System.Drawing.Color.White;
            this.AddIn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.AddIn.selected = false;
            this.AddIn.Size = new System.Drawing.Size(88, 30);
            this.AddIn.TabIndex = 41;
            this.AddIn.Text = "حفظ";
            this.AddIn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AddIn.Textcolor = System.Drawing.Color.White;
            this.AddIn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddIn.Click += new System.EventHandler(this.AddIn_Click);
            // 
            // paidCash
            // 
            this.paidCash.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paidCash.Location = new System.Drawing.Point(398, 4);
            this.paidCash.Name = "paidCash";
            this.paidCash.Size = new System.Drawing.Size(113, 32);
            this.paidCash.TabIndex = 43;
            this.paidCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.paidCash.EnabledChanged += new System.EventHandler(this.paidCash_EnabledChanged);
            this.paidCash.TextChanged += new System.EventHandler(this.paidCash_TextChanged);
            this.paidCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlynumwithsinglepoint);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cairo", 7F);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label16.Location = new System.Drawing.Point(519, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 19);
            this.label16.TabIndex = 38;
            this.label16.Text = "المبلغ المحصل";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cairo", 7F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label17.Location = new System.Drawing.Point(301, 11);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 19);
            this.label17.TabIndex = 38;
            this.label17.Text = "المبلغ الغير محصل";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cairo", 7F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label9.Location = new System.Drawing.Point(664, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 19);
            this.label9.TabIndex = 35;
            this.label9.Text = "الإجمالي";
            // 
            // Total
            // 
            this.Total.Font = new System.Drawing.Font("Cairo", 8F);
            this.Total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.Total.Location = new System.Drawing.Point(601, 8);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(53, 24);
            this.Total.TabIndex = 37;
            this.Total.Text = "0";
            this.Total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BarCode,
            this.Column5,
            this.count,
            this.BuyPrice,
            this.TotalBuy,
            this.SePrice,
            this.Earn});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.DoubleBuffered = true;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(20, 135);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(860, 545);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // BarCode
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BarCode.DefaultCellStyle = dataGridViewCellStyle3;
            this.BarCode.FillWeight = 58.50328F;
            this.BarCode.HeaderText = "باركود";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "إسم المنتج";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // count
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.count.DefaultCellStyle = dataGridViewCellStyle4;
            this.count.FillWeight = 56.99974F;
            this.count.HeaderText = "الكمية";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            // 
            // BuyPrice
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuyPrice.DefaultCellStyle = dataGridViewCellStyle5;
            this.BuyPrice.FillWeight = 78.06802F;
            this.BuyPrice.HeaderText = "السعر";
            this.BuyPrice.Name = "BuyPrice";
            this.BuyPrice.ReadOnly = true;
            // 
            // TotalBuy
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TotalBuy.DefaultCellStyle = dataGridViewCellStyle6;
            this.TotalBuy.FillWeight = 101.4645F;
            this.TotalBuy.HeaderText = "الإجمالي";
            this.TotalBuy.Name = "TotalBuy";
            this.TotalBuy.ReadOnly = true;
            // 
            // SePrice
            // 
            this.SePrice.HeaderText = "سعر الشراء";
            this.SePrice.Name = "SePrice";
            this.SePrice.ReadOnly = true;
            this.SePrice.Visible = false;
            // 
            // Earn
            // 
            this.Earn.HeaderText = "الربح";
            this.Earn.Name = "Earn";
            this.Earn.ReadOnly = true;
            this.Earn.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ShowCount);
            this.panel1.Controls.Add(this.Sprice);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.newSeller);
            this.panel1.Controls.Add(this.newbTN);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnDel);
            this.panel1.Controls.Add(this.sellerName);
            this.panel1.Controls.Add(this.iCount);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(20, 86);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(860, 49);
            this.panel1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Font = new System.Drawing.Font("Cairo", 7F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(351, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 19);
            this.label3.TabIndex = 48;
            this.label3.Text = "الكمية المتاحة";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Visible = false;
            // 
            // ShowCount
            // 
            this.ShowCount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ShowCount.Font = new System.Drawing.Font("Cairo", 8F);
            this.ShowCount.ForeColor = System.Drawing.Color.White;
            this.ShowCount.Location = new System.Drawing.Point(351, 23);
            this.ShowCount.Name = "ShowCount";
            this.ShowCount.Size = new System.Drawing.Size(79, 21);
            this.ShowCount.TabIndex = 49;
            this.ShowCount.Text = "0";
            this.ShowCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ShowCount.Visible = false;
            // 
            // Sprice
            // 
            this.Sprice.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Sprice.Enabled = false;
            this.Sprice.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.Sprice.Location = new System.Drawing.Point(616, 8);
            this.Sprice.Name = "Sprice";
            this.Sprice.Size = new System.Drawing.Size(79, 32);
            this.Sprice.TabIndex = 47;
            this.Sprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Sprice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlynumwithsinglepoint);
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Cairo", 7F);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label13.Location = new System.Drawing.Point(705, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 19);
            this.label13.TabIndex = 46;
            this.label13.Text = "السعر";
            // 
            // newSeller
            // 
            this.newSeller.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newSeller.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newSeller.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newSeller.BorderRadius = 7;
            this.newSeller.ButtonText = "عميل جديد";
            this.newSeller.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newSeller.DisabledColor = System.Drawing.Color.Gray;
            this.newSeller.Font = new System.Drawing.Font("Cairo", 8F);
            this.newSeller.Iconcolor = System.Drawing.Color.Transparent;
            this.newSeller.Iconimage = null;
            this.newSeller.Iconimage_right = null;
            this.newSeller.Iconimage_right_Selected = null;
            this.newSeller.Iconimage_Selected = null;
            this.newSeller.IconMarginLeft = 0;
            this.newSeller.IconMarginRight = 0;
            this.newSeller.IconRightVisible = false;
            this.newSeller.IconRightZoom = 0D;
            this.newSeller.IconVisible = false;
            this.newSeller.IconZoom = 20D;
            this.newSeller.IsTab = false;
            this.newSeller.Location = new System.Drawing.Point(16, 12);
            this.newSeller.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.newSeller.Name = "newSeller";
            this.newSeller.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newSeller.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.newSeller.OnHoverTextColor = System.Drawing.Color.White;
            this.newSeller.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.newSeller.selected = false;
            this.newSeller.Size = new System.Drawing.Size(94, 25);
            this.newSeller.TabIndex = 41;
            this.newSeller.Text = "عميل جديد";
            this.newSeller.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newSeller.Textcolor = System.Drawing.Color.White;
            this.newSeller.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newSeller.Click += new System.EventHandler(this.newSeller_Click);
            // 
            // newbTN
            // 
            this.newbTN.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newbTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newbTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newbTN.BorderRadius = 7;
            this.newbTN.ButtonText = "";
            this.newbTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newbTN.DisabledColor = System.Drawing.Color.Gray;
            this.newbTN.Font = new System.Drawing.Font("Cairo", 8F);
            this.newbTN.Iconcolor = System.Drawing.Color.Transparent;
            this.newbTN.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.newbTN.Iconimage_right = null;
            this.newbTN.Iconimage_right_Selected = null;
            this.newbTN.Iconimage_Selected = null;
            this.newbTN.IconMarginLeft = 0;
            this.newbTN.IconMarginRight = 0;
            this.newbTN.IconRightVisible = false;
            this.newbTN.IconRightZoom = 0D;
            this.newbTN.IconVisible = true;
            this.newbTN.IconZoom = 30D;
            this.newbTN.IsTab = false;
            this.newbTN.Location = new System.Drawing.Point(87, 12);
            this.newbTN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.newbTN.Name = "newbTN";
            this.newbTN.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(100)))));
            this.newbTN.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.newbTN.OnHoverTextColor = System.Drawing.Color.White;
            this.newbTN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.newbTN.selected = false;
            this.newbTN.Size = new System.Drawing.Size(34, 25);
            this.newbTN.TabIndex = 45;
            this.newbTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newbTN.Textcolor = System.Drawing.Color.White;
            this.newbTN.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newbTN.Visible = false;
            this.newbTN.Click += new System.EventHandler(this.newbTN_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 7F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label6.Location = new System.Drawing.Point(292, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 19);
            this.label6.TabIndex = 39;
            this.label6.Text = "العميل";
            // 
            // btnDel
            // 
            this.btnDel.Activecolor = System.Drawing.Color.Maroon;
            this.btnDel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDel.BackColor = System.Drawing.Color.Maroon;
            this.btnDel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDel.BorderRadius = 7;
            this.btnDel.ButtonText = "حذف";
            this.btnDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDel.DisabledColor = System.Drawing.Color.White;
            this.btnDel.Font = new System.Drawing.Font("Cairo", 8F);
            this.btnDel.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDel.Iconimage = null;
            this.btnDel.Iconimage_right = null;
            this.btnDel.Iconimage_right_Selected = null;
            this.btnDel.Iconimage_Selected = null;
            this.btnDel.IconMarginLeft = 0;
            this.btnDel.IconMarginRight = 0;
            this.btnDel.IconRightVisible = false;
            this.btnDel.IconRightZoom = 0D;
            this.btnDel.IconVisible = false;
            this.btnDel.IconZoom = 20D;
            this.btnDel.IsTab = false;
            this.btnDel.Location = new System.Drawing.Point(450, 13);
            this.btnDel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnDel.Name = "btnDel";
            this.btnDel.Normalcolor = System.Drawing.Color.Maroon;
            this.btnDel.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDel.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDel.selected = false;
            this.btnDel.Size = new System.Drawing.Size(65, 23);
            this.btnDel.TabIndex = 44;
            this.btnDel.Text = "حذف";
            this.btnDel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnDel.Textcolor = System.Drawing.Color.White;
            this.btnDel.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // sellerName
            // 
            this.sellerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.sellerName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.sellerName.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.sellerName.FormattingEnabled = true;
            this.sellerName.Location = new System.Drawing.Point(140, 8);
            this.sellerName.Name = "sellerName";
            this.sellerName.Size = new System.Drawing.Size(145, 32);
            this.sellerName.TabIndex = 42;
            this.sellerName.SelectedIndexChanged += new System.EventHandler(this.sellerName_SelectedIndexChanged);
            // 
            // iCount
            // 
            this.iCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.iCount.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.iCount.Location = new System.Drawing.Point(767, 8);
            this.iCount.Name = "iCount";
            this.iCount.Size = new System.Drawing.Size(41, 32);
            this.iCount.TabIndex = 43;
            this.iCount.Text = "1";
            this.iCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.iCount.TextChanged += new System.EventHandler(this.iCount_TextChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdd.BorderRadius = 7;
            this.btnAdd.ButtonText = "";
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.DisabledColor = System.Drawing.Color.Gray;
            this.btnAdd.Font = new System.Drawing.Font("Cairo", 10F);
            this.btnAdd.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAdd.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.btnAdd.Iconimage_right = null;
            this.btnAdd.Iconimage_right_Selected = null;
            this.btnAdd.Iconimage_Selected = null;
            this.btnAdd.IconMarginLeft = 25;
            this.btnAdd.IconMarginRight = 0;
            this.btnAdd.IconRightVisible = false;
            this.btnAdd.IconRightZoom = 0D;
            this.btnAdd.IconVisible = true;
            this.btnAdd.IconZoom = 30D;
            this.btnAdd.IsTab = false;
            this.btnAdd.Location = new System.Drawing.Point(529, 8);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.btnAdd.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.btnAdd.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAdd.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnAdd.selected = false;
            this.btnAdd.Size = new System.Drawing.Size(62, 32);
            this.btnAdd.TabIndex = 41;
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAdd.Textcolor = System.Drawing.Color.White;
            this.btnAdd.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Cairo", 7F);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label11.Location = new System.Drawing.Point(810, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 19);
            this.label11.TabIndex = 39;
            this.label11.Text = "الكمية";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(324, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // bPrice
            // 
            this.bPrice.AutoSize = true;
            this.bPrice.Location = new System.Drawing.Point(324, 27);
            this.bPrice.Name = "bPrice";
            this.bPrice.Size = new System.Drawing.Size(36, 13);
            this.bPrice.TabIndex = 48;
            this.bPrice.Text = "bPrice";
            this.bPrice.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.bPrice);
            this.panel6.Controls.Add(this.u1);
            this.panel6.Controls.Add(this.u2);
            this.panel6.Controls.Add(this.itemBarcode);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.isBarcode);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.itemBox);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(20, 30);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(860, 56);
            this.panel6.TabIndex = 8;
            // 
            // u1
            // 
            this.u1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.u1.Font = new System.Drawing.Font("Cairo", 7F);
            this.u1.ForeColor = System.Drawing.Color.White;
            this.u1.Location = new System.Drawing.Point(219, 6);
            this.u1.Name = "u1";
            this.u1.Size = new System.Drawing.Size(101, 24);
            this.u1.TabIndex = 44;
            this.u1.Text = "ديون العميل";
            this.u1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.u1.Visible = false;
            // 
            // u2
            // 
            this.u2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.u2.Font = new System.Drawing.Font("Cairo", 8F);
            this.u2.ForeColor = System.Drawing.Color.White;
            this.u2.Location = new System.Drawing.Point(219, 23);
            this.u2.Name = "u2";
            this.u2.Size = new System.Drawing.Size(101, 26);
            this.u2.TabIndex = 45;
            this.u2.Text = "0";
            this.u2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.u2.Visible = false;
            // 
            // itemBarcode
            // 
            this.itemBarcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.itemBarcode.Enabled = false;
            this.itemBarcode.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.itemBarcode.Location = new System.Drawing.Point(588, 11);
            this.itemBarcode.Name = "itemBarcode";
            this.itemBarcode.Size = new System.Drawing.Size(155, 32);
            this.itemBarcode.TabIndex = 43;
            this.itemBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.itemBarcode.TextChanged += new System.EventHandler(this.itemBarcode_TextChanged);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Cairo", 8F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label7.Location = new System.Drawing.Point(19, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 26);
            this.label7.TabIndex = 36;
            this.label7.Text = "0";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // isBarcode
            // 
            this.isBarcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.isBarcode.AutoSize = true;
            this.isBarcode.Font = new System.Drawing.Font("Cairo", 8F);
            this.isBarcode.Location = new System.Drawing.Point(749, 15);
            this.isBarcode.Name = "isBarcode";
            this.isBarcode.Size = new System.Drawing.Size(55, 24);
            this.isBarcode.TabIndex = 42;
            this.isBarcode.Text = "باركود";
            this.isBarcode.UseVisualStyleBackColor = true;
            this.isBarcode.CheckedChanged += new System.EventHandler(this.isBarcode_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cairo", 7F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(127, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 19);
            this.label8.TabIndex = 34;
            this.label8.Text = "رقم الفاتورة";
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(12, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(185, 35);
            this.label5.TabIndex = 37;
            this.label5.Text = " ";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Cairo", 7F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label10.Location = new System.Drawing.Point(811, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 19);
            this.label10.TabIndex = 39;
            this.label10.Text = "المنتج";
            // 
            // itemBox
            // 
            this.itemBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.itemBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.itemBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.itemBox.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.itemBox.FormattingEnabled = true;
            this.itemBox.Location = new System.Drawing.Point(365, 11);
            this.itemBox.Name = "itemBox";
            this.itemBox.Size = new System.Drawing.Size(213, 32);
            this.itemBox.TabIndex = 43;
            this.itemBox.SelectedIndexChanged += new System.EventHandler(this.itemBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(747, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 19);
            this.label1.TabIndex = 48;
            this.label1.Text = "فاتورة مبيعات ضريبية جديدة";
            // 
            // isTax
            // 
            this.isTax.AutoSize = true;
            this.isTax.Font = new System.Drawing.Font("Cairo", 7F);
            this.isTax.ForeColor = System.Drawing.Color.Maroon;
            this.isTax.Location = new System.Drawing.Point(127, 10);
            this.isTax.Name = "isTax";
            this.isTax.Size = new System.Drawing.Size(83, 19);
            this.isTax.TabIndex = 44;
            this.isTax.Text = "تم تفعيل خصم 1%";
            this.isTax.Visible = false;
            // 
            // NewSaleTax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 700);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel6);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewSaleTax";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Right;
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label unPaid;
        private Bunifu.Framework.UI.BunifuFlatButton AddIn;
        private System.Windows.Forms.TextBox paidCash;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label Total;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox Sprice;
        private System.Windows.Forms.Label label13;
        private Bunifu.Framework.UI.BunifuFlatButton newSeller;
        private Bunifu.Framework.UI.BunifuFlatButton newbTN;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuFlatButton btnDel;
        private System.Windows.Forms.ComboBox sellerName;
        private System.Windows.Forms.TextBox iCount;
        private Bunifu.Framework.UI.BunifuFlatButton btnAdd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox itemBox;
        private System.Windows.Forms.TextBox itemBarcode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox isBarcode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label u1;
        private System.Windows.Forms.Label u2;
        private System.Windows.Forms.Label bPrice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn SePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Earn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ShowCount;
        private System.Windows.Forms.Label isTax;
    }
}