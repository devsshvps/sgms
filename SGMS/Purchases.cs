﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SGMS.BarCodeGE;
using SGMS.Data;
using SGMS.Models;
using SGMS.Pages;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SGMS
{
    public partial class Purchases : UserControl
    {
        int NuM, riddd;
        int favBAR;
        string InvNNN;
        string InvViews;
        double inunpaid1;
        double invtotal1;
        double invpaid1;
        double inunpaid12;
        double invpaid12;
        double inunpaid13;
        double invpaid13;
        double reccc,bpccc;
        string Barrcode, Check12Digits, iName, SSPrice, brccc;
        public Purchases()
        {
            InitializeComponent();
            Roles();
            bPrice.KeyPress += onlynumwithsinglepoint;
            Sprice.KeyPress += onlynumwithsinglepoint;
        }
        private void Home_Load(object sender, EventArgs e)
        {
            fDay.SelectedIndex = 0;
            tDay.SelectedIndex = 30;
            fMonth.SelectedItem = DateTime.Now.ToString("MM");
            tMonth.SelectedItem = DateTime.Now.ToString("MM");
            fYear.SelectedItem = DateTime.Now.ToString("yyyy");
            tYear.SelectedItem = DateTime.Now.ToString("yyyy");
            this.Dock = DockStyle.Fill;
            InvoiceNum();
            LoadData();
            itemBox.SelectedIndex = -1;
            FavBar();
            LoadBadP();
            LoadBadItems();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                if (itemBarcode.Focused == true && itemBarcode.TextLength > 0)
                {
                    iCount.Focus();
                }
                else if (iCount.Focused == true && iCount.TextLength > 0)
                {
                    btnAdd_Click(this, new EventArgs());
                    if (isBarcode.Checked)
                    {
                        itemBarcode.Focus();
                    }
                    else
                    {
                        itemBox.Focus();
                    }
                }
                else if (
                    fDay.Focused == true ||
                    tDay.Focused == true ||
                    fMonth.Focused == true ||
                    tMonth.Focused == true ||
                    fYear.Focused == true ||
                    tYear.Focused == true)
                {
                    ViewBTN_Click(this, new EventArgs());
                }
                else
                {

                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void Roles()
        {
            if(Login.r8 == "0")
            {
                tabControl1.TabPages.Remove(tabPage1);
            }

            if (Login.r9 == "0")
            {
                tabControl1.TabPages.Remove(tabPage2);
            }

            if (Login.r10 == "0")
            {
                tabControl1.TabPages.Remove(tabPage3);
            }

            if (Login.r11 == "0")
            {
                tabControl1.TabPages.Remove(tabPage4);
            }
        }
        // صفحة المشتريات

        private static List<Item> ItemP = new List<Item>();
        private static List<Client> Clients = new List<Client>();
        private static List<BadItems> BadItemsP = new List<BadItems>();
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage3"])
            {
                InvNNN = "";
                ViewInvByNum.Enabled = false;
                PrintInvByNum.Enabled = false;
                LoadUnPaidInv();
            }
        }
        private void ClientsData()
        {
            Clients.Clear();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@r", "1");
            var Ccl = DBConnection.SqliteCommand("SELECT *  FROM Clients where r =@r", dic);
            if (Ccl == null) return;
            if (Ccl.Rows.Count > 0)
            {
                sellerName.DataSource = Ccl;
                sellerName.DisplayMember = "Name";
                sellerName.ValueMember = "id";

                for (int i = 0; i < Ccl.Rows.Count; i++)
                {
                    var _id = int.Parse(Ccl.Rows[i]["id"].ToString());
                    var _nameC = Ccl.Rows[i]["Name"].ToString();
                    var _PhoneC = Ccl.Rows[i]["Phone"].ToString();
                    var _Cash = Ccl.Rows[i]["Cash"].ToString();
                    var C = new Client
                    {
                        id = _id,
                        Name = _nameC,
                        Phone = _PhoneC,
                        Cash = Double.Parse(_Cash)
                    };
                    Clients.Add(C);

                }
            }
        }
        private void ItemsData()
        {
            ItemP.Clear();
            Dictionary<string, object> dics = new Dictionary<string, object>();
            dics.Add("@r", 1);
            var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where r =@r", dics);
            if (rIt == null) return;
            if (rIt.Rows.Count > 0)
            {
                itemBox.DataSource = rIt;
                itemBox.DisplayMember = "Name";
                itemBox.ValueMember = "id";


                for (int i = 0; i < rIt.Rows.Count; i++)
                {
                    var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                    var _name = rIt.Rows[i]["Name"].ToString();
                    var _sellprice = rIt.Rows[i]["SellPrice"].ToString();
                    var _buyprice = rIt.Rows[i]["ItemPrice"].ToString();
                    var _count = rIt.Rows[i]["Count"].ToString();
                    var _TB = rIt.Rows[i]["TotalBuy"].ToString();
                    var _barc = rIt.Rows[i]["BarCode"].ToString();
                    var _P = rIt.Rows[i]["NumOfPurc"].ToString();
                    var p = new Item
                    {
                        id = _id,
                        Name = _name,
                        Count = Double.Parse(_count),
                        TotalBuy = Double.Parse(_TB),
                        ItemPrice = Double.Parse(_buyprice),
                        SellPrice = Double.Parse(_sellprice),
                        NumOfPurc = Double.Parse(_P),
                        BarCode = _barc

                    };
                    ItemP.Add(p);
                }
            }
        }
        private void InvoiceNum()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                dic.Add("@r", 1);
                var check = DBConnection.SqliteCommand("SELECT MAX(Num) FROM Purchases where r=@r", dic);
                if (check == null) return;
                if (check.Rows.Count > 0)
                {
                    NuM = int.Parse(check.Rows[0]["MAX(Num)"].ToString());
                }
                if (NuM == 0)
                {
                    printLast.Enabled = false;
                }
                else
                {
                    printLast.Enabled = true;
                }

                NuM += 1;
                if (NuM >= 1 && NuM < 10)
                {
                    label7.Text = "000000" + NuM.ToString();
                }
                else if (NuM >= 10 && NuM < 100)
                {
                    label7.Text = "00000" + NuM.ToString();
                }
                else if (NuM >= 100 && NuM < 1000)
                {
                    label7.Text = "0000" + NuM.ToString();
                }
                else if (NuM >= 1000 && NuM < 10000)
                {
                    label7.Text = "000" + NuM.ToString();
                }
                else if (NuM >= 10000 && NuM < 100000)
                {
                    label7.Text = "00" + NuM.ToString();
                }
                else if (NuM >= 100000 && NuM < 1000000)
                {
                    label7.Text = "0" + NuM.ToString();
                }
                else if (NuM >= 1000000)
                {
                    label7.Text = NuM.ToString();
                }

            }
            catch { }
        }
        private void sellerName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void isBarcode_CheckedChanged(object sender, EventArgs e)
        {
            if (isBarcode.Checked == true)
            {
                itemBox.Enabled = false;
                itemBarcode.Enabled = true;
            }
            else
            {
                itemBox.Enabled = true;
                itemBarcode.Enabled = false;
            }
        }
        private void itemBarcode_TextChanged(object sender, EventArgs e)
        {
            if (itemBarcode.Enabled == true)
            {
                itemBox.SelectedValue = -1;
                bPrice.Text = "0";
                Sprice.Text = "0";
                try
                {
                    var brcode = itemBarcode.Text.ToString();
                    var cup = ItemP.Find(i11 => i11.BarCode == brcode);
                    var idd = cup.id.ToString();
                    itemBox.SelectedValue = int.Parse(idd.ToString());
                    bPrice.Text = cup.ItemPrice.ToString();
                    Sprice.Text = cup.SellPrice.ToString();
                    if (cup.SellPrice > 0)
                    {
                        Sprice.Enabled = false;
                    }
                    else
                    {
                        Sprice.Enabled = true;
                    }

                }
                catch { }
            }
            else
            {

            }

        }
        private void itemBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (itemBox.Enabled == true)
            {
                itemBarcode.Text = "";
                bPrice.Text = "0";
                Sprice.Text = "0";
                try
                {
                    int id = 0;
                    if (int.TryParse(itemBox?.SelectedValue?.ToString(),out id))
                    {
                        var cup = ItemP.Find(i => i.id == id);
                        if (cup == null) return;
                        itemBarcode.Text = cup?.BarCode.ToString();
                        bPrice.Text = cup?.ItemPrice.ToString();
                        Sprice.Text = cup?.SellPrice.ToString();
                        if (cup.SellPrice > 0)
                        {
                            Sprice.Enabled = false;
                        }
                        else
                        {
                            Sprice.Enabled = true;
                        }
                    } 
                    
                }
                catch
                {

                }
            }
            else
            {

            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int id;
            int uid = 0;

            try
            {
                id = int.Parse(sellerName.SelectedValue.ToString());
                var cup = Clients.Find(ur => ur.id == id);
                uid = cup.id;
            }
            catch
            {

            }

            if (string.IsNullOrWhiteSpace(itemBarcode.Text) || itemBox.SelectedIndex == -1)
            {
                MessageBox.Show("برجاء إختيار منتج بشكل صحيح");
            }
            else if (string.IsNullOrWhiteSpace(iCount.Text))
            {
                MessageBox.Show("برجاء كتابة الكمية التي تريد شرائها بشكل صحيح");
            }
            else if (string.IsNullOrWhiteSpace(bPrice.Text) || double.Parse(bPrice.Text) == 0)
            {
                MessageBox.Show("برجاء كتابة سعر الشراء");
            }
            else if (string.IsNullOrWhiteSpace(Sprice.Text) || double.Parse(Sprice.Text) == 0)
            {
                MessageBox.Show("برجاء كتابة سعر البيع");
            }
            else if (uid <= 0)
            {
                MessageBox.Show("برجاء إختيار المورد بشكل صحيح");
            }
            else
            {
                sellerName.Enabled = false;
                if (uid == 1)
                {
                    paidCash.Enabled = false;
                }

                ///
                bool Found = false;
                if (dataGridView1.Rows.Count > 0)
                {
                    foreach (DataGridViewRow drow in dataGridView1.Rows)
                    {
                        if (Convert.ToString(drow.Cells[0].Value) == itemBarcode.Text)
                        {
                            drow.Cells[2].Value = Convert.ToString(Convert.ToInt16(iCount.Text) + Convert.ToInt16(drow.Cells[2].Value));
                            Found = true;

                            sumMethod();
                            if (isBarcode.Checked)
                            {
                                itemBarcode.Text = "";
                            }
                            iCount.Text = "1";
                        }
                    }
                    if (!Found)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView1);
                        row.Cells[0].Value = itemBarcode.Text;
                        row.Cells[1].Value = this.itemBox.GetItemText(this.itemBox.SelectedItem);
                        row.Cells[2].Value = iCount.Text;
                        row.Cells[3].Value = bPrice.Text;
                        row.Cells[4].Value = "0";
                        row.Cells[5].Value = Sprice.Text;
                        dataGridView1.Rows.Add(row);
                        dataGridView1.CurrentCell = null;
                        if (isBarcode.Checked)
                        {
                            itemBarcode.Text = "";
                        }
                        iCount.Text = "1";
                    }
                }
                else
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = itemBarcode.Text;
                    row.Cells[1].Value = this.itemBox.GetItemText(this.itemBox.SelectedItem);
                    row.Cells[2].Value = iCount.Text;
                    row.Cells[3].Value = bPrice.Text;
                    row.Cells[4].Value = "0";
                    row.Cells[5].Value = Sprice.Text;
                    dataGridView1.Rows.Add(row);
                    dataGridView1.CurrentCell = null;
                    if (isBarcode.Checked)
                    {
                        itemBarcode.Text = "";
                    }
                    iCount.Text = "1";

                }

                ///


                if (newSeller.Text == "تعديل")
                {

                }
                else
                {
                    newSeller.Text = "تعديل";
                    newSeller.BackColor = Color.Red;
                    newSeller.Activecolor = Color.Red;
                    newSeller.Normalcolor = Color.Red;
                    newSeller.OnHovercolor = Color.Crimson;
                }

            }
        }
        private void newSeller_Click(object sender, EventArgs e)
        {
            if (newSeller.Text == "مورد جديد")
            {
                FastAddUser fastuser = new FastAddUser();
                fastuser.FormClosed += FastAddUserClosed;
                fastuser.ShowDialog();
            }
            else if (newSeller.Text == "تعديل")
            {
                sellerName.Enabled = true;
                AddIn.Enabled = false;
                newSeller.Text = "حفظ";
                newbTN.Visible = true;
                newSeller.Size = new Size(65, 25);
                newSeller.BackColor = Color.Blue;
                newSeller.Activecolor = Color.Blue;
                newSeller.Normalcolor = Color.Blue;
                newSeller.OnHovercolor = Color.DarkBlue;
            }
            else if (newSeller.Text == "حفظ")
            {
                int id;
                int uid = 0;
                try
                {
                    id = int.Parse(sellerName.SelectedValue.ToString());
                    var cup = Clients.Find(ur => ur.id == id);
                    uid = cup.id;
                }
                catch
                {

                }
                if (uid <= 0)
                {
                    MessageBox.Show("برجاء إختيار المورد بشكل صحيح");
                }
                else if (uid == 1)
                {
                    if (dataGridView1.Rows.Count > 0)
                    {
                        AddIn.Enabled = true;
                    }
                    else
                    {
                        AddIn.Enabled = false;
                    }
                    sellerName.Enabled = false;
                    paidCash.Enabled = false;
                    newSeller.Text = "تعديل";
                    newbTN.Visible = false;
                    newSeller.Size = new Size(94, 25);
                    newSeller.BackColor = Color.Red;
                    newSeller.Activecolor = Color.Red;
                    newSeller.Normalcolor = Color.Red;
                    newSeller.OnHovercolor = Color.Crimson;
                }

                else
                {
                    if (dataGridView1.Rows.Count > 0)
                    {
                        AddIn.Enabled = true;
                    }
                    else
                    {
                        AddIn.Enabled = false;
                    }
                    sellerName.Enabled = false;
                    paidCash.Enabled = true;
                    newSeller.Text = "تعديل";
                    newbTN.Visible = false;
                    newSeller.Size = new Size(94, 25);
                    newSeller.BackColor = Color.Red;
                    newSeller.Activecolor = Color.Red;
                    newSeller.Normalcolor = Color.Red;
                    newSeller.OnHovercolor = Color.Crimson;

                }

            }
        }
        private void sumMethod()
        {
            foreach (DataGridViewRow ll in dataGridView1.Rows)
            {

                int TP = ll.Index;
                double co = Convert.ToDouble(dataGridView1.Rows[TP].Cells[2].Value);
                double pr = Convert.ToDouble(dataGridView1.Rows[TP].Cells[3].Value);
                double tospr;
                tospr = co * pr;
                dataGridView1.Rows[TP].Cells[4].Value = tospr.ToString();
            }

            Total.Text = "0";
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                int n = item.Index;
                var finalsum = (Double.Parse(Total.Text.ToString())
                + Double.Parse(dataGridView1.Rows[n].Cells[4].Value.ToString())).ToString();
                Total.Text = finalsum;
                paidCash.Text = finalsum;

            }

        }
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            sumMethod();
            AddIn.Enabled = true;
        }
        private void iCount_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(iCount.Text, "[^0-9]"))
            {
                MessageBox.Show("برجاء كتابة ارقام صحيحة فقط.");
                iCount.Text = iCount.Text.Remove(iCount.Text.Length - 1);
            }
        }
        private void paidCash_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(paidCash.Text))
            {
                paidCash.Text = Total.Text;
            }
            try
            {
                double finalsum = Math.Round(double.Parse(Total.Text), 2, MidpointRounding.AwayFromZero);
                double paidd = Math.Round(double.Parse(paidCash.Text), 2, MidpointRounding.AwayFromZero);
                double nunpaid = finalsum - paidd;
                var f = nunpaid.ToString();
                if (nunpaid >= 0)
                {
                    unPaid.Text = f;
                }
                else
                {
                    paidCash.Text = finalsum.ToString();
                }
            }
            catch
            {

            }

        }
        private void paidCash_EnabledChanged(object sender, EventArgs e)
        {
            if (paidCash.Enabled == false)
            {
                paidCash.Text = Total.Text;
            }
        }
        private void FavBar()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM FavBar where id =@id", dic).ConvertFavBar();
            if (check == null) return;

            foreach (FavBar item in check)
            {
                favBAR = item.Fav;
                if (favBAR == 1)
                {
                    isBarcode.Checked = true;
                    itemBarcode.Select();
                }
            }
        }
        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count >= 1)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    dataGridView1.Rows.RemoveAt(row.Index);
                    sumMethod();
                    if (dataGridView1.SelectedRows.Count <= 0)
                    {
                        paidCash.Text = Total.Text;
                    }
                }
            }
            else
            {
                MessageBox.Show("برجاء تحديد عنصر للحذف");
            }

        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl tb)
            {
                tb.KeyDown -= dataGridView1_KeyDown;
                tb.KeyDown += dataGridView1_KeyDown;
            }
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Delete))
            {
                if (dataGridView1.SelectedRows.Count >= 1)
                {
                    foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                    {
                        dataGridView1.Rows.RemoveAt(row.Index);
                        sumMethod();
                        if (dataGridView1.SelectedRows.Count <= 0)
                        {
                            paidCash.Text = Total.Text;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("برجاء تحديد عنصر للحذف");
                }
            }
        }
        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dataGridView1.Rows.Count >= 1)
            {
                AddIn.Enabled = true;
            }
            else
            {
                AddIn.Enabled = false;
            }
        }
        private void UpdateItems()
        {
            for (int iy = 0; iy < dataGridView1.Rows.Count; iy++)
            {
                string nbarcode = dataGridView1.Rows[iy].Cells["BarCode"].Value.ToString();
                var barcode = nbarcode;
                var cou = ItemP.Find(i => i.BarCode == barcode);
                var nowCount = cou.Count;
                double Plus = double.Parse(dataGridView1.Rows[iy].Cells["count"].Value.ToString());
                double NewCount = nowCount + Plus;
                double NowTB = cou.TotalBuy;
                double newTB = double.Parse(dataGridView1.Rows[iy].Cells["TotalBuy"].Value.ToString());
                double NewTotalB = NowTB + newTB;
                var inBP = dataGridView1.Rows[iy].Cells["BuyPrice"].Value.ToString();
                var inSP = dataGridView1.Rows[iy].Cells["SePrice"].Value.ToString();

                //
                var NumOfPurc = cou.NumOfPurc;
                // إجمالي عدد المشتريات من المنتج
                double newNumOfPurc = NumOfPurc + Plus;
                //

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@Count", NewCount);
                dic.Add("@inBP", inBP);
                dic.Add("@inSP", inSP);
                dic.Add("@inTB", NewTotalB);
                dic.Add("@barcode", cou.BarCode);
                dic.Add("@NumOfPurc", newNumOfPurc);


                var UpdateItems = DBConnection.SqliteCommand("update Items Set  Count = @Count , ItemPrice =@inBP , SellPrice =@inSP , TotalBuy =@inTB , NumOfPurc =@NumOfPurc where BarCode =@barcode", dic);

            }
            ItemP.Clear();
        }
        private void UpdateClients()
        {
            try
            {
                var id = int.Parse(sellerName.SelectedValue.ToString());
                var cup = Clients.Find(ic => ic.id == id);
                var nowCash = cup.Cash;
                var unpaid = Double.Parse(unPaid.Text);
                int cID = id;
                double newCash = nowCash + unpaid;

                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cID);
                addC.Add("@Cash", newCash);
                //var addToPurchases = DBConnection.SqliteCommand("INSERT  INTO `Purchases`(`Num`, `item`, `Count`, `ItemPrice`, `SellPrice`,`TotalBuy`,`TotalSell`,`Earn`,`unpaid`,`Client`,`ClientName`,`UserID`,`UserName`,`Date_ct`,`Time_ct`) values(@innum,@initem,@incount,@inBP,@inSP,@inTB,@inTS,@inEarn,@inUnpaid,@cID,@cName,@UserID,@UserName,@Date_ct,@Time_ct)", addC);
                var UpdateCu = DBConnection.SqliteCommand("update Clients Set Cash = @Cash where id=@cID", addC);
                Clients.Clear();

            }
            catch { }
        }
        private void InsertPurchases()
        {
            try
            {

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    int innum = int.Parse(label7.Text.ToString());
                    string initem = dataGridView1.Rows[i].Cells["Column5"].Value.ToString();
                    string barcoded = dataGridView1.Rows[i].Cells["BarCode"].Value.ToString();
                    var incount = dataGridView1.Rows[i].Cells["count"].Value.ToString();
                    var inBP = dataGridView1.Rows[i].Cells["BuyPrice"].Value.ToString();
                    var inTB = dataGridView1.Rows[i].Cells["TotalBuy"].Value.ToString();
                    var inSSP = dataGridView1.Rows[i].Cells["SePrice"].Value.ToString();
                    var cID = int.Parse(sellerName.SelectedValue.ToString());
                    var cName = this.sellerName.GetItemText(this.sellerName.SelectedItem);

                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@innum", innum);
                    addC.Add("@initem", initem);
                    addC.Add("@inbarcode", barcoded);
                    addC.Add("@UserID", Login.cid);
                    addC.Add("@UserName", Login.cnn);
                    addC.Add("@incount", incount);
                    addC.Add("@inBP", inBP);
                    addC.Add("@inTB", inTB);
                    addC.Add("@inSSp", inSSP);
                    addC.Add("@cID", cID);
                    addC.Add("@cName", cName);
                    addC.Add("@Date_ct", DateTime.Now.ToString("yyyy-MM-dd"));
                    addC.Add("@Time_ct", DateTime.Now.ToString("HH:mm:ss"));
                    var addToPurchases = DBConnection.SqliteCommand("INSERT  INTO `Purchases`(`Num`, `item`, `BarCode`, `Count`, `ItemPrice`,`sPrice`,`TotalBuy`,`Client`,`ClientName`,`UserID`,`UserName`,`Date_ct`,`Time_ct`) values(@innum,@initem,@inbarcode,@incount,@inBP,@inSSp,@inTB,@cID,@cName,@UserID,@UserName,@Date_ct,@Time_ct)", addC);
                }
            }
            catch
            {
            }
        }
        private void InsertCashInvD()
        {
            try
            {
                int invNum = int.Parse(label7.Text.ToString());
                var unpaid = unPaid.Text;
                var totalInv = Total.Text;
                var clientID = int.Parse(sellerName.SelectedValue.ToString());
                var clientName = this.sellerName.GetItemText(this.sellerName.SelectedItem);

                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@invNum", invNum);
                addC.Add("@unpaid", unpaid);
                addC.Add("@totalInv", totalInv);
                addC.Add("@clientID", clientID);
                addC.Add("@userID", Login.cid);
                addC.Add("@clientName", clientName);
                addC.Add("@userName", Login.cnn);
                addC.Add("@invDateTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var addToPurchases = DBConnection.SqliteCommand("INSERT  INTO `CashInvD`(`invNum`, `unpaid`, `totalInv`, `clientID`, `userID`,`clientName`,`userName`,`invDateTime`) values(@invNum,@unpaid,@totalInv,@clientID,@userID,@clientName,@userName,@invDateTime)", addC);
            }
            catch
            {
            }
        }
        private void LoadData()
        {
            Clients.Clear();
            ItemP.Clear();
            ItemsData();
            ClientsData();
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
        }
        private void AfterAdd()
        {
            FavBar();
            Total.Text = "0";
            iCount.Text = "1";
            paidCash.Text = "0";
            bPrice.Text = "";
            Sprice.Text = "";
            if (sellerName.Enabled == false)
            {
                sellerName.Enabled = true;
            }
            AddIn.Enabled = false;
            itemBarcode.Text = "";
            newSeller.Text = "مورد جديد";
            newSeller.BackColor = Color.FromArgb(18, 119, 100);
            newSeller.Activecolor = Color.FromArgb(18, 119, 100);
            newSeller.Normalcolor = Color.FromArgb(18, 119, 100);
            newSeller.OnHovercolor = Color.FromArgb(18, 119, 210);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة فاتورة الشراء ؟", "تم إضافة الفاتورة بنجاح", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                PrintPurchasesInvoice();
                PrintBarCodes();
                InvoiceNum();
            }
            else if (dialogResult == DialogResult.No)
            {
                PrintBarCodes();
                InvoiceNum();
            }

        }
        private void PrintPurchasesInvoice()
        {

            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@r", NuM);
                var check = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@r", dic);
                var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@r", dic).ConvertCashInvoice();
                if (invCash == null) return;
                if (check == null) return;
                if (check.Rows.Count > 0)
                {
                    foreach (CashInvD item in invCash)
                    {
                        inunpaid12 = item.unpaid;
                        var invtotal12 = item.totalInv;
                        invpaid12 = invtotal12 - inunpaid12;
                    }

                    string d1 = DateTime.Now.ToString("dd");
                    string m1 = DateTime.Now.ToString("MM");
                    string y1 = DateTime.Now.ToString("yyyy");
                    string ti1 = DateTime.Now.ToString("t");
                    string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                    string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                    string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                    Print2P printPurch = new Print2P();
                    printPurch.SetDataSource(check);
                    printPurch.SetParameterValue("invsName", "فاتورة مشتريات");
                    printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                    printPurch.SetParameterValue("paid", invpaid12);
                    printPurch.SetParameterValue("unpaid", inunpaid12);
                    printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                    printPurch.SetParameterValue("name", Dashboard.invCoN);
                    printPurch.SetParameterValue("slug", Dashboard.invCoS);
                    printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                    printPurch.SetParameterValue("address", Dashboard.invCoA);
                    printPurch.PrintToPrinter(1, false, 0, 0);
                }

            }
            catch
            {
            }
        }
        private void printLast_Click(object sender, EventArgs e)
        {
            PrintLastInv();
        }
        private void PrintLastInv()
        {
            try
            {
                int Lastinv = NuM - 1;
                var finalnumber = "";
                if (Lastinv >= 1 && Lastinv < 10)
                {
                    finalnumber = "000000" + Lastinv.ToString();
                }
                else if (Lastinv >= 10 && Lastinv < 100)
                {
                    finalnumber = "00000" + Lastinv.ToString();
                }
                else if (Lastinv >= 100 && Lastinv < 1000)
                {
                    finalnumber = "0000" + Lastinv.ToString();
                }
                else if (Lastinv >= 1000 && Lastinv < 10000)
                {
                    finalnumber = "000" + Lastinv.ToString();
                }
                else if (Lastinv >= 10000 && Lastinv < 100000)
                {
                    finalnumber = "00" + Lastinv.ToString();
                }
                else if (Lastinv >= 100000 && Lastinv < 1000000)
                {
                    finalnumber = "0" + Lastinv.ToString();
                }
                else if (Lastinv >= 1000000)
                {
                    finalnumber = Lastinv.ToString();
                }
                DialogResult dialogResult = MessageBox.Show("هل تود طباعة اخر فاتورة مشتريات رقم" + " " + ":" + " " + finalnumber, "طباعة فاتورة مشتريات", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@r", Lastinv);
                    var check = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@r", dic);
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@r", dic).ConvertCashInvoice();
                    if (invCash == null) return;
                    if (check == null) return;
                    if (check.Rows.Count > 0)
                    {
                        foreach (CashInvD item in invCash)
                        {
                            inunpaid13 = item.unpaid;
                            var invtotal13 = item.totalInv;
                            invpaid13 = invtotal13 - inunpaid12;
                        }
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2P printPurch = new Print2P();
                        printPurch.SetDataSource(check);
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("invsName", "فاتورة مشتريات");
                        printPurch.SetParameterValue("paid", invpaid13);
                        printPurch.SetParameterValue("unpaid", inunpaid13);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                        printPurch.SetParameterValue("address", Dashboard.invCoA);
                        printPurch.PrintToPrinter(1, false, 0, 0);
                    }

                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
            catch
            {
            }
        }
        private void AddIn_Click(object sender, EventArgs e)
        {
            if (double.Parse(unPaid.Text) > 0)
            {
                UpdateClients();
                InsertPurchases();
                UpdateItems();
                InsertCashInvD();
                LoadData();
                AfterAdd();
            }
            else
            {
                InsertPurchases();
                UpdateItems();
                InsertCashInvD();
                LoadData();
                AfterAdd();
            }
        }
        private void PrintBarCodes()
        {
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة ملصقات باركود فاتورة المشتريات ؟", "طباعة ملصق باركود", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                int pn;
                int Lastinv = NuM;
                string value = "";
                if (Tmp.InputBox("طباعة ملصقات الباركود", "عدد الصفحات المراد طباعتها ؟ ، عدد الملصقات في كل صفحة 24 ملصق للمنتج", ref value) == DialogResult.OK)
                {
                    pn = int.Parse(value);
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@r", Lastinv);
                    var check = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@r", dic).ConvertPurchases();
                    if (check == null) return;
                    if (check.Count > 0)
                    {
                        foreach (Purchase item in check)
                        {
                            BarCodes BarC = new BarCodes();
                            BarC.SetDataSource(check);
                            string brCode = item.BarCode.Substring(0, (item.BarCode.Length - 1));
                            iName = item.item;
                            SSPrice = item.sPrice.ToString();
                            Check12Digits = brCode.PadRight(12, '0');
                            Barrcode = EAN13Class.EAN13(Check12Digits);
                            BarC.SetParameterValue("Name", iName);
                            BarC.SetParameterValue("Barcode", Barrcode);
                            BarC.SetParameterValue("Sprice", "السعر" + " " + SSPrice);
                            BarC.PrintToPrinter(pn, false, 0, 0);
                        }

                    }


                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }

        }
        // صفحة سجل المشتريات

        private void bPrint_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                PrintInvNum();
            }
            else
            {
                PrintDate();
            }
            checkBox1.Checked = false;
        }
        private void dataGridView2_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (dataGridView2.Rows.Count >= 1)
            {
                bPrint.Enabled = true;
            }
            else
            {
                bPrint.Enabled = false;
            }
        }
        private void dataGridView2_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dataGridView2.Rows.Count >= 1)
            {
                bPrint.Enabled = true;
            }
            else
            {
                bPrint.Enabled = false;
            }
        }
        private void ViewBTN_Click(object sender, EventArgs e)
        {
            try
            {
                TodaySells.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
                string fd = fDay.SelectedItem.ToString();
                string fm = fMonth.SelectedItem.ToString();
                string fy = fYear.SelectedItem.ToString();
                string td = tDay.SelectedItem.ToString();
                string tm = tMonth.SelectedItem.ToString();
                string ty = tYear.SelectedItem.ToString();
                string f = fy + "-" + fm + "-" + fd;
                string t = ty + "-" + tm + "-" + td;
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select * from Purchases where Date_ct between '" + f + "' and '" + t + "'", dic).ConvertPurchases();
                if (result == null) return;
                foreach (Purchase item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView2);
                    row.Cells[0].Value = item.Num;
                    row.Cells[1].Value = item.item;
                    row.Cells[2].Value = item.Count;
                    row.Cells[3].Value = item.ItemPrice;
                    row.Cells[4].Value = item.TotalBuy;
                    row.Cells[5].Value = item.ClientName;
                    row.Cells[6].Value = item.UserName;
                    row.Cells[7].Value = item.Time_ct.ToString("t");
                    row.Cells[8].Value = item.Date_ct.ToString("yyyy-MM-dd");
                    dataGridView2.Rows.Add(row);
                    dataGridView2.CurrentCell = null;
                }
                Calculate2();
            }
            catch { }
        }
        private void Calculate2()
        {

            TodaySells.Text = "0";
            try
            {

                foreach (DataGridViewRow item in dataGridView2.Rows)
                {
                    int n = item.Index;
                    TodaySells.Text = (Double.Parse(TodaySells.Text.ToString())
                    + Double.Parse(dataGridView2.Rows[n].Cells[4].Value.ToString())).ToString();
                }
            }
            catch { }

        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                fDay.Enabled = false;
                tDay.Enabled = false;
                fMonth.Enabled = false;
                tMonth.Enabled = false;
                fYear.Enabled = false;
                tYear.Enabled = false;
                ViewBTN.Enabled = false;
                invNumSearch.Enabled = true;
                TodaySells.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
            }
            else
            {
                fDay.Enabled = true;
                tDay.Enabled = true;
                fMonth.Enabled = true;
                tMonth.Enabled = true;
                fYear.Enabled = true;
                tYear.Enabled = true;
                ViewBTN.Enabled = true;
                invNumSearch.Enabled = false;
                TodaySells.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
            }
        }
        private void InvNumberSearching()
        {
            try
            {
                TodaySells.Text = "0";
                dataGridView2.Rows.Clear();
                dataGridView2.Refresh();
                string invN = invNumSearch.Text;
                invN = invN.TrimStart('0');
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("num", invN); ;
                var result = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@num", dic).ConvertPurchases();
                if (result == null) return;
                foreach (Purchase item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView2);
                    row.Cells[0].Value = item.Num;
                    row.Cells[1].Value = item.item;
                    row.Cells[2].Value = item.Count;
                    row.Cells[3].Value = item.ItemPrice;
                    row.Cells[4].Value = item.TotalBuy;
                    row.Cells[5].Value = item.ClientName;
                    row.Cells[6].Value = item.UserName;
                    row.Cells[7].Value = item.Time_ct.ToString("t");
                    row.Cells[8].Value = item.Date_ct.ToString("yyyy-MM-dd");
                    dataGridView2.Rows.Add(row);
                    dataGridView2.CurrentCell = null;
                }
                Calculate2();
            }
            catch
            {

            }
        }
        private void invNumSearch_TextChanged(object sender, EventArgs e)
        {
            InvNumberSearching();
        }
        private void PrintDate()
        {
            string fd = fDay.SelectedItem.ToString();
            string fm = fMonth.SelectedItem.ToString();
            string fy = fYear.SelectedItem.ToString();
            string td = tDay.SelectedItem.ToString();
            string tm = tMonth.SelectedItem.ToString();
            string ty = tYear.SelectedItem.ToString();
            string f = fy + "-" + fm + "-" + fd;
            string t = ty + "-" + tm + "-" + td;
            string f1 = Login.arNumber(fd) + " " + "-" + " " + Login.arDate(fm) + " " + "-" + " " + Login.arNumber(fy);
            string t1 = Login.arNumber(td) + " " + "-" + " " + Login.arDate(tm) + " " + "-" + " " + Login.arNumber(ty);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة سجل المشتريات في الفترة من" + " " + f1 + " " + "حتي" + " " + t1, "طباعة سجل مشتريات", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("Select * from Purchases where Date_ct between '" + f + "' and '" + t + "'", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        SpicialPurchprint printPurch = new SpicialPurchprint();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("dFrom", f1);
                        printPurch.SetParameterValue("dTo", t1);
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.PrintToPrinter(1, false, 0, 0);
                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void PrintInvNum()
        {
            string inv = invNumSearch.Text;
            int sNuM = int.Parse(inv.ToString());
            var finalnumber = "";
            if (sNuM >= 1 && sNuM < 10)
            {
                finalnumber = "000000" + sNuM.ToString();
            }
            else if (sNuM >= 10 && sNuM < 100)
            {
                finalnumber = "00000" + sNuM.ToString();
            }
            else if (sNuM >= 100 && sNuM < 1000)
            {
                finalnumber = "0000" + sNuM.ToString();
            }
            else if (sNuM >= 1000 && sNuM < 10000)
            {
                finalnumber = "000" + sNuM.ToString();
            }
            else if (sNuM >= 10000 && sNuM < 100000)
            {
                finalnumber = "00" + sNuM.ToString();
            }
            else if (sNuM >= 100000 && sNuM < 1000000)
            {
                finalnumber = "0" + sNuM.ToString();
            }
            else if (sNuM >= 1000000)
            {
                finalnumber = sNuM.ToString();
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("num", inv);
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة فاتورة مشتريات رقم" + " " + ":" + " " + finalnumber, "طباعة فاتورة مشتريات", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var result = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@num", dic);
                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@num", dic).ConvertCashInvoice();
                    if (invCash == null) return;
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        foreach (CashInvD item in invCash)
                        {
                            inunpaid1 = item.unpaid;
                            invtotal1 = item.totalInv;
                            invpaid1 = invtotal1 - inunpaid1;
                        }

                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        Print2P printPurch = new Print2P();
                        printPurch.SetDataSource(result);
                        printPurch.SetParameterValue("invsName", "فاتورة مشتريات");
                        printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                        printPurch.SetParameterValue("paid", invpaid1);
                        printPurch.SetParameterValue("unpaid", inunpaid1);
                        printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                        printPurch.SetParameterValue("name", Dashboard.invCoN);
                        printPurch.SetParameterValue("slug", Dashboard.invCoS);
                        printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                        printPurch.SetParameterValue("address", Dashboard.invCoA);
                        printPurch.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        // صفحة المؤجلات
        private void LoadUnPaidInv()
        {
            try
            {
                dataGridView3.Rows.Clear();
                dataGridView3.Refresh();

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@unpaid", 0);
                var result = DBConnection.SqliteCommand("Select * from CashInvD where unpaid > @unpaid ", dic).ConvertCashInvoice();
                if (result == null) return;
                foreach (CashInvD item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView3);
                    row.Cells[0].Value = item.invNum;
                    row.Cells[2].Value = item.unpaid;
                    row.Cells[3].Value = item.totalInv;
                    row.Cells[4].Value = item.clientName;
                    row.Cells[5].Value = item.userName;
                    row.Cells[6].Value = item.invDateTime.ToString("yyyy-MM-dd");
                    row.Cells[7].Value = item.invDateTime.ToString("t");
                    dataGridView3.Rows.Add(row);
                    dataGridView3.Sort(dataGridView3.Columns[0], ListSortDirection.Descending);
                    dataGridView3.Rows[0].Selected = true;
                }
                SumUnPaid();
            }
            catch { }
        }
        private void SumUnPaid()
        {
            try
            {
                foreach (DataGridViewRow ll in dataGridView3.Rows)
                {
                    int TP = ll.Index;
                    double u = Convert.ToDouble(dataGridView3.Rows[TP].Cells[2].Value);
                    double t = Convert.ToDouble(dataGridView3.Rows[TP].Cells[3].Value);
                    double pa;
                    pa = t - u;
                    dataGridView3.Rows[TP].Cells[1].Value = pa.ToString();
                }

                label3.Text = "0";
                foreach (DataGridViewRow item in dataGridView3.Rows)
                {
                    int n = item.Index;
                    var finalsum = (Double.Parse(label3.Text.ToString())
                    + Double.Parse(dataGridView3.Rows[n].Cells[2].Value.ToString())).ToString();
                    label3.Text = finalsum;
                }
            }
            catch { }
        }
        private void ViewInvByNum_Click(object sender, EventArgs e)
        {
            if (InvNNN.Length > 0)
            {
                ViewInvoicePage viewinv = new ViewInvoicePage(InvNNN);
                viewinv.ShowDialog();
            }
            else
            {
                MessageBox.Show("برجاء إختيار فاتورة للعرض");
            }
        }
        private void PrintInvByNum_Click(object sender, EventArgs e)
        {
            try
            {
                string inv = InvNNN;
                int sNuM = int.Parse(inv.ToString());
                var finalnumber = "";
                if (sNuM >= 1 && sNuM < 10)
                {
                    finalnumber = "000000" + sNuM.ToString();
                }
                else if (sNuM >= 10 && sNuM < 100)
                {
                    finalnumber = "00000" + sNuM.ToString();
                }
                else if (sNuM >= 100 && sNuM < 1000)
                {
                    finalnumber = "0000" + sNuM.ToString();
                }
                else if (sNuM >= 1000 && sNuM < 10000)
                {
                    finalnumber = "000" + sNuM.ToString();
                }
                else if (sNuM >= 10000 && sNuM < 100000)
                {
                    finalnumber = "00" + sNuM.ToString();
                }
                else if (sNuM >= 100000 && sNuM < 1000000)
                {
                    finalnumber = "0" + sNuM.ToString();
                }
                else if (sNuM >= 1000000)
                {
                    finalnumber = sNuM.ToString();
                }
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("num", inv);
                DialogResult dialogResult = MessageBox.Show("هل تود طباعة إيصال فاتورة مشتريات رقم" + " " + ":" + " " + finalnumber, "طباعة فاتورة مشتريات", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    try
                    {
                        var result = DBConnection.SqliteCommand("SELECT * FROM Purchases where Num=@num", dic);
                        var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvD where invNum=@num", dic).ConvertCashInvoice();
                        if (invCash == null) return;
                        if (result == null) return;
                        if (result.Rows.Count > 0)
                        {
                            foreach (CashInvD item in invCash)
                            {
                                inunpaid1 = item.unpaid;
                                invtotal1 = item.totalInv;
                                invpaid1 = invtotal1 - inunpaid1;
                            }

                            string d1 = DateTime.Now.ToString("dd");
                            string m1 = DateTime.Now.ToString("MM");
                            string y1 = DateTime.Now.ToString("yyyy");
                            string ti1 = DateTime.Now.ToString("t");
                            string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                            string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                            string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                            Print2P printPurch = new Print2P();
                            printPurch.SetDataSource(result);
                            printPurch.SetParameterValue("invsName", "فاتورة مشتريات");
                            printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                            printPurch.SetParameterValue("paid", invpaid1);
                            printPurch.SetParameterValue("unpaid", inunpaid1);
                            printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                            printPurch.SetParameterValue("name", Dashboard.invCoN);
                            printPurch.SetParameterValue("slug", Dashboard.invCoS);
                            printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                            printPurch.SetParameterValue("address", Dashboard.invCoA);
                            printPurch.PrintToPrinter(1, false, 0, 0);

                        }

                    }
                    catch
                    {
                    }

                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
            catch
            {

            }
        }
        private void dataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView3.SelectedRows)
                {
                    if (dataGridView3.SelectedRows.Count > 0)
                    {
                        InvNNN = row.Cells[0].Value.ToString();
                        ViewInvByNum.Enabled = true;
                        PrintInvByNum.Enabled = true;
                        PayBtn.Enabled = true;
                    }
                    else
                    {
                        InvNNN = "";
                        ViewInvByNum.Enabled = false;
                        PrintInvByNum.Enabled = false;
                        PayBtn.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void newItem_Click(object sender, EventArgs e)
        {
            FastAddItem fastnewitem = new FastAddItem();
            fastnewitem.FormClosed += FastAddItemClosed;
            fastnewitem.ShowDialog();
        }
        void FastAddItemClosed(object sender, FormClosedEventArgs e)
        {
            ItemP.Clear();
            ItemsData();
        }
        void FastAddUserClosed(object sender, FormClosedEventArgs e)
        {
            Clients.Clear();
            ClientsData();
        }
        void paynowClosed(object sender, FormClosedEventArgs e)
        {
            LoadUnPaidInv();
        }
        private void newbTN_Click(object sender, EventArgs e)
        {
            FastAddUser fastuser = new FastAddUser();
            fastuser.FormClosed += FastAddUserClosed;
            fastuser.ShowDialog();
        }
        private void PayBtn_Click(object sender, EventArgs e)
        {
            PayThePurchase paynow = new PayThePurchase(InvNNN);
            paynow.FormClosed += paynowClosed;
            paynow.ShowDialog();
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }

            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

         
        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView2.SelectedRows)
                {
                    if (dataGridView2.SelectedRows.Count > 0)
                    {
                        InvViews = row.Cells[0].Value.ToString();
                        invVIEW.Enabled = true;
                    }
                    else
                    {
                        InvViews = "";
                        invVIEW.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void invVIEW_Click_1(object sender, EventArgs e)
        {
            if (InvViews.Length > 0)
            {
                ViewInvoicePage viewinv = new ViewInvoicePage(InvViews);
                viewinv.ShowDialog();
            }
            else
            {
                MessageBox.Show("برجاء إختيار فاتورة للعرض");
            }
        }


        // صفحة المرتجعات التالفة

        private void LoadBadP()
        {
            try
            {
                RWait.Rows.Clear();
                RWait.Refresh();

                Dictionary<string, object> dic = new Dictionary<string, object>();
                var result = DBConnection.SqliteCommand("Select * from WaitRE", dic).ConvertWAR();
                if (result == null) return;
                foreach (WaitRE item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(RWait);
                    row.Cells[0].Value = item.id;
                    row.Cells[1].Value = item.BarCode;
                    row.Cells[2].Value = item.ItemName;
                    row.Cells[3].Value = item.reCount;
                    row.Cells[4].Value = item.rePrice;
                    row.Cells[5].Value = item.Reason;
                    row.Cells[6].Value = item.DateTime_re.ToString("yyyy-MM-dd");
                    RWait.Rows.Add(row);
                    RWait.Sort(RWait.Columns[0], ListSortDirection.Descending);
                    RWait.Rows[0].Selected = true;
                }
            }
            catch { }
        }
        private void LoadBadItems()
        {
            try
            {
                BadItemsP.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var Ccl = DBConnection.SqliteCommand("SELECT *  FROM BadItems", dic);
                if (Ccl == null) return;
                if (Ccl.Rows.Count > 0)
                {
                    foreach (DataRow item in Ccl.Rows)
                    {
                        BadItems u = new BadItems();
                        u.id = int.Parse(item["id"].ToString());
                        u.BarCode = item["BarCode"].ToString();
                        u.ItemName = item["ItemName"].ToString();
                        u.Count = double.Parse(item["Count"].ToString());
                        u.LastReDate = DateTime.Parse(item["LastReDate"].ToString());

                        BadItemsP.Add(u);
                    }
                }
            }
            catch { }
        }
        private void RWait_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in RWait.SelectedRows)
                {
                    if (RWait.SelectedRows.Count > 0)
                    {
                        riddd = int.Parse(row.Cells[0].Value.ToString());
                        brccc = row.Cells[1].Value.ToString();
                        reccc = Double.Parse(row.Cells[3].Value.ToString());
                        bpccc = Double.Parse(row.Cells[4].Value.ToString());
                        edP.Enabled = true;
                        reP.Enabled = true;
                    }
                    else
                    {
                        riddd = 0;
                        brccc = "";
                        reccc = 0;
                        bpccc = 0;
                        edP.Enabled = false;
                        reP.Enabled = false;
                    }
                }
            }
            catch { }
        }
        private void edP_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("إستبدال الكمية المحددة ؟", "إستبدال منتج", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    var ite = ItemP.Find(i => i.BarCode == brccc);
                    var newcounts = ite.Count + reccc;
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@Count", newcounts);
                    dic.Add("@barcode", brccc);
                    var UpdateItems = DBConnection.SqliteCommand("update Items Set  Count = @Count  where BarCode =@barcode", dic);
                    updateWA();
                    LoadBadP();
                    cMessgeBox mess = new cMessgeBox("تم إستبدال الكمية بنجاح", "done", "p", 1000);
                    mess.ShowDialog();
                }
                catch { }
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void reP_Click(object sender, EventArgs e)
        {
            ReItemP re = new ReItemP(brccc, reccc, riddd);
            re.FormClosed += reClosed;
            re.ShowDialog();

        }
        private void updateWA()
        {
            try
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@id", riddd);
                var DelCashInvS = DBConnection.SqliteCommand("DELETE FROM WaitRE WHERE id =@id", dics);
            }
            catch { }
        }
        void reClosed(object sender, FormClosedEventArgs e)
        {
            updateWA();
            LoadBadP();
        }

    }
}


