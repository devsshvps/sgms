﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using KimtToo.VisualReactive;
using WMPLib;
using SGMS.Data;
using SGMS.Models;
using System.Threading;

namespace SGMS
{
    public partial class Dashboard : Form
    {
        private System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        public static string invLogo = "";
        public static string logopath = "";
        public static string invCoN = "";
        public static string invCoS = "";
        public static string invCoA = "";
        public static string invCoP = "";

        string MediaFile;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        bool exed = false;
        WindowsMediaPlayer wplayer = new WindowsMediaPlayer();
        string fileName = "System.Silver";
        string newName = "dont.DELETE.Silver";
        string sourcePath = "";
        string targetPath = @"C:\ProgramData\SGMS\BACKUP";

        public Dashboard()
        {
            InitializeComponent();
            tip.AutoPopDelay = 5000;
            tip.InitialDelay = 1000;
            tip.ReshowDelay = 500;
            tip.SetToolTip(this.msg, "الرسائل");
            tip.SetToolTip(this.info, "المفكرة");
            tip.SetToolTip(this.myaccount, "حسابي الشخصي");



        }


        private void panel2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }

        }

        private void AMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void AMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void AClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AToolbar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            LoadStoreInfo();
            CheckMail();
            CheckNotice();
            checkStore();
            LoadNotes();
            CheckRoles();
            label4.Text = Login.cnn;
            myTimer.Start();
            myTimer.Enabled = true;
            myTimer.Interval = 1000;
            myTimer.Tick += myTimer_Tick;
            onload();
            backUPdb();



        }
        private void myTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                sec.Text = DateTime.Now.ToString("ss");
                tim.Text = DateTime.Now.ToString("t");
                day.Text = DateTime.Now.ToString("dddd");
                dat.Text = DateTime.Now.ToString("dd/M/yyyy");
                ProgressBar1.Value = Convert.ToInt32(sec.Text);

            }
            catch (Exception)
            {
                return;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool bHandled = false;
            switch (keyData)
            {
                case Keys.F5:
                    if (wplayer.playState == WMPLib.WMPPlayState.wmppsPlaying)
                    {
                        wplayer.controls.pause();
                    }
                    else
                    {
                        wplayer.controls.play();
                    }
                    bHandled = true;
                    break;
            }
            return bHandled;
        }

        private void aboutBTN_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void tab2_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var sales = new Sales();
            ShowPanel.Controls.Add(sales);
        }

        private void tab1_Click(object sender, EventArgs e)
        {
            if (Login.r1 == "0")
            {
                ShowPanel.Controls.Clear();
                var User = new UserPage();
                ShowPanel.Controls.Add(User);
            }else
            {
                ShowPanel.Controls.Clear();
                var Admin = new AdminPage();
                ShowPanel.Controls.Add(Admin);
            }
        }

        private void tab3_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var purchases = new Purchases();
            ShowPanel.Controls.Add(purchases);
        }

        private void tab4_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var reports = new Reports();
            ShowPanel.Controls.Add(reports);
        }

        private void tab5_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var ite = new Items();
            ShowPanel.Controls.Add(ite);
        }

        private void tab6_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var Clie = new Clients();
            ShowPanel.Controls.Add(Clie);
        }

        private void tab7_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var PuClie = new PurchaseClients();
            ShowPanel.Controls.Add(PuClie);
        }

        private void tab8_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var sett = new Settings(this);
            ShowPanel.Controls.Add(sett);
        }

        private void myaccount_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var MyAcc = new MyAccount();
            ShowPanel.Controls.Add(MyAcc);
        }
        private void msg_Click(object sender, EventArgs e)
        {
            toolTip2.Dispose();
            ShowPanel.Controls.Clear();
            var Msg = new Mail();
            ShowPanel.Controls.Add(Msg);
        }
        private void info_Click(object sender, EventArgs e)
        {
            tip.Dispose();
            ShowPanel.Controls.Clear();
            var infor = new AccNotice();
            ShowPanel.Controls.Add(infor);
        }
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            CheckMail();

        }
        private void CheckMail()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", Login.cid);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Mail where toID =@id", dic).ConvertMail();
            if (check == null) return;

            foreach (Models.Mail item in check)
            {

                var Mes = item.m.ToString();

                if (Mes == "1")
                {
                    toolTip2.IsBalloon = true;
                    toolTip2.ToolTipIcon = ToolTipIcon.Info;
                    toolTip2.ShowAlways = true;
                    toolTip2.UseFading = true;
                    toolTip2.UseAnimation = true;
                    toolTip2.ToolTipTitle = "رسالة جديدة";
                    toolTip2.Show("لديك رسالة جديدة", msg);
                    msg.BackColor = Color.FromArgb(18, 119, 165);

                }
            }
        }
        private void CheckRoles()
        {
            if (Login.r2 == "0" && Login.r3 == "0" && Login.r4 == "0" && Login.r5 == "0" && Login.r6 == "0" && Login.r7 == "0")
            {
                //المبيعات
                tab2.Visible = false;
            }

            if (Login.r8 == "0" && Login.r9 == "0" && Login.r10 == "0" && Login.r11 == "0")
            {
                //المشتريات
                tab3.Visible = false;
            }

            if (Login.r12 == "0" && Login.r13 == "0" && Login.r14 == "0")
            {
                //التقارير
                tab4.Visible = false;
            }

            if (Login.r15 == "0")
            {
                //المنتجات
                tab5.Visible = false;
            }

            if (Login.r16 == "0")
            {
                //قائمة العملاء
                tab6.Visible = false;
            }
            if (Login.r17 == "0")
            {
                //قائمة الموردين
                tab7.Visible = false;
            }

            if (Login.r18 == "0")
            {
                //حركة الخزنة
                tab9.Visible = false;
            }
            if (Login.r19 == "0" && Login.r20 == "0")
            {
                //الماليات
                tabX.Visible = false;
            }
            if (Login.r21 == "0" && Login.r22 == "0" && Login.r23 == "0")
            {
                // الإعدادات
                tab8.Visible = false;
            }
        }
        private void checkStore()
        {
            if (StoreName.Text == "برنامج سيلفر لإدارة الحسابات" || StoreLogo.ImageLocation == "usrData/Logo.png")
            {
                toolTip1.IsBalloon = true;
                toolTip1.ToolTipIcon = ToolTipIcon.Info;
                toolTip1.ShowAlways = true;
                toolTip1.UseFading = true;
                toolTip1.UseAnimation = true;
                toolTip1.ToolTipTitle = "ملاحظة";
                toolTip1.Show("يمكنك تعديل إسم الشركة والشعار من خلال - البيانات - إعدادات البرنامج - إعدادات عامة", StoreName);

            }
        }
        private static List<Setting> SettingP = new List<Setting>();
        public void LoadStoreInfo()
        {
            try
            {
                SettingP.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id <=@id", dic);

                for (int i = 0; i < result.Rows.Count; i++)
                {
                    var _id = int.Parse(result.Rows[i]["id"].ToString());
                    var _StoreName = result.Rows[i]["StoreName"].ToString();
                    var _StoreLogo = result.Rows[i]["StoreLogo"].ToString();
                    var _MediaName = result.Rows[i]["MediaName"].ToString();
                    var _PrintLogo = result.Rows[i]["PrintLogo"].ToString();
                    var _PrintCoName = result.Rows[i]["PrintCoName"].ToString();
                    var _PrintCoSlu = result.Rows[i]["PrintCoSlu"].ToString();
                    var _PrintCoAddress = result.Rows[i]["PrintCoAddress"].ToString();
                    var _PrintCoPhone = result.Rows[i]["PrintCoPhone"].ToString();
                    var p = new Setting
                    {
                        id = _id,
                        StoreName = _StoreName,
                        StoreLogo = _StoreLogo,
                        MediaName = _MediaName,
                        PrintLogo = _PrintLogo,
                        PrintCoName = _PrintCoName,
                        PrintCoSlu = _PrintCoSlu,
                        PrintCoAddress = _PrintCoAddress,
                        PrintCoPhone = _PrintCoPhone
                    };
                    SettingP.Add(p);
                }

                int nid = 1;
                var cup = SettingP.Find(i => i.id == nid);
                var nStoreName = cup.StoreName.ToString();
                var nStoreLogo = cup.StoreLogo.ToString();

                StoreName.Text = nStoreName;
                StoreLogo.ImageLocation = nStoreLogo;

                invLogo = cup.PrintLogo;
                logopath = Application.StartupPath + "//" + invLogo;
                invCoN = cup.PrintCoName;
                invCoS = cup.PrintCoSlu;
                invCoA = cup.PrintCoAddress;
                invCoP = cup.PrintCoPhone;

                MediaFile = cup.MediaName.ToString();
                wplayer.URL = MediaFile;
                wplayer.controls.stop();

                while (StoreName.Width < System.Windows.Forms.TextRenderer.MeasureText(StoreName.Text,
         new Font(StoreName.Font.FontFamily, StoreName.Font.Size, StoreName.Font.Style)).Width)
                {
                    StoreName.Font = new Font(StoreName.Font.FontFamily, StoreName.Font.Size - 0.5f, StoreName.Font.Style);
                }
            }
            catch { }
 
        }
        private void CheckNotice()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", Login.cid);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Notice where UserID =@id and r = 1", dic).ConvertNotices();
            if (check == null) return;

            foreach (Notice item in check)
            {
                var nID = item.id.ToString();
                var RemmDate = item.RemDate.ToString("yyyy-MM-dd");
                var ShowDate = item.showDate.ToString("yyyy-MM-dd");
                var TMouth = DateTime.Now.ToString("yyyy-MM");
                var Ttoday = DateTime.Now.ToString("dd");

                var Todaydate = DateTime.Now.ToString("yyyy-MM"+"-"+Ttoday);

                var yesterday = int.Parse(Ttoday) - 1;
                var yesterdaydate = DateTime.Now.ToString("yyyy-MM" + "-" + yesterday);

                if (ShowDate == Todaydate)
                {
                    tip.IsBalloon = true;
                    tip.ToolTipIcon = ToolTipIcon.Info;
                    tip.ShowAlways = true;
                    tip.UseFading = true;
                    tip.UseAnimation = true;
                    tip.ToolTipTitle = "مفكرة بتاريخ اليوم";
                    tip.Show("تم تحديد عرض مفكرة بتاريخ اليوم", info);
                    info.BackColor = Color.FromArgb(18, 119, 165);

                }
                else if (RemmDate == Todaydate)
                {
                    tip.IsBalloon = true;
                    tip.ToolTipIcon = ToolTipIcon.Info;
                    tip.ShowAlways = true;
                    tip.UseFading = true;
                    tip.UseAnimation = true;
                    tip.ToolTipTitle = "تذكير بمفكرة الغد";
                    tip.Show("تم تحديد عرض مفكرة بتاريخ الغد", info);
                    info.BackColor = Color.FromArgb(18, 119, 165);

                } else if (ShowDate == yesterdaydate)
                {
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@r", "0");
                    addC.Add("@id", nID);
                    var UpdateNotice = DBConnection.SqliteCommand("update Notice Set r = @r where id=@id", addC);
                }
            }
        }
        private void Login2()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.Run(new Login());
            }
            catch { }
        }
        private void Logout_Click(object sender, EventArgs e)
        {
            Thread _str = new Thread(Login2);
            _str.SetApartmentState(ApartmentState.STA);
            _str.Start();
            Close();
        }
        private void tab9_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var PE = new PExpenses();
            ShowPanel.Controls.Add(PE);
        }
        private void onload()
        {
            if (Login.r1 == "0")
            {
                ShowPanel.Controls.Clear();
                var User = new UserPage();
                ShowPanel.Controls.Add(User);
            }
            else
            {
                ShowPanel.Controls.Clear();
                var Admin = new AdminPage();
                ShowPanel.Controls.Add(Admin);
            }
        }
        public void isNotes()
        {
            LoadNotes();
        }
        private void LoadNotes()
        {
            Dictionary<string, object> prams = new Dictionary<string, object>();
            prams.Add("@id", "1");
            var Note = DBConnection.SqliteCommand("select * from AdminNotes where id=@id", prams).ConvertNotes();
            if (Note.Count > 0)
            {
                string aNote = Note[0].Note;
                AdminNotes.Visible = true;
                AdminNotes.Text = "ملاحظة إدارية إضغط هنا لعرضها";
                Notee note = new Notee();
                note.Show();
            }
            else
            {
                AdminNotes.Visible = false;
            }
        }
        private void AdminNotes_Click(object sender, EventArgs e)
        {
            Notee note = new Notee();
            note.Show();
        }
        private void backUPdb()
        {
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, newName);
            System.IO.Directory.CreateDirectory(targetPath);
            System.IO.File.Copy(sourceFile, destFile, true);
            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                foreach (string s in files)
                {
                    fileName = System.IO.Path.GetFileName(s);
                    destFile = System.IO.Path.Combine(targetPath, fileName);
                    System.IO.File.Copy(s, destFile, true);
                }
            }
        }
        private void tabX_Click(object sender, EventArgs e)
        {
            ShowPanel.Controls.Clear();
            var UsC = new UserCash();
            ShowPanel.Controls.Add(UsC);
        }
        private void Logo_Click(object sender, EventArgs e)
        {
                if (!exed)
                {
                    exed = true;
                    MainMenu.Width = 160;
                    Logo.Visible = false;
                    bunifuTransition1.Show(MainMenu);
                }
            else if (exed)
            {
                exed = false;
                MainMenu.Visible = true;
                MainMenu.Width = 50;
                Logo.Visible = true;
                bunifuTransition1.Show(MainMenu);
            }
              
        }
    }
}
