﻿namespace SGMS
{
    partial class MyAccount
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.bunifuMaterialTextbox1 = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.UsrPass = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.UsrN = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.UsrFN = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.Send = new Bunifu.Framework.UI.BunifuTileButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.fYear = new System.Windows.Forms.ComboBox();
            this.fMonth = new System.Windows.Forms.ComboBox();
            this.fDay = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.AddIn = new Bunifu.Framework.UI.BunifuTileButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.bunifuCards3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel6.SuspendLayout();
            this.bunifuCards1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(70, 35);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(876, 537);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Location = new System.Drawing.Point(4, 39);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(868, 494);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "حسابي الشخصي";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 113);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(862, 256);
            this.panel7.TabIndex = 36;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.Controls.Add(this.bunifuMaterialTextbox1);
            this.panel10.Controls.Add(this.label5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(33, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(313, 256);
            this.panel10.TabIndex = 38;
            // 
            // bunifuMaterialTextbox1
            // 
            this.bunifuMaterialTextbox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuMaterialTextbox1.Font = new System.Drawing.Font("Cairo", 8F);
            this.bunifuMaterialTextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuMaterialTextbox1.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuMaterialTextbox1.HintText = "";
            this.bunifuMaterialTextbox1.isPassword = false;
            this.bunifuMaterialTextbox1.LineFocusedColor = System.Drawing.Color.Blue;
            this.bunifuMaterialTextbox1.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuMaterialTextbox1.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.bunifuMaterialTextbox1.LineThickness = 3;
            this.bunifuMaterialTextbox1.Location = new System.Drawing.Point(34, 132);
            this.bunifuMaterialTextbox1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuMaterialTextbox1.Name = "bunifuMaterialTextbox1";
            this.bunifuMaterialTextbox1.Size = new System.Drawing.Size(243, 33);
            this.bunifuMaterialTextbox1.TabIndex = 46;
            this.bunifuMaterialTextbox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bunifuMaterialTextbox1.OnValueChanged += new System.EventHandler(this.bunifuMaterialTextbox1_OnValueChanged);
            this.bunifuMaterialTextbox1.Enter += new System.EventHandler(this.bunifuMaterialTextbox1_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cairo", 7F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label5.Location = new System.Drawing.Point(38, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(235, 19);
            this.label5.TabIndex = 45;
            this.label5.Text = "لتعديل بيانات حسابك يرجي كتابة كلمة المرور الخاصة بك";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(33, 256);
            this.panel9.TabIndex = 37;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.Controls.Add(this.bunifuCards1);
            this.panel8.Controls.Add(this.UsrPass);
            this.panel8.Controls.Add(this.UsrN);
            this.panel8.Controls.Add(this.UsrFN);
            this.panel8.Controls.Add(this.Send);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(352, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(510, 256);
            this.panel8.TabIndex = 36;
            this.panel8.Visible = false;
            // 
            // UsrPass
            // 
            this.UsrPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.UsrPass.Font = new System.Drawing.Font("Cairo", 8F);
            this.UsrPass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrPass.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrPass.HintText = "";
            this.UsrPass.isPassword = false;
            this.UsrPass.LineFocusedColor = System.Drawing.Color.Blue;
            this.UsrPass.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrPass.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.UsrPass.LineThickness = 3;
            this.UsrPass.Location = new System.Drawing.Point(218, 166);
            this.UsrPass.Margin = new System.Windows.Forms.Padding(4);
            this.UsrPass.Name = "UsrPass";
            this.UsrPass.Size = new System.Drawing.Size(243, 33);
            this.UsrPass.TabIndex = 49;
            this.UsrPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UsrN
            // 
            this.UsrN.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.UsrN.Font = new System.Drawing.Font("Cairo", 8F);
            this.UsrN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrN.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrN.HintText = "";
            this.UsrN.isPassword = false;
            this.UsrN.LineFocusedColor = System.Drawing.Color.Blue;
            this.UsrN.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrN.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.UsrN.LineThickness = 3;
            this.UsrN.Location = new System.Drawing.Point(218, 103);
            this.UsrN.Margin = new System.Windows.Forms.Padding(4);
            this.UsrN.Name = "UsrN";
            this.UsrN.Size = new System.Drawing.Size(243, 33);
            this.UsrN.TabIndex = 48;
            this.UsrN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UsrFN
            // 
            this.UsrFN.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.UsrFN.Enabled = false;
            this.UsrFN.Font = new System.Drawing.Font("Cairo", 8F);
            this.UsrFN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrFN.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrFN.HintText = "";
            this.UsrFN.isPassword = false;
            this.UsrFN.LineFocusedColor = System.Drawing.Color.Blue;
            this.UsrFN.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.UsrFN.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.UsrFN.LineThickness = 3;
            this.UsrFN.Location = new System.Drawing.Point(218, 42);
            this.UsrFN.Margin = new System.Windows.Forms.Padding(4);
            this.UsrFN.Name = "UsrFN";
            this.UsrFN.Size = new System.Drawing.Size(243, 33);
            this.UsrFN.TabIndex = 47;
            this.UsrFN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Send
            // 
            this.Send.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.Send.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.Send.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.Send.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Send.Font = new System.Drawing.Font("Cairo", 8F);
            this.Send.ForeColor = System.Drawing.Color.White;
            this.Send.Image = global::SGMS.Properties.Resources.save_60px;
            this.Send.ImagePosition = 2;
            this.Send.ImageZoom = 20;
            this.Send.LabelPosition = 25;
            this.Send.LabelText = "حفظ التعديلات";
            this.Send.Location = new System.Drawing.Point(20, 157);
            this.Send.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Send.Name = "Send";
            this.Send.Size = new System.Drawing.Size(135, 51);
            this.Send.TabIndex = 37;
            this.Send.Click += new System.EventHandler(this.Send_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 457);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 34);
            this.panel1.TabIndex = 35;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(862, 110);
            this.panel4.TabIndex = 7;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(343, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(519, 110);
            this.panel5.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 30F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(9, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(507, 75);
            this.label4.TabIndex = 47;
            this.label4.Text = "تعديل بيانات حسابك الشخصي";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel13);
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(868, 494);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "المفكرة";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkBox1);
            this.panel3.Controls.Add(this.bunifuCards3);
            this.panel3.Controls.Add(this.AddIn);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 113);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(291, 378);
            this.panel3.TabIndex = 46;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(84, 109);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(126, 28);
            this.checkBox1.TabIndex = 46;
            this.checkBox1.Text = "التذكير قبلها بيوم";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.White;
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = false;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards3.Controls.Add(this.fYear);
            this.bunifuCards3.Controls.Add(this.fMonth);
            this.bunifuCards3.Controls.Add(this.fDay);
            this.bunifuCards3.Controls.Add(this.label14);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(35, 28);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = false;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(224, 75);
            this.bunifuCards3.TabIndex = 43;
            // 
            // fYear
            // 
            this.fYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fYear.Font = new System.Drawing.Font("Cairo", 8F);
            this.fYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fYear.FormattingEnabled = true;
            this.fYear.Items.AddRange(new object[] {
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.fYear.Location = new System.Drawing.Point(15, 33);
            this.fYear.Name = "fYear";
            this.fYear.Size = new System.Drawing.Size(68, 28);
            this.fYear.TabIndex = 40;
            // 
            // fMonth
            // 
            this.fMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fMonth.Font = new System.Drawing.Font("Cairo", 8F);
            this.fMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fMonth.FormattingEnabled = true;
            this.fMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.fMonth.Location = new System.Drawing.Point(89, 33);
            this.fMonth.Name = "fMonth";
            this.fMonth.Size = new System.Drawing.Size(66, 28);
            this.fMonth.TabIndex = 39;
            // 
            // fDay
            // 
            this.fDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fDay.Font = new System.Drawing.Font("Cairo", 8F);
            this.fDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fDay.FormattingEnabled = true;
            this.fDay.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.fDay.Location = new System.Drawing.Point(161, 33);
            this.fDay.Name = "fDay";
            this.fDay.Size = new System.Drawing.Size(48, 28);
            this.fDay.TabIndex = 38;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Cairo", 7F);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(125, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 19);
            this.label14.TabIndex = 37;
            this.label14.Text = "تحديد تاريخ العرض";
            // 
            // AddIn
            // 
            this.AddIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AddIn.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.AddIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddIn.Font = new System.Drawing.Font("Cairo", 10F);
            this.AddIn.ForeColor = System.Drawing.Color.White;
            this.AddIn.Image = global::SGMS.Properties.Resources.plus_480px;
            this.AddIn.ImagePosition = 10;
            this.AddIn.ImageZoom = 20;
            this.AddIn.LabelPosition = 30;
            this.AddIn.LabelText = "إضافة مفكرة";
            this.AddIn.Location = new System.Drawing.Point(80, 150);
            this.AddIn.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.AddIn.Name = "AddIn";
            this.AddIn.Size = new System.Drawing.Size(134, 63);
            this.AddIn.TabIndex = 44;
            this.AddIn.Click += new System.EventHandler(this.AddIn_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(300, 113);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(565, 378);
            this.panel2.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 7F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(343, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(206, 19);
            this.label3.TabIndex = 45;
            this.label3.Text = " يتم عرض المفكرة ان هناك شئ يحب عليك فعله";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label2.Location = new System.Drawing.Point(188, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(361, 19);
            this.label2.TabIndex = 44;
            this.label2.Text = "المفكرة هي اداة لتنبيه المستخدم بشئ يجب فعله في تاريخ معين وعند إقتراب التاريخ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Cairo", 8F);
            this.richTextBox1.ForeColor = System.Drawing.Color.White;
            this.richTextBox1.Location = new System.Drawing.Point(17, 28);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(533, 109);
            this.richTextBox1.TabIndex = 43;
            this.richTextBox1.Text = "أكتب مفكرتك هنا";
            this.richTextBox1.Enter += new System.EventHandler(this.richTextBox1_Enter);
            this.richTextBox1.Leave += new System.EventHandler(this.richTextBox1_Leave);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel13.Controls.Add(this.panel6);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(3, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(862, 110);
            this.panel13.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(558, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(304, 110);
            this.panel6.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 30F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(67, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 75);
            this.label1.TabIndex = 47;
            this.label1.Text = "المفكرة";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = false;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.bunifuCards1.Controls.Add(this.label6);
            this.bunifuCards1.Controls.Add(this.label7);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(20, 62);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = false;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(135, 75);
            this.bunifuCards1.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Cairo", 12F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label6.Location = new System.Drawing.Point(14, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 36);
            this.label6.TabIndex = 39;
            this.label6.Text = "0";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cairo", 7F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label7.Location = new System.Drawing.Point(43, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 19);
            this.label7.TabIndex = 38;
            this.label7.Text = "مصروفاتك";
            // 
            // MyAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl1);
            this.Name = "MyAccount";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(876, 537);
            this.Load += new System.EventHandler(this.Home_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBox1;
        private Bunifu.Framework.UI.BunifuTileButton AddIn;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private System.Windows.Forms.ComboBox fYear;
        private System.Windows.Forms.ComboBox fMonth;
        private System.Windows.Forms.ComboBox fDay;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuTileButton Send;
        private Bunifu.Framework.UI.BunifuMaterialTextbox UsrPass;
        private Bunifu.Framework.UI.BunifuMaterialTextbox UsrN;
        private Bunifu.Framework.UI.BunifuMaterialTextbox UsrFN;
        private System.Windows.Forms.Panel panel10;
        private Bunifu.Framework.UI.BunifuMaterialTextbox bunifuMaterialTextbox1;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}
