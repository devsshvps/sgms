﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using System.Reflection;

namespace SGMS
{
    public partial class UserPage : UserControl
    {
        public UserPage()
        {
            InitializeComponent();
            Roles();
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Ver.Text = Login.arNumber(String.Format("{0}", version));

        }

        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadStoreInfo();
        }
        private static List<Setting> SettingP = new List<Setting>();

        private void LoadStoreInfo()
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id <=@id", dic);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var _id = int.Parse(result.Rows[i]["id"].ToString());
                var _StoreName = result.Rows[i]["StoreName"].ToString();
                var _StoreLogo = result.Rows[i]["StoreLogo"].ToString();
                var _MediaName = result.Rows[i]["MediaName"].ToString();
                var p = new Setting
                {
                    id = _id,
                    StoreName = _StoreName,
                    StoreLogo = _StoreLogo,
                    MediaName = _MediaName
                };
                SettingP.Add(p);
            }

            int nid = 1;
            var cup = SettingP.Find(i => i.id == nid);
            var nStoreName = cup.StoreName.ToString();
            var nStoreLogo = cup.StoreLogo.ToString();

            StoreName.Text = nStoreName;
            StoreLogo.ImageLocation = nStoreLogo;

            while (StoreName.Width < System.Windows.Forms.TextRenderer.MeasureText(StoreName.Text,
     new Font(StoreName.Font.FontFamily, StoreName.Font.Size, StoreName.Font.Style)).Width)
            {
                StoreName.Font = new Font(StoreName.Font.FontFamily, StoreName.Font.Size - 0.5f, StoreName.Font.Style);
            }
        }

        private void Roles()
        {
            if (Login.r24 == "0")
            {
                bunifuFlatButton4.Visible = false;
                bunifuFlatButton5.Visible = false;
            }

            if (Login.r3 == "0")
            {
                bunifuFlatButton6.Visible = false;
            }

        }

        private void bunifuFlatButton6_Click(object sender, EventArgs e)
        {
            invTypes Newnow = new invTypes();
            Newnow.ShowDialog();
        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            NewOffer offer = new NewOffer(0);
            offer.ShowDialog();
        }

        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            ViewOffers Voffer = new ViewOffers();
            Voffer.ShowDialog();
        }
    }
}
