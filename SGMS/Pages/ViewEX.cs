﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;
using SGMS.Print;

namespace SGMS
{
    public partial class ViewEX : MetroFramework.Forms.MetroForm
    {
        int vr;

        public ViewEX(int vir)
        {
            InitializeComponent();
            vr = vir;
            if (vr == 956135252)
            {
                title.Text = "مصروفات إدارية";
                PDatagride.Columns["Column1"].Visible = false;
                PDatagride.Columns["Column2"].Visible = false;
            }
            else if (vr == 956135253)
            {
                title.Text = "مصروفات شخصية";
                PDatagride.Columns["Column1"].Visible = true;
                PDatagride.Columns["Column2"].Visible = true;

            }
            else if (vr == 956135251)
            {
                title.Text = "مصروفات مدفوعة";
                PDatagride.Columns["Column1"].Visible = true;
                PDatagride.Columns["Column2"].Visible = true;
            }
            LoadV();
        }
        private void LoadV()
        {
            var m = DateTime.Now.ToString("MM");
            string y = DateTime.Now.ToString("yyyy");
            string f = y + "-" + m + "-" + "01";
            string t = y + "-" + m + "-" + "31";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@r", vr);
            var result = DBConnection.SqliteCommand("Select * from PExpenses where r_Date between '" + f + "' and '" + t + "' AND r = @r", dic).ConvertExpenses();
            if (result == null) return;

            foreach (Expense item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(PDatagride);
                row.Cells[0].Value = item.LoginName;
                row.Cells[1].Value = Login.arNumber(item.Cash.ToString());
                row.Cells[2].Value = Login.arNumber(item.TCash.ToString());
                row.Cells[3].Value = item.Reason;
                row.Cells[4].Value = item.UserName;
                row.Cells[5].Value = Login.arNumber(item.r_Date.ToString("dd")) + " " +
                    Login.arDate(item.r_Date.ToString("MM")) + " " +
                    Login.arNumber(item.r_Date.ToString("yyyy"));
                row.Cells[6].Value = Login.arNumber(item.r_Time.ToString("t"));
                PDatagride.Rows.Add(row);
            }
        }
    }
}
