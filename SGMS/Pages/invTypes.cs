﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class invTypes : MetroFramework.Forms.MetroForm
    {

        public invTypes()
        {
            InitializeComponent();
        }


        private void SellsTaxInv_Click(object sender, EventArgs e)
        {
            NewSaleTax Newnow = new NewSaleTax();
            Newnow.FormClosed += NewInvoiceClosed;
            Newnow.ShowDialog();
            Hide();
        }

        private void SellsInv_Click(object sender, EventArgs e)
        {
            NewSale Newnow = new NewSale();
            Newnow.FormClosed += NewInvoiceClosed;
            Newnow.ShowDialog();
            Hide();
        }

        void NewInvoiceClosed(object sender, FormClosedEventArgs e)
        {
            Close();
        }

    }
}
