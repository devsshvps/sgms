﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class FR : MetroFramework.Forms.MetroForm
    {
        cMessgeBox cmbox;
        string BarCode, InvNum;
        string BrCo, Check12Digits;
        string finalnumber;
        int sNuM, CID;
        double buyprice, CUNPAID, ItemPrice;
        double Cash;
        string datere;
        public FR(string B, string I)
        {
            InitializeComponent();
            BarCode = B;
            InvNum = I;
            LoadItem();
            FillItemsLists();
            FillSalesLists();
            FillCashInvSList();
            rp.SelectedIndex = 0;
        }

        private static List<Models.Sales> SalesList = new List<Models.Sales>();
        private static List<Item> ItemsList = new List<Item>();
        private static List<CashInvS> CashInvSList = new List<CashInvS>();
        private void LoadItem()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@barcode", BarCode);
                dic.Add("@inv", InvNum);
                var psale = DBConnection.SqliteCommand("SELECT * FROM Sells where BarCode = @barcode AND Num = @inv", dic).ConvertSales();
                if (psale == null) return;
                if (psale.Count > 0)
                {
                    foreach (Models.Sales item in psale)
                    {
                        var y = item.ddate.ToString("yyyy");
                        var m = item.ddate.ToString("MM");
                        var d = item.ddate.ToString("dd");
                        var ymd = Login.arNumber(d) + " " + Login.arDate(m) + " " + Login.arNumber(y);
                        datere = item.ddate.ToString("yyyy-MM-dd");

                        string brCode = item.BarCode.Substring(0, (item.BarCode.Length - 1));
                        Check12Digits = brCode.PadRight(12, '0');
                        BrCo = EAN13Class.EAN13(Check12Digits);

                        sNuM = item.Num;
                        changeInvNum();

                        pname.Text = item.itemname;
                        pbarcode.Text = BrCo;
                        pdate.Text = ymd;
                        ptime.Text = Login.arNumber(item.ttime.ToString("t"));
                        puser.Text = item.User;
                        pinvnum.Text = Login.arNumber(finalnumber);
                        pncount.Text = item.count.ToString();
                        pcount.Text = item.count.ToString();
                        pprice.Text = item.itemprice.ToString();
                        ItemPrice = item.itemprice;
                        buyprice = item.buyprice;
                    }

                    var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@inv AND Tax = 0", dic).ConvertCashSales();
                    if (invCash == null) return;
                    foreach (CashInvS item in invCash)
                    {
                        CUNPAID = item.unpaid;
                        CID = item.clientID;
                        double tp = item.totalInv;
                        double pa = tp - CUNPAID;

                        pcname.Text = item.clientName;
                        ptotal.Text = tp.ToString();
                        ppaid.Text = pa.ToString();
                        punpaid.Text = CUNPAID.ToString();
                    }
                }
            }
            catch { }
        }
        private void FillItemsLists()
        {
            // Items List
            try
            {
                ItemsList.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 1);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where r =@r", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    foreach (DataRow item in rIt.Rows)
                    {
                        Item u = new Item();
                        u.id = int.Parse(item["id"].ToString());
                        u.BarCode = item["BarCode"].ToString();
                        u.Catalog = int.Parse(item["Catalog"].ToString());
                        u.Name = item["Name"].ToString();
                        u.Count = double.Parse(item["Count"].ToString());
                        u.ItemPrice = double.Parse(item["ItemPrice"].ToString());
                        u.SellPrice = double.Parse(item["SellPrice"].ToString());
                        u.TotalBuy = double.Parse(item["TotalBuy"].ToString());
                        u.TotalSell = double.Parse(item["TotalSell"].ToString());
                        u.Earn = double.Parse(item["Earn"].ToString());
                        u.NumOfSales = double.Parse(item["NumOfSales"].ToString());
                        u.NumOfPurc = double.Parse(item["NumOfPurc"].ToString());
                        u.r = int.Parse(item["r"].ToString());
                        ItemsList.Add(u);
                    }
                }
            }
            catch { }

        }
        private void FillSalesLists()
        {
            // Sells List
            try
            {
                SalesList.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 1);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Sells where id >@r", dics).ConvertSales();
                if (rIt == null) return;
                if (rIt.Count > 0)
                {
                    foreach (Models.Sales item in rIt)
                    {

                        int _id = item.id;
                        int _Num = item.Num;
                        var _BarCode = item.BarCode;
                        var _itemname = item.itemname;
                        double _count = item.count;
                        double _itemprice = item.itemprice;
                        double _buyprice = item.buyprice;
                        double _totalprice = item.totalprice;
                        int _sellforid = item.sellforid;
                        var _sellfor = item.sellfor;
                        int _Userid = item.Userid;
                        var _User = item.User;
                        var _ddate = item.ddate;
                        var _ttime = item.ttime;
                        int _r = item.r;
                        double _earn = item.earn;


                        var p = new Models.Sales
                        {
                            id = _id,
                            Num = _Num,
                            BarCode = _BarCode,
                            itemname = _itemname,
                            count = _count,
                            itemprice = _itemprice,
                            buyprice = _buyprice,
                            totalprice = _totalprice,
                            sellforid = _sellforid,
                            sellfor = _sellfor,
                            Userid = _Userid,
                            User = _User,
                            ddate = _ddate,
                            ttime = _ttime,
                            r = _r,
                            earn = _earn
                        };
                        SalesList.Add(p);
                    }

                }
            }
            catch { }

        }
        private void FillCashInvSList()
        {
            // CashInvS List
            try
            {
                CashInvSList.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 0);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM CashInvS where id >@r AND Tax = 0", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    foreach (DataRow item in rIt.Rows)
                    {
                        CashInvS u = new CashInvS();
                        u.id = int.Parse(item["id"].ToString());
                        u.invNum = int.Parse(item["invNum"].ToString());
                        u.unpaid = double.Parse(item["unpaid"].ToString());
                        u.totalInv = double.Parse(item["totalInv"].ToString());
                        u.clientID = int.Parse(item["clientID"].ToString());
                        u.userID = int.Parse(item["userID"].ToString());
                        u.clientName = item["clientName"].ToString();
                        u.userName = item["userName"].ToString();
                        u.invDateTime = DateTime.Parse(item["invDateTime"].ToString());
                        u.Tax = int.Parse(item["Tax"].ToString());
                        CashInvSList.Add(u);
                    }
                }
            }
            catch { }

        }
        private void pncount_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(pncount.Text, "[^0-9]") || pncount.Text == "0")
            {
                MessageBox.Show("برجاء كتابة ارقام صحيحة فقط.");
                pncount.Text = pncount.Text.Remove(pncount.Text.Length - 1);
            }
            else
            {
                try
                {
                    if (int.Parse(pncount.Text) > int.Parse(pcount.Text))
                    {
                        cmbox = new cMessgeBox("الكمية المراد إرتجاعها أكبر من الكمية المباعة", "error", "p", 1500);
                        cmbox.ShowDialog();
                        pncount.Text = pcount.Text;
                    }
                }
                catch { }

            }

        }
        private void reitem_Click(object sender, EventArgs e)
        {
            if (rp.SelectedIndex == 1)
            {
                if (string.IsNullOrWhiteSpace(pncount.Text) || string.IsNullOrWhiteSpace(prf.Text))
                {
                    cmbox = new cMessgeBox("برجاء التأكد من ملئ جميع البيانات", "error", "p", 1500);
                    cmbox.ShowDialog();
                }
                else
                {
                    ReturnToWait();
                    cmbox = new cMessgeBox("تم إرتجاع المنتج" + " " + pname.Text + " " + "بنجاح و إضافته في قائمة الإنتظار", "done", "p", 1000);
                    cmbox.ShowDialog();
                    this.Close();
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(pncount.Text))
                {
                    cmbox = new cMessgeBox("برجاء التأكد من الكمية المرتجعة", "error", "p", 1500);
                    cmbox.ShowDialog();
                }
                else
                {
                    ReturnToItems();
                    cmbox = new cMessgeBox("تم إرتجاع المنتج" + " " + pname.Text + " " + "بنجاح", "done", "p", 1000);
                    cmbox.ShowDialog();
                    this.Close();
                }

            }
        }
        private void rp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rp.SelectedIndex == 1)
            {
                label17.Visible = true;
                prf.Visible = true;
            }
            else
            {
                label17.Visible = false;
                prf.Visible = false;
            }
        }
        private void changeInvNum()
        {
            if (sNuM >= 1 && sNuM < 10)
            {
                finalnumber = "000000" + sNuM.ToString();
            }
            else if (sNuM >= 10 && sNuM < 100)
            {
                finalnumber = "00000" + sNuM.ToString();
            }
            else if (sNuM >= 100 && sNuM < 1000)
            {
                finalnumber = "0000" + sNuM.ToString();
            }
            else if (sNuM >= 1000 && sNuM < 10000)
            {
                finalnumber = "000" + sNuM.ToString();
            }
            else if (sNuM >= 10000 && sNuM < 100000)
            {
                finalnumber = "00" + sNuM.ToString();
            }
            else if (sNuM >= 100000 && sNuM < 1000000)
            {
                finalnumber = "0" + sNuM.ToString();
            }
            else if (sNuM >= 1000000)
            {
                finalnumber = sNuM.ToString();
            }
        }
        private void ReturnToItems()
        {
            try
            {
                double cxs = Double.Parse(pncount.Text) * ItemPrice; // ضرب الكمية المرتجعة في سعر المنتج

                if (CUNPAID >= cxs) // معرفة لو اجمالي تمن الكمية المرتجعة اكبر من او يساوي الغير محصل
                {
                    DialogResult dialogResult = MessageBox.Show("المبلغ الغير محصل من هذه الفاتورة أكبر من او يساوي إجمالي مبلغ المرتجع ، هل تود خصم المبلغ المرتجع من المبلغ الغير محصل ؟", "مبلغ غير محصل", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        updateI();
                        updateS();
                        updateCSU();
                        InsertRE();
                        updateCustomer();
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        updateI();
                        updateS();
                        updateCS();
                        InsertRE();
                    }
                }
                else if (CUNPAID > 0 && CUNPAID < cxs)
                {
                    DialogResult dialogResult = MessageBox.Show("يوجد مبلغ غير محصل في الفاتورة هل تود خصمه من قيمة المرتجع", "مبلغ غير محصل", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        updateI();
                        updateS();
                        updateCSU2();
                        InsertRE();
                        updateCustomer2();
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        updateI();
                        updateS();
                        updateCS();
                        InsertRE();
                    }
                }
                else
                {
                    updateI();
                    updateS();
                    updateCS();
                    InsertRE();
                }
            }
            catch { }
        }
        private void ReturnToWait()
        {
            double cxs = Double.Parse(pncount.Text) * ItemPrice;
            if (CUNPAID >= cxs)
            {
                DialogResult dialogResult = MessageBox.Show("المبلغ الغير محصل من هذه الفاتورة أكبر من او يساوي إجمالي مبلغ المرتجع ، هل تود خصم المبلغ المرتجع من المبلغ الغير محصل ؟", "مبلغ غير محصل", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    updateI2();
                    updateS();
                    updateCSU();
                    InsertWA();
                    updateCustomer();
                }
                else if (dialogResult == DialogResult.No)
                {
                    updateI2();
                    updateS();
                    updateCS();
                    InsertWA();
                }
            }else if (CUNPAID > 0 && CUNPAID < cxs)
            {
                updateI2();
                updateS();
                updateCSU2();
                InsertWA();
                updateCustomer2();
            }
            else
            {
                updateI2();
                updateS();
                updateCS();
                InsertWA();
            }
        }
        private void updateI()
        {
            //update items barcode
            try
            {
                var finItems = ItemsList.Find(i => i.BarCode == BarCode);
                var ncot = finItems.Count; // الكمية
                var ntotal = finItems.TotalSell; // اجمالي سعر البيع
                var nearn = finItems.Earn; // اجمالي المكسب من المنتج
                var nNumOfSales = finItems.NumOfSales;

                double totalsum = Double.Parse(pncount.Text) * ItemPrice; // الكمية المرتجعة في سعر البيع
                double total1 = ntotal - totalsum; // طرح اجمالي المرتجع من اجمالي المبيعات

                double count1 = ncot + Double.Parse(pncount.Text); // اجمالي الكمية بعد الارتجاع

                double earnsum1 = Double.Parse(pncount.Text) * buyprice; // الكمية المترجعة في سعر الشراء
                double earnsum2 = totalsum - earnsum1; // طرح اجمالي سعر البيع - اجمالي سعر الشراء
                double earn1 = nearn - earnsum2; // طرح اجمالي المكسب - المكسب المرتجع

                double NumOfSales1 = nNumOfSales - Double.Parse(pncount.Text); // طرح الكمية المرتجعة من اجمالي عدد مبيعات المنتج

                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@BarCode", BarCode);
                dics.Add("@Count", count1); //
                dics.Add("@TotalSell", total1);
                dics.Add("@Earn", earn1);
                dics.Add("@NumOfSales", NumOfSales1);
                var uitems = DBConnection.SqliteCommand("update Items Set Count = @Count , TotalSell = @TotalSell , Earn = @Earn , NumOfSales = @NumOfSales where BarCode=@BarCode", dics);
            }
            catch { }

        }
        private void updateI2()
        {
            //update items barcode
            try
            {
                var finItems = ItemsList.Find(i => i.BarCode == BarCode);
                var ncot = finItems.Count; // الكمية
                var ntotal = finItems.TotalSell; // اجمالي سعر البيع
                var nearn = finItems.Earn; // اجمالي المكسب من المنتج
                var nNumOfSales = finItems.NumOfSales;

                double totalsum = Double.Parse(pncount.Text) * ItemPrice; // الكمية المرتجعة في سعر البيع
                double total1 = ntotal - totalsum; // طرح اجمالي المرتجع من اجمالي المبيعات

                double count1 = ncot + Double.Parse(pncount.Text); // اجمالي الكمية بعد الارتجاع

                double earnsum1 = Double.Parse(pncount.Text) * buyprice; // الكمية المترجعة في سعر الشراء
                double earnsum2 = totalsum - earnsum1; // طرح اجمالي سعر البيع - اجمالي سعر الشراء
                double earn1 = nearn - earnsum2; // طرح اجمالي المكسب - المكسب المرتجع

                double NumOfSales1 = nNumOfSales - Double.Parse(pncount.Text); // طرح الكمية المرتجعة من اجمالي عدد مبيعات المنتج

                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@BarCode", BarCode);
                dics.Add("@TotalSell", total1);
                dics.Add("@Earn", earn1);
                dics.Add("@NumOfSales", NumOfSales1);
                var uitems = DBConnection.SqliteCommand("update Items Set TotalSell = @TotalSell , Earn = @Earn , NumOfSales = @NumOfSales where BarCode=@BarCode", dics);
            }
            catch { }

        }
        private void updateS()
        {
            //update Sells inv number & barcode
            try
            {
                int num = int.Parse(InvNum);
                var finSells = SalesList.Find(i => i.Num == num && i.BarCode == BarCode);
                var ncot = finSells.count; // الكمية المباعة
                var ntotal = finSells.totalprice; // اجمالي البيع
                var nearn = finSells.earn; // اجمالي المكسب

                double count1 = ncot - Double.Parse(pncount.Text);

                double totalsum = Double.Parse(pncount.Text) * ItemPrice;
                double total1 = ntotal - totalsum;

                double earnsum1 = Double.Parse(pncount.Text) * buyprice;
                double earnsum2 = totalsum - earnsum1;
                double earn1 = nearn - earnsum2;


                if (count1 == 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@Num", InvNum);
                    dics.Add("@BarCode", BarCode);
                    var DelSale = DBConnection.SqliteCommand("DELETE FROM Sells WHERE Num =@Num AND BarCode =@BarCode", dics);
                }
                else
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@count", count1);
                    dics.Add("@totalprice", total1);
                    dics.Add("@earn", earn1);
                    dics.Add("@Num", InvNum);
                    dics.Add("@BarCode", BarCode);
                    var uSales = DBConnection.SqliteCommand("update Sells Set count = @count , totalprice = @totalprice , earn = @earn where Num = @Num AND BarCode = @BarCode", dics);
                }


            }
            catch { }

        }
        private void updateCS()
        {
            //update CashInvS inv number
            try
            {
                int num = int.Parse(InvNum);
                var finCashInvS = CashInvSList.Find(i => i.invNum == num);
                var ntotalinv = finCashInvS.totalInv;

                double totalsum = Double.Parse(pncount.Text) * ItemPrice;
                double total1 = ntotalinv - totalsum;

                if (total1 == 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@invNum", InvNum);
                    var DelCashInvS = DBConnection.SqliteCommand("DELETE FROM CashInvS WHERE invNum =@invNum AND Tax = 0", dics);
                }
                else
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@totalInv", total1);
                    dics.Add("@invNum", InvNum);
                    var uSales = DBConnection.SqliteCommand("update CashInvS Set totalInv = @totalInv where invNum = @invNum AND Tax = 0", dics);

                }
            }
            catch { }

        }
        private void updateCSU()
        {
            //update CashInvS inv number
            try
            {
                int num = int.Parse(InvNum);
                var finCashInvS = CashInvSList.Find(i => i.invNum == num);
                var ntotalinv = finCashInvS.totalInv;
                var nunpaid = finCashInvS.unpaid;

                double totalsum = Double.Parse(pncount.Text) * ItemPrice;
                double total1 = ntotalinv - totalsum;
                double unpaid1 = nunpaid - totalsum;

                if (total1 == 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@invNum", InvNum);
                    var DelCashInvS = DBConnection.SqliteCommand("DELETE FROM CashInvS WHERE invNum =@invNum AND Tax = 0", dics);
                }
                else
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@totalInv", total1);
                    dics.Add("@unpaid", unpaid1);
                    dics.Add("@invNum", InvNum);
                    var uSales = DBConnection.SqliteCommand("update CashInvS Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum AND Tax = 0", dics);
                }
            }
            catch { }

        }
        private void InsertRE()
        {
            //insert in return
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@BarCode", BarCode);
                addC.Add("@ItemName", pname.Text);
                addC.Add("@reCount", pncount.Text);
                addC.Add("@rePrice", pprice.Text);
                addC.Add("@Customer", pcname.Text);
                addC.Add("@Cid", CID);
                addC.Add("@ReUser", Login.cnn);
                addC.Add("@ReUserID", Login.cid);
                addC.Add("@DateTime_re", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                addC.Add("@date_ct", datere);
                var insertRET = DBConnection.SqliteCommand("INSERT  INTO `Returned`(`BarCode`, `ItemName`, `reCount`, `rePrice`, `Customer`, `Cid`, `ReUser`, `ReUserID`, `DateTime_re`, `date_ct`) values(@BarCode,@ItemName,@reCount,@rePrice,@Customer,@Cid,@ReUser,@ReUserID,@DateTime_re,@date_ct)", addC);
            }
            catch { }
        }
        private void InsertWA()
        {
            //insert in WAreturn
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@BarCode", BarCode);
                addC.Add("@ItemName", pname.Text);
                addC.Add("@reCount", pncount.Text);
                addC.Add("@rePrice", buyprice);
                addC.Add("@Reason", prf.Text);
                addC.Add("@Customer", pcname.Text);
                addC.Add("@Cid", CID);
                addC.Add("@ReUser", Login.cnn);
                addC.Add("@ReUserID", Login.cid);
                addC.Add("@DateTime_re", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                addC.Add("@date_ct", datere);
                var insertWAR = DBConnection.SqliteCommand("INSERT  INTO `WaitRE`(`BarCode`, `ItemName`, `reCount`, `rePrice`, `Reason`, `Customer`, `Cid`, `ReUser`, `ReUserID`, `DateTime_re`, `date_ct`) values(@BarCode,@ItemName,@reCount,@rePrice,@Reason,@Customer,@Cid,@ReUser,@ReUserID,@DateTime_re,@date_ct)", addC);
            }
            catch { }
        }
        private void updateCustomer()
        {

            try
            {

                Dictionary<string, object> prams = new Dictionary<string, object>();
                prams.Add("@id", CID);
                var C = DBConnection.SqliteCommand("select * from Customers where id=@id", prams).ConvertCustomers();
                if (C.Count > 0)
                {
                    Cash = C[0].Cash;
                }

                double totalsum = Double.Parse(pncount.Text) * ItemPrice;
                double ncash = Cash - totalsum;
                //update cash
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@id", CID);
                dics.Add("@Cash", ncash);
                var uSales = DBConnection.SqliteCommand("update Customers Set Cash = @Cash where id = @id", dics);

            }
            catch { }
        }
        private void updateCSU2()
        {
            //update CashInvS inv number
            try
            {
                int num = int.Parse(InvNum);
                var finCashInvS = CashInvSList.Find(i => i.invNum == num);
                var ntotalinv = finCashInvS.totalInv;
                var nunpaid = finCashInvS.unpaid;

                double totalsum = Double.Parse(pncount.Text) * ItemPrice;
                double total1 = ntotalinv - totalsum;
                double unpaid1 = nunpaid - totalsum;

                if (total1 <= 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@invNum", InvNum);
                    var DelCashInvS = DBConnection.SqliteCommand("DELETE FROM CashInvS WHERE invNum =@invNum AND Tax = 0", dics);
                }else if (total1 > 0 && unpaid1 < 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@totalInv", total1);
                    dics.Add("@unpaid", 0);
                    dics.Add("@invNum", InvNum);
                    var uSales = DBConnection.SqliteCommand("update CashInvS Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum AND Tax = 0", dics);
                }
                else
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@totalInv", total1);
                    dics.Add("@unpaid", unpaid1);
                    dics.Add("@invNum", InvNum);
                    var uSales = DBConnection.SqliteCommand("update CashInvS Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum AND Tax = 0", dics);
                }
            }
            catch { }

        }
        private void updateCustomer2()
        {
            try
            {

                Dictionary<string, object> prams = new Dictionary<string, object>();
                prams.Add("@id", CID);
                var C = DBConnection.SqliteCommand("select * from Customers where id=@id", prams).ConvertCustomers();
                if (C.Count > 0)
                {
                    Cash = C[0].Cash;
                }

                double totalsum = Double.Parse(pncount.Text) * ItemPrice;
                double ncash = Cash - totalsum;
                if(ncash < 0)
                {
                    ncash = 0;
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@id", CID);
                    dics.Add("@Cash", ncash);
                    var uSales = DBConnection.SqliteCommand("update Customers Set Cash = @Cash where id = @id", dics);
                    var payforu = totalsum - Cash;
                    MessageBox.Show("برجاء دفع للعميل مبلغ وقدره" + " " + Login.arNumber(payforu.ToString()) + " " + "جنيه");
                }
                else
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@id", CID);
                    dics.Add("@Cash", ncash);
                    var uSales = DBConnection.SqliteCommand("update Customers Set Cash = @Cash where id = @id", dics);
                }
                //update cash

            }
            catch { }
        }

    }
}