﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class ViewS : MetroFramework.Forms.MetroForm
    {

        string invN;
        int invType;
        public ViewS()
        {
            InitializeComponent();
            LoadS();
        }
        private void LoadS()
        {
            var m = DateTime.Now.ToString("MM");
            string y = DateTime.Now.ToString("yyyy");
            string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
            string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);
            var result = DBConnection.SqliteCommand("Select * from CashInvS where invDateTime between '" + f + "' and '" + t + "'", dic).ConvertCashSales ();
            if (result == null) return;

            foreach (CashInvS item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(SDatagride);
                row.Cells[0].Value = item.invNum;
                row.Cells[1].Value = item.totalInv;
                row.Cells[2].Value = item.clientName;
                row.Cells[3].Value = item.userName;
                row.Cells[4].Value = item.invDateTime.ToString("yyyy-MM-dd");
                row.Cells[5].Value = item.Tax;
                if (item.Tax == 1)
                {
                    row.DefaultCellStyle.BackColor = Color.Beige;
                }
                SDatagride.Rows.Add(row);
            }
        }

        private void SDatagride_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in SDatagride.SelectedRows)
                {
                    if (SDatagride.SelectedRows.Count > 0)
                    {
                        invN = row.Cells[0].Value.ToString();
                        invType = int.Parse(row.Cells[5].Value.ToString());
                        view.Enabled = true;
                    }
                    else
                    {
                        invN = "";
                        view.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void view_Click(object sender, EventArgs e)
        {
            if (invN != "")
            {
                SalesInvP viewSaleinv = new SalesInvP(invN,invType);
                viewSaleinv.ShowDialog();
            }
            else
            {
                cMessgeBox mess = new cMessgeBox("برجاء إختيار فاتورة للعرض", "error", "p", 1500);
                mess.ShowDialog();
            }
        }
    }
}
