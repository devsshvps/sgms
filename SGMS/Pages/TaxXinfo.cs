﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class TaxXinfo : MetroFramework.Forms.MetroForm
    {

        public TaxXinfo()
        {
            InitializeComponent();
        }

        private void view_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(CoNum.Text) || string.IsNullOrWhiteSpace(TaxNum.Text))
                MessageBox.Show("لا يمكنك ترك البيانات فارغة");
            else
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@TaxNum", TaxNum.Text);
                addC.Add("@CoNum", CoNum.Text);
                var UpdateCu = DBConnection.SqliteCommand("update TaxInfo Set TaxNum = @TaxNum,CoRecord = @CoNum where id=1", addC);
                Close();
            }
        }
    }
}
