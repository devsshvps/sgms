﻿namespace SGMS
{
    partial class ViewUP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewUP));
            this.PDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.inv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unpaa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label14 = new System.Windows.Forms.Label();
            this.view = new Bunifu.Framework.UI.BunifuFlatButton();
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).BeginInit();
            this.SuspendLayout();
            // 
            // PDatagride
            // 
            this.PDatagride.AllowUserToAddRows = false;
            this.PDatagride.AllowUserToDeleteRows = false;
            this.PDatagride.AllowUserToResizeColumns = false;
            this.PDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.PDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.PDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PDatagride.BackgroundColor = System.Drawing.Color.White;
            this.PDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.PDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inv,
            this.ItemiName,
            this.unpaa,
            this.Client,
            this.User,
            this.Date});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PDatagride.DefaultCellStyle = dataGridViewCellStyle4;
            this.PDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PDatagride.DoubleBuffered = true;
            this.PDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.PDatagride.EnableHeadersVisualStyles = false;
            this.PDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.PDatagride.Location = new System.Drawing.Point(23, 35);
            this.PDatagride.MultiSelect = false;
            this.PDatagride.Name = "PDatagride";
            this.PDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.PDatagride.RowHeadersVisible = false;
            this.PDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PDatagride.Size = new System.Drawing.Size(756, 449);
            this.PDatagride.TabIndex = 9;
            this.PDatagride.SelectionChanged += new System.EventHandler(this.PDatagride_SelectionChanged);
            // 
            // inv
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.inv.DefaultCellStyle = dataGridViewCellStyle3;
            this.inv.FillWeight = 58.50328F;
            this.inv.HeaderText = "رقم الفاتورة";
            this.inv.Name = "inv";
            this.inv.ReadOnly = true;
            // 
            // ItemiName
            // 
            this.ItemiName.HeaderText = "إجمالي الفاتورة";
            this.ItemiName.Name = "ItemiName";
            this.ItemiName.ReadOnly = true;
            // 
            // unpaa
            // 
            this.unpaa.HeaderText = "المؤجل";
            this.unpaa.Name = "unpaa";
            this.unpaa.ReadOnly = true;
            // 
            // Client
            // 
            this.Client.HeaderText = "إسم المورد";
            this.Client.Name = "Client";
            this.Client.ReadOnly = true;
            // 
            // User
            // 
            this.User.HeaderText = "إسم المستخدم";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.HeaderText = "التاريخ";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(364, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 14);
            this.label14.TabIndex = 66;
            this.label14.Text = "مؤجلات هذا الشهر";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // view
            // 
            this.view.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view.BorderRadius = 0;
            this.view.ButtonText = "عرض الفاتورة";
            this.view.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view.DisabledColor = System.Drawing.Color.White;
            this.view.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Iconcolor = System.Drawing.Color.Transparent;
            this.view.Iconimage = null;
            this.view.Iconimage_right = null;
            this.view.Iconimage_right_Selected = null;
            this.view.Iconimage_Selected = null;
            this.view.IconMarginLeft = 0;
            this.view.IconMarginRight = 0;
            this.view.IconRightVisible = false;
            this.view.IconRightZoom = 0D;
            this.view.IconVisible = false;
            this.view.IconZoom = 20D;
            this.view.IsTab = false;
            this.view.Location = new System.Drawing.Point(23, 11);
            this.view.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.view.Name = "view";
            this.view.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.OnHoverTextColor = System.Drawing.Color.White;
            this.view.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.view.selected = false;
            this.view.Size = new System.Drawing.Size(98, 17);
            this.view.TabIndex = 70;
            this.view.Text = "عرض الفاتورة";
            this.view.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.view.Textcolor = System.Drawing.Color.White;
            this.view.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // ViewUP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 506);
            this.Controls.Add(this.view);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.PDatagride);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewUP";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCustomDataGrid PDatagride;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuFlatButton view;
        private System.Windows.Forms.DataGridViewTextBoxColumn inv;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn unpaa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Client;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
    }
}