﻿namespace SGMS.Pages
{
    partial class ReItemP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReItemP));
            this.ClientName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.InvNum = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.reitem = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tPrice = new System.Windows.Forms.Label();
            this.tDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ina = new System.Windows.Forms.Label();
            this.iba = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tCount = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ClientName
            // 
            this.ClientName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ClientName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ClientName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ClientName.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.ClientName.FormattingEnabled = true;
            this.ClientName.Location = new System.Drawing.Point(111, 110);
            this.ClientName.Name = "ClientName";
            this.ClientName.Size = new System.Drawing.Size(229, 32);
            this.ClientName.TabIndex = 52;
            this.ClientName.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 7F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label4.Location = new System.Drawing.Point(49, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 19);
            this.label4.TabIndex = 51;
            this.label4.Text = "إسم المورد";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(133, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 19);
            this.label1.TabIndex = 53;
            this.label1.Text = "إرتجاع من فاتورة مشتريات";
            // 
            // InvNum
            // 
            this.InvNum.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.InvNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.InvNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.InvNum.Font = new System.Drawing.Font("Cairo", 9.749999F);
            this.InvNum.FormattingEnabled = true;
            this.InvNum.Location = new System.Drawing.Point(111, 158);
            this.InvNum.Name = "InvNum";
            this.InvNum.Size = new System.Drawing.Size(155, 32);
            this.InvNum.TabIndex = 55;
            this.InvNum.SelectedIndexChanged += new System.EventHandler(this.InvNum_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label2.Location = new System.Drawing.Point(44, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 19);
            this.label2.TabIndex = 54;
            this.label2.Text = "رقم الفاتورة";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(52, 209);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 14);
            this.label14.TabIndex = 68;
            this.label14.Text = "الكمية المشتراه";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(52, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 14);
            this.label3.TabIndex = 69;
            this.label3.Text = "سعر الشراء";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label5.Location = new System.Drawing.Point(52, 265);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 70;
            this.label5.Text = "تاريخ الفاتورة";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // reitem
            // 
            this.reitem.Activecolor = System.Drawing.Color.Navy;
            this.reitem.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.reitem.BackColor = System.Drawing.Color.Navy;
            this.reitem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.reitem.BorderRadius = 7;
            this.reitem.ButtonText = "إرتجاع من الفاتورة المحددة";
            this.reitem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reitem.DisabledColor = System.Drawing.Color.White;
            this.reitem.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reitem.Iconcolor = System.Drawing.Color.Transparent;
            this.reitem.Iconimage = null;
            this.reitem.Iconimage_right = null;
            this.reitem.Iconimage_right_Selected = null;
            this.reitem.Iconimage_Selected = null;
            this.reitem.IconMarginLeft = 0;
            this.reitem.IconMarginRight = 0;
            this.reitem.IconRightVisible = false;
            this.reitem.IconRightZoom = 0D;
            this.reitem.IconVisible = false;
            this.reitem.IconZoom = 20D;
            this.reitem.IsTab = false;
            this.reitem.Location = new System.Drawing.Point(112, 299);
            this.reitem.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.reitem.Name = "reitem";
            this.reitem.Normalcolor = System.Drawing.Color.Navy;
            this.reitem.OnHovercolor = System.Drawing.Color.Blue;
            this.reitem.OnHoverTextColor = System.Drawing.Color.White;
            this.reitem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.reitem.selected = false;
            this.reitem.Size = new System.Drawing.Size(159, 25);
            this.reitem.TabIndex = 95;
            this.reitem.Text = "إرتجاع من الفاتورة المحددة";
            this.reitem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reitem.Textcolor = System.Drawing.Color.White;
            this.reitem.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reitem.Click += new System.EventHandler(this.reitem_Click);
            // 
            // tPrice
            // 
            this.tPrice.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tPrice.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.tPrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.tPrice.Location = new System.Drawing.Point(117, 236);
            this.tPrice.Name = "tPrice";
            this.tPrice.Size = new System.Drawing.Size(101, 14);
            this.tPrice.TabIndex = 69;
            this.tPrice.Text = " ";
            this.tPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tDate
            // 
            this.tDate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tDate.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.tDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.tDate.Location = new System.Drawing.Point(117, 265);
            this.tDate.Name = "tDate";
            this.tDate.Size = new System.Drawing.Size(110, 14);
            this.tDate.TabIndex = 70;
            this.tDate.Text = " ";
            this.tDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label6.Location = new System.Drawing.Point(23, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 14);
            this.label6.TabIndex = 96;
            this.label6.Text = "إسم المنتج";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label7.Location = new System.Drawing.Point(219, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 14);
            this.label7.TabIndex = 97;
            this.label7.Text = "باركود ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ina
            // 
            this.ina.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ina.Font = new System.Drawing.Font("Cairo", 7F);
            this.ina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ina.Location = new System.Drawing.Point(74, 65);
            this.ina.Name = "ina";
            this.ina.Size = new System.Drawing.Size(139, 19);
            this.ina.TabIndex = 98;
            this.ina.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // iba
            // 
            this.iba.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.iba.Font = new System.Drawing.Font("Cairo", 7F);
            this.iba.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.iba.Location = new System.Drawing.Point(259, 65);
            this.iba.Name = "iba";
            this.iba.Size = new System.Drawing.Size(98, 19);
            this.iba.TabIndex = 99;
            this.iba.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(276, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 14);
            this.label8.TabIndex = 100;
            this.label8.Text = "مؤجلات الفاتورة";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tCount
            // 
            this.tCount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tCount.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.tCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.tCount.Location = new System.Drawing.Point(117, 209);
            this.tCount.Name = "tCount";
            this.tCount.Size = new System.Drawing.Size(114, 14);
            this.tCount.TabIndex = 68;
            this.tCount.Text = " ";
            this.tCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(277, 168);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 21);
            this.label9.TabIndex = 101;
            this.label9.Text = " ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ReItemP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 351);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.iba);
            this.Controls.Add(this.ina);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.reitem);
            this.Controls.Add(this.tDate);
            this.Controls.Add(this.tPrice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.InvNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ClientName);
            this.Controls.Add(this.label4);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReItemP";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.ShowIcon = false;
            this.Text = "ReItemP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ClientName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox InvNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuFlatButton reitem;
        private System.Windows.Forms.Label tPrice;
        private System.Windows.Forms.Label tDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label ina;
        private System.Windows.Forms.Label iba;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label tCount;
        private System.Windows.Forms.Label label9;
    }
}