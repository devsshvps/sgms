﻿namespace SGMS
{
    partial class FR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FR));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pname = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pdate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ptime = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.puser = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbarcode = new System.Windows.Forms.Label();
            this.rp = new MetroFramework.Controls.MetroComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pinvnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pncount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pprice = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pcount = new System.Windows.Forms.Label();
            this.pcname = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.ptotal = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.ppaid = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.punpaid = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.prf = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.reitem = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pbarcode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.pname);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(23, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(756, 100);
            this.panel1.TabIndex = 10;
            // 
            // pname
            // 
            this.pname.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pname.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold);
            this.pname.ForeColor = System.Drawing.Color.White;
            this.pname.Location = new System.Drawing.Point(3, 23);
            this.pname.Name = "pname";
            this.pname.Size = new System.Drawing.Size(466, 66);
            this.pname.TabIndex = 67;
            this.pname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(185, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 14);
            this.label2.TabIndex = 70;
            this.label2.Text = "إسم المنتج المراد إرتجاعه";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(627, 148);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 14);
            this.label14.TabIndex = 68;
            this.label14.Text = "تمت عملية بيع المنتج يوم";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pdate
            // 
            this.pdate.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pdate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.pdate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pdate.Location = new System.Drawing.Point(512, 145);
            this.pdate.Name = "pdate";
            this.pdate.Size = new System.Drawing.Size(113, 21);
            this.pdate.TabIndex = 69;
            this.pdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(473, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 70;
            this.label3.Text = "الساعة";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ptime
            // 
            this.ptime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ptime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.ptime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ptime.Location = new System.Drawing.Point(402, 145);
            this.ptime.Name = "ptime";
            this.ptime.Size = new System.Drawing.Size(65, 21);
            this.ptime.TabIndex = 71;
            this.ptime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label5.Location = new System.Drawing.Point(326, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 14);
            this.label5.TabIndex = 72;
            this.label5.Text = "بمعرفة المستخدم";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // puser
            // 
            this.puser.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.puser.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.puser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.puser.Location = new System.Drawing.Point(211, 145);
            this.puser.Name = "puser";
            this.puser.Size = new System.Drawing.Size(109, 21);
            this.puser.TabIndex = 73;
            this.puser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(537, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 71;
            this.label1.Text = "باركود المنتج";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbarcode
            // 
            this.pbarcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pbarcode.Font = new System.Drawing.Font("Code EAN13", 34F);
            this.pbarcode.ForeColor = System.Drawing.Color.White;
            this.pbarcode.Location = new System.Drawing.Point(460, 23);
            this.pbarcode.Name = "pbarcode";
            this.pbarcode.Size = new System.Drawing.Size(210, 66);
            this.pbarcode.TabIndex = 72;
            this.pbarcode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rp
            // 
            this.rp.FormattingEnabled = true;
            this.rp.ItemHeight = 23;
            this.rp.Items.AddRange(new object[] {
            "جديد",
            "تالف"});
            this.rp.Location = new System.Drawing.Point(507, 285);
            this.rp.Name = "rp";
            this.rp.Size = new System.Drawing.Size(184, 29);
            this.rp.TabIndex = 74;
            this.rp.UseSelectable = true;
            this.rp.SelectedIndexChanged += new System.EventHandler(this.rp_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(575, 264);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 14);
            this.label6.TabIndex = 75;
            this.label6.Text = "حالة المنتج";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pinvnum
            // 
            this.pinvnum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pinvnum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.pinvnum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pinvnum.Location = new System.Drawing.Point(80, 145);
            this.pinvnum.Name = "pinvnum";
            this.pinvnum.Size = new System.Drawing.Size(65, 21);
            this.pinvnum.TabIndex = 77;
            this.pinvnum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(151, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 76;
            this.label8.Text = "من فاتورة رقم";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pncount
            // 
            this.pncount.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.pncount.Location = new System.Drawing.Point(373, 285);
            this.pncount.Name = "pncount";
            this.pncount.Size = new System.Drawing.Size(117, 29);
            this.pncount.TabIndex = 78;
            this.pncount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.pncount.TextChanged += new System.EventHandler(this.pncount_TextChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(389, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 14);
            this.label9.TabIndex = 79;
            this.label9.Text = "الكمية المراد إرتجعها";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(144, 264);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 14);
            this.label10.TabIndex = 80;
            this.label10.Text = "سعر الشراء";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pprice
            // 
            this.pprice.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pprice.Enabled = false;
            this.pprice.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.pprice.ForeColor = System.Drawing.Color.Black;
            this.pprice.Location = new System.Drawing.Point(114, 285);
            this.pprice.Name = "pprice";
            this.pprice.Size = new System.Drawing.Size(111, 29);
            this.pprice.TabIndex = 81;
            this.pprice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(267, 264);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 14);
            this.label11.TabIndex = 80;
            this.label11.Text = "الكمية المشتراه";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcount
            // 
            this.pcount.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pcount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcount.Enabled = false;
            this.pcount.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.pcount.ForeColor = System.Drawing.Color.Black;
            this.pcount.Location = new System.Drawing.Point(243, 285);
            this.pcount.Name = "pcount";
            this.pcount.Size = new System.Drawing.Size(111, 29);
            this.pcount.TabIndex = 81;
            this.pcount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcname
            // 
            this.pcname.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pcname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.pcname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pcname.Location = new System.Drawing.Point(526, 202);
            this.pcname.Name = "pcname";
            this.pcname.Size = new System.Drawing.Size(86, 21);
            this.pcname.TabIndex = 83;
            this.pcname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label13.Location = new System.Drawing.Point(618, 205);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 14);
            this.label13.TabIndex = 82;
            this.label13.Text = "إسم العميل";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ptotal
            // 
            this.ptotal.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ptotal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.ptotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ptotal.Location = new System.Drawing.Point(383, 202);
            this.ptotal.Name = "ptotal";
            this.ptotal.Size = new System.Drawing.Size(71, 21);
            this.ptotal.TabIndex = 85;
            this.ptotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label16.Location = new System.Drawing.Point(460, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 14);
            this.label16.TabIndex = 84;
            this.label16.Text = "إجمالي الفاتورة";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ppaid
            // 
            this.ppaid.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ppaid.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.ppaid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ppaid.Location = new System.Drawing.Point(273, 202);
            this.ppaid.Name = "ppaid";
            this.ppaid.Size = new System.Drawing.Size(54, 21);
            this.ppaid.TabIndex = 87;
            this.ppaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label18.Location = new System.Drawing.Point(332, 205);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 14);
            this.label18.TabIndex = 86;
            this.label18.Text = "المدفوع";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label20.Location = new System.Drawing.Point(234, 205);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 14);
            this.label20.TabIndex = 88;
            this.label20.Text = "المتبقي";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(162, 190);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(513, 43);
            this.label12.TabIndex = 90;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // punpaid
            // 
            this.punpaid.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.punpaid.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.punpaid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.punpaid.Location = new System.Drawing.Point(174, 202);
            this.punpaid.Name = "punpaid";
            this.punpaid.Size = new System.Drawing.Size(54, 21);
            this.punpaid.TabIndex = 87;
            this.punpaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label15.Location = new System.Drawing.Point(363, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 14);
            this.label15.TabIndex = 91;
            this.label15.Text = "عملية إرتجاع منتج";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // prf
            // 
            this.prf.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.prf.Location = new System.Drawing.Point(114, 354);
            this.prf.Name = "prf";
            this.prf.Size = new System.Drawing.Size(577, 29);
            this.prf.TabIndex = 92;
            this.prf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.prf.Visible = false;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(308, 330);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(188, 14);
            this.label17.TabIndex = 93;
            this.label17.Text = "برجاء تحديد سبب العطل / التلف الواقع في الجهاز ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.Visible = false;
            // 
            // reitem
            // 
            this.reitem.Activecolor = System.Drawing.Color.Navy;
            this.reitem.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.reitem.BackColor = System.Drawing.Color.Navy;
            this.reitem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.reitem.BorderRadius = 7;
            this.reitem.ButtonText = "إرتجاع المنتج المحدد";
            this.reitem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reitem.DisabledColor = System.Drawing.Color.White;
            this.reitem.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reitem.Iconcolor = System.Drawing.Color.Transparent;
            this.reitem.Iconimage = null;
            this.reitem.Iconimage_right = null;
            this.reitem.Iconimage_right_Selected = null;
            this.reitem.Iconimage_Selected = null;
            this.reitem.IconMarginLeft = 0;
            this.reitem.IconMarginRight = 0;
            this.reitem.IconRightVisible = false;
            this.reitem.IconRightZoom = 0D;
            this.reitem.IconVisible = false;
            this.reitem.IconZoom = 20D;
            this.reitem.IsTab = false;
            this.reitem.Location = new System.Drawing.Point(323, 408);
            this.reitem.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.reitem.Name = "reitem";
            this.reitem.Normalcolor = System.Drawing.Color.Navy;
            this.reitem.OnHovercolor = System.Drawing.Color.Blue;
            this.reitem.OnHoverTextColor = System.Drawing.Color.White;
            this.reitem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.reitem.selected = false;
            this.reitem.Size = new System.Drawing.Size(159, 25);
            this.reitem.TabIndex = 94;
            this.reitem.Text = "إرتجاع المنتج المحدد";
            this.reitem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.reitem.Textcolor = System.Drawing.Color.White;
            this.reitem.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reitem.Click += new System.EventHandler(this.reitem_Click);
            // 
            // FR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 506);
            this.Controls.Add(this.reitem);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.prf);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.punpaid);
            this.Controls.Add(this.ppaid);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.ptotal);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.pcname);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pcount);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pprice);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pncount);
            this.Controls.Add(this.pinvnum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rp);
            this.Controls.Add(this.puser);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ptime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pdate);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label12);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FR";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label pname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label pbarcode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label pdate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ptime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label puser;
        private MetroFramework.Controls.MetroComboBox rp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label pinvnum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox pncount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label pprice;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label pcount;
        private System.Windows.Forms.Label pcname;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label ptotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label ppaid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label punpaid;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox prf;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuFlatButton reitem;
    }
}