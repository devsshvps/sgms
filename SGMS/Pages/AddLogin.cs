﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class AddLogin : MetroFramework.Forms.MetroForm
    {
        string r1;
        string r2;
        string r3;
        string r4;
        string r5;
        string r6;
        string r7;
        string r8;
        string r9;
        string r10;
        string r11;
        string r12;
        string r13;
        string r14;
        string r15;
        string r16;
        string r17;
        string r18;
        string r19;
        string r20;
        string r21;
        string r22;
        string r23;
        string r24;
        string r25 = "0";
        string r26 = "0";
        string r27 = "0";
        string r28 = "0";
        string r29 = "0";
        string r30 = "0";
        public AddLogin()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            LoadUsers();
        }
        private static List<User> UsersP = new List<User>();
        private void LoadUsers()
        {
            try
            {
                UsersP.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var result = DBConnection.SqliteCommand("SELECT *  FROM Login", dic);
                if (result.Rows.Count > 0)
                {

                    foreach (DataRow item in result.Rows)
                    {
                        User u = new User();
                        u.uid = int.Parse(item["uid"].ToString());
                        u.username = item["username"].ToString();
                        u.fullname = item["fullname"].ToString();
                        u.password = item["password"].ToString();
                        u.Cash = double.Parse(item["Cash"].ToString());
                        u.BlockState = int.Parse(item["BlockState"].ToString());
                        UsersP.Add(u);
                    }
                }
            }
            catch { }
        }
        private void add_Click(object sender, EventArgs e)
        {
            CheckRoles();
            if (string.IsNullOrWhiteSpace(Username.Text) || string.IsNullOrWhiteSpace(Fullname.Text) || string.IsNullOrWhiteSpace(Password.Text))
            {
                cMessgeBox mess = new cMessgeBox("يرجي التأكد من ملئ جميع البيانات", "error", "c", 1500);
                mess.ShowDialog();
            }
            else
            {
                var usrn = UsersP.Find(u => u.username == Username.Text);
                var fuln = UsersP.Find(u => u.fullname == Fullname.Text);
                if(usrn != null)
                {
                    cMessgeBox mess = new cMessgeBox("يوجد مستخدم بهذا الإسم الحقيقي", "error", "c", 1500);
                    mess.ShowDialog();
                }
                else if(fuln != null)
                {
                    cMessgeBox mess = new cMessgeBox("يوجد مستخدم بنفس إسم المستخدم", "error", "c", 1500);
                    mess.ShowDialog();
                }
                else
                {
                    try
                    {
                        var username = Username.Text.ToString();
                        var password = Password.Text.ToString();
                        var fullname = Fullname.Text.ToString();
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@username", username);
                        addC.Add("@password", password);
                        addC.Add("@fullname", fullname);
                        addC.Add("@role1", r1);
                        addC.Add("@role2", r2);
                        addC.Add("@role3", r3);
                        addC.Add("@role4", r4);
                        addC.Add("@role5", r5);
                        addC.Add("@role6", r6);
                        addC.Add("@role7", r7);
                        addC.Add("@role8", r8);
                        addC.Add("@role9", r9);
                        addC.Add("@role10", r10);
                        addC.Add("@role11", r11);
                        addC.Add("@role12", r12);
                        addC.Add("@role13", r13);
                        addC.Add("@role14", r14);
                        addC.Add("@role15", r15);
                        addC.Add("@role16", r16);
                        addC.Add("@role17", r17);
                        addC.Add("@role18", r18);
                        addC.Add("@role19", r19);
                        addC.Add("@role20", r20);
                        addC.Add("@role21", r21);
                        addC.Add("@role22", r22);
                        addC.Add("@role23", r23);
                        addC.Add("@role24", r24);
                        addC.Add("@role25", r25);
                        addC.Add("@role26", r26);
                        addC.Add("@role27", r27);
                        addC.Add("@role28", r28);
                        addC.Add("@role29", r29);
                        addC.Add("@role30", r30);
                        var addNotice = DBConnection.SqliteCommand("INSERT  INTO `Login`(`username`, `password`, `fullname`, `role1`, `role2`,`role3`, `role4`, `role5`,`role6`, `role7`, `role8`,`role9`, `role10`, `role11`,`role12`, `role13`, `role14`,`role15`, `role16`, `role17`,`role18`, `role19`, `role20`,`role21`, `role22`, `role23`, `role24`, `role25`, `role26`, `role27`, `role28`, `role29`, `role30`) values(@username,@password,@fullname,@role1,@role2,@role3,@role4,@role5,@role6,@role7,@role8,@role9,@role10,@role11,@role12,@role13,@role14,@role15,@role16,@role17,@role18,@role19,@role20,@role21,@role22,@role23,@role24,@role25,@role26,@role27,@role28,@role29,@role30)", addC);
                        cMessgeBox mess = new cMessgeBox("تمت إضافة المستخدم بنجاح", "done", "c", 1000);
                        mess.ShowDialog();
                        this.Close();
                    }
                    catch { }

                }
            }
        }
        private void CheckRoles()
        {
            if (treeView1.Nodes["s1"].Checked)
            {
                r1 = "1";
            }
            else
            {
                r1 = "0";

            }
            if (treeView1.Nodes["s2"].Checked)
            {
                r2 = "1";
            }
            else
            {
                r2 = "0";

            }
            if (treeView1.Nodes["s3"].Checked)
            {
                r3 = "1";
            }
            else
            {
                r3 = "0";

            }
            if (treeView1.Nodes["s4"].Checked)
            {
                r4 = "1";
            }
            else
            {
                r4 = "0";

            }
            if (treeView1.Nodes["s5"].Checked)
            {
                r5 = "1";
            }
            else
            {
                r5 = "0";

            }
            if (treeView1.Nodes["s6"].Checked)
            {
                r6 = "1";
            }
            else
            {
                r6 = "0";

            }
            if (treeView1.Nodes["s7"].Checked)
            {
                r7 = "1";
            }
            else
            {
                r7 = "0";

            }
            if (treeView1.Nodes["s8"].Checked)
            {
                r8 = "1";
            }
            else
            {
                r8 = "0";

            }
            if (treeView1.Nodes["s9"].Checked)
            {
                r9 = "1";
            }
            else
            {
                r9 = "0";

            }
            if (treeView1.Nodes["s10"].Checked)
            {
                r10 = "1";
            }
            else
            {
                r10 = "0";

            }
            if (treeView1.Nodes["s11"].Checked)
            {
                r11 = "1";
            }
            else
            {
                r11 = "0";

            }
            if (treeView1.Nodes["s12"].Checked)
            {
                r12 = "1";
            }
            else
            {
                r12 = "0";

            }
            if (treeView1.Nodes["s13"].Checked)
            {
                r13 = "1";
            }
            else
            {
                r13 = "0";

            }
            if (treeView1.Nodes["s14"].Checked)
            {
                r14 = "1";
            }
            else
            {
                r14 = "0";

            }
            if (treeView1.Nodes["s15"].Checked)
            {
                r15 = "1";
            }
            else
            {
                r15 = "0";

            }
            if (treeView1.Nodes["s16"].Checked)
            {
                r16 = "1";
            }
            else
            {
                r16 = "0";

            }
            if (treeView1.Nodes["s17"].Checked)
            {
                r17 = "1";
            }
            else
            {
                r17 = "0";

            }
            if (treeView1.Nodes["s18"].Checked)
            {
                r18 = "1";
            }
            else
            {
                r18 = "0";

            }
            if (treeView1.Nodes["s19"].Checked)
            {
                r19 = "1";
            }
            else
            {
                r19 = "0";

            }
            if (treeView1.Nodes["s20"].Checked)
            {
                r20 = "1";
            }
            else
            {
                r20 = "0";

            }
            if (treeView1.Nodes["s21"].Checked)
            {
                r21 = "1";
            }
            else
            {
                r21 = "0";

            }
            if (treeView1.Nodes["s22"].Checked)
            {
                r22 = "1";
            }
            else
            {
                r22 = "0";

            }

            if (treeView1.Nodes["s23"].Checked)
            {
                r23 = "1";
            }
            else
            {
                r23 = "0";

            }
            if (treeView1.Nodes["s24"].Checked)
            {
                r24 = "1";
            }
            else
            {
                r24 = "0";

            }
            //if (treeView1.Nodes["s25"].Checked)
            //{
            //    r25 = "1";
            //}
            //else
            //{
            //    r25 = "0";

            //}
            //if (treeView1.Nodes["s26"].Checked)
            //{
            //    r26 = "1";
            //}
            //else
            //{
            //    r26 = "0";

            //}
            //if (treeView1.Nodes["s27"].Checked)
            //{
            //    r27 = "1";
            //}
            //else
            //{
            //    r27 = "0";

            //}
            //if (treeView1.Nodes["s28"].Checked)
            //{
            //    r28 = "1";
            //}
            //else
            //{
            //    r28 = "0";

            //}
            //if (treeView1.Nodes["s29"].Checked)
            //{
            //    r29 = "1";
            //}
            //else
            //{
            //    r29 = "0";

            //}
            //if (treeView1.Nodes["s30"].Checked)
            //{
            //    r30 = "1";
            //}
            //else
            //{
            //    r30 = "0";

            //}
        }
    }
}
