﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace SGMS.Pages
{
    public partial class ReItemP : MetroFramework.Forms.MetroForm
    {
        string brcode;
        double count, bprice, ClientCash;
        int IINum, riid, ClientIDD;
        public ReItemP(string BAR, double CO, int rid)
        {
            brcode = BAR;
            count = CO;
            riid = rid;
            InitializeComponent();
            LoadUser();
            loadDe();
            ItemsData();
            LoadBadItems();
            FillCashList();
        }
        List<Purchase> pLi = new List<Purchase>();
        private static List<BadItems> BadItemsP = new List<BadItems>();
        private static List<Item> ItemP = new List<Item>();
        private static List<CashInvD> CashList = new List<CashInvD>();

        private void LoadUser()
        {
            try
            {
                pLi.Clear();
                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("@BarCode", brcode);
                di.Add("@Count", count);
                var x2 = DBConnection.SqliteCommand("SELECT DISTINCT * FROM Purchases where BarCode = @BarCode AND Count >= @Count GROUP BY ClientName", di);
                var x = DBConnection.SqliteCommand("SELECT DISTINCT * FROM Purchases where BarCode = @BarCode AND Count >= @Count", di);

                ClientName.DataSource = x2;
                ClientName.DisplayMember = "ClientName";
                ClientName.ValueMember = "Client";
                ClientName.SelectedIndex = -1;

                if (x.Rows.Count > 0)
                {
                    foreach (DataRow item in x.Rows)
                    {
                        Purchase u = new Purchase();
                        u.id = int.Parse(item["id"].ToString());
                        u.Num = int.Parse(item["Num"].ToString());
                        u.item = item["item"].ToString();
                        u.BarCode = item["BarCode"].ToString();
                        u.Count = double.Parse(item["Count"].ToString());
                        u.ItemPrice = double.Parse(item["ItemPrice"].ToString());
                        u.sPrice = double.Parse(item["sPrice"].ToString());
                        u.TotalBuy = double.Parse(item["TotalBuy"].ToString());
                        u.Client = int.Parse(item["Client"].ToString());
                        u.ClientName = item["ClientName"].ToString();
                        u.UserID = int.Parse(item["UserID"].ToString());
                        u.UserName = item["UserName"].ToString();
                        u.Date_ct = DateTime.Parse(item["Date_ct"].ToString());
                        u.Time_ct = DateTime.Parse(item["Time_ct"].ToString());
                        pLi.Add(u);
                    }
                }
            }
            catch { }

        }
        private void LoadBadItems()
        {
            try
            {
                BadItemsP.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var Ccl = DBConnection.SqliteCommand("SELECT *  FROM BadItems", dic);
                if (Ccl == null) return;
                if (Ccl.Rows.Count > 0)
                {
                    foreach (DataRow item in Ccl.Rows)
                    {
                        BadItems u = new BadItems();
                        u.id = int.Parse(item["id"].ToString());
                        u.BarCode = item["BarCode"].ToString();
                        u.ItemName = item["ItemName"].ToString();
                        u.Count = double.Parse(item["Count"].ToString());
                        u.LastReDate = DateTime.Parse(item["LastReDate"].ToString());

                        BadItemsP.Add(u);
                    }
                }
            }
            catch { }
        }
        private void ItemsData()
        {
            ItemP.Clear();
            Dictionary<string, object> dics = new Dictionary<string, object>();
            dics.Add("@r", 1);
            var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where r =@r", dics);
            if (rIt == null) return;
            if (rIt.Rows.Count > 0)
            {
                for (int i = 0; i < rIt.Rows.Count; i++)
                {
                    var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                    var _name = rIt.Rows[i]["Name"].ToString();
                    var _sellprice = rIt.Rows[i]["SellPrice"].ToString();
                    var _buyprice = rIt.Rows[i]["ItemPrice"].ToString();
                    var _count = rIt.Rows[i]["Count"].ToString();
                    var _TB = rIt.Rows[i]["TotalBuy"].ToString();
                    var _barc = rIt.Rows[i]["BarCode"].ToString();
                    var _P = rIt.Rows[i]["NumOfPurc"].ToString();
                    var p = new Item
                    {
                        id = _id,
                        Name = _name,
                        Count = Double.Parse(_count),
                        TotalBuy = Double.Parse(_TB),
                        ItemPrice = Double.Parse(_buyprice),
                        SellPrice = Double.Parse(_sellprice),
                        NumOfPurc = Double.Parse(_P),
                        BarCode = _barc

                    };
                    ItemP.Add(p);
                }
            }
        }
        private void FillCashList()
        {
            try
            {
                CashList.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@r", 0);
                var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM CashInvD where id >@r", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    foreach (DataRow item in rIt.Rows)
                    {
                        CashInvD u = new CashInvD();
                        u.id = int.Parse(item["id"].ToString());
                        u.invNum = int.Parse(item["invNum"].ToString());
                        u.unpaid = double.Parse(item["unpaid"].ToString());
                        u.totalInv = double.Parse(item["totalInv"].ToString());
                        u.clientID = int.Parse(item["clientID"].ToString());
                        u.userID = int.Parse(item["userID"].ToString());
                        u.clientName = item["clientName"].ToString();
                        u.userName = item["userName"].ToString();
                        u.invDateTime = DateTime.Parse(item["invDateTime"].ToString());

                        CashList.Add(u);
                    }
                }
            }
            catch { }

        }
        private void LoadClient()
        {

            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", ClientIDD);
                var Ccl = DBConnection.SqliteCommand("SELECT *  FROM Clients where id =@id", dic);
                if (Ccl == null) return;
                if (Ccl.Rows.Count > 0)
                {
                    for (int i = 0; i < Ccl.Rows.Count; i++)
                    {
                        ClientCash = Double.Parse(Ccl.Rows[i]["Cash"].ToString());

                    }
                }

            }
            catch
            {
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var CID = int.Parse(ClientName.SelectedValue.ToString());
                var p = pLi.FindAll(i => i.Client == CID);
                var s = pLi.Find(i2 => i2.Client == CID);
                InvNum.DataSource = p;
                InvNum.DisplayMember = "Num";
                InvNum.ValueMember = "Num";
                InvNum.SelectedIndex = -1;
                ClientIDD = s.Client;
                LoadClient();
            }
            catch { }

        }
        private void InvNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                var nu = int.Parse(InvNum.SelectedValue.ToString());
                var p = pLi.FindAll(i => i.Num == nu);
                foreach (var i in p)
                {
                    IINum = i.Num;
                    tCount.Text = i.Count.ToString();
                    tPrice.Text = i.ItemPrice.ToString();
                    tDate.Text = i.Date_ct.ToString("yyyy-MM-dd");
                }

                var iii = CashList.Find(i2 => i2.invNum == nu);
                label9.Text = iii.unpaid.ToString(); ; ;
            }
            catch { }
        }
        private void loadDe()
        {
            var ix = pLi.Find(i => i.BarCode == brcode);
            ina.Text = ix.item;
            iba.Text = ix.BarCode;
        }
        private void reitem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("إرتجاع الكمية المحددة ؟", "إرتجاع منتج", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    UpdateItems();
                    UpdateP();
                    UpdateCashInvD();
                    updateWA();
                    uInsertBadItems();
                    cMessgeBox mess = new cMessgeBox("تم إرتجاع الكمية بنجاح", "done", "p", 1000);
                    mess.ShowDialog();
                    this.Close();
                }
                catch { }
            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }
        private void uInsertBadItems()
        {
            try
            {
                var found = BadItemsP.Find(i => i.BarCode == brcode);
                if (found == null)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@BarCode", brcode);
                    dic.Add("@ItemName", ina.Text);
                    dic.Add("@Count", count);
                    dic.Add("@LastReDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    var addToBad = DBConnection.SqliteCommand("INSERT  INTO `BadItems`(`BarCode`, `ItemName`, `Count`, `LastReDate`) values(@BarCode,@ItemName,@Count,@LastReDate)", dic);

                }
                else
                {
                    double newcount = found.Count + count;
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@Count", newcount);
                    dic.Add("@barcode", brcode);
                    dic.Add("@LastReDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    var UpdateBad = DBConnection.SqliteCommand("update BadItems Set  Count = @Count, LastReDate = @LastReDate where BarCode =@barcode", dic);
                }
            }
            catch { }
        }
        private void updateWA()
        {
            try
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@id", riid);
                var DelCashInvS = DBConnection.SqliteCommand("DELETE FROM WaitRE WHERE id =@id", dics);
            }
            catch { }
        }
        private void UpdateItems()
        {
            var pte = pLi.Find(i => i.Num == IINum && i.BarCode == brcode);
            bprice = pte.ItemPrice;
            var tbb = count * bprice;
            var ite = ItemP.Find(i => i.BarCode == brcode);
            var ntot = ite.TotalBuy - tbb;
            var neNumO = ite.NumOfPurc - count;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@TotalBuy", ntot);
            dic.Add("@NumOfPurc", neNumO);
            dic.Add("@barcode", brcode);
            var UpdateItems = DBConnection.SqliteCommand("update Items Set TotalBuy =@TotalBuy , NumOfPurc =@NumOfPurc where BarCode =@barcode", dic);
        }
        private void UpdateP()
        {
            var pte = pLi.Find(i => i.Num == IINum && i.BarCode == brcode);
            var ncot = pte.Count; // الكمية المباعة
            var ntotal = pte.TotalBuy; // اجمالي البيع
            bprice = pte.ItemPrice;
            double count1 = ncot - count;
            double totalsum = count * bprice;
            double total1 = ntotal - totalsum;
            if (count1 == 0)
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@Num", IINum);
                dics.Add("@BarCode", brcode);
                var DelP = DBConnection.SqliteCommand("DELETE FROM Purchases WHERE Num =@Num AND BarCode =@BarCode", dics);
            }
            else
            {
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@count", count1);
                dics.Add("@totalprice", total1);
                dics.Add("@Num", IINum);
                dics.Add("@BarCode", brcode);
                var uP = DBConnection.SqliteCommand("update Purchases Set Count = @count , TotalBuy = @totalprice where Num = @Num AND BarCode = @BarCode", dics);

            }
        }
        private void UpdateCashInvD()
        {
            try
            {
                var finCashInvS = CashList.Find(i => i.invNum == IINum);
                var ntotalinv = finCashInvS.totalInv;
                var nunpaid = finCashInvS.unpaid;

                double totalsum = count * bprice;
                double total1 = ntotalinv - totalsum;
                double unpaid1 = nunpaid - totalsum;
                double money = totalsum - nunpaid;

                if (total1 == 0 && nunpaid == 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@invNum", IINum);
                    var DelCashInv = DBConnection.SqliteCommand("DELETE FROM CashInvD WHERE invNum =@invNum", dics);
                    MessageBox.Show("يرجي تحصيل مبلغ وقدره" + " " + totalsum + " " + "جنية من المورد");
                }
                else if (total1 == 0 && nunpaid > 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@invNum", IINum);
                    var DelCashInv = DBConnection.SqliteCommand("DELETE FROM CashInvD WHERE invNum =@invNum", dics);

                    double cc = ClientCash - nunpaid;
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@cID", ClientIDD);
                    addC.Add("@Cash", cc);
                    var UpdateCu = DBConnection.SqliteCommand("update Clients Set Cash = @Cash where id=@cID", addC);
                    MessageBox.Show("يرجي تحصيل مبلغ وقدره" + " " + money + " " + "جنية من المورد");

                }
                else if (total1 > 0 && nunpaid == 0)
                {
                    Dictionary<string, object> dics = new Dictionary<string, object>();
                    dics.Add("@totalInv", total1);
                    dics.Add("@unpaid", 0);
                    dics.Add("@invNum", IINum);
                    var uSales = DBConnection.SqliteCommand("update CashInvD Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum", dics);
                    MessageBox.Show("يرجي تحصيل مبلغ وقدره" + " " + totalsum + " " + "جنية من المورد");
                }
                else if (total1 > 0 && nunpaid > 0)
                {   
                    if (totalsum == nunpaid)
                    {
                        double cc = ClientCash - nunpaid;
                        unpaid1 = 0;
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@invNum", IINum);
                        addC.Add("@cID", ClientIDD);
                        addC.Add("@Cash", cc);
                        addC.Add("@unpaid", unpaid1);
                        addC.Add("@totalInv", total1);
                        var UpdateCu = DBConnection.SqliteCommand("update Clients Set Cash = @Cash where id=@cID", addC);
                        MessageBox.Show("تم خصم مبلغ وقدرة" + " " + nunpaid + " " + "من مؤجلات المورد");
                        var uSales = DBConnection.SqliteCommand("update CashInvD Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum", addC);
                    }
                    else if (totalsum > nunpaid)
                    {

                        var nc = totalsum - nunpaid;
                        double cc = ClientCash - nunpaid;
                        unpaid1 = 0;
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@invNum", IINum);
                        addC.Add("@cID", ClientIDD);
                        addC.Add("@Cash", cc);
                        addC.Add("@unpaid", unpaid1);
                        addC.Add("@totalInv", total1);
                        var UpdateCu = DBConnection.SqliteCommand("update Clients Set Cash = @Cash where id=@cID", addC);
                        var uSales = DBConnection.SqliteCommand("update CashInvD Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum", addC);
                        MessageBox.Show("يرجي تحصيل مبلغ وقدره" + " " + nc + " " + "جنية من المورد");
                    }
                    else if (totalsum < nunpaid)
                    {
                        var nc = nunpaid - totalsum;
                        double cc = ClientCash - totalsum;
                        unpaid1 = nc;
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@invNum", IINum);
                        addC.Add("@cID", ClientIDD);
                        addC.Add("@Cash", cc);
                        addC.Add("@unpaid", unpaid1);
                        addC.Add("@totalInv", total1);
                        var UpdateCu = DBConnection.SqliteCommand("update Clients Set Cash = @Cash where id=@cID", addC);
                        var uSales = DBConnection.SqliteCommand("update CashInvD Set totalInv = @totalInv , unpaid = @unpaid where invNum = @invNum", addC);
                        MessageBox.Show("تم خصم مبلغ وقدرة" + " " + totalsum + " " + "من مؤجلات المورد");
                    }
                }
            }
            catch { }
        }
    }
}
