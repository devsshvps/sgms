﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class UpdateInfo : MetroFramework.Forms.MetroForm
    {

        public UpdateInfo()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            informations.Text = "الجديد في الإصدار 1.07.2020.0"
                + "\n"
                + "\n"
                + "1 - إصلاح بعض الأخطاء ."
                + "\n"
                + "2 - إضافة إنشاء فاتورة ضريبية."
                + "\n"
                + "\n"
                + "\n"
                + "برجاء مراعاة ان الفواتير الضريبية ماذالت تحت الإختبار و يرجي عدم إستخدامها في اي عملية مبياعات بها أجل.";
        }
        
    }
}

