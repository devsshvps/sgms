﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class AddAdminEX : MetroFramework.Forms.MetroForm
    {
        public AddAdminEX()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
        }

        private void add_Click(object sender, EventArgs e)
        {
                InsertEX();
        }
        private void TextChanged(object sender, EventArgs e)
        {
            if (Reason.TextLength > 0 && Cash.TextLength > 0)
            {
                add.Enabled = true;
            }
            else
            {
                add.Enabled = false;
            }
        }
        private void InsertEX()
        {
            try
            {

                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@Reason", Reason.Text);
                addC.Add("@Cash", Cash.Text);
                addC.Add("@TCash", Cash.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@r_Time", DateTime.Now.ToString("HH:mm:ss"));
                addC.Add("@r_Date", DateTime.Now.ToString("yyyy-MM-dd"));

                var addEX = DBConnection.SqliteCommand("INSERT  INTO `PExpenses`(`Reason`, `Cash`, `TCash`, `UserID`, `UserName`, `r_Time`, `r_Date`) " +
                    "values(@Reason,@Cash,@TCash,@UserID,@UserName,@r_Time,@r_Date)", addC);

                cMessgeBox mess = new cMessgeBox("تمت إضافة المصروف الإداري بنجاح", "done", "p",1000);
                mess.ShowDialog();
                this.Close();
            }
            catch
            {
            }
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

    }
}
