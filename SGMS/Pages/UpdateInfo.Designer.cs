﻿namespace SGMS
{
    partial class UpdateInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AToolbar = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.informations = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // AToolbar
            // 
            this.AToolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AToolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.AToolbar.Location = new System.Drawing.Point(20, 60);
            this.AToolbar.Name = "AToolbar";
            this.AToolbar.Size = new System.Drawing.Size(387, 29);
            this.AToolbar.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 12F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label2.Location = new System.Drawing.Point(139, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 30);
            this.label2.TabIndex = 58;
            this.label2.Text = "معلومات التحديث";
            // 
            // informations
            // 
            this.informations.Location = new System.Drawing.Point(20, 95);
            this.informations.Name = "informations";
            this.informations.ReadOnly = true;
            this.informations.Size = new System.Drawing.Size(384, 279);
            this.informations.TabIndex = 59;
            this.informations.Text = "";
            // 
            // UpdateInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 397);
            this.Controls.Add(this.informations);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AToolbar);
            this.Name = "UpdateInfo";
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel AToolbar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox informations;
    }
}