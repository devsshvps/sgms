﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;
using SGMS.Print;

namespace SGMS
{
    public partial class ViewPaidP : MetroFramework.Forms.MetroForm
    {
        string invN;

        public ViewPaidP()
        {
            InitializeComponent();
            LoadPaid();
        }
        private void LoadPaid()
        {
            var m = DateTime.Now.ToString("MM");
            string y = DateTime.Now.ToString("yyyy");
            string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
            string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);

            var result = DBConnection.SqliteCommand("Select * from PaidPurchases where PaidDate between '" + f + "' and '" + t + "'", dic).ConvertPaidPurchases();
            if (result == null) return;

            foreach (PaidPurchases item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(PDatagride);
                row.Cells[0].Value = item.InvNumber;
                row.Cells[1].Value = item.Paid;
                row.Cells[2].Value = item.UnPaid;
                row.Cells[3].Value = item.ClientName;
                row.Cells[4].Value = item.UserName;
                row.Cells[5].Value = item.PaidDate.ToString("yyyy-MM-dd");
                row.Cells[6].Value = item.id;
                PDatagride.Rows.Add(row);
            }
        }

        private void PDatagride_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in PDatagride.SelectedRows)
                {
                    if (PDatagride.SelectedRows.Count > 0)
                    {
                        invN = row.Cells[0].Value.ToString();
                        view.Enabled = true;
                    }
                    else
                    {
                        invN = "";
                        view.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void view_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(invN))
            {
                try
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("@num", invN);
                    var result = DBConnection.SqliteCommand("SELECT * FROM PaidPurchases where id=@num", dic);
                    if (result == null) return;
                    if (result.Rows.Count > 0)
                    {
                        string d1 = DateTime.Now.ToString("dd");
                        string m1 = DateTime.Now.ToString("MM");
                        string y1 = DateTime.Now.ToString("yyyy");
                        string ti1 = DateTime.Now.ToString("t");
                        string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                        string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                        string PrintDateTime = "بتاريخ" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                        payInvoice Payinva = new payInvoice();
                        Payinva.SetDataSource(result);
                        Payinva.SetParameterValue("invsName", "إيصال صرف نقدي");
                        Payinva.SetParameterValue("logoUrl", Dashboard.logopath);
                        Payinva.SetParameterValue("PrintDateTime", PrintDateTime);
                        Payinva.SetParameterValue("name", Dashboard.invCoN);
                        Payinva.SetParameterValue("slug", Dashboard.invCoS);
                        Payinva.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                        Payinva.SetParameterValue("address", Dashboard.invCoA);
                        Payinva.PrintToPrinter(1, false, 0, 0);

                    }

                }
                catch
                {
                }
            }
            else
            {
                cMessgeBox mess = new cMessgeBox("برجاء إختيار سجل سداد لطباعته", "error", "p", 1500);
                mess.ShowDialog();
            }
        }
    }
}
