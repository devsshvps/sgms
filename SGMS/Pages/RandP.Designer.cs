﻿namespace SGMS
{
    partial class RandP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RandP));
            this.label14 = new System.Windows.Forms.Label();
            this.view = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tr1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tr2 = new System.Windows.Forms.TextBox();
            this.tr3 = new System.Windows.Forms.TextBox();
            this.tr4 = new System.Windows.Forms.TextBox();
            this.tr5 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pr2 = new System.Windows.Forms.TextBox();
            this.pr1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(69, 35);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(259, 29);
            this.label14.TabIndex = 66;
            this.label14.Text = "ملاحظات وشروط عرض السعر";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // view
            // 
            this.view.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view.BorderRadius = 0;
            this.view.ButtonText = "حفظ";
            this.view.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view.DisabledColor = System.Drawing.Color.White;
            this.view.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.view.Iconcolor = System.Drawing.Color.Transparent;
            this.view.Iconimage = null;
            this.view.Iconimage_right = null;
            this.view.Iconimage_right_Selected = null;
            this.view.Iconimage_Selected = null;
            this.view.IconMarginLeft = 0;
            this.view.IconMarginRight = 0;
            this.view.IconRightVisible = false;
            this.view.IconRightZoom = 0D;
            this.view.IconVisible = false;
            this.view.IconZoom = 20D;
            this.view.IsTab = false;
            this.view.Location = new System.Drawing.Point(116, 334);
            this.view.Margin = new System.Windows.Forms.Padding(5, 9, 5, 9);
            this.view.Name = "view";
            this.view.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.OnHoverTextColor = System.Drawing.Color.White;
            this.view.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.view.selected = false;
            this.view.Size = new System.Drawing.Size(176, 42);
            this.view.TabIndex = 70;
            this.view.Text = "حفظ";
            this.view.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.view.Textcolor = System.Drawing.Color.White;
            this.view.TextFont = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // tr1
            // 
            this.tr1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tr1.Font = new System.Drawing.Font("Cairo", 12F);
            this.tr1.Location = new System.Drawing.Point(392, 108);
            this.tr1.Name = "tr1";
            this.tr1.Size = new System.Drawing.Size(359, 37);
            this.tr1.TabIndex = 71;
            this.tr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(551, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 22);
            this.label1.TabIndex = 72;
            this.label1.Text = "الشروط";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tr2
            // 
            this.tr2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tr2.Font = new System.Drawing.Font("Cairo", 12F);
            this.tr2.Location = new System.Drawing.Point(392, 166);
            this.tr2.Name = "tr2";
            this.tr2.Size = new System.Drawing.Size(359, 37);
            this.tr2.TabIndex = 73;
            this.tr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tr3
            // 
            this.tr3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tr3.Font = new System.Drawing.Font("Cairo", 12F);
            this.tr3.Location = new System.Drawing.Point(392, 225);
            this.tr3.Name = "tr3";
            this.tr3.Size = new System.Drawing.Size(359, 37);
            this.tr3.TabIndex = 74;
            this.tr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tr4
            // 
            this.tr4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tr4.Font = new System.Drawing.Font("Cairo", 12F);
            this.tr4.Location = new System.Drawing.Point(392, 281);
            this.tr4.Name = "tr4";
            this.tr4.Size = new System.Drawing.Size(359, 37);
            this.tr4.TabIndex = 75;
            this.tr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tr5
            // 
            this.tr5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tr5.Font = new System.Drawing.Font("Cairo", 12F);
            this.tr5.Location = new System.Drawing.Point(392, 337);
            this.tr5.Name = "tr5";
            this.tr5.Size = new System.Drawing.Size(359, 37);
            this.tr5.TabIndex = 76;
            this.tr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label2.Location = new System.Drawing.Point(168, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 22);
            this.label2.TabIndex = 77;
            this.label2.Text = "الملاحظات";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pr2
            // 
            this.pr2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pr2.Font = new System.Drawing.Font("Cairo", 12F);
            this.pr2.Location = new System.Drawing.Point(69, 264);
            this.pr2.Name = "pr2";
            this.pr2.Size = new System.Drawing.Size(270, 37);
            this.pr2.TabIndex = 79;
            this.pr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pr1
            // 
            this.pr1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pr1.Font = new System.Drawing.Font("Cairo", 12F);
            this.pr1.Location = new System.Drawing.Point(69, 206);
            this.pr1.Name = "pr1";
            this.pr1.Size = new System.Drawing.Size(270, 37);
            this.pr1.TabIndex = 78;
            this.pr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RandP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 410);
            this.ControlBox = false;
            this.Controls.Add(this.pr2);
            this.Controls.Add(this.pr1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tr5);
            this.Controls.Add(this.tr4);
            this.Controls.Add(this.tr3);
            this.Controls.Add(this.tr2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tr1);
            this.Controls.Add(this.view);
            this.Controls.Add(this.label14);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RandP";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuFlatButton view;
        private System.Windows.Forms.TextBox tr1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tr2;
        private System.Windows.Forms.TextBox tr3;
        private System.Windows.Forms.TextBox tr4;
        private System.Windows.Forms.TextBox tr5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pr2;
        private System.Windows.Forms.TextBox pr1;
    }
}