﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;
using SGMS.Print;

namespace SGMS
{
    public partial class ViewR : MetroFramework.Forms.MetroForm
    {

        public ViewR()
        {
            InitializeComponent();
            LoadV();
        }
        private void LoadV()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            var result = DBConnection.SqliteCommand("Select * from Returned", dic).ConvertReturned();
            if (result == null) return;

            foreach (Returned item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(PDatagride);
                row.Cells[0].Value = item.BarCode;
                row.Cells[1].Value = item.ItemName;
                row.Cells[2].Value = item.reCount;
                row.Cells[3].Value = item.rePrice;
                row.Cells[4].Value = item.Customer;
                row.Cells[5].Value = item.ReUser;
                row.Cells[6].Value = Login.arNumber(item.DateTime_re.ToString("dd")) + " " +
                    Login.arDate(item.DateTime_re.ToString("MM")) + " " +
                    Login.arNumber(item.DateTime_re.ToString("yyyy"));

                row.Cells[7].Value = Login.arNumber(item.date_ct.ToString("dd")) + " " +
                    Login.arDate(item.date_ct.ToString("MM")) + " " +
                    Login.arNumber(item.date_ct.ToString("yyyy"));
                PDatagride.Rows.Add(row);
            }
        }
    }
}
