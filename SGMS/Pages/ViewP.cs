﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class ViewP : MetroFramework.Forms.MetroForm
    {
        string invN;

        public ViewP()
        {
            InitializeComponent();
            LoadP();
        }
        private void LoadP()
        {
            var m = DateTime.Now.ToString("MM");
            string y = DateTime.Now.ToString("yyyy");
            string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
            string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("df", f);
            dic.Add("dt", t);

            var result = DBConnection.SqliteCommand("Select * from CashInvD where invDateTime between '" + f + "' and '" + t + "'", dic).ConvertCashInvoice();
            if (result == null) return;

            foreach (CashInvD item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(PDatagride);
                row.Cells[0].Value = item.invNum;
                row.Cells[1].Value = item.totalInv;
                row.Cells[2].Value = item.clientName;
                row.Cells[3].Value = item.userName;
                row.Cells[4].Value = item.invDateTime.ToString("yyyy-MM-dd");
                PDatagride.Rows.Add(row);
            }
        }

        private void PDatagride_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in PDatagride.SelectedRows)
                {
                    if (PDatagride.SelectedRows.Count > 0)
                    {
                        invN = row.Cells[0].Value.ToString();
                        view.Enabled = true;
                    }
                    else
                    {
                        invN = "";
                        view.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }

        private void view_Click(object sender, EventArgs e)
        {
            if (invN.Length > 0)
            {
                ViewInvoicePage viewinv = new ViewInvoicePage(invN);
                viewinv.ShowDialog();
            }
            else
            {
                cMessgeBox mess = new cMessgeBox("برجاء إختيار فاتورة للعرض", "error", "p", 1500);
                mess.ShowDialog();
            }
        }
    }
}
