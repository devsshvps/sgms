﻿namespace SGMS
{
    partial class invTypes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(invTypes));
            this.label14 = new System.Windows.Forms.Label();
            this.SellsInv = new Bunifu.Framework.UI.BunifuFlatButton();
            this.SellsTaxInv = new Bunifu.Framework.UI.BunifuFlatButton();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(118, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(221, 29);
            this.label14.TabIndex = 66;
            this.label14.Text = "برجاء إختيار نوع الفاتورة";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SellsInv
            // 
            this.SellsInv.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.SellsInv.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.SellsInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.SellsInv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SellsInv.BorderRadius = 0;
            this.SellsInv.ButtonText = "فاتورة عادية";
            this.SellsInv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SellsInv.DisabledColor = System.Drawing.Color.White;
            this.SellsInv.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.SellsInv.Iconcolor = System.Drawing.Color.Transparent;
            this.SellsInv.Iconimage = null;
            this.SellsInv.Iconimage_right = null;
            this.SellsInv.Iconimage_right_Selected = null;
            this.SellsInv.Iconimage_Selected = null;
            this.SellsInv.IconMarginLeft = 0;
            this.SellsInv.IconMarginRight = 0;
            this.SellsInv.IconRightVisible = false;
            this.SellsInv.IconRightZoom = 0D;
            this.SellsInv.IconVisible = false;
            this.SellsInv.IconZoom = 20D;
            this.SellsInv.IsTab = false;
            this.SellsInv.Location = new System.Drawing.Point(230, 85);
            this.SellsInv.Margin = new System.Windows.Forms.Padding(5, 9, 5, 9);
            this.SellsInv.Name = "SellsInv";
            this.SellsInv.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.SellsInv.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.SellsInv.OnHoverTextColor = System.Drawing.Color.White;
            this.SellsInv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SellsInv.selected = false;
            this.SellsInv.Size = new System.Drawing.Size(176, 42);
            this.SellsInv.TabIndex = 70;
            this.SellsInv.Text = "فاتورة عادية";
            this.SellsInv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SellsInv.Textcolor = System.Drawing.Color.White;
            this.SellsInv.TextFont = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.SellsInv.Click += new System.EventHandler(this.SellsInv_Click);
            // 
            // SellsTaxInv
            // 
            this.SellsTaxInv.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.SellsTaxInv.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.SellsTaxInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.SellsTaxInv.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SellsTaxInv.BorderRadius = 0;
            this.SellsTaxInv.ButtonText = "فاتورة ضريبية";
            this.SellsTaxInv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SellsTaxInv.DisabledColor = System.Drawing.Color.White;
            this.SellsTaxInv.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.SellsTaxInv.Iconcolor = System.Drawing.Color.Transparent;
            this.SellsTaxInv.Iconimage = null;
            this.SellsTaxInv.Iconimage_right = null;
            this.SellsTaxInv.Iconimage_right_Selected = null;
            this.SellsTaxInv.Iconimage_Selected = null;
            this.SellsTaxInv.IconMarginLeft = 0;
            this.SellsTaxInv.IconMarginRight = 0;
            this.SellsTaxInv.IconRightVisible = false;
            this.SellsTaxInv.IconRightZoom = 0D;
            this.SellsTaxInv.IconVisible = false;
            this.SellsTaxInv.IconZoom = 20D;
            this.SellsTaxInv.IsTab = false;
            this.SellsTaxInv.Location = new System.Drawing.Point(40, 85);
            this.SellsTaxInv.Margin = new System.Windows.Forms.Padding(5, 9, 5, 9);
            this.SellsTaxInv.Name = "SellsTaxInv";
            this.SellsTaxInv.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.SellsTaxInv.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.SellsTaxInv.OnHoverTextColor = System.Drawing.Color.White;
            this.SellsTaxInv.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SellsTaxInv.selected = false;
            this.SellsTaxInv.Size = new System.Drawing.Size(176, 42);
            this.SellsTaxInv.TabIndex = 71;
            this.SellsTaxInv.Text = "فاتورة ضريبية";
            this.SellsTaxInv.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SellsTaxInv.Textcolor = System.Drawing.Color.White;
            this.SellsTaxInv.TextFont = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.SellsTaxInv.Click += new System.EventHandler(this.SellsTaxInv_Click);
            // 
            // invTypes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 161);
            this.ControlBox = false;
            this.Controls.Add(this.SellsTaxInv);
            this.Controls.Add(this.SellsInv);
            this.Controls.Add(this.label14);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "invTypes";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuFlatButton SellsInv;
        private Bunifu.Framework.UI.BunifuFlatButton SellsTaxInv;
    }
}