﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class ReturnS : MetroFramework.Forms.MetroForm
    {
        string BarCode , InvNum;
        int favBAR;
        public ReturnS()
        {
            InitializeComponent();
            FavBar();
            FillList();
            fDay.SelectedItem = DateTime.Now.ToString("dd");
            fMonth.SelectedItem = DateTime.Now.ToString("MM");
            fYear.SelectedItem = DateTime.Now.ToString("yyyy");
            LoadToday();

        }
        private static List<Models.Sales> SalesP = new List<Models.Sales>();
        private void FillList()
        {
            PDatagride.Rows.Clear();
            PDatagride.Refresh();
            SalesP.Clear();
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@count", 0);
                var sale = DBConnection.SqliteCommand("SELECT DISTINCT * FROM Sells where count > @count", dic).ConvertSales();
                var ite = DBConnection.SqliteCommand("SELECT * FROM Items where NumOfSales > @count", dic);

                if (sale == null) return;
                if (sale.Count > 0)
                {
                    foreach (Models.Sales item in sale)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(PDatagride);
                        row.Cells[0].Value = item.Num;
                        row.Cells[1].Value = item.BarCode;
                        row.Cells[2].Value = item.itemname;
                        row.Cells[3].Value = item.count;
                        row.Cells[4].Value = item.itemprice;
                        row.Cells[5].Value = item.sellfor;
                        row.Cells[6].Value = item.ttime.ToString("t");
                        row.Cells[7].Value = item.ddate.ToString("yyyy-MM-dd");
                        PDatagride.Rows.Add(row);

                        int _Num = item.Num;
                        string _BarCode = item.BarCode;
                        string _itemname = item.itemname;
                        double _count = item.count;
                        double _itemprice = item.itemprice;
                        string _sellfor = item.sellfor;
                        var _ttime = item.ttime;
                        var _ddate= item.ddate;
                        var p = new Models.Sales
                        {
                            Num = _Num,
                            BarCode = _BarCode,
                            itemname = _itemname,
                            count = _count,

                            itemprice = _itemprice,
                            sellfor = _sellfor,
                            ttime = _ttime,
                            ddate = _ddate,
                        };
                        SalesP.Add(p);
                    }
                }

                if (ite == null) return;
                if(ite.Rows.Count > 0)
                {
                    itemsNames.DataSource = ite;
                    itemsNames.DisplayMember = "Name";
                    itemsNames.ValueMember = "BarCode";
                    itemsNames.SelectedIndex = -1;
                }


            }
            catch { }
        }
        private void FavBar()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM FavBar where id =@id", dic).ConvertFavBar();
            if (check == null) return;

            foreach (FavBar item in check)
            {
                favBAR = item.Fav;
                if (favBAR == 1)
                {
                    itemsBarcodes.Select();
                }
            }
        }
        private void LoadToday()
        {
            PDatagride.Rows.Clear();
            PDatagride.Refresh();
            try
            {
                var item = SalesP.FindAll(i => i.count >= 1);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = item1.Num;
                    row.Cells[1].Value = item1.BarCode;
                    row.Cells[2].Value = item1.itemname;
                    row.Cells[3].Value = item1.count;
                    row.Cells[4].Value = item1.itemprice;
                    row.Cells[5].Value = item1.sellfor;
                    row.Cells[6].Value = item1.ttime.ToString("t");
                    row.Cells[7].Value = item1.ddate.ToString("yyyy-MM-dd");
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[7], ListSortDirection.Descending);
                }

            }
            catch { }
        }
        private void ShowByBarcode()
        {
            PDatagride.Rows.Clear();
            PDatagride.Refresh();
            try
            {
                var iBarcode = itemsBarcodes.Text;
                var item = SalesP.FindAll(i => i.BarCode == iBarcode);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = item1.Num;
                    row.Cells[1].Value = item1.BarCode;
                    row.Cells[2].Value = item1.itemname;
                    row.Cells[3].Value = item1.count;
                    row.Cells[4].Value = item1.itemprice;
                    row.Cells[5].Value = item1.sellfor;
                    row.Cells[6].Value = item1.ttime.ToString("t");
                    row.Cells[7].Value = item1.ddate.ToString("yyyy-MM-dd");
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[7], ListSortDirection.Descending);
                }

            }
            catch { }
        }
        private void ShowByName()
        {
            PDatagride.Rows.Clear();
            PDatagride.Refresh();
            try
            {
                var iBarcode = itemsNames.SelectedValue.ToString();
                var item = SalesP.FindAll(i => i.BarCode == iBarcode);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = item1.Num;
                    row.Cells[1].Value = item1.BarCode;
                    row.Cells[2].Value = item1.itemname;
                    row.Cells[3].Value = item1.count;
                    row.Cells[4].Value = item1.itemprice;
                    row.Cells[5].Value = item1.sellfor;
                    row.Cells[6].Value = item1.ttime.ToString("t");
                    row.Cells[7].Value = item1.ddate.ToString("yyyy-MM-dd");
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[7], ListSortDirection.Descending);

                }

            }
            catch { }
        }
        private void ShowByInv()
        {
            PDatagride.Rows.Clear();
            PDatagride.Refresh();
            try
            {
                int invNumb = int.Parse(invN.Text);
                var item = SalesP.FindAll(i => i.Num == invNumb);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = item1.Num;
                    row.Cells[1].Value = item1.BarCode;
                    row.Cells[2].Value = item1.itemname;
                    row.Cells[3].Value = item1.count;
                    row.Cells[4].Value = item1.itemprice;
                    row.Cells[5].Value = item1.sellfor;
                    row.Cells[6].Value = item1.ttime.ToString("t");
                    row.Cells[7].Value = item1.ddate.ToString("yyyy-MM-dd");
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[6], ListSortDirection.Descending);

                }

            }
            catch { }
        }
        private void ShowByDate()
        {

            try
            {
                PDatagride.Rows.Clear();
                PDatagride.Refresh();
                string fd = fDay.SelectedItem.ToString();
                string fm = fMonth.SelectedItem.ToString();
                string fy = fYear.SelectedItem.ToString();
                DateTime f = DateTime.Parse(fy + "-" + fm + "-" + fd);
                var item = SalesP.FindAll(i => i.ddate == f);
                foreach (var item1 in item)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = item1.Num;
                    row.Cells[1].Value = item1.BarCode;
                    row.Cells[2].Value = item1.itemname;
                    row.Cells[3].Value = item1.count;
                    row.Cells[4].Value = item1.itemprice;
                    row.Cells[5].Value = item1.sellfor;
                    row.Cells[6].Value = item1.ttime.ToString("t");
                    row.Cells[7].Value = item1.ddate.ToString("yyyy-MM-dd");
                    PDatagride.Rows.Add(row);
                    PDatagride.Sort(PDatagride.Columns[6], ListSortDirection.Descending);
                }

            }
            catch { }

        }
        private void itemsNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(itemsNames.SelectedIndex == -1)
            {
                LoadToday();
            }
            else
            {
                ShowByName();

            }

        }
        private void itemsBarcodes_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(itemsBarcodes.Text))
            {
                ShowByBarcode();
            }
            else
            {
                LoadToday();
            }
        }
        private void invN_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(invN.Text))
            {
                ShowByInv();
            }
            else
            {
                LoadToday();
            }
        }
        private void PDatagride_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if(PDatagride.Rows.Count > 0)
            {
                View.Enabled = true;
            }
            else
            {
                View.Enabled = false;
            }
        }
        private void PDatagride_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (PDatagride.Rows.Count > 0)
            {
                View.Enabled = true;
            }
            else
            {
                View.Enabled = false;
            }
        }
        private void fDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowByDate();
        }

        private void View_Click(object sender, EventArgs e)
        {
            FR fr = new FR(BarCode,InvNum);
            fr.FormClosed += frClosed;
            fr.ShowDialog();
        }
        void frClosed(object sender, FormClosedEventArgs e)
        {
            FavBar();
            FillList();
            fDay.SelectedItem = DateTime.Now.ToString("dd");
            fMonth.SelectedItem = DateTime.Now.ToString("MM");
            fYear.SelectedItem = DateTime.Now.ToString("yyyy");
        }
        private void PDatagride_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in PDatagride.SelectedRows)
                {
                    if (PDatagride.SelectedRows.Count > 0)
                    {
                        BarCode = row.Cells[1].Value.ToString();
                        InvNum = row.Cells[0].Value.ToString();
                        View.Enabled = true;
                    }
                    else
                    {
                        BarCode = "";
                        InvNum = "";
                        View.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }
    }
}
