﻿namespace SGMS
{
    partial class ViewS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewS));
            this.SDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.label14 = new System.Windows.Forms.Label();
            this.view = new Bunifu.Framework.UI.BunifuFlatButton();
            this.inv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.SDatagride)).BeginInit();
            this.SuspendLayout();
            // 
            // SDatagride
            // 
            this.SDatagride.AllowUserToAddRows = false;
            this.SDatagride.AllowUserToDeleteRows = false;
            this.SDatagride.AllowUserToResizeColumns = false;
            this.SDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.SDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SDatagride.BackgroundColor = System.Drawing.Color.White;
            this.SDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inv,
            this.ItemiName,
            this.Client,
            this.User,
            this.Date,
            this.tax});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SDatagride.DefaultCellStyle = dataGridViewCellStyle4;
            this.SDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SDatagride.DoubleBuffered = true;
            this.SDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.SDatagride.EnableHeadersVisualStyles = false;
            this.SDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.SDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.SDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.SDatagride.Location = new System.Drawing.Point(23, 35);
            this.SDatagride.MultiSelect = false;
            this.SDatagride.Name = "SDatagride";
            this.SDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.SDatagride.RowHeadersVisible = false;
            this.SDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SDatagride.Size = new System.Drawing.Size(756, 449);
            this.SDatagride.TabIndex = 9;
            this.SDatagride.SelectionChanged += new System.EventHandler(this.SDatagride_SelectionChanged);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(355, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 14);
            this.label14.TabIndex = 66;
            this.label14.Text = "سجل مبيعات هذا الشهر";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // view
            // 
            this.view.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view.BorderRadius = 0;
            this.view.ButtonText = "عرض الفاتورة";
            this.view.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view.DisabledColor = System.Drawing.Color.White;
            this.view.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Iconcolor = System.Drawing.Color.Transparent;
            this.view.Iconimage = null;
            this.view.Iconimage_right = null;
            this.view.Iconimage_right_Selected = null;
            this.view.Iconimage_Selected = null;
            this.view.IconMarginLeft = 0;
            this.view.IconMarginRight = 0;
            this.view.IconRightVisible = false;
            this.view.IconRightZoom = 0D;
            this.view.IconVisible = false;
            this.view.IconZoom = 20D;
            this.view.IsTab = false;
            this.view.Location = new System.Drawing.Point(23, 11);
            this.view.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.view.Name = "view";
            this.view.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.OnHoverTextColor = System.Drawing.Color.White;
            this.view.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.view.selected = false;
            this.view.Size = new System.Drawing.Size(98, 17);
            this.view.TabIndex = 71;
            this.view.Text = "عرض الفاتورة";
            this.view.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.view.Textcolor = System.Drawing.Color.White;
            this.view.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // inv
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.inv.DefaultCellStyle = dataGridViewCellStyle3;
            this.inv.FillWeight = 58.50328F;
            this.inv.HeaderText = "رقم الفاتورة";
            this.inv.Name = "inv";
            this.inv.ReadOnly = true;
            // 
            // ItemiName
            // 
            this.ItemiName.HeaderText = "إجمالي الفاتورة";
            this.ItemiName.Name = "ItemiName";
            this.ItemiName.ReadOnly = true;
            // 
            // Client
            // 
            this.Client.HeaderText = "إسم العميل";
            this.Client.Name = "Client";
            this.Client.ReadOnly = true;
            // 
            // User
            // 
            this.User.HeaderText = "إسم المستخدم";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.HeaderText = "التاريخ";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // tax
            // 
            this.tax.HeaderText = "tax";
            this.tax.Name = "tax";
            this.tax.ReadOnly = true;
            this.tax.Visible = false;
            // 
            // ViewS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 506);
            this.Controls.Add(this.view);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.SDatagride);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewS";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            ((System.ComponentModel.ISupportInitialize)(this.SDatagride)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCustomDataGrid SDatagride;
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuFlatButton view;
        private System.Windows.Forms.DataGridViewTextBoxColumn inv;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Client;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn tax;
    }
}