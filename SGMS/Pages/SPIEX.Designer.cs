﻿namespace SGMS
{
    partial class SPIEX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SPIEX));
            this.title = new System.Windows.Forms.Label();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.total = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.view = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dP2 = new System.Windows.Forms.DateTimePicker();
            this.dP1 = new System.Windows.Forms.DateTimePicker();
            this.PDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cash = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.print = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).BeginInit();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.title.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.title.Location = new System.Drawing.Point(328, 13);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(146, 14);
            this.title.TabIndex = 66;
            this.title.Text = "بحث محدد في المصروفات";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.print);
            this.metroPanel1.Controls.Add(this.total);
            this.metroPanel1.Controls.Add(this.label3);
            this.metroPanel1.Controls.Add(this.view);
            this.metroPanel1.Controls.Add(this.label2);
            this.metroPanel1.Controls.Add(this.label1);
            this.metroPanel1.Controls.Add(this.dP2);
            this.metroPanel1.Controls.Add(this.dP1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 35);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(756, 41);
            this.metroPanel1.TabIndex = 67;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // total
            // 
            this.total.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.total.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.total.Location = new System.Drawing.Point(31, 13);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(106, 14);
            this.total.TabIndex = 74;
            this.total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(140, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 14);
            this.label3.TabIndex = 73;
            this.label3.Text = "إجمالي مصروفات الفترة";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // view
            // 
            this.view.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view.BorderRadius = 3;
            this.view.ButtonText = "بحث";
            this.view.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view.DisabledColor = System.Drawing.Color.White;
            this.view.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Iconcolor = System.Drawing.Color.Transparent;
            this.view.Iconimage = null;
            this.view.Iconimage_right = null;
            this.view.Iconimage_right_Selected = null;
            this.view.Iconimage_Selected = null;
            this.view.IconMarginLeft = 0;
            this.view.IconMarginRight = 0;
            this.view.IconRightVisible = false;
            this.view.IconRightZoom = 0D;
            this.view.IconVisible = false;
            this.view.IconZoom = 20D;
            this.view.IsTab = false;
            this.view.Location = new System.Drawing.Point(340, 8);
            this.view.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.view.Name = "view";
            this.view.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.OnHoverTextColor = System.Drawing.Color.White;
            this.view.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.view.selected = false;
            this.view.Size = new System.Drawing.Size(64, 24);
            this.view.TabIndex = 72;
            this.view.Text = "بحث";
            this.view.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.view.Textcolor = System.Drawing.Color.White;
            this.view.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label2.Location = new System.Drawing.Point(550, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 68;
            this.label2.Text = "الي";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(710, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 14);
            this.label1.TabIndex = 67;
            this.label1.Text = "من";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dP2
            // 
            this.dP2.CustomFormat = "";
            this.dP2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dP2.Location = new System.Drawing.Point(433, 10);
            this.dP2.Name = "dP2";
            this.dP2.Size = new System.Drawing.Size(111, 20);
            this.dP2.TabIndex = 3;
            this.dP2.Value = new System.DateTime(2020, 6, 30, 0, 0, 0, 0);
            // 
            // dP1
            // 
            this.dP1.CustomFormat = "";
            this.dP1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dP1.Location = new System.Drawing.Point(597, 10);
            this.dP1.Name = "dP1";
            this.dP1.Size = new System.Drawing.Size(111, 20);
            this.dP1.TabIndex = 2;
            this.dP1.Value = new System.DateTime(2020, 6, 1, 0, 0, 0, 0);
            // 
            // PDatagride
            // 
            this.PDatagride.AllowUserToAddRows = false;
            this.PDatagride.AllowUserToDeleteRows = false;
            this.PDatagride.AllowUserToResizeColumns = false;
            this.PDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.PDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.PDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PDatagride.BackgroundColor = System.Drawing.Color.White;
            this.PDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.PDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.cash,
            this.Column2,
            this.reason,
            this.User,
            this.Date,
            this.time});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PDatagride.DefaultCellStyle = dataGridViewCellStyle3;
            this.PDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PDatagride.DoubleBuffered = true;
            this.PDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.PDatagride.EnableHeadersVisualStyles = false;
            this.PDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.PDatagride.Location = new System.Drawing.Point(23, 76);
            this.PDatagride.MultiSelect = false;
            this.PDatagride.Name = "PDatagride";
            this.PDatagride.ReadOnly = true;
            this.PDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.PDatagride.RowHeadersVisible = false;
            this.PDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PDatagride.Size = new System.Drawing.Size(756, 408);
            this.PDatagride.TabIndex = 68;
            this.PDatagride.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.PDatagride_RowsAdded);
            this.PDatagride.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.PDatagride_RowsRemoved);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "المستفيد";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // cash
            // 
            this.cash.FillWeight = 54.0521F;
            this.cash.HeaderText = "المبلغ";
            this.cash.Name = "cash";
            this.cash.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "المبلغ المصروف";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // reason
            // 
            this.reason.FillWeight = 210.5565F;
            this.reason.HeaderText = "السبب";
            this.reason.Name = "reason";
            this.reason.ReadOnly = true;
            // 
            // User
            // 
            this.User.FillWeight = 74.70072F;
            this.User.HeaderText = "منفذ العملية";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.FillWeight = 74.70072F;
            this.Date.HeaderText = "التاريخ";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // time
            // 
            this.time.FillWeight = 56.70084F;
            this.time.HeaderText = "الوقت";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Strikeout))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(141, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 14);
            this.label4.TabIndex = 75;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // print
            // 
            this.print.Activecolor = System.Drawing.Color.Green;
            this.print.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.print.BackColor = System.Drawing.Color.Green;
            this.print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.print.BorderRadius = 3;
            this.print.ButtonText = "طباعة";
            this.print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.print.DisabledColor = System.Drawing.Color.White;
            this.print.Enabled = false;
            this.print.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.print.Iconcolor = System.Drawing.Color.Transparent;
            this.print.Iconimage = null;
            this.print.Iconimage_right = null;
            this.print.Iconimage_right_Selected = null;
            this.print.Iconimage_Selected = null;
            this.print.IconMarginLeft = 0;
            this.print.IconMarginRight = 0;
            this.print.IconRightVisible = false;
            this.print.IconRightZoom = 0D;
            this.print.IconVisible = false;
            this.print.IconZoom = 20D;
            this.print.IsTab = false;
            this.print.Location = new System.Drawing.Point(269, 8);
            this.print.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.print.Name = "print";
            this.print.Normalcolor = System.Drawing.Color.Green;
            this.print.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.print.OnHoverTextColor = System.Drawing.Color.White;
            this.print.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.print.selected = false;
            this.print.Size = new System.Drawing.Size(64, 24);
            this.print.TabIndex = 75;
            this.print.Text = "طباعة";
            this.print.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.print.Textcolor = System.Drawing.Color.White;
            this.print.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // SPIEX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 506);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PDatagride);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.title);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SPIEX";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.metroPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label title;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private Bunifu.Framework.UI.BunifuCustomDataGrid PDatagride;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dP2;
        private System.Windows.Forms.DateTimePicker dP1;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuFlatButton view;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cash;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn reason;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuFlatButton print;
    }
}