﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class AddUsernEX : MetroFramework.Forms.MetroForm
    {
        int idd;
        double uc;
        public AddUsernEX()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            LoadLogin();
            LoginNames.SelectedIndex = -1;
        }
        private static List<User> UserP = new List<User>();
        private void LoadLogin()
        {
            try
            {
                UserP.Clear();
                Dictionary<string, object> dics = new Dictionary<string, object>();
                dics.Add("@BlockState", 1);
                var rIt = DBConnection.SqliteCommand("SELECT uid,fullname,Cash  FROM Login where BlockState =@BlockState", dics);
                if (rIt == null) return;
                if (rIt.Rows.Count > 0)
                {
                    LoginNames.DataSource = rIt;
                    LoginNames.DisplayMember = "fullname";
                    LoginNames.ValueMember = "uid";


                    for (int i = 0; i < rIt.Rows.Count; i++)
                    {
                        var _id = int.Parse(rIt.Rows[i]["uid"].ToString());
                        var _fullname = rIt.Rows[i]["fullname"].ToString();
                        var _Cash = Double.Parse(rIt.Rows[i]["Cash"].ToString());
                        var p = new User
                        {
                            uid = _id,
                            fullname = _fullname,
                            Cash = _Cash
                        };
                        UserP.Add(p);
                    }
                }
            }
            catch { }
           
        }
        private void add_Click(object sender, EventArgs e)
        {
            InsertEX();
        }
        private void InsertEX()
        {
            try
            {
                var nan = this.LoginNames.GetItemText(this.LoginNames.SelectedItem);
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@Reason", Reason.Text);
                addC.Add("@Cash", Cash.Text);
                addC.Add("@TCash", Cash.Text);
                addC.Add("@UserID", Login.cid);
                addC.Add("@UserName", Login.cnn);
                addC.Add("@r_Time", DateTime.Now.ToString("HH:mm:ss"));
                addC.Add("@r_Date", DateTime.Now.ToString("yyyy-MM-dd"));
                addC.Add("@LoginID", LoginNames.SelectedValue.ToString());
                addC.Add("@LoginName", nan);
                addC.Add("@r", 956135253);


                var addEX = DBConnection.SqliteCommand("INSERT  INTO `PExpenses`(`Reason`, `Cash`, `TCash`, `UserID`, `UserName`, `r_Time`, `r_Date`, `LoginID`, `LoginName`, `r`) " +
                    "values(@Reason,@Cash,@TCash,@UserID,@UserName,@r_Time,@r_Date,@LoginID,@LoginName,@r)", addC);

                UpdateLogin();
                cMessgeBox mess = new cMessgeBox("تمت إضافة المصروف الشخصي بنجاح", "done", "p", 1000);
                mess.ShowDialog();
                this.Close();
            }
            catch
            {
            }
        }
        private void UpdateLogin()
        {
            double newcash = uc + Double.Parse(Cash.Text);
            Dictionary<string, object> addC = new Dictionary<string, object>();
            addC.Add("@uid", idd);
            addC.Add("@Cash", newcash);
            var UpdateCu = DBConnection.SqliteCommand("update Login Set Cash = @Cash where uid=@uid", addC);
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
        private void Cash_TextChanged(object sender, EventArgs e)
        {
            if (Reason.TextLength > 0 && Cash.TextLength > 0 && LoginNames.SelectedIndex != -1)
            {
                add.Enabled = true;
            }
            else
            {
                add.Enabled = false;
            }
        }
        private void LoginNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                idd = int.Parse(LoginNames.SelectedValue.ToString());
                var nca = UserP.Find(i => i.uid == idd);
                uc = nca.Cash;
                uCash.Text = uc.ToString();


                if (Reason.TextLength > 0 && Cash.TextLength > 0 && LoginNames.SelectedIndex != -1)
                {
                    add.Enabled = true;
                }
                else
                {
                    add.Enabled = false;
                }
            }
            catch { }
        }
    }
}
