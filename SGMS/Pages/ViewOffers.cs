﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class ViewOffers : MetroFramework.Forms.MetroForm
    {
        int Num;

        public ViewOffers()
        {
            InitializeComponent();
            LoadP();
        }
        private void LoadP()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var result = DBConnection.SqliteCommand("Select * from OffersData", dic).ConvertOffersData();
                if (result == null) return;

                foreach (OffersData item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[0].Value = item.Num;
                    row.Cells[1].Value = item.CoName;
                    row.Cells[2].Value = item.OfferBy;
                    row.Cells[3].Value = item.cDate.ToString("yyyy-MM-dd");
                    PDatagride.Rows.Add(row);
                }
            }
            catch { }
        }
        private void PDatagride_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in PDatagride.SelectedRows)
                {
                    if (PDatagride.SelectedRows.Count > 0)
                    {
                        Num = int.Parse(row.Cells[0].Value.ToString());
                        view.Enabled = true;
                    }
                    else
                    {
                        Num = 0;
                        view.Enabled = false;
                    }
                }
            }
            catch
            {
            }
        }
        private void view_Click(object sender, EventArgs e)
        {
            if (Num > 0)
            {
                NewOffer viewinv = new NewOffer(Num);
                viewinv.ShowDialog();
            }
            else
            {
                cMessgeBox mess = new cMessgeBox("برجاء إختيار عرض سعر للعرض", "error", "p", 1500);
                mess.ShowDialog();
            }
        }
    }
}
