﻿namespace SGMS
{
    partial class TaxXinfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaxXinfo));
            this.label14 = new System.Windows.Forms.Label();
            this.view = new Bunifu.Framework.UI.BunifuFlatButton();
            this.CoNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TaxNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(428, 35);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(338, 29);
            this.label14.TabIndex = 66;
            this.label14.Text = "بيانات السجل التجاري والبطاقة الضريبية";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // view
            // 
            this.view.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.view.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.view.BorderRadius = 0;
            this.view.ButtonText = "حفظ";
            this.view.Cursor = System.Windows.Forms.Cursors.Hand;
            this.view.DisabledColor = System.Drawing.Color.White;
            this.view.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.view.Iconcolor = System.Drawing.Color.Transparent;
            this.view.Iconimage = null;
            this.view.Iconimage_right = null;
            this.view.Iconimage_right_Selected = null;
            this.view.Iconimage_Selected = null;
            this.view.IconMarginLeft = 0;
            this.view.IconMarginRight = 0;
            this.view.IconRightVisible = false;
            this.view.IconRightZoom = 0D;
            this.view.IconVisible = false;
            this.view.IconZoom = 20D;
            this.view.IsTab = false;
            this.view.Location = new System.Drawing.Point(31, 202);
            this.view.Margin = new System.Windows.Forms.Padding(5, 9, 5, 9);
            this.view.Name = "view";
            this.view.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.view.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(200)))));
            this.view.OnHoverTextColor = System.Drawing.Color.White;
            this.view.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.view.selected = false;
            this.view.Size = new System.Drawing.Size(176, 42);
            this.view.TabIndex = 70;
            this.view.Text = "حفظ";
            this.view.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.view.Textcolor = System.Drawing.Color.White;
            this.view.TextFont = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // CoNum
            // 
            this.CoNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.CoNum.Font = new System.Drawing.Font("Cairo", 12F);
            this.CoNum.Location = new System.Drawing.Point(413, 140);
            this.CoNum.Name = "CoNum";
            this.CoNum.Size = new System.Drawing.Size(359, 37);
            this.CoNum.TabIndex = 71;
            this.CoNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label1.Location = new System.Drawing.Point(638, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 22);
            this.label1.TabIndex = 72;
            this.label1.Text = "رقم السجل التجاري";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(256, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 22);
            this.label3.TabIndex = 79;
            this.label3.Text = "رقم البطاقة الضريبية";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TaxNum
            // 
            this.TaxNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.TaxNum.Font = new System.Drawing.Font("Cairo", 12F);
            this.TaxNum.Location = new System.Drawing.Point(31, 140);
            this.TaxNum.Name = "TaxNum";
            this.TaxNum.Size = new System.Drawing.Size(359, 37);
            this.TaxNum.TabIndex = 78;
            this.TaxNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TaxXinfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 276);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TaxNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CoNum);
            this.Controls.Add(this.view);
            this.Controls.Add(this.label14);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TaxXinfo";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label14;
        private Bunifu.Framework.UI.BunifuFlatButton view;
        private System.Windows.Forms.TextBox CoNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TaxNum;
    }
}