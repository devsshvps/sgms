﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;

namespace SGMS
{
    public partial class RandP : MetroFramework.Forms.MetroForm
    {
        public static string r1;
        public static string r2;
        public static string r3;
        public static string r4;
        public static string r5;
        public static string p1;
        public static string p2;
        public RandP()
        {
            InitializeComponent();
            iLoad();
        }

        public void iLoad()
        {
            Dictionary<string, object> di = new Dictionary<string, object>();
            var x2 = DBConnection.SqliteCommand("SELECT *  FROM Offers WHERE id = (SELECT MAX(id)  FROM Offers)", di);
            if (x2.Rows == null) return;
            if (x2.Rows.Count > 0)
            {
                foreach (DataRow item in x2.Rows)
                {
                    tr1.Text = item["r1"].ToString();
                    tr2.Text = item["r2"].ToString();
                    tr3.Text = item["r3"].ToString();
                    tr4.Text = item["r4"].ToString();
                    tr5.Text = item["r5"].ToString();
                    pr1.Text = item["p1"].ToString();
                    pr2.Text = item["p2"].ToString();
                }
            }
        }
        private void view_Click(object sender, EventArgs e)
        {
            r1 = tr1.Text;
            r2 = tr2.Text;
            r3 = tr3.Text;
            r4 = tr4.Text;
            r5 = tr5.Text;
            p1 = pr1.Text;
            p2 = pr2.Text;
            Close();
        }
    }
}
