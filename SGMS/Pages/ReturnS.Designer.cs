﻿namespace SGMS
{
    partial class ReturnS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReturnS));
            this.panel1 = new System.Windows.Forms.Panel();
            this.View = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label4 = new System.Windows.Forms.Label();
            this.invN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fYear = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fMonth = new System.Windows.Forms.ComboBox();
            this.itemsNames = new System.Windows.Forms.ComboBox();
            this.fDay = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.itemsBarcodes = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.PDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.inv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.panel1.Controls.Add(this.View);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.invN);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.fYear);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.fMonth);
            this.panel1.Controls.Add(this.itemsNames);
            this.panel1.Controls.Add(this.fDay);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.itemsBarcodes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(23, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(756, 100);
            this.panel1.TabIndex = 10;
            // 
            // View
            // 
            this.View.Activecolor = System.Drawing.SystemColors.ButtonHighlight;
            this.View.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.View.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.View.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.View.BorderRadius = 7;
            this.View.ButtonText = "إرتجاع المنتج";
            this.View.Cursor = System.Windows.Forms.Cursors.Hand;
            this.View.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.View.Enabled = false;
            this.View.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.View.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.View.Iconcolor = System.Drawing.Color.Transparent;
            this.View.Iconimage = null;
            this.View.Iconimage_right = null;
            this.View.Iconimage_right_Selected = null;
            this.View.Iconimage_Selected = null;
            this.View.IconMarginLeft = 0;
            this.View.IconMarginRight = 0;
            this.View.IconRightVisible = false;
            this.View.IconRightZoom = 0D;
            this.View.IconVisible = false;
            this.View.IconZoom = 20D;
            this.View.IsTab = false;
            this.View.Location = new System.Drawing.Point(30, 47);
            this.View.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.View.Name = "View";
            this.View.Normalcolor = System.Drawing.SystemColors.ButtonHighlight;
            this.View.OnHovercolor = System.Drawing.SystemColors.ControlLight;
            this.View.OnHoverTextColor = System.Drawing.Color.White;
            this.View.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.View.selected = false;
            this.View.Size = new System.Drawing.Size(82, 27);
            this.View.TabIndex = 74;
            this.View.Text = "إرتجاع المنتج";
            this.View.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.View.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.View.TextFont = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.View.Click += new System.EventHandler(this.View_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(146, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 14);
            this.label4.TabIndex = 73;
            this.label4.Text = "رقم فاتورة المبيعات";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // invN
            // 
            this.invN.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invN.Location = new System.Drawing.Point(127, 49);
            this.invN.Name = "invN";
            this.invN.Size = new System.Drawing.Size(117, 22);
            this.invN.TabIndex = 72;
            this.invN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.invN.TextChanged += new System.EventHandler(this.invN_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(604, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 14);
            this.label3.TabIndex = 71;
            this.label3.Text = "بحث عن تاريخ محدد";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fYear
            // 
            this.fYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fYear.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fYear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fYear.FormattingEnabled = true;
            this.fYear.Items.AddRange(new object[] {
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030",
            "2031",
            "2032",
            "2033",
            "2034",
            "2035",
            "2036",
            "2037",
            "2038",
            "2039",
            "2040"});
            this.fYear.Location = new System.Drawing.Point(538, 48);
            this.fYear.Name = "fYear";
            this.fYear.Size = new System.Drawing.Size(68, 24);
            this.fYear.TabIndex = 70;
            this.fYear.SelectedIndexChanged += new System.EventHandler(this.fDay_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(412, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 14);
            this.label2.TabIndex = 69;
            this.label2.Text = "بحث عن صنف بالإسم";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fMonth
            // 
            this.fMonth.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fMonth.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fMonth.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fMonth.FormattingEnabled = true;
            this.fMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.fMonth.Location = new System.Drawing.Point(612, 48);
            this.fMonth.Name = "fMonth";
            this.fMonth.Size = new System.Drawing.Size(66, 24);
            this.fMonth.TabIndex = 69;
            this.fMonth.SelectedIndexChanged += new System.EventHandler(this.fDay_SelectedIndexChanged);
            // 
            // itemsNames
            // 
            this.itemsNames.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.itemsNames.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.itemsNames.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemsNames.FormattingEnabled = true;
            this.itemsNames.Location = new System.Drawing.Point(378, 48);
            this.itemsNames.Name = "itemsNames";
            this.itemsNames.Size = new System.Drawing.Size(152, 24);
            this.itemsNames.TabIndex = 68;
            this.itemsNames.SelectedIndexChanged += new System.EventHandler(this.itemsNames_SelectedIndexChanged);
            // 
            // fDay
            // 
            this.fDay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.fDay.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fDay.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.fDay.FormattingEnabled = true;
            this.fDay.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.fDay.Location = new System.Drawing.Point(684, 48);
            this.fDay.Name = "fDay";
            this.fDay.Size = new System.Drawing.Size(48, 24);
            this.fDay.TabIndex = 68;
            this.fDay.SelectedIndexChanged += new System.EventHandler(this.fDay_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(253, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 14);
            this.label1.TabIndex = 68;
            this.label1.Text = "بحث بالباركود الخاص بالمنتج";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // itemsBarcodes
            // 
            this.itemsBarcodes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemsBarcodes.Location = new System.Drawing.Point(253, 49);
            this.itemsBarcodes.Name = "itemsBarcodes";
            this.itemsBarcodes.Size = new System.Drawing.Size(117, 22);
            this.itemsBarcodes.TabIndex = 3;
            this.itemsBarcodes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.itemsBarcodes.TextChanged += new System.EventHandler(this.itemsBarcodes_TextChanged);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label14.Location = new System.Drawing.Point(358, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 14);
            this.label14.TabIndex = 67;
            this.label14.Text = "إرتجاع منتجات مباعة";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PDatagride
            // 
            this.PDatagride.AllowUserToAddRows = false;
            this.PDatagride.AllowUserToDeleteRows = false;
            this.PDatagride.AllowUserToResizeColumns = false;
            this.PDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.PDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.PDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PDatagride.BackgroundColor = System.Drawing.Color.White;
            this.PDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.PDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inv,
            this.ItemiName,
            this.Client,
            this.User,
            this.Date,
            this.Column1,
            this.Column3,
            this.Column2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PDatagride.DefaultCellStyle = dataGridViewCellStyle4;
            this.PDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PDatagride.DoubleBuffered = true;
            this.PDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.PDatagride.EnableHeadersVisualStyles = false;
            this.PDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.PDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.PDatagride.Location = new System.Drawing.Point(23, 135);
            this.PDatagride.MultiSelect = false;
            this.PDatagride.Name = "PDatagride";
            this.PDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.PDatagride.RowHeadersVisible = false;
            this.PDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PDatagride.Size = new System.Drawing.Size(756, 349);
            this.PDatagride.TabIndex = 9;
            this.PDatagride.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.PDatagride_RowsAdded);
            this.PDatagride.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.PDatagride_RowsRemoved);
            this.PDatagride.SelectionChanged += new System.EventHandler(this.PDatagride_SelectionChanged);
            // 
            // inv
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.inv.DefaultCellStyle = dataGridViewCellStyle3;
            this.inv.FillWeight = 58.50328F;
            this.inv.HeaderText = "فاتورة";
            this.inv.Name = "inv";
            this.inv.ReadOnly = true;
            this.inv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ItemiName
            // 
            this.ItemiName.HeaderText = "باركود الصنف";
            this.ItemiName.Name = "ItemiName";
            this.ItemiName.ReadOnly = true;
            this.ItemiName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Client
            // 
            this.Client.HeaderText = "إسم الصنف";
            this.Client.Name = "Client";
            this.Client.ReadOnly = true;
            this.Client.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // User
            // 
            this.User.HeaderText = "الكمية";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            this.User.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Date
            // 
            this.Date.HeaderText = "سعر الشراء";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "إسم العميل";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "وقت الشراء";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "تاريخ الشراء";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ReturnS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 506);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.PDatagride);
            this.Controls.Add(this.panel1);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReturnS";
            this.Padding = new System.Windows.Forms.Padding(23, 35, 23, 22);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PDatagride)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox itemsBarcodes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox itemsNames;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox fYear;
        private System.Windows.Forms.ComboBox fMonth;
        private System.Windows.Forms.ComboBox fDay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox invN;
        private Bunifu.Framework.UI.BunifuFlatButton View;
        private Bunifu.Framework.UI.BunifuCustomDataGrid PDatagride;
        private System.Windows.Forms.DataGridViewTextBoxColumn inv;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemiName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Client;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}