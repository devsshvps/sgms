﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class PayPE : MetroFramework.Forms.MetroForm
    {
        int cid, pid;
        double ucash , unpCash;
        public PayPE(int uid , int peID)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            cid = uid;
            pid = peID;
            loadOldCash();
            LoadPay();
        }
        private void loadOldCash()
        {
            try
            {

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", cid);
                var AC = DBConnection.SqliteCommand("SELECT Cash FROM Login where uid = @id", dic);
                if (AC == null) return;
                if (AC.Rows.Count > 0)
                {
                    ucash = Double.Parse(AC.Rows[0]["Cash"].ToString());
                }
            }
            catch { }
        }
        private void LoadPay()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", pid);
                dic.Add("@LoginID", cid);
                var luser = DBConnection.SqliteCommand("SELECT * FROM PExpenses where id=@id AND LoginID = @LoginID", dic).ConvertExpenses();
                if (luser == null) return;

                foreach (Expense item in luser)
                {
                    invClient.Text = item.LoginName;
                    Aunpaid.Text = item.Cash.ToString();
                    unpCash = item.Cash;
                    NeedPay.Text = item.Cash.ToString();
                }
            }

            catch
            {
            }
        }
        private void AddIn_Click(object sender, EventArgs e)
        {
            double cin = Double.Parse(NeedPay.Text);
            if (cin > 0)
            {
                NeedPay.Enabled = false;

                updateClient();
                updatePE();
                cMessgeBox box = new cMessgeBox("تمت عملية دفع ديون المستخدم بنجاح", "done", "p", 1000);
                box.ShowDialog();
                this.Close();
            }
            else
            {
                cMessgeBox box = new cMessgeBox("فشلت العملية لعدم تحديد مبلغ", "error", "p", 3000);
                box.ShowDialog();
            }

        }
        private void Calculate()
        {
            try
            {
                double needpay = Double.Parse(NeedPay.Text.ToString());
                double unpa = unpCash - needpay;
                unPaid.Text = unpa.ToString();
            }
            catch
            {

            }
        }
        private void NeedPay_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double needpay = 0;

                if (!string.IsNullOrWhiteSpace(NeedPay.Text))
                {
                    needpay = Double.Parse(NeedPay.Text.ToString());
                    if (needpay > unpCash)
                    {
                        MessageBox.Show("المبلغ المدفوع أكبر من مصروف المستخدم");
                        unPaid.Text = unpCash.ToString();
                        NeedPay.Text = unpCash.ToString();

                    }
                    else
                    {
                        AddIn.Enabled = true;
                        Calculate();
                    }
                }
                else
                {
                    unPaid.Text = unpCash.ToString();
                    AddIn.Enabled = false;
                }
            }
            catch
            {
            }
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
        private void updateClient()
        {
            try
            {
                double needpay = Double.Parse(NeedPay.Text.ToString());
                double ncash = ucash - needpay;
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@uid", cid);
                addC.Add("@Cash", ncash);
                var UpdateCu = DBConnection.SqliteCommand("update Login Set Cash = @Cash where uid=@uid", addC);
            }
            catch
            {
            }
        }
        private void updatePE()
        {
            try
            {
                double needpay = Double.Parse(NeedPay.Text.ToString());
                double ncash = unpCash - needpay;
                if (ncash == 0)
                {
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@id", pid);
                    addC.Add("@Cash", ncash);
                    addC.Add("@LoginID", cid);
                    addC.Add("@r", 956135251);
                    var UpdateCu = DBConnection.SqliteCommand("update PExpenses Set Cash = @Cash, r = @r where id=@id AND LoginID = @LoginID", addC);
                }
                else
                {
                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@id", pid);
                    addC.Add("@Cash", ncash);
                    addC.Add("@LoginID", cid);
                    var UpdateCu = DBConnection.SqliteCommand("update PExpenses Set Cash = @Cash where id=@id AND LoginID = @LoginID", addC);
                }
            }
            catch
            {
            }
        }
    }
}

