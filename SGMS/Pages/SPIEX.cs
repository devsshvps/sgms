﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SGMS.Models;
using System.Drawing;
using SGMS.Print;

namespace SGMS
{
    public partial class SPIEX : MetroFramework.Forms.MetroForm
    {

        double v = 0;
        public SPIEX()
        {
            InitializeComponent();
        }
        private void LoadV()
        {
            PDatagride.Rows.Clear();
            PDatagride.Refresh();
            try
            {
                string f = dP1.Value.ToString("yyyy-MM-dd");
                string t = dP2.Value.ToString("yyyy-MM-dd");
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var result = DBConnection.SqliteCommand("Select * from PExpenses where r_Date between '" + f + "' and '" + t + "'", dic).ConvertExpenses();
                if (result == null) return;

                foreach (Expense item in result)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(PDatagride);
                    row.Cells[1].Value = Login.arNumber(item.Cash.ToString());
                    row.Cells[3].Value = item.Reason;
                    row.Cells[4].Value = item.UserName;
                    row.Cells[5].Value = Login.arNumber(item.r_Date.ToString("dd")) + " " +
                        Login.arDate(item.r_Date.ToString("MM")) + " " +
                        Login.arNumber(item.r_Date.ToString("yyyy"));
                    row.Cells[6].Value = Login.arNumber(item.r_Time.ToString("t"));
                    if (item.r == 956135252)
                    {
                        row.Cells[0].Value = "مصروف إداري";
                        row.Cells[2].Value = "-";
                    }
                    else
                    {
                        row.Cells[0].Value = item.LoginName;
                        row.Cells[2].Value = Login.arNumber(item.TCash.ToString());

                        if (item.r == 956135251)
                        {
                            row.DefaultCellStyle.Font = label4.Font;
                            row.DefaultCellStyle.ForeColor = label4.ForeColor;
                        }
                    }

                    PDatagride.Rows.Add(row);
                }
                Sum();
            }
            catch { }
        }

        private void view_Click(object sender, EventArgs e)
        {
            LoadV();
        }

        private void Sum()
        {
            v = 0;
            try
            {
                foreach (DataGridViewRow item in PDatagride.Rows)
                {
                    int n = item.Index;
                    v = v + Double.Parse(arNumber(PDatagride.Rows[n].Cells[1].Value.ToString()));
                }

                total.Text = Login.arNumber(v.ToString());
            }
            catch(Exception ex) { MessageBox.Show(ex.ToString()); }
        }
        private string arNumber(string x)
        {
            string text = x.Replace("٠", "0");
            text = text.Replace("١", "1");
            text = text.Replace("٢", "2");
            text = text.Replace("٣", "3");
            text = text.Replace("٤", "4");
            text = text.Replace("٥", "5");
            text = text.Replace("٦", "6");
            text = text.Replace("٧", "7");
            text = text.Replace("٨", "8");
            return text.Replace("٩", "9");
        }
        private void PDatagride_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if(PDatagride.Rows.Count > 0)
            {
                print.Enabled = true;
            }
            else
            {
                print.Enabled = false;
            }
        }
        private void PDatagride_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (PDatagride.Rows.Count > 0)
            {
                print.Enabled = true;
            }
            else
            {
                print.Enabled = false;
            }
        }

        private void print_Click(object sender, EventArgs e)
        {
            //
            var fy = dP1.Value.ToString("yyyy");
            var fm = dP1.Value.ToString("MM");
            var fd = dP1.Value.ToString("dd");
            var ty = dP2.Value.ToString("yyyy");
            var tm = dP2.Value.ToString("MM");
            var td = dP2.Value.ToString("dd");
            //
            var fr = dP1.Value.ToString("yyyy-MM-dd");
            var tor = dP2.Value.ToString("yyyy-MM-dd");
            var f1 = "الفترة من" + " " 
                + Login.arNumber(fd) + " "
                + Login.arDate(fm) + " "
                + Login.arNumber(fy) + " "
                + "حتي" + " "
                 + Login.arNumber(td) + " "
                + Login.arDate(tm) + " "
                + Login.arNumber(ty);
            Dictionary<string, object> dic = new Dictionary<string, object>();
            var df = fr;
            var dt = tor;

            var result = DBConnection.SqliteCommand("Select * from PExpenses where r_Date between '" + df + "' and '" + dt + "'", dic);
            if (result == null) return;
            if (result.Rows.Count > 0)
            {
                Print2PE printPurch = new Print2PE();
                printPurch.SetDataSource(result);
                printPurch.SetParameterValue("dFrom", f1);
                printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                printPurch.SetParameterValue("name", Dashboard.invCoN);
                printPurch.SetParameterValue("slug", Dashboard.invCoS);
                printPurch.PrintToPrinter(1, false, 0, 0);
              cMessgeBox cmbox = new cMessgeBox("تم طباعة التقرير بنجاح", "done", "p", 1000);
                cmbox.ShowDialog();
            }
        }
    }
}
