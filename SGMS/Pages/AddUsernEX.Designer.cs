﻿namespace SGMS
{
    partial class AddUsernEX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.add = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Reason = new System.Windows.Forms.TextBox();
            this.Cash = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LoginNames = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uCash = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.add.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.add.BorderRadius = 0;
            this.add.ButtonText = "إضافة المصروف";
            this.add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add.DisabledColor = System.Drawing.Color.Gray;
            this.add.Enabled = false;
            this.add.Font = new System.Drawing.Font("Cairo", 8F);
            this.add.Iconcolor = System.Drawing.Color.Transparent;
            this.add.Iconimage = null;
            this.add.Iconimage_right = null;
            this.add.Iconimage_right_Selected = null;
            this.add.Iconimage_Selected = null;
            this.add.IconMarginLeft = 0;
            this.add.IconMarginRight = 0;
            this.add.IconRightVisible = false;
            this.add.IconRightZoom = 0D;
            this.add.IconVisible = false;
            this.add.IconZoom = 20D;
            this.add.IsTab = false;
            this.add.Location = new System.Drawing.Point(55, 191);
            this.add.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.add.Name = "add";
            this.add.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.add.OnHovercolor = System.Drawing.Color.Blue;
            this.add.OnHoverTextColor = System.Drawing.Color.White;
            this.add.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.add.selected = false;
            this.add.Size = new System.Drawing.Size(127, 33);
            this.add.TabIndex = 43;
            this.add.Text = "إضافة المصروف";
            this.add.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.add.Textcolor = System.Drawing.Color.White;
            this.add.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // Reason
            // 
            this.Reason.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Reason.Font = new System.Drawing.Font("Cairo", 12F);
            this.Reason.Location = new System.Drawing.Point(55, 118);
            this.Reason.Name = "Reason";
            this.Reason.Size = new System.Drawing.Size(589, 37);
            this.Reason.TabIndex = 46;
            this.Reason.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Reason.TextChanged += new System.EventHandler(this.Cash_TextChanged);
            // 
            // Cash
            // 
            this.Cash.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Cash.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cash.Location = new System.Drawing.Point(210, 189);
            this.Cash.Name = "Cash";
            this.Cash.Size = new System.Drawing.Size(86, 37);
            this.Cash.TabIndex = 45;
            this.Cash.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Cash.TextChanged += new System.EventHandler(this.Cash_TextChanged);
            this.Cash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlynumwithsinglepoint);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label1.Location = new System.Drawing.Point(571, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 19);
            this.label1.TabIndex = 50;
            this.label1.Text = "سبب المصروف";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label2.Location = new System.Drawing.Point(302, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 19);
            this.label2.TabIndex = 51;
            this.label2.Text = "المبلغ";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 25F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label3.Location = new System.Drawing.Point(186, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(326, 63);
            this.label3.TabIndex = 53;
            this.label3.Text = "مصروف شخصي جديد";
            // 
            // LoginNames
            // 
            this.LoginNames.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.LoginNames.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.LoginNames.Font = new System.Drawing.Font("Cairo", 12F);
            this.LoginNames.FormattingEnabled = true;
            this.LoginNames.Location = new System.Drawing.Point(377, 188);
            this.LoginNames.Name = "LoginNames";
            this.LoginNames.Size = new System.Drawing.Size(267, 38);
            this.LoginNames.TabIndex = 54;
            this.LoginNames.SelectedIndexChanged += new System.EventHandler(this.LoginNames_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 7F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label4.Location = new System.Drawing.Point(593, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 19);
            this.label4.TabIndex = 55;
            this.label4.Text = "المستفيد";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cairo", 7F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label5.Location = new System.Drawing.Point(521, 233);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 19);
            this.label5.TabIndex = 56;
            this.label5.Text = "ديون المستفيد الحالية";
            // 
            // uCash
            // 
            this.uCash.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.uCash.Font = new System.Drawing.Font("Cairo", 7F);
            this.uCash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.uCash.Location = new System.Drawing.Point(446, 233);
            this.uCash.Name = "uCash";
            this.uCash.Size = new System.Drawing.Size(71, 19);
            this.uCash.TabIndex = 56;
            this.uCash.Text = "0";
            this.uCash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AddUsernEX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 283);
            this.Controls.Add(this.uCash);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LoginNames);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.add);
            this.Controls.Add(this.Reason);
            this.Controls.Add(this.Cash);
            this.Name = "AddUsernEX";
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuFlatButton add;
        private System.Windows.Forms.TextBox Reason;
        private System.Windows.Forms.TextBox Cash;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox LoginNames;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label uCash;
    }
}