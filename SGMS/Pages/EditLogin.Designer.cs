﻿namespace SGMS
{
    partial class EditLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("الرئيسية");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("عروض الأسعار");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("المبيعات");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("فاتورة جديدة");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("تعديل سعر البيع");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("إرتجاع منتج");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("سجل المبيعات");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("الغير محصل");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("المشتريات");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("سجل المشتريات");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("المؤجلات");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("المرتجعات التالفة");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("تقارير الأصناف");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("تقارير تعديل العملاء");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("تقارير تعديل الموردين");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("المنتجات");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("قائمة العملاء");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("قائمة الموردين");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("حركة الخزنة");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("المصاريف");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("الشركاء");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("الإعدادات");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("المستخدمين");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("الملاحظات الإدارية");
            this.add = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Fullname = new System.Windows.Forms.TextBox();
            this.Username = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.unBlock = new Bunifu.Framework.UI.BunifuFlatButton();
            this.treeView1 = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.add.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.add.BorderRadius = 7;
            this.add.ButtonText = "تعديل المستخدم";
            this.add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add.DisabledColor = System.Drawing.Color.Gray;
            this.add.Font = new System.Drawing.Font("Cairo", 8F);
            this.add.Iconcolor = System.Drawing.Color.Transparent;
            this.add.Iconimage = null;
            this.add.Iconimage_right = null;
            this.add.Iconimage_right_Selected = null;
            this.add.Iconimage_Selected = null;
            this.add.IconMarginLeft = 0;
            this.add.IconMarginRight = 0;
            this.add.IconRightVisible = false;
            this.add.IconRightZoom = 0D;
            this.add.IconVisible = false;
            this.add.IconZoom = 20D;
            this.add.IsTab = false;
            this.add.Location = new System.Drawing.Point(347, 374);
            this.add.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.add.Name = "add";
            this.add.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.add.OnHovercolor = System.Drawing.Color.Blue;
            this.add.OnHoverTextColor = System.Drawing.Color.White;
            this.add.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.add.selected = false;
            this.add.Size = new System.Drawing.Size(127, 33);
            this.add.TabIndex = 43;
            this.add.Text = "تعديل المستخدم";
            this.add.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.add.Textcolor = System.Drawing.Color.White;
            this.add.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // Fullname
            // 
            this.Fullname.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Fullname.Enabled = false;
            this.Fullname.Font = new System.Drawing.Font("Cairo", 12F);
            this.Fullname.Location = new System.Drawing.Point(347, 183);
            this.Fullname.Name = "Fullname";
            this.Fullname.Size = new System.Drawing.Size(184, 37);
            this.Fullname.TabIndex = 46;
            this.Fullname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Username
            // 
            this.Username.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Username.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Username.Location = new System.Drawing.Point(347, 245);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(184, 37);
            this.Username.TabIndex = 45;
            this.Username.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label1.Location = new System.Drawing.Point(545, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 19);
            this.label1.TabIndex = 50;
            this.label1.Text = "الإسم الحقيقي";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label2.Location = new System.Drawing.Point(546, 254);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 19);
            this.label2.TabIndex = 51;
            this.label2.Text = "إسم المستخدم";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SGMS.Properties.Resources.editC;
            this.pictureBox1.Location = new System.Drawing.Point(558, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(102, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 25F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label3.Location = new System.Drawing.Point(292, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(260, 63);
            this.label3.TabIndex = 53;
            this.label3.Text = "تعديل المستخدم";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cairo", 7F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label4.Location = new System.Drawing.Point(558, 316);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 19);
            this.label4.TabIndex = 55;
            this.label4.Text = "كلمة المرور";
            // 
            // Password
            // 
            this.Password.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Password.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password.Location = new System.Drawing.Point(347, 307);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(184, 37);
            this.Password.TabIndex = 54;
            this.Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Password.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cairo", 7F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label5.Location = new System.Drawing.Point(109, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 19);
            this.label5.TabIndex = 57;
            this.label5.Text = "صلاحيات المستخدم";
            // 
            // unBlock
            // 
            this.unBlock.Activecolor = System.Drawing.Color.Green;
            this.unBlock.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.unBlock.BackColor = System.Drawing.Color.Green;
            this.unBlock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.unBlock.BorderRadius = 7;
            this.unBlock.ButtonText = "الغاء الحظر";
            this.unBlock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.unBlock.DisabledColor = System.Drawing.Color.Gray;
            this.unBlock.Font = new System.Drawing.Font("Cairo", 8F);
            this.unBlock.Iconcolor = System.Drawing.Color.Transparent;
            this.unBlock.Iconimage = null;
            this.unBlock.Iconimage_right = null;
            this.unBlock.Iconimage_right_Selected = null;
            this.unBlock.Iconimage_Selected = null;
            this.unBlock.IconMarginLeft = 0;
            this.unBlock.IconMarginRight = 0;
            this.unBlock.IconRightVisible = false;
            this.unBlock.IconRightZoom = 0D;
            this.unBlock.IconVisible = false;
            this.unBlock.IconZoom = 20D;
            this.unBlock.IsTab = false;
            this.unBlock.Location = new System.Drawing.Point(486, 374);
            this.unBlock.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.unBlock.Name = "unBlock";
            this.unBlock.Normalcolor = System.Drawing.Color.Green;
            this.unBlock.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.unBlock.OnHoverTextColor = System.Drawing.Color.White;
            this.unBlock.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.unBlock.selected = false;
            this.unBlock.Size = new System.Drawing.Size(127, 33);
            this.unBlock.TabIndex = 58;
            this.unBlock.Text = "الغاء الحظر";
            this.unBlock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.unBlock.Textcolor = System.Drawing.Color.White;
            this.unBlock.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unBlock.Visible = false;
            this.unBlock.Click += new System.EventHandler(this.unBlock_Click);
            // 
            // treeView1
            // 
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.CheckBoxes = true;
            this.treeView1.Location = new System.Drawing.Point(23, 173);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "s1";
            treeNode1.Text = "الرئيسية";
            treeNode2.Name = "s24";
            treeNode2.Text = "عروض الأسعار";
            treeNode3.Name = "s2";
            treeNode3.Text = "المبيعات";
            treeNode4.Name = "s3";
            treeNode4.Text = "فاتورة جديدة";
            treeNode5.Name = "s4";
            treeNode5.Text = "تعديل سعر البيع";
            treeNode6.Name = "s5";
            treeNode6.Text = "إرتجاع منتج";
            treeNode7.Name = "s6";
            treeNode7.Text = "سجل المبيعات";
            treeNode8.Name = "s7";
            treeNode8.Text = "الغير محصل";
            treeNode9.Name = "s8";
            treeNode9.Text = "المشتريات";
            treeNode10.Name = "s9";
            treeNode10.Text = "سجل المشتريات";
            treeNode11.Name = "s10";
            treeNode11.Text = "المؤجلات";
            treeNode12.Name = "s11";
            treeNode12.Text = "المرتجعات التالفة";
            treeNode13.Name = "s12";
            treeNode13.Text = "تقارير الأصناف";
            treeNode14.Name = "s13";
            treeNode14.Text = "تقارير تعديل العملاء";
            treeNode15.Name = "s14";
            treeNode15.Text = "تقارير تعديل الموردين";
            treeNode16.Name = "s15";
            treeNode16.Text = "المنتجات";
            treeNode17.Name = "s16";
            treeNode17.Text = "قائمة العملاء";
            treeNode18.Name = "s17";
            treeNode18.Text = "قائمة الموردين";
            treeNode19.Name = "s18";
            treeNode19.Text = "حركة الخزنة";
            treeNode20.Name = "s19";
            treeNode20.Text = "المصاريف";
            treeNode21.Name = "s20";
            treeNode21.Text = "الشركاء";
            treeNode22.Name = "s21";
            treeNode22.Text = "الإعدادات";
            treeNode23.Name = "s22";
            treeNode23.Text = "المستخدمين";
            treeNode24.Name = "s23";
            treeNode24.Text = "الملاحظات الإدارية";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17,
            treeNode18,
            treeNode19,
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24});
            this.treeView1.RightToLeftLayout = true;
            this.treeView1.Size = new System.Drawing.Size(258, 263);
            this.treeView1.TabIndex = 59;
            // 
            // EditLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 459);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.unBlock);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.add);
            this.Controls.Add(this.Fullname);
            this.Controls.Add(this.Username);
            this.Name = "EditLogin";
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuFlatButton add;
        private System.Windows.Forms.TextBox Fullname;
        private System.Windows.Forms.TextBox Username;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuFlatButton unBlock;
        private System.Windows.Forms.TreeView treeView1;
    }
}