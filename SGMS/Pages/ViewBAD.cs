﻿using SGMS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using SGMS.BarCodeGE;
using SGMS.Models;
using SGMS.Print;

namespace SGMS
{
    public partial class ViewBAD : MetroFramework.Forms.MetroForm
    {

        public ViewBAD()
        {
            InitializeComponent();
            LoadV();
        }
        private void LoadV()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            var result = DBConnection.SqliteCommand("Select * from BadItems", dic).ConvertBadItems();
            if (result == null) return;

            foreach (BadItems item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(PDatagride);
                row.Cells[0].Value = item.BarCode;
                row.Cells[1].Value = item.ItemName;
                row.Cells[2].Value = item.Count;
                row.Cells[3].Value = Login.arNumber(item.LastReDate.ToString("dd")) + " " +
                    Login.arDate(item.LastReDate.ToString("MM")) + " " +
                    Login.arNumber(item.LastReDate.ToString("yyyy"));
                PDatagride.Rows.Add(row);
            }
        }
    }
}
