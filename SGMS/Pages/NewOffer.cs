﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SGMS
{

    public partial class NewOffer : MetroFramework.Forms.MetroForm
    {
        int NuM, rn;
        string r1;
        string r2;
        string r3;
        string r4;
        string r5;
        string p1;
        string p2;
        private static List<Item> ItemP = new List<Item>();
        public NewOffer(int nu)
        {
            InitializeComponent();
            if (nu == 0)
            {
                rn = 0;
                InvoiceNum();
                FillItems();
            }
            else
            {
                NuM = nu;
                rn = nu;
                label1.Text = "عرض لعرض سعر قديم";
                label3.Text = "إسم مقدم العرض";
                LoadOld();
            }

        }
        private void InvoiceNum()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                var check = DBConnection.SqliteCommand("SELECT MAX(Num) FROM Offers", dic);
                if (check == null) return;
                if (check.Rows.Count > 0)
                {
                    NuM = int.Parse(check.Rows[0]["MAX(Num)"].ToString());
                }

                NuM += 1;
                rn = NuM;

            }
            catch { }
        }
        private void FillItems()
        {
            ItemP.Clear();
            Dictionary<string, object> dics = new Dictionary<string, object>();
            dics.Add("@r", 1);
            var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where r =@r", dics);
            if (rIt == null) return;
            if (rIt.Rows.Count > 0)
            {
                itemsNames.DataSource = rIt;
                itemsNames.DisplayMember = "Name";
                itemsNames.ValueMember = "id";
                itemsNames.SelectedIndex = -1;

                for (int i = 0; i < rIt.Rows.Count; i++)
                {
                    var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                    var _Barcode = rIt.Rows[i]["BarCode"].ToString();
                    var _name = rIt.Rows[i]["Name"].ToString();
                    var _price = rIt.Rows[i]["SellPrice"].ToString();
                    var _count = rIt.Rows[i]["Count"].ToString();
                    var _TB = rIt.Rows[i]["TotalBuy"].ToString();
                    var p = new Item
                    {
                        id = _id,
                        BarCode = _Barcode,
                        Name = _name,
                        Count = Double.Parse(_count),
                        TotalBuy = Double.Parse(_TB),
                        ItemPrice = Double.Parse(_price)

                    };
                    ItemP.Add(p);
                }
            }


        }
        private void itemsNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var name = itemsNames.Text;
                var iP = ItemP.Find(P => P.Name == name);
                Sprice.Text = "";
                if (iP == null)
                {
                    Sprice.Text = "";
                }
                else
                {
                    Sprice.Text = iP.ItemPrice.ToString();
                }
            }
            catch { }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Sprice.Text))
            {
                MessageBox.Show("برجاء كتابة سعر المنتج");
            }
            else if (string.IsNullOrWhiteSpace(iCount.Text))
            {
                MessageBox.Show("برجاء كتابة كمية المنتج");
            }
            else if (string.IsNullOrWhiteSpace(itemsNames.Text))
            {
                MessageBox.Show("برجاء كتابة إسم المنتج");
            }
            else
            {
                ///
                bool Found = false;
                if (dataGridView1.Rows.Count > 0)
                {
                    foreach (DataGridViewRow drow in dataGridView1.Rows)
                    {
                        if (Convert.ToString(drow.Cells[0].Value) == itemsNames.Text)
                        {
                            drow.Cells[1].Value = Convert.ToString(Convert.ToInt16(iCount.Text) + Convert.ToInt16(drow.Cells[1].Value));
                            Found = true;
                            sumMethod();
                            iCount.Text = "1";
                            itemsNames.SelectedIndex = -1;
                            itemsNames.Text = "";
                            Sprice.Text = "";
                            iNote.Text = "";
                        }
                    }
                    if (!Found)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView1);
                        row.Cells[0].Value = itemsNames.Text;
                        row.Cells[1].Value = iCount.Text;
                        row.Cells[2].Value = Sprice.Text;
                        if (string.IsNullOrWhiteSpace(iNote.Text))
                        {
                            row.Cells[4].Value = "-";
                        }
                        else
                        {
                            row.Cells[4].Value = iNote.Text;
                        }
                        dataGridView1.Rows.Add(row);
                        dataGridView1.CurrentCell = null;
                        sumMethod();
                        iCount.Text = "1";
                        itemsNames.SelectedIndex = -1;
                        itemsNames.Text = "";
                        Sprice.Text = "";
                        iNote.Text = "";
                    }
                }
                else
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = itemsNames.Text;
                    row.Cells[1].Value = iCount.Text;
                    row.Cells[2].Value = Sprice.Text;
                    if (string.IsNullOrWhiteSpace(iNote.Text))
                    {
                        row.Cells[4].Value = "-";
                    }
                    else
                    {
                        row.Cells[4].Value = iNote.Text;
                    }
                    dataGridView1.Rows.Add(row);
                    dataGridView1.CurrentCell = null;
                    sumMethod();
                    iCount.Text = "1";
                    itemsNames.SelectedIndex = -1;
                    itemsNames.Text = "";
                    Sprice.Text = "";
                    iNote.Text = "";
                }

                ///
            }
        }
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            sumMethod();
            AddIn.Enabled = true;
        }
        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dataGridView1.Rows.Count >= 1)
            {
                sumMethod();
                AddIn.Enabled = true;
            }
            else
            {
                sumMethod();
                AddIn.Enabled = false;
            }
        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //
            int colIndex;
            int rowIndex;
            var txtBox = e.Control as TextBox;
            colIndex = dataGridView1.CurrentCell.ColumnIndex;
            rowIndex = dataGridView1.CurrentCell.RowIndex;
            if (e.Control is TextBox)
            {
                if (txtBox != null)
                {
                    txtBox.TextChanged += new EventHandler(ItemTxtBox_TextChanged);;
                }
            }
            //



            if (e.Control is DataGridViewTextBoxEditingControl tb)
            {
                tb.KeyDown -= dataGridView1_KeyDown;
                tb.KeyDown += dataGridView1_KeyDown;
            }
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (rn == 0)
            {
                if (e.KeyData == (Keys.Delete))
                {
                    if (dataGridView1.SelectedRows.Count >= 1)
                    {
                        foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                        {
                            dataGridView1.Rows.RemoveAt(row.Index);
                        }
                    }
                    else
                    {
                        MessageBox.Show("برجاء تحديد عنصر للحذف");
                    }
                }
            }
        }
        private void sumMethod()
        {
            try
            {
                if(rn != 0)
                {
                    foreach (DataGridViewRow ll in dataGridView1.Rows)
                    {
                        int TP = ll.Index;
                        double co = Convert.ToDouble(dataGridView1.Rows[TP].Cells[1].Value);
                        double pr = Convert.ToDouble(dataGridView1.Rows[TP].Cells[2].Value);
                        double tospr = co * pr;
                        dataGridView1.Rows[TP].Cells[3].Value = tospr.ToString();

                    }
                }

                Total.Text = "0";
                foreach (DataGridViewRow item in dataGridView1.Rows)
                {
                    int n = item.Index;
                    var finalsum = (Double.Parse(Total.Text.ToString())
                    + Double.Parse(dataGridView1.Rows[n].Cells[3].Value.ToString())).ToString();
                    Total.Text = finalsum;
                }
            }
            catch { }

        }
        private void AddIn_Click(object sender, EventArgs e)
        {
            if (AddIn.Text == "طباعة")
            {
                PrintOffer();
            }
            else
            {
                if (string.IsNullOrWhiteSpace(CoName.Text))
                {
                    MessageBox.Show("برجاء كتابة اسم الشركة المقدم اليها عرض السعر");
                }
                else
                {
                    randpFind();
                    InsertOffer();
                    PrintOffer();
                    this.Close();
                }

            }
        }
        private void InsertOffer()
        {
            try
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    var iiName = dataGridView1.Rows[i].Cells["ItemName"].Value.ToString();
                    var iiCount = dataGridView1.Rows[i].Cells["count"].Value.ToString();
                    var iiPrice = dataGridView1.Rows[i].Cells["Price"].Value.ToString();
                    var iiTotal = dataGridView1.Rows[i].Cells["TotalPrice"].Value.ToString();
                    var iiNote = dataGridView1.Rows[i].Cells["Note"].Value.ToString();


                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@Num", NuM);
                    addC.Add("@Company", CoName.Text);
                    addC.Add("@ItemName", iiName);
                    addC.Add("@Count", iiCount);
                    addC.Add("@ItemPrice", iiPrice);
                    addC.Add("@Total", iiTotal);
                    addC.Add("@Note", iiNote);
                    addC.Add("@userID", Login.cid);
                    addC.Add("@userName", Login.cnn);
                    addC.Add("@cDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    addC.Add("@r1", r1);
                    addC.Add("@r2", r2);
                    addC.Add("@r3", r3);
                    addC.Add("@r4", r4);
                    addC.Add("@r5", r5);
                    addC.Add("@p1", p1);
                    addC.Add("@p2", p2);

                    var addToOffers = DBConnection.SqliteCommand("INSERT  INTO `Offers`(`Num`, `Company`, `ItemName`, `Count`, `ItemPrice`,`Total`,`Note`,`userID`,`userName`,`cDate`,`r1`,`r2`,`r3`,`r4`,`r5`,`p1`,`p2`) " +
                        "values(@Num,@Company,@ItemName,@Count,@ItemPrice,@Total,@Note,@userID,@userName,@cDate,@r1,@r2,@r3,@r4,@r5,@p1,@p2)", addC);
                    
                }
                InsertOfferData();
            }
            catch
            {
            }
        }
        private void InsertOfferData()
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@Num", NuM);
                addC.Add("@CoName", CoName.Text);
                addC.Add("@OfferBy", Login.cnn);
                addC.Add("@cDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var addToOffers = DBConnection.SqliteCommand("INSERT  INTO `OffersData`(`Num`, `CoName`, `OfferBy`,`cDate`) " +
                    "values(@Num,@CoName,@OfferBy,@cDate)", addC);

            }
            catch
            {
            }
        }
        private void PrintOffer()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@r", NuM);
                var check = DBConnection.SqliteCommand("SELECT * FROM Offers where Num=@r", dic);
                if (check == null) return;
                if (check.Rows.Count > 0)
                {

                    string d1 = DateTime.Now.ToString("dd");
                    string m1 = DateTime.Now.ToString("MM");
                    string y1 = DateTime.Now.ToString("yyyy");
                    string ti1 = DateTime.Now.ToString("t");
                    string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                    string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                    string PrintDateTime = "تمت طباعة عرض السعر يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                    Print.Offers printPurch = new Print.Offers();
                    printPurch.SetDataSource(check);
                    printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                    printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                    printPurch.SetParameterValue("name", Dashboard.invCoN);
                    printPurch.SetParameterValue("slug", Dashboard.invCoS);
                    printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                    printPurch.SetParameterValue("address", Dashboard.invCoA);
                    printPurch.PrintToPrinter(1, false, 0, 0);
                }

            }
            catch
            {
            }
        }
        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            sumMethod();
        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            sumMethod();
        }

        private void LoadOld()
        {
            AddIn.Text = "طباعة";
            CoName.Enabled = false;
            itemsNames.Enabled = false;
            Sprice.Enabled = false;
            iCount.Text = "";
            iCount.Enabled = false;
            iNote.Enabled = false;
            btnAdd.Enabled = false;
            foreach (DataGridViewBand band in dataGridView1.Columns)
            {
                band.ReadOnly = true;
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@Num", NuM);
            var result = DBConnection.SqliteCommand("Select * from Offers where Num = @Num", dic).ConvertOffers();
            if (result == null) return;

            foreach (Models.Offers item in result)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = item.ItemName;
                row.Cells[1].Value = item.Count;
                row.Cells[2].Value = item.ItemPrice;
                row.Cells[3].Value = item.Total;
                row.Cells[4].Value = item.Note;
                dataGridView1.Rows.Add(row);
                CoName.Text = item.Company;
                iNote.Text = item.userName;
            }

        }
        void ItemTxtBox_TextChanged(object sender, EventArgs e)
        {
            if ((sender as TextBox).Text != null && (sender as TextBox).Text.Trim() != "")
            {
                sumMethod();
            }
        }

       private void randpFind()
        {
            RandP rp = new RandP();
            rp.FormClosed += payClosed;
            rp.ShowDialog();
        }
        void payClosed(object sender, FormClosedEventArgs e)
        {
            r1 = RandP.r1;
            r2 = RandP.r2;
            r3 = RandP.r3;
            r4 = RandP.r4;
            r5 = RandP.r5;
            p1 = RandP.p1;
            p2 = RandP.p2;
        }
    }
}

