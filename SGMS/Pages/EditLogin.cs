﻿using SGMS.Data;
using SGMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGMS
{
    public partial class EditLogin : MetroFramework.Forms.MetroForm
    {
        string r1;
        string r2;
        string r3;
        string r4;
        string r5;
        string r6;
        string r7;
        string r8;
        string r9;
        string r10;
        string r11;
        string r12;
        string r13;
        string r14;
        string r15;
        string r16;
        string r17;
        string r18;
        string r19;
        string r20;
        string r21;
        string r22;
        string r23;
        string r24;
        string r25 = "0";
        string r26 = "0";
        string r27 = "0";
        string r28 = "0";
        string r29 = "0";
        string r30 = "0";
        int UserID;
        string usern;
        public EditLogin(int ciid)
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            UserID = ciid;
            LoadUser();
        }
        private static List<User> UsersP = new List<User>();
        private void LoadUser()
        {
            try
            {
                UsersP.Clear();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@uid", UserID);
                var result = DBConnection.SqliteCommand("SELECT *  FROM Login where uid = @uid", dic);
                if (result.Rows.Count > 0)
                {

                    foreach (DataRow item in result.Rows)
                    {
                        User u = new User();
                        u.uid = int.Parse(item["uid"].ToString());
                        u.username = item["username"].ToString();
                        u.fullname = item["fullname"].ToString();
                        u.password = item["password"].ToString();
                        u.Cash = double.Parse(item["Cash"].ToString());
                        u.role1 = int.Parse(item["role1"].ToString());
                        u.role2 = int.Parse(item["role2"].ToString());
                        u.role3 = int.Parse(item["role3"].ToString());
                        u.role4 = int.Parse(item["role4"].ToString());
                        u.role5 = int.Parse(item["role5"].ToString());
                        u.role6 = int.Parse(item["role6"].ToString());
                        u.role7 = int.Parse(item["role7"].ToString());
                        u.role8 = int.Parse(item["role8"].ToString());
                        u.role9 = int.Parse(item["role9"].ToString());
                        u.role10 = int.Parse(item["role10"].ToString());
                        u.role11 = int.Parse(item["role11"].ToString());
                        u.role12 = int.Parse(item["role12"].ToString());
                        u.role13 = int.Parse(item["role13"].ToString());
                        u.role14 = int.Parse(item["role14"].ToString());
                        u.role15 = int.Parse(item["role15"].ToString());
                        u.role16 = int.Parse(item["role16"].ToString());
                        u.role17 = int.Parse(item["role17"].ToString());
                        u.role18 = int.Parse(item["role18"].ToString());
                        u.role19 = int.Parse(item["role19"].ToString());
                        u.role20 = int.Parse(item["role20"].ToString());
                        u.role21 = int.Parse(item["role21"].ToString());
                        u.role22 = int.Parse(item["role22"].ToString());
                        u.role23 = int.Parse(item["role23"].ToString());
                        u.role24 = int.Parse(item["role24"].ToString());
                        u.role25 = int.Parse(item["role25"].ToString());
                        u.role26 = int.Parse(item["role26"].ToString());
                        u.role27 = int.Parse(item["role27"].ToString());
                        u.role28 = int.Parse(item["role28"].ToString());
                        u.role29 = int.Parse(item["role29"].ToString());
                        u.role30 = int.Parse(item["role30"].ToString());
                        u.BlockState = int.Parse(item["BlockState"].ToString());
                        u.Blocker = item["Blocker"].ToString();
                        UsersP.Add(u);
                    }
                    var item1 = UsersP.Find(i => i.uid == UserID);
                    Fullname.Text = item1.fullname;
                    Username.Text = item1.username;
                    Password.Text = item1.password;
                    usern = item1.username;

                    //
                    if (item1.role1 == 1)
                    {
                        treeView1.Nodes["s1"].Checked = true;
                    }
                    if (item1.role2 == 1)
                    {
                        treeView1.Nodes["s2"].Checked = true;
                    }
                    if (item1.role3 == 1)
                    {
                        treeView1.Nodes["s3"].Checked = true;
                    }
                    if (item1.role4 == 1)
                    {
                        treeView1.Nodes["s4"].Checked = true;
                    }
                    if (item1.role5 == 1)
                    {
                        treeView1.Nodes["s5"].Checked = true;
                    }
                    if (item1.role6 == 1)
                    {
                        treeView1.Nodes["s6"].Checked = true;
                    }
                    if (item1.role7 == 1)
                    {
                        treeView1.Nodes["s7"].Checked = true;
                    }
                    if (item1.role8 == 1)
                    {
                        treeView1.Nodes["s8"].Checked = true;
                    }
                    if (item1.role9 == 1)
                    {
                        treeView1.Nodes["s9"].Checked = true;
                    }
                    if (item1.role10 == 1)
                    {
                        treeView1.Nodes["s10"].Checked = true;
                    }
                    if (item1.role11 == 1)
                    {
                        treeView1.Nodes["s11"].Checked = true;
                    }
                    if (item1.role12 == 1)
                    {
                        treeView1.Nodes["s12"].Checked = true;
                    }
                    if (item1.role13 == 1)
                    {
                        treeView1.Nodes["s13"].Checked = true;
                    }
                    if (item1.role14 == 1)
                    {
                        treeView1.Nodes["s14"].Checked = true;
                    }
                    if (item1.role15 == 1)
                    {
                        treeView1.Nodes["s15"].Checked = true;
                    }
                    if (item1.role16 == 1)
                    {
                        treeView1.Nodes["s16"].Checked = true;
                    }
                    if (item1.role17 == 1)
                    {
                        treeView1.Nodes["s17"].Checked = true;
                    }
                    if (item1.role18 == 1)
                    {
                        treeView1.Nodes["s18"].Checked = true;
                    }
                    if (item1.role19 == 1)
                    {
                        treeView1.Nodes["s19"].Checked = true;
                    }
                    if (item1.role20 == 1)
                    {
                        treeView1.Nodes["s20"].Checked = true;
                    }
                    if (item1.role21 == 1)
                    {
                        treeView1.Nodes["s21"].Checked = true;
                    }
                    if (item1.role22 == 1)
                    {
                        treeView1.Nodes["s22"].Checked = true;
                    }
                    if (item1.role23 == 1)
                    {
                        treeView1.Nodes["s23"].Checked = true;
                    }
                    if (item1.role24 == 1)
                    {
                        treeView1.Nodes["s24"].Checked = true;
                    }
                    if (item1.role25 == 1)
                    {
                        treeView1.Nodes["s25"].Checked = true;
                    }
                    if (item1.role26 == 1)
                    {
                        treeView1.Nodes["s26"].Checked = true;
                    }
                    if (item1.role27 == 1)
                    {
                        treeView1.Nodes["s27"].Checked = true;
                    }
                    if (item1.role28 == 1)
                    {
                        treeView1.Nodes["s28"].Checked = true;
                    }
                    if (item1.role29 == 1)
                    {
                        treeView1.Nodes["s29"].Checked = true;
                    }
                    if (item1.role30 == 1)
                    {
                        treeView1.Nodes["s30"].Checked = true;
                    }
                    //
                    if (item1.BlockState == 0)
                    {
                        unBlock.Visible = true;
                    }
                    else
                    {
                        unBlock.Visible = false;
                    }
                }
            }
            catch { }
        }
        private void add_Click(object sender, EventArgs e)
        {
            CheckRoles();
            if (string.IsNullOrWhiteSpace(Username.Text) || string.IsNullOrWhiteSpace(Fullname.Text) || string.IsNullOrWhiteSpace(Password.Text))
            {
                cMessgeBox mess = new cMessgeBox("يرجي التأكد من ملئ جميع البيانات", "error", "c", 1500);
                mess.ShowDialog();
            }
            else
            {
                if (usern == Username.Text)
                {


                    try
                    {
                        var username = Username.Text.ToString();
                        var password = Password.Text.ToString();
                        Dictionary<string, object> addC = new Dictionary<string, object>();
                        addC.Add("@username", username);
                        addC.Add("@password", password);
                        addC.Add("@uid", UserID);
                        addC.Add("@role1", r1);
                        addC.Add("@role2", r2);
                        addC.Add("@role3", r3);
                        addC.Add("@role4", r4);
                        addC.Add("@role5", r5);
                        addC.Add("@role6", r6);
                        addC.Add("@role7", r7);
                        addC.Add("@role8", r8);
                        addC.Add("@role9", r9);
                        addC.Add("@role10", r10);
                        addC.Add("@role11", r11);
                        addC.Add("@role12", r12);
                        addC.Add("@role13", r13);
                        addC.Add("@role14", r14);
                        addC.Add("@role15", r15);
                        addC.Add("@role16", r16);
                        addC.Add("@role17", r17);
                        addC.Add("@role18", r18);
                        addC.Add("@role19", r19);
                        addC.Add("@role20", r20);
                        addC.Add("@role21", r21);
                        addC.Add("@role22", r22);
                        addC.Add("@role23", r23);
                        addC.Add("@role24", r24);
                        addC.Add("@role25", r25);
                        addC.Add("@role26", r26);
                        addC.Add("@role27", r27);
                        addC.Add("@role28", r28);
                        addC.Add("@role29", r29);
                        addC.Add("@role30", r30);
                        var addNotice = DBConnection.SqliteCommand("update Login Set username = @username, password = @password, role1 = @role1, role2 = @role2, role3 = @role3, role4 = @role4, role5 = @role5, role6 = @role6, role7 = @role7, role8 = @role8, role9 = @role9, role10 = @role10, role11 = @role11, role12 = @role12, role13 = @role13, role14 = @role14, role15 = @role15, role16 = @role16, role17 = @role17, role18 = @role18, role19 = @role19, role20 = @role20, role21 = @role21, role22 = @role22, role23 = @role23, role24 = @role24, role25 = @role25, role26 = @role26, role27 = @role27, role28 = @role28, role29 = @role29, role30 = @role30 where uid = @uid", addC);
                        cMessgeBox mess = new cMessgeBox("تم تعديل المستخدم بنجاح", "done", "c", 1000);
                        mess.ShowDialog();
                        this.Close();
                    }
                    catch { }

                }
                else
                {
                    var usrn = UsersP.Find(u => u.username == Username.Text);
                    if (usrn != null)
                    {
                        cMessgeBox mess = new cMessgeBox("يوجد مستخدم مسجل بنفس إسم المستخدم", "error", "c", 1500);
                        mess.ShowDialog();
                    }
                    else
                    {
                        try
                        {
                            var username = Username.Text.ToString();
                            var password = Password.Text.ToString();
                            Dictionary<string, object> addC = new Dictionary<string, object>();
                            addC.Add("@username", username);
                            addC.Add("@password", password);
                            addC.Add("@uid", UserID);
                            addC.Add("@role1", r1);
                            addC.Add("@role2", r2);
                            addC.Add("@role3", r3);
                            addC.Add("@role4", r4);
                            addC.Add("@role5", r5);
                            addC.Add("@role6", r6);
                            addC.Add("@role7", r7);
                            addC.Add("@role8", r8);
                            addC.Add("@role9", r9);
                            addC.Add("@role10", r10);
                            addC.Add("@role11", r11);
                            addC.Add("@role12", r12);
                            addC.Add("@role13", r13);
                            addC.Add("@role14", r14);
                            addC.Add("@role15", r15);
                            addC.Add("@role16", r16);
                            addC.Add("@role17", r17);
                            addC.Add("@role18", r18);
                            addC.Add("@role19", r19);
                            addC.Add("@role20", r20);
                            addC.Add("@role21", r21);
                            addC.Add("@role22", r22);
                            addC.Add("@role23", r23);
                            addC.Add("@role24", r24);
                            addC.Add("@role25", r25);
                            addC.Add("@role26", r26);
                            addC.Add("@role27", r27);
                            addC.Add("@role28", r28);
                            addC.Add("@role29", r29);
                            addC.Add("@role30", r30);
                            var addNotice = DBConnection.SqliteCommand("update Login Set username = @username, password = @password, role1 = @role1, role2 = @role2, role3 = @role3, role4 = @role4, role5 = @role5, role6 = @role6, role7 = @role7, role8 = @role8, role9 = @role9, role10 = @role10, role11 = @role11, role12 = @role12, role13 = @role13, role14 = @role14, role15 = @role15, role16 = @role16, role17 = @role17, role18 = @role18, role19 = @role19, role20 = @role20, role21 = @role21, role22 = @role22, role23 = @role23, role24 = @role24, role25 = @role25, role26 = @role26, role27 = @role27, role28 = @role28, role29 = @role29, role30 = @role30 where uid = @uid", addC);
                            cMessgeBox mess = new cMessgeBox("تم تعديل المستخدم بنجاح", "done", "c", 1000);
                            mess.ShowDialog();
                            this.Close();
                        }
                        catch { }
                    }
                }
            }
        }
        private void CheckRoles()
        {
            if (treeView1.Nodes["s1"].Checked)
            {
                r1 = "1";
            }
            else
            {
                r1 = "0";

            }
            if (treeView1.Nodes["s2"].Checked)
            {
                r2 = "1";
            }
            else
            {
                r2 = "0";

            }
            if (treeView1.Nodes["s3"].Checked)
            {
                r3 = "1";
            }
            else
            {
                r3 = "0";

            }
            if (treeView1.Nodes["s4"].Checked)
            {
                r4 = "1";
            }
            else
            {
                r4 = "0";

            }
            if (treeView1.Nodes["s5"].Checked)
            {
                r5 = "1";
            }
            else
            {
                r5 = "0";

            }
            if (treeView1.Nodes["s6"].Checked)
            {
                r6 = "1";
            }
            else
            {
                r6 = "0";

            }
            if (treeView1.Nodes["s7"].Checked)
            {
                r7 = "1";
            }
            else
            {
                r7 = "0";

            }
            if (treeView1.Nodes["s8"].Checked)
            {
                r8 = "1";
            }
            else
            {
                r8 = "0";

            }
            if (treeView1.Nodes["s9"].Checked)
            {
                r9 = "1";
            }
            else
            {
                r9 = "0";

            }
            if (treeView1.Nodes["s10"].Checked)
            {
                r10 = "1";
            }
            else
            {
                r10 = "0";

            }
            if (treeView1.Nodes["s11"].Checked)
            {
                r11 = "1";
            }
            else
            {
                r11 = "0";

            }
            if (treeView1.Nodes["s12"].Checked)
            {
                r12 = "1";
            }
            else
            {
                r12 = "0";

            }
            if (treeView1.Nodes["s13"].Checked)
            {
                r13 = "1";
            }
            else
            {
                r13 = "0";

            }
            if (treeView1.Nodes["s14"].Checked)
            {
                r14 = "1";
            }
            else
            {
                r14 = "0";

            }
            if (treeView1.Nodes["s15"].Checked)
            {
                r15 = "1";
            }
            else
            {
                r15 = "0";

            }
            if (treeView1.Nodes["s16"].Checked)
            {
                r16 = "1";
            }
            else
            {
                r16 = "0";

            }
            if (treeView1.Nodes["s17"].Checked)
            {
                r17 = "1";
            }
            else
            {
                r17 = "0";

            }
            if (treeView1.Nodes["s18"].Checked)
            {
                r18 = "1";
            }
            else
            {
                r18 = "0";

            }
            if (treeView1.Nodes["s19"].Checked)
            {
                r19 = "1";
            }
            else
            {
                r19 = "0";

            }
            if (treeView1.Nodes["s20"].Checked)
            {
                r20 = "1";
            }
            else
            {
                r20 = "0";

            }
            if (treeView1.Nodes["s21"].Checked)
            {
                r21 = "1";
            }
            else
            {
                r21 = "0";

            }
            if (treeView1.Nodes["s22"].Checked)
            {
                r22 = "1";
            }
            else
            {
                r22 = "0";

            }

            if (treeView1.Nodes["s23"].Checked)
            {
                r23 = "1";
            }
            else
            {
                r23 = "0";

            }
            if (treeView1.Nodes["s24"].Checked)
            {
                r24 = "1";
            }
            else
            {
                r24 = "0";

            }
            //if (treeView1.Nodes["s25"].Checked)
            //{
            //    r25 = "1";
            //}
            //else
            //{
            //    r25 = "0";

            //}
            //if (treeView1.Nodes["s26"].Checked)
            //{
            //    r26 = "1";
            //}
            //else
            //{
            //    r26 = "0";

            //}
            //if (treeView1.Nodes["s27"].Checked)
            //{
            //    r27 = "1";
            //}
            //else
            //{
            //    r27 = "0";

            //}
            //if (treeView1.Nodes["s28"].Checked)
            //{
            //    r28 = "1";
            //}
            //else
            //{
            //    r28 = "0";

            //}
            //if (treeView1.Nodes["s29"].Checked)
            //{
            //    r29 = "1";
            //}
            //else
            //{
            //    r29 = "0";

            //}
            //if (treeView1.Nodes["s30"].Checked)
            //{
            //    r30 = "1";
            //}
            //else
            //{
            //    r30 = "0";

            //}
        }
        private void unBlock_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@BlockState", 1);
                addC.Add("@Blocker", "الإدارة");
                addC.Add("@uid", UserID);
                var UpdateCu = DBConnection.SqliteCommand("update Login Set BlockState = @BlockState, Blocker = @Blocker where uid=@uid", addC);
                cMessgeBox mess = new cMessgeBox("تمت إلغاء حظر المستخدم بنجاح", "done", "c", 1000);
                mess.ShowDialog();
                this.Close();
            }
            catch { }
        }
    }
}
