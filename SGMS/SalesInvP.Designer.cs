﻿namespace SGMS
{
    partial class SalesInvP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesInvP));
            this.AToolbar = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PrintInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.Paid = new System.Windows.Forms.Label();
            this.Unpaid = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.invDate = new System.Windows.Forms.Label();
            this.invClient = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InvNum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.invDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invDatagride)).BeginInit();
            this.SuspendLayout();
            // 
            // AToolbar
            // 
            this.AToolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AToolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.AToolbar.Location = new System.Drawing.Point(0, 0);
            this.AToolbar.Name = "AToolbar";
            this.AToolbar.Size = new System.Drawing.Size(506, 29);
            this.AToolbar.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.PrintInvByNum);
            this.panel1.Controls.Add(this.Paid);
            this.panel1.Controls.Add(this.Unpaid);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.Total);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 495);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 57);
            this.panel1.TabIndex = 6;
            // 
            // PrintInvByNum
            // 
            this.PrintInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PrintInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PrintInvByNum.BorderRadius = 7;
            this.PrintInvByNum.ButtonText = "طباعة";
            this.PrintInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PrintInvByNum.DisabledColor = System.Drawing.Color.Gray;
            this.PrintInvByNum.Font = new System.Drawing.Font("Cairo", 8F);
            this.PrintInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.PrintInvByNum.Iconimage = null;
            this.PrintInvByNum.Iconimage_right = null;
            this.PrintInvByNum.Iconimage_right_Selected = null;
            this.PrintInvByNum.Iconimage_Selected = null;
            this.PrintInvByNum.IconMarginLeft = 0;
            this.PrintInvByNum.IconMarginRight = 0;
            this.PrintInvByNum.IconRightVisible = false;
            this.PrintInvByNum.IconRightZoom = 0D;
            this.PrintInvByNum.IconVisible = false;
            this.PrintInvByNum.IconZoom = 20D;
            this.PrintInvByNum.IsTab = false;
            this.PrintInvByNum.Location = new System.Drawing.Point(13, 12);
            this.PrintInvByNum.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PrintInvByNum.Name = "PrintInvByNum";
            this.PrintInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.OnHovercolor = System.Drawing.Color.Green;
            this.PrintInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.PrintInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PrintInvByNum.selected = false;
            this.PrintInvByNum.Size = new System.Drawing.Size(78, 33);
            this.PrintInvByNum.TabIndex = 43;
            this.PrintInvByNum.Text = "طباعة";
            this.PrintInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrintInvByNum.Textcolor = System.Drawing.Color.White;
            this.PrintInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintInvByNum.Click += new System.EventHandler(this.PrintInvByNum_Click);
            // 
            // Paid
            // 
            this.Paid.Font = new System.Drawing.Font("Cairo", 8F);
            this.Paid.ForeColor = System.Drawing.Color.Black;
            this.Paid.Location = new System.Drawing.Point(252, 17);
            this.Paid.Name = "Paid";
            this.Paid.Size = new System.Drawing.Size(53, 24);
            this.Paid.TabIndex = 45;
            this.Paid.Text = "0";
            this.Paid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Unpaid
            // 
            this.Unpaid.Font = new System.Drawing.Font("Cairo", 8F);
            this.Unpaid.ForeColor = System.Drawing.Color.Black;
            this.Unpaid.Location = new System.Drawing.Point(110, 17);
            this.Unpaid.Name = "Unpaid";
            this.Unpaid.Size = new System.Drawing.Size(55, 25);
            this.Unpaid.TabIndex = 44;
            this.Unpaid.Text = "0";
            this.Unpaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cairo", 7F);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label16.Location = new System.Drawing.Point(311, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 19);
            this.label16.TabIndex = 42;
            this.label16.Text = "المحصل";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cairo", 7F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label17.Location = new System.Drawing.Point(173, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 19);
            this.label17.TabIndex = 43;
            this.label17.Text = "الغير محصل";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cairo", 7F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label9.Location = new System.Drawing.Point(448, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 19);
            this.label9.TabIndex = 40;
            this.label9.Text = "الإجمالي";
            // 
            // Total
            // 
            this.Total.Font = new System.Drawing.Font("Cairo", 8F);
            this.Total.ForeColor = System.Drawing.Color.Black;
            this.Total.Location = new System.Drawing.Point(390, 16);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(53, 24);
            this.Total.TabIndex = 41;
            this.Total.Text = "0";
            this.Total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.invDate);
            this.panel2.Controls.Add(this.invClient);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.InvNum);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(506, 75);
            this.panel2.TabIndex = 7;
            // 
            // invDate
            // 
            this.invDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invDate.Font = new System.Drawing.Font("Cairo", 8F);
            this.invDate.ForeColor = System.Drawing.Color.Black;
            this.invDate.Location = new System.Drawing.Point(252, 40);
            this.invDate.Name = "invDate";
            this.invDate.Size = new System.Drawing.Size(166, 19);
            this.invDate.TabIndex = 46;
            // 
            // invClient
            // 
            this.invClient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invClient.Font = new System.Drawing.Font("Cairo", 8F);
            this.invClient.ForeColor = System.Drawing.Color.Black;
            this.invClient.Location = new System.Drawing.Point(267, 15);
            this.invClient.Name = "invClient";
            this.invClient.Size = new System.Drawing.Size(159, 19);
            this.invClient.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 7F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label3.Location = new System.Drawing.Point(415, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 19);
            this.label3.TabIndex = 44;
            this.label3.Text = "تاريخ الفاتورة :";
            // 
            // InvNum
            // 
            this.InvNum.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InvNum.ForeColor = System.Drawing.Color.Black;
            this.InvNum.Location = new System.Drawing.Point(29, 23);
            this.InvNum.Name = "InvNum";
            this.InvNum.Size = new System.Drawing.Size(101, 26);
            this.InvNum.TabIndex = 42;
            this.InvNum.Text = "0";
            this.InvNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cairo", 7F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label8.Location = new System.Drawing.Point(137, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 19);
            this.label8.TabIndex = 41;
            this.label8.Text = "رقم الفاتورة";
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(22, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(185, 35);
            this.label5.TabIndex = 43;
            this.label5.Text = " ";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 7F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label6.Location = new System.Drawing.Point(422, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 19);
            this.label6.TabIndex = 40;
            this.label6.Text = "إسم العميل :";
            // 
            // invDatagride
            // 
            this.invDatagride.AllowUserToAddRows = false;
            this.invDatagride.AllowUserToDeleteRows = false;
            this.invDatagride.AllowUserToResizeColumns = false;
            this.invDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.invDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.invDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.invDatagride.BackgroundColor = System.Drawing.Color.White;
            this.invDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.invDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.invDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.invDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.invDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.count,
            this.BuyPrice,
            this.TotalBuy});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.invDatagride.DefaultCellStyle = dataGridViewCellStyle6;
            this.invDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.invDatagride.DoubleBuffered = true;
            this.invDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.invDatagride.EnableHeadersVisualStyles = false;
            this.invDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.invDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.invDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.invDatagride.Location = new System.Drawing.Point(0, 104);
            this.invDatagride.Name = "invDatagride";
            this.invDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.invDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.invDatagride.RowHeadersVisible = false;
            this.invDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.invDatagride.Size = new System.Drawing.Size(506, 391);
            this.invDatagride.TabIndex = 8;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "إسم المنتج";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // count
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.count.DefaultCellStyle = dataGridViewCellStyle3;
            this.count.FillWeight = 56.99974F;
            this.count.HeaderText = "الكمية";
            this.count.Name = "count";
            this.count.ReadOnly = true;
            // 
            // BuyPrice
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BuyPrice.DefaultCellStyle = dataGridViewCellStyle4;
            this.BuyPrice.FillWeight = 78.06802F;
            this.BuyPrice.HeaderText = "السعر";
            this.BuyPrice.Name = "BuyPrice";
            this.BuyPrice.ReadOnly = true;
            // 
            // TotalBuy
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TotalBuy.DefaultCellStyle = dataGridViewCellStyle5;
            this.TotalBuy.FillWeight = 101.4645F;
            this.TotalBuy.HeaderText = "الإجمالي";
            this.TotalBuy.Name = "TotalBuy";
            this.TotalBuy.ReadOnly = true;
            // 
            // SalesInvP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 552);
            this.Controls.Add(this.invDatagride);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.AToolbar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SalesInvP";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.invDatagride)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel AToolbar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuCustomDataGrid invDatagride;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label InvNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Paid;
        private System.Windows.Forms.Label Unpaid;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label Total;
        private Bunifu.Framework.UI.BunifuFlatButton PrintInvByNum;
        private System.Windows.Forms.Label invDate;
        private System.Windows.Forms.Label invClient;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBuy;
    }
}