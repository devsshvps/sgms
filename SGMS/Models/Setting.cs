﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public class Setting
    {

        public int id { get; set; }
        public string StoreName { get; set; }
        public string StoreLogo { get; set; }
        public string MediaName { get; set; }
        public string PrintLogo { get; set; }
        public string PrintCoName { get; set; }
        public string PrintCoSlu { get; set; }
        public string PrintCoAddress { get; set; }
        public string PrintCoPhone { get; set; }

    }
}
