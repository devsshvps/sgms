﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class CashInvS
    {
        public int id { get; set; }
        public int invNum { get; set; }
        public double unpaid { get; set; }
        public double totalInv { get; set; }
        public int clientID { get; set; }
        public int userID { get; set; }
        public string clientName { get; set; }
        public string userName { get; set; }
        public DateTime invDateTime { get; set; }
        public int Tax { get; set; }
    }

}
