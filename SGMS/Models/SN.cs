﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public class SN
    {
        public int id { get; set; }
        public string sn1 { get; set; }
        public string PC { get; set; }
        public string HDD { get; set; }
        public string PC1 { get; set; }
        public string HDD1 { get; set; }
        public DateTime ActiveDate { get; set; }
        public string ver { get; set; }

    }
}
