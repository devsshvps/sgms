﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
  public  class Purchase
    {
        public int id { get; set; }
        public int Num { get; set; }
        public string item { get; set; }
        public string BarCode { get; set; }
        public double Count { get; set; }
        public double ItemPrice { get; set; }
        public double sPrice { get; set; }
        public double TotalBuy { get; set; }
        public int Client { get; set; }
        public string ClientName { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public DateTime Date_ct { get; set; }
        public DateTime Time_ct { get; set; }
    }
  
}
