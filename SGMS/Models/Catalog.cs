﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class Catalog
    {
        public int id { get; set; }
        public string Name { get; set; }
        public int r { get; set; }
    }
}
