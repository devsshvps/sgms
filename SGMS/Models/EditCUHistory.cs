﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class EditCUHistory
    {
        public int id { get; set; }
        public int UserID { get; set; }
        public int ClientID { get; set; }
        public string UserName { get; set; }
        public string Reason { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime EditDate { get; set; }
    }
}
