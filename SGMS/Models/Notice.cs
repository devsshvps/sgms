﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public class Notice
    {

        public int id { get; set; }
        public int UserID { get; set; }
        public string remNotice { get; set; }
        public DateTime ct_date { get; set; }
        public DateTime ct_time { get; set; }
        public DateTime RemDate { get; set; }
        public DateTime showDate { get; set; }
        public int r { get; set; }

    }
}
