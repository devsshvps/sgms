﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
 public class Sales
    {
        public int id { get; set; }
        public int Num { get; set; }
        public string BarCode { get; set; }
        public string itemname { get; set; }
        public double count { get; set; }
        public double itemprice { get; set; }
        public double buyprice { get; set; }
        public double totalprice { get; set; }
        public int sellforid { get; set; }
        public string sellfor { get; set; }
        public int Userid { get; set; }
        public string User { get; set; }
        public DateTime ddate { get; set; }
        public DateTime ttime { get; set; }
        public int r { get; set; }
        public double earn { get; set; }
    }
}
