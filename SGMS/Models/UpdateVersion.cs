﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class UpdateVersion
    {
        public int id { get; set; }
        public string v { get; set; }
        public string url { get; set; }
        public string filename { get; set; }
        public DateTime updated_at { get; set; }
    }
}
