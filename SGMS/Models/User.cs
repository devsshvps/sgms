﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
  public  class User
    {
        public int uid { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string password { get; set; }
        public double Cash { get; set; }
        public int role1 { get; set; }
        public int role2 { get; set; }
        public int role3 { get; set; }
        public int role4 { get; set; }
        public int role5 { get; set; }
        public int role6 { get; set; }
        public int role7 { get; set; }
        public int role8 { get; set; }
        public int role9 { get; set; }
        public int role10 { get; set; }
        public int role11 { get; set; }
        public int role12 { get; set; }
        public int role13 { get; set; }
        public int role14 { get; set; }
        public int role15 { get; set; }
        public int role16 { get; set; }
        public int role17 { get; set; }
        public int role18 { get; set; }
        public int role19 { get; set; }
        public int role20 { get; set; }
        public int role21 { get; set; }
        public int role22 { get; set; }
        public int role23 { get; set; }
        public int role24 { get; set; }
        public int role25 { get; set; }
        public int role26 { get; set; }
        public int role27 { get; set; }
        public int role28 { get; set; }
        public int role29 { get; set; }
        public int role30 { get; set; }
        public int BlockState { get; set; }
        public string Blocker { get; set; }

    }
}
