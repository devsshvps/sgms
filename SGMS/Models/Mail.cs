﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public class Mail
    {
        public int id { get; set; }
        public string subject { get; set; }
        public string Message { get; set; }
        public int fromID { get; set; }
        public string fromName { get; set; }
        public int toID { get; set; }
        public string toName { get; set; }
        public DateTime m_date { get; set; }
        public DateTime m_time { get; set; }
        public int m { get; set; }
    }
}
