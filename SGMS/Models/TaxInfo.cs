﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class TaxInfo
    {
        public int id { get; set; }
        public string TaxNum { get; set; }
        public string CoRecord { get; set; }
    }
}
