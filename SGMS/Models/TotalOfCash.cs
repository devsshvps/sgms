﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class TotalOfCash
    {
        public int id { get; set; }
        public double TCash { get; set; }
    }
}
