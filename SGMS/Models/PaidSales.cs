﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class PaidSales
    {
        public int id { get; set; }
        public int InvNumber { get; set; }
        public double Paid { get; set; }
        public double UnPaid { get; set; }
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public string ClientName { get; set; }
        public string UserName { get; set; }
        public DateTime PaidDate { get; set; }
    }
}
