﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class Returned
    {

        public int id { get; set; }
        public string BarCode { get; set; }
        public string ItemName { get; set; }
        public double reCount { get; set; }
        public double rePrice { get; set; }
        public string Customer { get; set; }
        public int Cid { get; set; }
        public string ReUser { get; set; }
        public int ReUserID { get; set; }
        public DateTime DateTime_re { get; set; }
        public DateTime date_ct { get; set; }
    }
}
