﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public class Item
    {
        public int id { get; set; }
        public string BarCode { get; set; }
        public int Catalog { get; set; }
        public string Name { get; set; }
        public double Count { get; set; }
        public double ItemPrice { get; set; }
        public double SellPrice { get; set; }
        public double TotalBuy { get; set; }
        public double TotalSell { get; set; }
        public double Earn { get; set; }
        public int r { get; set; }
        public double NumOfSales { get; set; }
        public double NumOfPurc { get; set; }
    }
}
