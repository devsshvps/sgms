﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class AdminNotes
    {
        public int id { get; set; }
        public string Note { get; set; }
        public string User { get; set; }
        public DateTime ct_at { get; set; }
        public DateTime ti_at { get; set; }
        public int r { get; set; }
    }
}
