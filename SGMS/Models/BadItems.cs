﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class BadItems
    {
        public int id { get; set; }
        public string BarCode { get; set; }
        public string ItemName { get; set; }
        public double Count { get; set; }
        public DateTime LastReDate { get; set; }
    }
}
