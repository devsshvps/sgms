﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
   public class Offers
    {
        public int id { get; set; }
        public int Num { get; set; }
        public string Company { get; set; }
        public string ItemName { get; set; }
        public double Count { get; set; }
        public double ItemPrice { get; set; }
        public double Total { get; set; }
        public string Note { get; set; }
        public int userID { get; set; }
        public string userName { get; set; }
        public DateTime cDate { get; set; }

        public string r1 { get; set; }
        public string r2 { get; set; }
        public string r3 { get; set; }
        public string r4 { get; set; }
        public string r5 { get; set; }
        public string p1 { get; set; }
        public string p2 { get; set; }

    }

}
