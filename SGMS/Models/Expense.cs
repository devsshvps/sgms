﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public class Expense
    {
        public int id { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public double Cash { get; set; }
        public double TCash { get; set; }
        public string Reason { get; set; }
        public DateTime r_Time { get; set; }
        public DateTime r_Date { get; set; }
        public int LoginID { get; set; }
        public string LoginName { get; set; }
        public int r { get; set; }
    }
}
