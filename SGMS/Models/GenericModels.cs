﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGMS.Models
{
    public static class GenericModels
    {

        public static List<User> ConvertUser (this DataTable dt)
        {
            List<User> users = new List<User>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    User u = new User();
                    u.uid = int.Parse(item["uid"].ToString());
                    u.username = item["username"].ToString();
                    u.fullname = item["fullname"].ToString();
                    u.password = item["password"].ToString();
                    u.Cash = double.Parse(item["Cash"].ToString());
                    u.role1 = int.Parse(item["role1"].ToString());
                    u.role2 = int.Parse(item["role2"].ToString());
                    u.role3 = int.Parse(item["role3"].ToString());
                    u.role4 = int.Parse(item["role4"].ToString());
                    u.role5 = int.Parse(item["role5"].ToString());
                    u.role6 = int.Parse(item["role6"].ToString());
                    u.role7 = int.Parse(item["role7"].ToString());
                    u.role8 = int.Parse(item["role8"].ToString());
                    u.role9 = int.Parse(item["role9"].ToString());
                    u.role10 = int.Parse(item["role10"].ToString());
                    u.role11 = int.Parse(item["role11"].ToString());
                    u.role12 = int.Parse(item["role12"].ToString());
                    u.role13 = int.Parse(item["role13"].ToString());
                    u.role14 = int.Parse(item["role14"].ToString());
                    u.role15 = int.Parse(item["role15"].ToString());
                    u.role16 = int.Parse(item["role16"].ToString());
                    u.role17 = int.Parse(item["role17"].ToString());
                    u.role18 = int.Parse(item["role18"].ToString());
                    u.role19 = int.Parse(item["role19"].ToString());
                    u.role20 = int.Parse(item["role20"].ToString());
                    u.role21 = int.Parse(item["role21"].ToString());
                    u.role22 = int.Parse(item["role22"].ToString());
                    u.role23 = int.Parse(item["role23"].ToString());
                    u.role24 = int.Parse(item["role24"].ToString());
                    u.role25 = int.Parse(item["role25"].ToString());
                    u.role26 = int.Parse(item["role26"].ToString());
                    u.role27 = int.Parse(item["role27"].ToString());
                    u.role28 = int.Parse(item["role28"].ToString());
                    u.role29 = int.Parse(item["role29"].ToString());
                    u.role30 = int.Parse(item["role30"].ToString());
                    u.BlockState = int.Parse(item["BlockState"].ToString());
                    u.Blocker = item["Blocker"].ToString();
                    users.Add(u);
                }
            }
            return users;
        }
        public static List<Client> ConvertClients (this DataTable dt)
        {
            List<Client> clients = new List<Client>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Client u = new Client();
                    u.id = int.Parse(item["id"].ToString());
                    u.Name = item["Name"].ToString();
                    u.Phone = item["Phone"].ToString();
                    u.Cash = double.Parse(item["Cash"].ToString());
                    u.r = int.Parse(item["r"].ToString());
                    clients.Add(u);
                }
            }
            return clients;
        }
        public static List<Purchase> ConvertPurchases (this DataTable dt)
        {
            List<Purchase> purchases = new List<Purchase>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Purchase u = new Purchase();
                    u.id = int.Parse(item["id"].ToString());
                    u.Num = int.Parse(item["Num"].ToString());
                    u.item = item["item"].ToString();
                    u.BarCode = item["BarCode"].ToString();
                    u.Count = double.Parse(item["Count"].ToString());
                    u.ItemPrice = double.Parse(item["ItemPrice"].ToString());
                    u.sPrice = double.Parse(item["sPrice"].ToString());
                    u.TotalBuy = double.Parse(item["TotalBuy"].ToString());
                    u.Client = int.Parse(item["Client"].ToString());
                    u.ClientName = item["ClientName"].ToString();
                    u.UserID = int.Parse(item["UserID"].ToString());
                    u.UserName = item["UserName"].ToString();
                    u.Date_ct = DateTime.Parse(item["Date_ct"].ToString());
                    u.Time_ct = DateTime.Parse(item["Time_ct"].ToString());
                    purchases.Add(u);
                }
            }
            return purchases;
        }
        public static List<Customer> ConvertCustomers(this DataTable dt)
        {
            List<Customer> customers = new List<Customer>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Customer c = new Customer();
                    c.id = int.Parse(item["id"].ToString());
                    c.Name = item["Name"].ToString();
                    c.Phone = item["Phone"].ToString();
                    c.Address = item["Address"].ToString();
                    c.Cash = double.Parse(item["Cash"].ToString());
                    c.r = int.Parse(item["r"].ToString());
                    customers.Add(c);
                }
            }
            return customers;
        }
        public static List<Catalog> ConvertCatalogs (this DataTable dt)
        {
            List<Catalog> catalogs = new List<Catalog>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Catalog u = new Catalog();
                    u.id = int.Parse(item["id"].ToString());
                    u.Name = item["Name"].ToString();
                    u.r = int.Parse(item["r"].ToString());
                    catalogs.Add(u);
                }
            }
            return catalogs;
        }
        public static List<AdminNotes> ConvertNotes(this DataTable dt)
        {
            List<AdminNotes> Note = new List<AdminNotes>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    AdminNotes u = new AdminNotes();
                    u.id = int.Parse(item["id"].ToString());
                    u.Note = item["Note"].ToString();
                    u.User = item["User"].ToString();
                    u.ct_at = DateTime.Parse(item["ct_at"].ToString());
                    u.ti_at = DateTime.Parse(item["ti_at"].ToString());
                    u.r = int.Parse(item["r"].ToString());
                    Note.Add(u);
                }
            }
            return Note;
        }
        public static List<SN> ConvertSN(this DataTable dt)
        {

            List<SN> SNN = new List<SN>();
            if (dt?.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    SN u = new SN();
                    u.id = int.Parse(item["id"].ToString());
                    u.sn1 = item["sn1"].ToString();
                    u.PC = item["PC"].ToString();
                    u.HDD = item["HDD"].ToString();
                    u.PC1 = item["PC1"].ToString();
                    u.HDD1 = item["HDD1"].ToString();
                    u.ActiveDate = DateTime.Parse(item["ActiveDate"].ToString());
                    SNN.Add(u);
                }
            }
            return SNN;
        }
        public static List<Item> ConvertItems(this DataTable dt)
        {
            List<Item> Items = new List<Item>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Item u = new Item();
                    u.id = int.Parse(item["id"].ToString());
                    u.BarCode = item["BarCode"].ToString();
                    u.Catalog = int.Parse(item["Catalog"].ToString());
                    u.Name = item["Name"].ToString();
                    u.Count = double.Parse(item["Count"].ToString());
                    u.ItemPrice = double.Parse(item["ItemPrice"].ToString());
                    u.SellPrice = double.Parse(item["SellPrice"].ToString());
                    u.TotalBuy = double.Parse(item["TotalBuy"].ToString());
                    u.TotalSell = double.Parse(item["TotalSell"].ToString());
                    u.Earn = double.Parse(item["Earn"].ToString());
                    u.NumOfSales = double.Parse(item["NumOfSales"].ToString());
                    u.NumOfPurc = double.Parse(item["NumOfPurc"].ToString());
                    u.r = int.Parse(item["r"].ToString());
                    Items.Add(u);
                }
            }
            return Items;
        }
        public static List<Sales> ConvertSales(this DataTable dt)
        {
            List<Sales> sales = new List<Sales>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Sales u = new Sales();
                    u.id = int.Parse(item["id"].ToString());
                    u.Num = int.Parse(item["Num"].ToString());
                    u.BarCode = item["BarCode"].ToString();
                    u.itemname = item["itemname"].ToString();
                    u.count = double.Parse(item["count"].ToString());
                    u.itemprice = double.Parse(item["itemprice"].ToString());
                    u.buyprice = double.Parse(item["buyprice"].ToString());
                    u.totalprice = double.Parse(item["totalprice"].ToString());
                    u.sellforid = int.Parse(item["sellforid"].ToString());
                    u.sellfor = item["sellfor"].ToString();
                    u.Userid = int.Parse(item["Userid"].ToString());
                    u.User = item["User"].ToString();
                    u.ddate = DateTime.Parse(item["ddate"].ToString());
                    u.ttime = DateTime.Parse(item["ttime"].ToString());
                    u.r = int.Parse(item["r"].ToString());
                    u.earn = double.Parse(item["earn"].ToString());
                    sales.Add(u);
                }
            }
            return sales;
        }
        public static List<Returned> ConvertReturned(this DataTable dt)
        {
            List<Returned> REtu = new List<Returned>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Returned u = new Returned();
                    u.id = int.Parse(item["id"].ToString());
                    u.BarCode = item["BarCode"].ToString();
                    u.ItemName = item["ItemName"].ToString();
                    u.reCount = double.Parse(item["reCount"].ToString());
                    u.rePrice = double.Parse(item["rePrice"].ToString());
                    u.Customer = item["Customer"].ToString();
                    u.Cid = int.Parse(item["Cid"].ToString());
                    u.ReUser = item["ReUser"].ToString();
                    u.ReUserID = int.Parse(item["ReUserID"].ToString());
                    u.DateTime_re = DateTime.Parse(item["DateTime_re"].ToString());
                    u.date_ct = DateTime.Parse(item["date_ct"].ToString());
                    REtu.Add(u);
                }
            }
            return REtu;
        }
        public static List<WaitRE> ConvertWAR(this DataTable dt)
        {
            List<WaitRE> REtu = new List<WaitRE>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    WaitRE u = new WaitRE();
                    u.id = int.Parse(item["id"].ToString());
                    u.BarCode = item["BarCode"].ToString();
                    u.ItemName = item["ItemName"].ToString();
                    u.reCount = double.Parse(item["reCount"].ToString());
                    u.rePrice = double.Parse(item["rePrice"].ToString());
                    u.Reason = item["Reason"].ToString();
                    u.Customer = item["Customer"].ToString();
                    u.Cid = int.Parse(item["Cid"].ToString());
                    u.ReUser = item["ReUser"].ToString();
                    u.ReUserID = int.Parse(item["ReUserID"].ToString());
                    u.DateTime_re = DateTime.Parse(item["DateTime_re"].ToString());
                    u.date_ct = DateTime.Parse(item["date_ct"].ToString());
                    REtu.Add(u);
                }
            }
            return REtu;
        }
        public static List<Expense> ConvertExpenses(this DataTable eex)
        {
            List<Expense> exp = new List<Expense>();
            if (eex.Rows.Count > 0)
            {
                foreach (DataRow item in eex.Rows)
                {
                    Expense e = new Expense();
                    e.id = int.Parse(item["id"].ToString());
                    e.UserID = int.Parse(item["UserID"].ToString());
                    e.UserName = item["UserName"].ToString();
                    e.Cash = double.Parse(item["Cash"].ToString());
                    e.TCash = double.Parse(item["TCash"].ToString());
                    e.Reason = item["Reason"].ToString();
                    e.LoginID = int.Parse(item["LoginID"].ToString());
                    e.LoginName = item["LoginName"].ToString();
                    e.r_Time = DateTime.Parse(item["r_Time"].ToString());
                    e.r_Date = DateTime.Parse(item["r_Date"].ToString());
                    e.r = int.Parse(item["r"].ToString());
                    exp.Add(e);
                }
            }
            return exp;
        }
        public static List<Setting> ConvertSetting(this DataTable dt)
        {
            List<Setting> TSettings = new List<Setting>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Setting c = new Setting();
                    c.id = int.Parse(item["id"].ToString());
                    c.StoreName = item["StoreName"].ToString();
                    c.StoreLogo = item["StoreLogo"].ToString();
                    c.MediaName = item["MediaName"].ToString();
                    c.PrintLogo = item["PrintLogo"].ToString();
                    c.PrintCoName = item["PrintCoName"].ToString();
                    c.PrintCoSlu = item["PrintCoSlu"].ToString();
                    c.PrintCoAddress = item["PrintCoAddress"].ToString();
                    c.PrintCoPhone = item["PrintCoPhone"].ToString();
                    TSettings.Add(c);
                }
            }
            return TSettings;
        }
        public static List<Notice> ConvertNotices(this DataTable dt)
        {
            List<Notice> Notic = new List<Notice>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Notice u = new Notice();
                    u.id = int.Parse(item["id"].ToString());
                    u.UserID = int.Parse(item["UserID"].ToString());
                    u.remNotice = item["remNotice"].ToString();
                    
                    u.ct_date = DateTime.Parse(item["ct_date"].ToString());
                    u.ct_time = DateTime.Parse(item["ct_time"].ToString());
                    u.RemDate = DateTime.Parse(item["RemDate"].ToString());
                    u.showDate = DateTime.Parse(item["showDate"].ToString());
                    u.r = int.Parse(item["r"].ToString());
                    Notic.Add(u);
                }
            }
            return Notic;
        }
        public static List<Mail> ConvertMail(this DataTable dt)
        {
            List<Mail> mail = new List<Mail>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Mail u = new Mail();
                    u.id = int.Parse(item["id"].ToString());
                    u.subject = item["subject"].ToString();
                    u.Message = item["Message"].ToString();
                    u.fromID = int.Parse(item["fromID"].ToString());
                    u.fromName = item["fromName"].ToString();
                    u.toID = int.Parse(item["toID"].ToString());
                    u.toName = item["toName"].ToString();
                    u.m_date = DateTime.Parse(item["m_date"].ToString());
                    u.m_time = DateTime.Parse(item["m_time"].ToString());
                    u.m = int.Parse(item["m"].ToString());
                    mail.Add(u);
                }
            }
            return mail;
        }
        public static List<UserOfCash> ConvertUsersOfCash(this DataTable dt)
        {
            List<UserOfCash> UsersOfCash = new List<UserOfCash>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    UserOfCash c = new UserOfCash();
                    c.id = int.Parse(item["id"].ToString());
                    c.Name = item["Name"].ToString();
                    c.Phone = item["Phone"].ToString();
                    c.Address = item["Address"].ToString();
                    c.Cash = double.Parse(item["Cash"].ToString());
                    c.Perc = double.Parse(item["Perc"].ToString());
                    UsersOfCash.Add(c);
                }
            }
            return UsersOfCash;
        }
        public static List<TotalOfCash> ConvertTotalsOfCash(this DataTable dt)
        {
            List<TotalOfCash> TotalsOfCash = new List<TotalOfCash>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    TotalOfCash c = new TotalOfCash();
                    c.id = int.Parse(item["id"].ToString());
                    c.TCash = double.Parse(item["TCash"].ToString());
                    TotalsOfCash.Add(c);
                }
            }
            return TotalsOfCash;
        }
        public static List<OutPerc> ConvertOutPerc(this DataTable dt)
        {
            List<OutPerc> OutPercs = new List<OutPerc>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    OutPerc c = new OutPerc();
                    c.id = int.Parse(item["id"].ToString());
                    c.Name = item["Name"].ToString();
                    c.Perc = double.Parse(item["Perc"].ToString());
                    OutPercs.Add(c);
                }
            }
            return OutPercs;
        }
        public static List<FavBar> ConvertFavBar(this DataTable dt)
        {
            List<FavBar> favBars = new List<FavBar>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    FavBar c = new FavBar();
                    c.id = int.Parse(item["id"].ToString());
                    c.Fav = int.Parse(item["Fav"].ToString());
                    favBars.Add(c);
                }
            }
            return favBars;
        }
        public static List<CashInvD> ConvertCashInvoice(this DataTable dt)
        {
            List<CashInvD> CashInvoice = new List<CashInvD>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    CashInvD u = new CashInvD();
                    u.id = int.Parse(item["id"].ToString());
                    u.invNum = int.Parse(item["invNum"].ToString());
                    u.unpaid = double.Parse(item["unpaid"].ToString());
                    u.totalInv = double.Parse(item["totalInv"].ToString());
                    u.clientID = int.Parse(item["clientID"].ToString());
                    u.userID = int.Parse(item["userID"].ToString());
                    u.clientName = item["clientName"].ToString();
                    u.userName = item["userName"].ToString();
                    u.invDateTime = DateTime.Parse(item["invDateTime"].ToString());

                    CashInvoice.Add(u);
                }
            }
            return CashInvoice;
        }
        public static List<CashInvS> ConvertCashSales(this DataTable dt)
        {
            List<CashInvS> CashInvoice = new List<CashInvS>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    CashInvS u = new CashInvS();
                    u.id = int.Parse(item["id"].ToString());
                    u.invNum = int.Parse(item["invNum"].ToString());
                    u.unpaid = double.Parse(item["unpaid"].ToString());
                    u.totalInv = double.Parse(item["totalInv"].ToString());
                    u.clientID = int.Parse(item["clientID"].ToString());
                    u.userID = int.Parse(item["userID"].ToString());
                    u.clientName = item["clientName"].ToString();
                    u.userName = item["userName"].ToString();
                    u.invDateTime = DateTime.Parse(item["invDateTime"].ToString());
                    u.Tax = int.Parse(item["Tax"].ToString());
                    CashInvoice.Add(u);
                }
            }
            return CashInvoice;
        }
        public static List<PaidPurchases> ConvertPaidPurchases(this DataTable dt)
        {
            List<PaidPurchases> PaidPurchase = new List<PaidPurchases>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    PaidPurchases u = new PaidPurchases();
                    u.id = int.Parse(item["id"].ToString());
                    u.InvNumber = int.Parse(item["InvNumber"].ToString());
                    u.Paid = double.Parse(item["Paid"].ToString());
                    u.UnPaid = double.Parse(item["UnPaid"].ToString());
                    u.ClientID = int.Parse(item["ClientID"].ToString());
                    u.UserID = int.Parse(item["UserID"].ToString());
                    u.ClientName = item["ClientName"].ToString();
                    u.UserName = item["UserName"].ToString();
                    u.PaidDate = DateTime.Parse(item["PaidDate"].ToString());

                    PaidPurchase.Add(u);
                }
            }
            return PaidPurchase;
        }
        public static List<PaidSales> ConvertPaidSales(this DataTable dt)
        {
            List<PaidSales> PaidSale = new List<PaidSales>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    PaidSales u = new PaidSales();
                    u.id = int.Parse(item["id"].ToString());
                    u.InvNumber = int.Parse(item["InvNumber"].ToString());
                    u.Paid = double.Parse(item["Paid"].ToString());
                    u.UnPaid = double.Parse(item["UnPaid"].ToString());
                    u.ClientID = int.Parse(item["ClientID"].ToString());
                    u.UserID = int.Parse(item["UserID"].ToString());
                    u.ClientName = item["ClientName"].ToString();
                    u.UserName = item["UserName"].ToString();
                    u.PaidDate = DateTime.Parse(item["PaidDate"].ToString());

                    PaidSale.Add(u);
                }
            }
            return PaidSale;
        }
        public static List<EditCLHistory> ConvertEditCLHistory(this DataTable dt)
        {
            List<EditCLHistory> eHistory = new List<EditCLHistory>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    EditCLHistory u = new EditCLHistory();
                    u.id = int.Parse(item["id"].ToString());
                    u.UserID = int.Parse(item["UserID"].ToString());
                    u.ClientID = int.Parse(item["ClientID"].ToString());
                    u.UserName = item["UserName"].ToString();
                    u.Reason = item["Reason"].ToString();
                    u.From = item["From"].ToString();
                    u.To = item["To"].ToString();
                    u.EditDate = DateTime.Parse(item["EditDate"].ToString());

                    eHistory.Add(u);
                }
            }
            return eHistory;
        }
        public static List<EditCUHistory> ConvertEditCUHistory(this DataTable dt)
        {
            List<EditCUHistory> eHistory = new List<EditCUHistory>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    EditCUHistory u = new EditCUHistory();
                    u.id = int.Parse(item["id"].ToString());
                    u.UserID = int.Parse(item["UserID"].ToString());
                    u.ClientID = int.Parse(item["ClientID"].ToString());
                    u.UserName = item["UserName"].ToString();
                    u.Reason = item["Reason"].ToString();
                    u.From = item["From"].ToString();
                    u.To = item["To"].ToString();
                    u.EditDate = DateTime.Parse(item["EditDate"].ToString());

                    eHistory.Add(u);
                }
            }
            return eHistory;
        }
        public static List<BadItems> ConvertBadItems(this DataTable dt)
        {
            List<BadItems> eBadItems = new List<BadItems>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    BadItems u = new BadItems();
                    u.id = int.Parse(item["id"].ToString());
                    u.BarCode = item["BarCode"].ToString();
                    u.ItemName = item["ItemName"].ToString();
                    u.Count = double.Parse(item["Count"].ToString());
                    u.LastReDate = DateTime.Parse(item["LastReDate"].ToString());

                    eBadItems.Add(u);
                }
            }
            return eBadItems;
        }
        public static List<Offers> ConvertOffers(this DataTable dt)
        {
            List<Offers> Offer = new List<Offers>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Offers u = new Offers();
                    u.id = int.Parse(item["id"].ToString());
                    u.Num = int.Parse(item["Num"].ToString());
                    u.Company = item["Company"].ToString();
                    u.ItemName = item["ItemName"].ToString();
                    u.Count = double.Parse(item["Count"].ToString());
                    u.ItemPrice = double.Parse(item["ItemPrice"].ToString());
                    u.Total = double.Parse(item["Total"].ToString());
                    u.Note = item["Note"].ToString();
                    u.userID = int.Parse(item["userID"].ToString());
                    u.userName = item["userName"].ToString();
                    u.cDate = DateTime.Parse(item["cDate"].ToString());
                    u.r1 = item["r1"].ToString();
                    u.r2 = item["r2"].ToString();
                    u.r3 = item["r3"].ToString();
                    u.r4 = item["r4"].ToString();
                    u.r5 = item["r5"].ToString();
                    u.p1 = item["p1"].ToString();
                    u.p2 = item["p2"].ToString();
                    Offer.Add(u);
                }
            }
            return Offer;
        }
        public static List<OffersData> ConvertOffersData(this DataTable dt)
        {
            List<OffersData> OfferDAta = new List<OffersData>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    OffersData u = new OffersData();
                    u.id = int.Parse(item["id"].ToString());
                    u.Num = int.Parse(item["Num"].ToString());
                    u.CoName = item["CoName"].ToString();
                    u.OfferBy = item["OfferBy"].ToString();
                    u.cDate = DateTime.Parse(item["cDate"].ToString());
                    OfferDAta.Add(u);
                }
            }
            return OfferDAta;
        }
        public static List<TaxInfo> ConvertTaxInfo(this DataTable dt)
        {
            List<TaxInfo> taxes = new List<TaxInfo>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    TaxInfo u = new TaxInfo();
                    u.id = int.Parse(item["id"].ToString());
                    u.TaxNum = item["TaxNum"].ToString();
                    u.CoRecord = item["CoRecord"].ToString();
                    taxes.Add(u);
                }
            }
            return taxes;
        }
    }
}