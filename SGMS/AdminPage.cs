﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;
using System.Reflection;

namespace SGMS
{
    public partial class AdminPage : UserControl
    {
        public AdminPage()
        {
            InitializeComponent();
            Roles();
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Ver.Text = Login.arNumber(String.Format("{0}", version));
        }

        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadStoreInfo();
            LoadMonth();
            FinalCheck();
        }
        private static List<Setting> SettingP = new List<Setting>();
        private void LoadStoreInfo()
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var result = DBConnection.SqliteCommand("SELECT *  FROM Settings where id <=@id", dic);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var _id = int.Parse(result.Rows[i]["id"].ToString());
                var _StoreName = result.Rows[i]["StoreName"].ToString();
                var _StoreLogo = result.Rows[i]["StoreLogo"].ToString();
                var p = new Setting
                {
                    id = _id,
                    StoreName = _StoreName,
                    StoreLogo = _StoreLogo,
                };
                SettingP.Add(p);
            }

            int nid = 1;
            var cup = SettingP.Find(i => i.id == nid);
            var nStoreName = cup.StoreName.ToString();
            var nStoreLogo = cup.StoreLogo.ToString();

            StoreName.Text = nStoreName;
            StoreLogo.ImageLocation = nStoreLogo;
        }
        private void LoadMonth()
        {
            LoadNewSales();
            LoadNewPu();
            LoadNewUnPaidSales();
            LoadNewUnPaidPu();
            LoadNewEarn();
            LoadNewEX();
        }
        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            ViewPaidS viewPS = new ViewPaidS();
            viewPS.ShowDialog();
        }
        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            ViewPaidP viewPP = new ViewPaidP();
            viewPP.ShowDialog();
        }
        private void LoadNewSales()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = y + "-" + m + "-" + "01";
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(totalprice) totalprice from Sells where ddate between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    label14.Text = Login.arNumber(result.Rows[0]["totalprice"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewPu()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = y + "-" + m + "-" + "01";
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(TotalBuy) TotalBuy from Purchases where Date_ct between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    label17.Text = Login.arNumber(result.Rows[0]["TotalBuy"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewUnPaidSales()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
                string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(unpaid) unpaid from CashInvS where invDateTime between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    label19.Text = Login.arNumber(result.Rows[0]["unpaid"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewUnPaidPu()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");

                string f = y + "-" + m + "-" + "01" + " " + "00:00:00";
                string t = y + "-" + m + "-" + "31" + " " + "23:59:59";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(unpaid) unpaid from CashInvD where invDateTime between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    label18.Text = Login.arNumber(result.Rows[0]["unpaid"].ToString());
                }

            }
            catch
            {

            }
        }
        private void LoadNewEarn()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = y + "-" + m + "-" + "01";
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(earn) earn from Sells where ddate between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    label16.Text = Login.arNumber(result.Rows[0]["earn"].ToString());
                }
            }
            catch
            {

            }
        }
        private void LoadNewEX()
        {
            try
            {
                var m = DateTime.Now.ToString("MM");
                string y = DateTime.Now.ToString("yyyy");
                string f = y + "-" + m + "-" + "01";
                string t = y + "-" + m + "-" + "31";
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("df", f);
                dic.Add("dt", t);
                var result = DBConnection.SqliteCommand("Select sum(Cash) Cash from PExpenses where r_Date between '" + f + "' and '" + t + "'", dic);
                if (result == null) return;
                if (result.Rows.Count > 0)
                {

                    label15.Text = Login.arNumber(result.Rows[0]["Cash"].ToString());
                }
            }
            catch
            {

            }
        }
        private void bunifuFlatButton6_Click(object sender, EventArgs e)
        {
            invTypes Newnow = new invTypes();
            Newnow.FormClosed += NewInvoiceClosed;
            Newnow.ShowDialog();
        }
        void NewInvoiceClosed(object sender, FormClosedEventArgs e)
        {
            LoadMonth();
        }
        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            ViewBAD viewB = new ViewBAD();
            viewB.ShowDialog();
        }
        private void btnDel_Click(object sender, EventArgs e)
        {
            ViewR viewre = new ViewR();
            viewre.ShowDialog();
        }
        private void FinalCheck()
        {
            if (string.IsNullOrEmpty(label14.Text) || label14.Text == "٠")
            {
                label14.Text = "لا يوجد";
            }
            if (string.IsNullOrEmpty(label17.Text) || label17.Text == "٠")
            {
                label17.Text = "لا يوجد";
            }
            if (string.IsNullOrEmpty(label19.Text) || label19.Text == "٠")
            {
                label19.Text = "لا يوجد";
            }
            if (string.IsNullOrEmpty(label16.Text) || label16.Text == "٠")
            {
                label16.Text = "لا يوجد";
            }
            if (string.IsNullOrEmpty(label15.Text) || label15.Text == "٠")
            {
                label15.Text = "لا يوجد";
            }
            if (string.IsNullOrEmpty(label18.Text) || label18.Text == "٠")
            {
                label18.Text = "لا يوجد";
            }
        }
        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            NewOffer offer = new NewOffer(0);
            offer.ShowDialog();
        }
        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            ViewOffers Voffer = new ViewOffers();
            Voffer.ShowDialog();
        }
        private void Roles()
        {
            if (Login.r24 == "0")
            {
                bunifuFlatButton4.Visible = false;
                bunifuFlatButton5.Visible = false;
            }

            if (Login.r3 == "0")
            {
                bunifuFlatButton6.Visible = false;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.R))
            {
                if (Login.r5 == "1")
                {
                    ReturnS rs = new ReturnS();
                    rs.FormClosed += NewInvoiceClosed;
                    rs.ShowDialog();
                }

            }
            else if (keyData == Keys.F1)
            {
                if (Login.r3 == "1")
                {
                    NewSale Newnow = new NewSale();
                    Newnow.FormClosed += NewInvoiceClosed;
                    Newnow.ShowDialog();
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

    }
}

