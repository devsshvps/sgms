﻿namespace SGMS
{
    partial class FastAddItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FastAddItem));
            this.AToolbar = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDel = new Bunifu.Framework.UI.BunifuFlatButton();
            this.addFastItems = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.insertBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.itemName = new System.Windows.Forms.TextBox();
            this.itemBarcode = new System.Windows.Forms.TextBox();
            this.ItemsDatagride = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.BarCode11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemiName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemsDatagride)).BeginInit();
            this.SuspendLayout();
            // 
            // AToolbar
            // 
            this.AToolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AToolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.AToolbar.Location = new System.Drawing.Point(0, 0);
            this.AToolbar.Name = "AToolbar";
            this.AToolbar.Size = new System.Drawing.Size(506, 29);
            this.AToolbar.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Controls.Add(this.btnDel);
            this.panel1.Controls.Add(this.addFastItems);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 495);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(506, 57);
            this.panel1.TabIndex = 7;
            // 
            // btnDel
            // 
            this.btnDel.Activecolor = System.Drawing.Color.Maroon;
            this.btnDel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnDel.BackColor = System.Drawing.Color.Maroon;
            this.btnDel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDel.BorderRadius = 7;
            this.btnDel.ButtonText = "حذف";
            this.btnDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDel.DisabledColor = System.Drawing.Color.White;
            this.btnDel.Enabled = false;
            this.btnDel.Font = new System.Drawing.Font("Cairo", 8F);
            this.btnDel.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDel.Iconimage = null;
            this.btnDel.Iconimage_right = null;
            this.btnDel.Iconimage_right_Selected = null;
            this.btnDel.Iconimage_Selected = null;
            this.btnDel.IconMarginLeft = 0;
            this.btnDel.IconMarginRight = 0;
            this.btnDel.IconRightVisible = false;
            this.btnDel.IconRightZoom = 0D;
            this.btnDel.IconVisible = false;
            this.btnDel.IconZoom = 20D;
            this.btnDel.IsTab = false;
            this.btnDel.Location = new System.Drawing.Point(171, 17);
            this.btnDel.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnDel.Name = "btnDel";
            this.btnDel.Normalcolor = System.Drawing.Color.Maroon;
            this.btnDel.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDel.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDel.selected = false;
            this.btnDel.Size = new System.Drawing.Size(65, 23);
            this.btnDel.TabIndex = 45;
            this.btnDel.Text = "حذف";
            this.btnDel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnDel.Textcolor = System.Drawing.Color.White;
            this.btnDel.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // addFastItems
            // 
            this.addFastItems.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.addFastItems.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.addFastItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.addFastItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addFastItems.BorderRadius = 7;
            this.addFastItems.ButtonText = "إضافة المنتجات";
            this.addFastItems.Cursor = System.Windows.Forms.Cursors.Hand;
            this.addFastItems.DisabledColor = System.Drawing.Color.Gray;
            this.addFastItems.Enabled = false;
            this.addFastItems.Font = new System.Drawing.Font("Cairo", 8F);
            this.addFastItems.Iconcolor = System.Drawing.Color.Transparent;
            this.addFastItems.Iconimage = null;
            this.addFastItems.Iconimage_right = null;
            this.addFastItems.Iconimage_right_Selected = null;
            this.addFastItems.Iconimage_Selected = null;
            this.addFastItems.IconMarginLeft = 0;
            this.addFastItems.IconMarginRight = 0;
            this.addFastItems.IconRightVisible = false;
            this.addFastItems.IconRightZoom = 0D;
            this.addFastItems.IconVisible = false;
            this.addFastItems.IconZoom = 20D;
            this.addFastItems.IsTab = false;
            this.addFastItems.Location = new System.Drawing.Point(13, 12);
            this.addFastItems.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.addFastItems.Name = "addFastItems";
            this.addFastItems.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.addFastItems.OnHovercolor = System.Drawing.Color.Green;
            this.addFastItems.OnHoverTextColor = System.Drawing.Color.White;
            this.addFastItems.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.addFastItems.selected = false;
            this.addFastItems.Size = new System.Drawing.Size(138, 33);
            this.addFastItems.TabIndex = 43;
            this.addFastItems.Text = "إضافة المنتجات";
            this.addFastItems.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.addFastItems.Textcolor = System.Drawing.Color.White;
            this.addFastItems.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addFastItems.Click += new System.EventHandler(this.addFastItems_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.insertBtn);
            this.panel2.Controls.Add(this.itemName);
            this.panel2.Controls.Add(this.itemBarcode);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(506, 75);
            this.panel2.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label1.Location = new System.Drawing.Point(171, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 19);
            this.label1.TabIndex = 49;
            this.label1.Text = "إسم المنتج";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Cairo", 7F);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label13.Location = new System.Drawing.Point(372, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 19);
            this.label13.TabIndex = 48;
            this.label13.Text = "الباركود";
            // 
            // insertBtn
            // 
            this.insertBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.insertBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.insertBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.insertBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.insertBtn.BorderRadius = 7;
            this.insertBtn.ButtonText = "";
            this.insertBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.insertBtn.DisabledColor = System.Drawing.Color.Gray;
            this.insertBtn.Enabled = false;
            this.insertBtn.Font = new System.Drawing.Font("Cairo", 10F);
            this.insertBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.insertBtn.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.insertBtn.Iconimage_right = null;
            this.insertBtn.Iconimage_right_Selected = null;
            this.insertBtn.Iconimage_Selected = null;
            this.insertBtn.IconMarginLeft = 25;
            this.insertBtn.IconMarginRight = 0;
            this.insertBtn.IconRightVisible = false;
            this.insertBtn.IconRightZoom = 0D;
            this.insertBtn.IconVisible = true;
            this.insertBtn.IconZoom = 30D;
            this.insertBtn.IsTab = false;
            this.insertBtn.Location = new System.Drawing.Point(31, 29);
            this.insertBtn.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.insertBtn.Name = "insertBtn";
            this.insertBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.insertBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.insertBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.insertBtn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.insertBtn.selected = false;
            this.insertBtn.Size = new System.Drawing.Size(70, 38);
            this.insertBtn.TabIndex = 47;
            this.insertBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.insertBtn.Textcolor = System.Drawing.Color.White;
            this.insertBtn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertBtn.Click += new System.EventHandler(this.insertBtn_Click);
            // 
            // itemName
            // 
            this.itemName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.itemName.Font = new System.Drawing.Font("Cairo", 12F);
            this.itemName.Location = new System.Drawing.Point(116, 29);
            this.itemName.Name = "itemName";
            this.itemName.Size = new System.Drawing.Size(167, 37);
            this.itemName.TabIndex = 46;
            this.itemName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.itemName.TextChanged += new System.EventHandler(this.itemBarcode_TextChanged);
            // 
            // itemBarcode
            // 
            this.itemBarcode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.itemBarcode.Enabled = false;
            this.itemBarcode.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemBarcode.Location = new System.Drawing.Point(310, 29);
            this.itemBarcode.Name = "itemBarcode";
            this.itemBarcode.Size = new System.Drawing.Size(167, 37);
            this.itemBarcode.TabIndex = 45;
            this.itemBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ItemsDatagride
            // 
            this.ItemsDatagride.AllowUserToAddRows = false;
            this.ItemsDatagride.AllowUserToDeleteRows = false;
            this.ItemsDatagride.AllowUserToResizeColumns = false;
            this.ItemsDatagride.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.ItemsDatagride.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ItemsDatagride.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ItemsDatagride.BackgroundColor = System.Drawing.Color.White;
            this.ItemsDatagride.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ItemsDatagride.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ItemsDatagride.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.ItemsDatagride.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemsDatagride.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BarCode11,
            this.ItemiName});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ItemsDatagride.DefaultCellStyle = dataGridViewCellStyle4;
            this.ItemsDatagride.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemsDatagride.DoubleBuffered = true;
            this.ItemsDatagride.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.ItemsDatagride.EnableHeadersVisualStyles = false;
            this.ItemsDatagride.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ItemsDatagride.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ItemsDatagride.HeaderForeColor = System.Drawing.Color.White;
            this.ItemsDatagride.Location = new System.Drawing.Point(0, 104);
            this.ItemsDatagride.Name = "ItemsDatagride";
            this.ItemsDatagride.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ItemsDatagride.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.ItemsDatagride.RowHeadersVisible = false;
            this.ItemsDatagride.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ItemsDatagride.Size = new System.Drawing.Size(506, 391);
            this.ItemsDatagride.TabIndex = 9;
            this.ItemsDatagride.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.ItemsDatagride_RowsAdded);
            this.ItemsDatagride.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.ItemsDatagride_RowsRemoved);
            // 
            // BarCode11
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BarCode11.DefaultCellStyle = dataGridViewCellStyle3;
            this.BarCode11.FillWeight = 58.50328F;
            this.BarCode11.HeaderText = "باركود";
            this.BarCode11.Name = "BarCode11";
            this.BarCode11.ReadOnly = true;
            // 
            // ItemiName
            // 
            this.ItemiName.HeaderText = "إسم المنتج";
            this.ItemiName.Name = "ItemiName";
            this.ItemiName.ReadOnly = true;
            // 
            // FastAddItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(506, 552);
            this.Controls.Add(this.ItemsDatagride);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.AToolbar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FastAddItem";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "إضافة منتجات جديدة";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemsDatagride)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel AToolbar;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuFlatButton addFastItems;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox itemBarcode;
        private System.Windows.Forms.TextBox itemName;
        private Bunifu.Framework.UI.BunifuFlatButton insertBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private Bunifu.Framework.UI.BunifuFlatButton btnDel;
        private Bunifu.Framework.UI.BunifuCustomDataGrid ItemsDatagride;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode11;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemiName;
    }
}