﻿namespace SGMS
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.AToolbar = new System.Windows.Forms.Panel();
            this.AdminNotes = new System.Windows.Forms.Label();
            this.aboutBTN = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AMin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AMax = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AClose = new Bunifu.Framework.UI.BunifuFlatButton();
            this.MainMenu = new System.Windows.Forms.Panel();
            this.tab8 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tabX = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab9 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab7 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab6 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tab1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.SGMS = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Logout = new Bunifu.Framework.UI.BunifuTileButton();
            this.info = new Bunifu.Framework.UI.BunifuTileButton();
            this.myaccount = new Bunifu.Framework.UI.BunifuTileButton();
            this.msg = new Bunifu.Framework.UI.BunifuTileButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.StoreLogo = new System.Windows.Forms.PictureBox();
            this.StoreName = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dat = new System.Windows.Forms.Label();
            this.tim = new System.Windows.Forms.Label();
            this.day = new System.Windows.Forms.Label();
            this.sec = new System.Windows.Forms.Label();
            this.ProgressBar1 = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.ShowPanel = new System.Windows.Forms.Panel();
            this.MouseDetect = new System.Windows.Forms.Timer(this.components);
            this.tip = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.AToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.MainMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StoreLogo)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // AToolbar
            // 
            this.AToolbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AToolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AToolbar.Controls.Add(this.AdminNotes);
            this.AToolbar.Controls.Add(this.aboutBTN);
            this.AToolbar.Controls.Add(this.label1);
            this.AToolbar.Controls.Add(this.pictureBox2);
            this.AToolbar.Controls.Add(this.label2);
            this.AToolbar.Controls.Add(this.AMin);
            this.AToolbar.Controls.Add(this.AMax);
            this.AToolbar.Controls.Add(this.AClose);
            this.bunifuTransition1.SetDecoration(this.AToolbar, BunifuAnimatorNS.DecorationType.None);
            this.AToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.AToolbar.Location = new System.Drawing.Point(0, 0);
            this.AToolbar.Name = "AToolbar";
            this.AToolbar.Size = new System.Drawing.Size(911, 29);
            this.AToolbar.TabIndex = 4;
            this.AToolbar.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDoubleClick);
            this.AToolbar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AToolbar_MouseMove);
            // 
            // AdminNotes
            // 
            this.bunifuTransition1.SetDecoration(this.AdminNotes, BunifuAnimatorNS.DecorationType.None);
            this.AdminNotes.Dock = System.Windows.Forms.DockStyle.Left;
            this.AdminNotes.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdminNotes.ForeColor = System.Drawing.Color.Yellow;
            this.AdminNotes.Location = new System.Drawing.Point(215, 0);
            this.AdminNotes.Name = "AdminNotes";
            this.AdminNotes.Size = new System.Drawing.Size(177, 29);
            this.AdminNotes.TabIndex = 27;
            this.AdminNotes.Text = "ملاحظة إدارية إضغط هنا لعرضها";
            this.AdminNotes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AdminNotes.Click += new System.EventHandler(this.AdminNotes_Click);
            // 
            // aboutBTN
            // 
            this.aboutBTN.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.aboutBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.aboutBTN.BorderRadius = 0;
            this.aboutBTN.ButtonText = "";
            this.aboutBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.aboutBTN, BunifuAnimatorNS.DecorationType.None);
            this.aboutBTN.DisabledColor = System.Drawing.Color.Gray;
            this.aboutBTN.Dock = System.Windows.Forms.DockStyle.Right;
            this.aboutBTN.Iconcolor = System.Drawing.Color.Transparent;
            this.aboutBTN.Iconimage = global::SGMS.Properties.Resources.info_512px;
            this.aboutBTN.Iconimage_right = null;
            this.aboutBTN.Iconimage_right_Selected = null;
            this.aboutBTN.Iconimage_Selected = null;
            this.aboutBTN.IconMarginLeft = 0;
            this.aboutBTN.IconMarginRight = 0;
            this.aboutBTN.IconRightVisible = true;
            this.aboutBTN.IconRightZoom = 0D;
            this.aboutBTN.IconVisible = true;
            this.aboutBTN.IconZoom = 40D;
            this.aboutBTN.IsTab = false;
            this.aboutBTN.Location = new System.Drawing.Point(787, 0);
            this.aboutBTN.Name = "aboutBTN";
            this.aboutBTN.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.aboutBTN.OnHoverTextColor = System.Drawing.Color.White;
            this.aboutBTN.selected = false;
            this.aboutBTN.Size = new System.Drawing.Size(31, 29);
            this.aboutBTN.TabIndex = 26;
            this.aboutBTN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutBTN.Textcolor = System.Drawing.Color.White;
            this.aboutBTN.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.aboutBTN.Click += new System.EventHandler(this.aboutBTN_Click);
            // 
            // label1
            // 
            this.bunifuTransition1.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(38, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 29);
            this.label1.TabIndex = 25;
            this.label1.Text = "برنامج سيلفر لإدارة الحسابات الخاصة";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDoubleClick);
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AToolbar_MouseMove);
            // 
            // pictureBox2
            // 
            this.bunifuTransition1.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::SGMS.Properties.Resources.DB_01;
            this.pictureBox2.Location = new System.Drawing.Point(13, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 29);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDoubleClick);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AToolbar_MouseMove);
            // 
            // label2
            // 
            this.bunifuTransition1.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 29);
            this.label2.TabIndex = 23;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDoubleClick);
            this.label2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AToolbar_MouseMove);
            // 
            // AMin
            // 
            this.AMin.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AMin.BorderRadius = 0;
            this.AMin.ButtonText = "";
            this.AMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.AMin, BunifuAnimatorNS.DecorationType.None);
            this.AMin.DisabledColor = System.Drawing.Color.Gray;
            this.AMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.AMin.Iconcolor = System.Drawing.Color.Transparent;
            this.AMin.Iconimage = global::SGMS.Properties.Resources._1_03;
            this.AMin.Iconimage_right = null;
            this.AMin.Iconimage_right_Selected = null;
            this.AMin.Iconimage_Selected = null;
            this.AMin.IconMarginLeft = 0;
            this.AMin.IconMarginRight = 0;
            this.AMin.IconRightVisible = true;
            this.AMin.IconRightZoom = 0D;
            this.AMin.IconVisible = true;
            this.AMin.IconZoom = 40D;
            this.AMin.IsTab = false;
            this.AMin.Location = new System.Drawing.Point(818, 0);
            this.AMin.Name = "AMin";
            this.AMin.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.AMin.OnHoverTextColor = System.Drawing.Color.White;
            this.AMin.selected = false;
            this.AMin.Size = new System.Drawing.Size(31, 29);
            this.AMin.TabIndex = 20;
            this.AMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AMin.Textcolor = System.Drawing.Color.White;
            this.AMin.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AMin.Click += new System.EventHandler(this.AMin_Click);
            // 
            // AMax
            // 
            this.AMax.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AMax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMax.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AMax.BorderRadius = 0;
            this.AMax.ButtonText = "";
            this.AMax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.AMax, BunifuAnimatorNS.DecorationType.None);
            this.AMax.DisabledColor = System.Drawing.Color.Gray;
            this.AMax.Dock = System.Windows.Forms.DockStyle.Right;
            this.AMax.Iconcolor = System.Drawing.Color.Transparent;
            this.AMax.Iconimage = global::SGMS.Properties.Resources._1_04;
            this.AMax.Iconimage_right = null;
            this.AMax.Iconimage_right_Selected = null;
            this.AMax.Iconimage_Selected = null;
            this.AMax.IconMarginLeft = 0;
            this.AMax.IconMarginRight = 0;
            this.AMax.IconRightVisible = true;
            this.AMax.IconRightZoom = 0D;
            this.AMax.IconVisible = true;
            this.AMax.IconZoom = 40D;
            this.AMax.IsTab = false;
            this.AMax.Location = new System.Drawing.Point(849, 0);
            this.AMax.Name = "AMax";
            this.AMax.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMax.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.AMax.OnHoverTextColor = System.Drawing.Color.White;
            this.AMax.selected = false;
            this.AMax.Size = new System.Drawing.Size(31, 29);
            this.AMax.TabIndex = 19;
            this.AMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AMax.Textcolor = System.Drawing.Color.White;
            this.AMax.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AMax.Click += new System.EventHandler(this.AMax_Click);
            // 
            // AClose
            // 
            this.AClose.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AClose.BorderRadius = 0;
            this.AClose.ButtonText = "";
            this.AClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.AClose, BunifuAnimatorNS.DecorationType.None);
            this.AClose.DisabledColor = System.Drawing.Color.Gray;
            this.AClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.AClose.Iconcolor = System.Drawing.Color.Transparent;
            this.AClose.Iconimage = global::SGMS.Properties.Resources._1_02;
            this.AClose.Iconimage_right = null;
            this.AClose.Iconimage_right_Selected = null;
            this.AClose.Iconimage_Selected = null;
            this.AClose.IconMarginLeft = 0;
            this.AClose.IconMarginRight = 0;
            this.AClose.IconRightVisible = true;
            this.AClose.IconRightZoom = 0D;
            this.AClose.IconVisible = true;
            this.AClose.IconZoom = 40D;
            this.AClose.IsTab = false;
            this.AClose.Location = new System.Drawing.Point(880, 0);
            this.AClose.Name = "AClose";
            this.AClose.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.OnHovercolor = System.Drawing.Color.Red;
            this.AClose.OnHoverTextColor = System.Drawing.Color.White;
            this.AClose.selected = false;
            this.AClose.Size = new System.Drawing.Size(31, 29);
            this.AClose.TabIndex = 18;
            this.AClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AClose.Textcolor = System.Drawing.Color.White;
            this.AClose.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AClose.Click += new System.EventHandler(this.AClose_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.MainMenu.Controls.Add(this.tab8);
            this.MainMenu.Controls.Add(this.tabX);
            this.MainMenu.Controls.Add(this.tab9);
            this.MainMenu.Controls.Add(this.tab7);
            this.MainMenu.Controls.Add(this.tab6);
            this.MainMenu.Controls.Add(this.tab5);
            this.MainMenu.Controls.Add(this.tab4);
            this.MainMenu.Controls.Add(this.tab3);
            this.MainMenu.Controls.Add(this.tab2);
            this.MainMenu.Controls.Add(this.tab1);
            this.MainMenu.Controls.Add(this.panel2);
            this.bunifuTransition1.SetDecoration(this.MainMenu, BunifuAnimatorNS.DecorationType.None);
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Right;
            this.MainMenu.Location = new System.Drawing.Point(861, 29);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(50, 616);
            this.MainMenu.TabIndex = 9;
            // 
            // tab8
            // 
            this.tab8.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab8.BorderRadius = 0;
            this.tab8.ButtonText = "الإعدادات   ";
            this.tab8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab8, BunifuAnimatorNS.DecorationType.None);
            this.tab8.DisabledColor = System.Drawing.Color.Gray;
            this.tab8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab8.Iconcolor = System.Drawing.Color.Transparent;
            this.tab8.Iconimage = global::SGMS.Properties.Resources.settings_filled_100px;
            this.tab8.Iconimage_right = null;
            this.tab8.Iconimage_right_Selected = null;
            this.tab8.Iconimage_Selected = null;
            this.tab8.IconMarginLeft = 0;
            this.tab8.IconMarginRight = 0;
            this.tab8.IconRightVisible = true;
            this.tab8.IconRightZoom = 0D;
            this.tab8.IconVisible = true;
            this.tab8.IconZoom = 40D;
            this.tab8.IsTab = false;
            this.tab8.Location = new System.Drawing.Point(0, 548);
            this.tab8.Name = "tab8";
            this.tab8.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab8.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab8.OnHoverTextColor = System.Drawing.Color.White;
            this.tab8.selected = false;
            this.tab8.Size = new System.Drawing.Size(50, 48);
            this.tab8.TabIndex = 43;
            this.tab8.Tag = "7";
            this.tab8.Text = "الإعدادات   ";
            this.tab8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab8.Textcolor = System.Drawing.Color.White;
            this.tab8.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab8.Click += new System.EventHandler(this.tab8_Click);
            // 
            // tabX
            // 
            this.tabX.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tabX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tabX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabX.BorderRadius = 0;
            this.tabX.ButtonText = "الماليات   ";
            this.tabX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tabX, BunifuAnimatorNS.DecorationType.None);
            this.tabX.DisabledColor = System.Drawing.Color.Gray;
            this.tabX.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabX.Iconcolor = System.Drawing.Color.Transparent;
            this.tabX.Iconimage = global::SGMS.Properties.Resources.low_price_60px;
            this.tabX.Iconimage_right = null;
            this.tabX.Iconimage_right_Selected = null;
            this.tabX.Iconimage_Selected = null;
            this.tabX.IconMarginLeft = 0;
            this.tabX.IconMarginRight = 0;
            this.tabX.IconRightVisible = true;
            this.tabX.IconRightZoom = 0D;
            this.tabX.IconVisible = true;
            this.tabX.IconZoom = 40D;
            this.tabX.IsTab = true;
            this.tabX.Location = new System.Drawing.Point(0, 500);
            this.tabX.Name = "tabX";
            this.tabX.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tabX.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tabX.OnHoverTextColor = System.Drawing.Color.White;
            this.tabX.selected = false;
            this.tabX.Size = new System.Drawing.Size(50, 48);
            this.tabX.TabIndex = 42;
            this.tabX.Tag = "7";
            this.tabX.Text = "الماليات   ";
            this.tabX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tabX.Textcolor = System.Drawing.Color.White;
            this.tabX.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tabX.Click += new System.EventHandler(this.tabX_Click);
            // 
            // tab9
            // 
            this.tab9.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab9.BorderRadius = 0;
            this.tab9.ButtonText = "حركة الخزنه   ";
            this.tab9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab9, BunifuAnimatorNS.DecorationType.None);
            this.tab9.DisabledColor = System.Drawing.Color.Gray;
            this.tab9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab9.Iconcolor = System.Drawing.Color.Transparent;
            this.tab9.Iconimage = global::SGMS.Properties.Resources.cash_in_hand_filled_100px;
            this.tab9.Iconimage_right = null;
            this.tab9.Iconimage_right_Selected = null;
            this.tab9.Iconimage_Selected = null;
            this.tab9.IconMarginLeft = 0;
            this.tab9.IconMarginRight = 0;
            this.tab9.IconRightVisible = true;
            this.tab9.IconRightZoom = 0D;
            this.tab9.IconVisible = true;
            this.tab9.IconZoom = 40D;
            this.tab9.IsTab = true;
            this.tab9.Location = new System.Drawing.Point(0, 452);
            this.tab9.Name = "tab9";
            this.tab9.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab9.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab9.OnHoverTextColor = System.Drawing.Color.White;
            this.tab9.selected = false;
            this.tab9.Size = new System.Drawing.Size(50, 48);
            this.tab9.TabIndex = 41;
            this.tab9.Tag = "7";
            this.tab9.Text = "حركة الخزنه   ";
            this.tab9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab9.Textcolor = System.Drawing.Color.White;
            this.tab9.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab9.Click += new System.EventHandler(this.tab9_Click);
            // 
            // tab7
            // 
            this.tab7.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab7.BorderRadius = 0;
            this.tab7.ButtonText = "الموردين   ";
            this.tab7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab7, BunifuAnimatorNS.DecorationType.None);
            this.tab7.DisabledColor = System.Drawing.Color.Gray;
            this.tab7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab7.Iconcolor = System.Drawing.Color.Transparent;
            this.tab7.Iconimage = global::SGMS.Properties.Resources.contacts_48px;
            this.tab7.Iconimage_right = null;
            this.tab7.Iconimage_right_Selected = null;
            this.tab7.Iconimage_Selected = null;
            this.tab7.IconMarginLeft = 0;
            this.tab7.IconMarginRight = 0;
            this.tab7.IconRightVisible = true;
            this.tab7.IconRightZoom = 0D;
            this.tab7.IconVisible = true;
            this.tab7.IconZoom = 40D;
            this.tab7.IsTab = true;
            this.tab7.Location = new System.Drawing.Point(0, 404);
            this.tab7.Name = "tab7";
            this.tab7.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab7.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab7.OnHoverTextColor = System.Drawing.Color.White;
            this.tab7.selected = false;
            this.tab7.Size = new System.Drawing.Size(50, 48);
            this.tab7.TabIndex = 40;
            this.tab7.Tag = "6";
            this.tab7.Text = "الموردين   ";
            this.tab7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab7.Textcolor = System.Drawing.Color.White;
            this.tab7.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab7.Click += new System.EventHandler(this.tab7_Click);
            // 
            // tab6
            // 
            this.tab6.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab6.BorderRadius = 0;
            this.tab6.ButtonText = "العملاء   ";
            this.tab6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab6, BunifuAnimatorNS.DecorationType.None);
            this.tab6.DisabledColor = System.Drawing.Color.Gray;
            this.tab6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab6.Iconcolor = System.Drawing.Color.Transparent;
            this.tab6.Iconimage = global::SGMS.Properties.Resources.contacts_filled_100px;
            this.tab6.Iconimage_right = null;
            this.tab6.Iconimage_right_Selected = null;
            this.tab6.Iconimage_Selected = null;
            this.tab6.IconMarginLeft = 0;
            this.tab6.IconMarginRight = 0;
            this.tab6.IconRightVisible = true;
            this.tab6.IconRightZoom = 0D;
            this.tab6.IconVisible = true;
            this.tab6.IconZoom = 40D;
            this.tab6.IsTab = true;
            this.tab6.Location = new System.Drawing.Point(0, 356);
            this.tab6.Name = "tab6";
            this.tab6.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab6.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab6.OnHoverTextColor = System.Drawing.Color.White;
            this.tab6.selected = false;
            this.tab6.Size = new System.Drawing.Size(50, 48);
            this.tab6.TabIndex = 39;
            this.tab6.Tag = "5";
            this.tab6.Text = "العملاء   ";
            this.tab6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab6.Textcolor = System.Drawing.Color.White;
            this.tab6.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab6.Click += new System.EventHandler(this.tab6_Click);
            // 
            // tab5
            // 
            this.tab5.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab5.BorderRadius = 0;
            this.tab5.ButtonText = "المنتجات   ";
            this.tab5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab5, BunifuAnimatorNS.DecorationType.None);
            this.tab5.DisabledColor = System.Drawing.Color.Gray;
            this.tab5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab5.Iconcolor = System.Drawing.Color.Transparent;
            this.tab5.Iconimage = global::SGMS.Properties.Resources.bookmark_480px;
            this.tab5.Iconimage_right = null;
            this.tab5.Iconimage_right_Selected = null;
            this.tab5.Iconimage_Selected = null;
            this.tab5.IconMarginLeft = 0;
            this.tab5.IconMarginRight = 0;
            this.tab5.IconRightVisible = true;
            this.tab5.IconRightZoom = 0D;
            this.tab5.IconVisible = true;
            this.tab5.IconZoom = 40D;
            this.tab5.IsTab = true;
            this.tab5.Location = new System.Drawing.Point(0, 308);
            this.tab5.Name = "tab5";
            this.tab5.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab5.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab5.OnHoverTextColor = System.Drawing.Color.White;
            this.tab5.selected = false;
            this.tab5.Size = new System.Drawing.Size(50, 48);
            this.tab5.TabIndex = 38;
            this.tab5.Tag = "4";
            this.tab5.Text = "المنتجات   ";
            this.tab5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab5.Textcolor = System.Drawing.Color.White;
            this.tab5.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab5.Click += new System.EventHandler(this.tab5_Click);
            // 
            // tab4
            // 
            this.tab4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab4.BorderRadius = 0;
            this.tab4.ButtonText = "التقارير   ";
            this.tab4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab4, BunifuAnimatorNS.DecorationType.None);
            this.tab4.DisabledColor = System.Drawing.Color.Gray;
            this.tab4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab4.Iconcolor = System.Drawing.Color.Transparent;
            this.tab4.Iconimage = global::SGMS.Properties.Resources.file_filled_100px;
            this.tab4.Iconimage_right = null;
            this.tab4.Iconimage_right_Selected = null;
            this.tab4.Iconimage_Selected = null;
            this.tab4.IconMarginLeft = 0;
            this.tab4.IconMarginRight = 0;
            this.tab4.IconRightVisible = true;
            this.tab4.IconRightZoom = 0D;
            this.tab4.IconVisible = true;
            this.tab4.IconZoom = 40D;
            this.tab4.IsTab = true;
            this.tab4.Location = new System.Drawing.Point(0, 260);
            this.tab4.Name = "tab4";
            this.tab4.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab4.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab4.OnHoverTextColor = System.Drawing.Color.White;
            this.tab4.selected = false;
            this.tab4.Size = new System.Drawing.Size(50, 48);
            this.tab4.TabIndex = 37;
            this.tab4.Tag = "3";
            this.tab4.Text = "التقارير   ";
            this.tab4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab4.Textcolor = System.Drawing.Color.White;
            this.tab4.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab4.Click += new System.EventHandler(this.tab4_Click);
            // 
            // tab3
            // 
            this.tab3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab3.BorderRadius = 0;
            this.tab3.ButtonText = "المشتريات   ";
            this.tab3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab3, BunifuAnimatorNS.DecorationType.None);
            this.tab3.DisabledColor = System.Drawing.Color.Gray;
            this.tab3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab3.Iconcolor = System.Drawing.Color.Transparent;
            this.tab3.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.tab3.Iconimage_right = null;
            this.tab3.Iconimage_right_Selected = null;
            this.tab3.Iconimage_Selected = null;
            this.tab3.IconMarginLeft = 0;
            this.tab3.IconMarginRight = 0;
            this.tab3.IconRightVisible = true;
            this.tab3.IconRightZoom = 0D;
            this.tab3.IconVisible = true;
            this.tab3.IconZoom = 40D;
            this.tab3.IsTab = true;
            this.tab3.Location = new System.Drawing.Point(0, 212);
            this.tab3.Name = "tab3";
            this.tab3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab3.OnHoverTextColor = System.Drawing.Color.White;
            this.tab3.selected = false;
            this.tab3.Size = new System.Drawing.Size(50, 48);
            this.tab3.TabIndex = 36;
            this.tab3.Tag = "2";
            this.tab3.Text = "المشتريات   ";
            this.tab3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab3.Textcolor = System.Drawing.Color.White;
            this.tab3.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab3.Click += new System.EventHandler(this.tab3_Click);
            // 
            // tab2
            // 
            this.tab2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab2.BorderRadius = 0;
            this.tab2.ButtonText = "المبيعات   ";
            this.tab2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab2, BunifuAnimatorNS.DecorationType.None);
            this.tab2.DisabledColor = System.Drawing.Color.Gray;
            this.tab2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab2.Iconcolor = System.Drawing.Color.Transparent;
            this.tab2.Iconimage = global::SGMS.Properties.Resources.restart_filled_500px;
            this.tab2.Iconimage_right = null;
            this.tab2.Iconimage_right_Selected = null;
            this.tab2.Iconimage_Selected = null;
            this.tab2.IconMarginLeft = 0;
            this.tab2.IconMarginRight = 0;
            this.tab2.IconRightVisible = true;
            this.tab2.IconRightZoom = 0D;
            this.tab2.IconVisible = true;
            this.tab2.IconZoom = 40D;
            this.tab2.IsTab = true;
            this.tab2.Location = new System.Drawing.Point(0, 164);
            this.tab2.Name = "tab2";
            this.tab2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab2.OnHoverTextColor = System.Drawing.Color.White;
            this.tab2.selected = false;
            this.tab2.Size = new System.Drawing.Size(50, 48);
            this.tab2.TabIndex = 35;
            this.tab2.Tag = "1";
            this.tab2.Text = "المبيعات   ";
            this.tab2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab2.Textcolor = System.Drawing.Color.White;
            this.tab2.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab2.Click += new System.EventHandler(this.tab2_Click);
            // 
            // tab1
            // 
            this.tab1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab1.BorderRadius = 0;
            this.tab1.ButtonText = "الرئيسية   ";
            this.tab1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.tab1, BunifuAnimatorNS.DecorationType.None);
            this.tab1.DisabledColor = System.Drawing.Color.Gray;
            this.tab1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab1.Iconcolor = System.Drawing.Color.Transparent;
            this.tab1.Iconimage = global::SGMS.Properties.Resources.home_filled_100px;
            this.tab1.Iconimage_right = null;
            this.tab1.Iconimage_right_Selected = null;
            this.tab1.Iconimage_Selected = null;
            this.tab1.IconMarginLeft = 0;
            this.tab1.IconMarginRight = 0;
            this.tab1.IconRightVisible = true;
            this.tab1.IconRightZoom = 0D;
            this.tab1.IconVisible = true;
            this.tab1.IconZoom = 40D;
            this.tab1.IsTab = true;
            this.tab1.Location = new System.Drawing.Point(0, 116);
            this.tab1.Name = "tab1";
            this.tab1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab1.OnHoverTextColor = System.Drawing.Color.White;
            this.tab1.selected = true;
            this.tab1.Size = new System.Drawing.Size(50, 48);
            this.tab1.TabIndex = 34;
            this.tab1.Tag = "0";
            this.tab1.Text = "الرئيسية   ";
            this.tab1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab1.Textcolor = System.Drawing.Color.White;
            this.tab1.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab1.Click += new System.EventHandler(this.tab1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Logo);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.SGMS);
            this.panel2.Controls.Add(this.label3);
            this.bunifuTransition1.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(50, 116);
            this.panel2.TabIndex = 0;
            // 
            // Logo
            // 
            this.bunifuTransition1.SetDecoration(this.Logo, BunifuAnimatorNS.DecorationType.None);
            this.Logo.Image = global::SGMS.Properties.Resources.SGMS_01;
            this.Logo.Location = new System.Drawing.Point(5, 38);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(41, 45);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Logo.TabIndex = 26;
            this.Logo.TabStop = false;
            this.Logo.Click += new System.EventHandler(this.Logo_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTransition1.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox3.Image = global::SGMS.Properties.Resources.DB_01;
            this.pictureBox3.Location = new System.Drawing.Point(34, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(0, 61);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 26;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.Logo_Click);
            // 
            // SGMS
            // 
            this.SGMS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SGMS.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.SGMS, BunifuAnimatorNS.DecorationType.None);
            this.SGMS.Font = new System.Drawing.Font("Anita  Semi-square", 20F);
            this.SGMS.ForeColor = System.Drawing.Color.White;
            this.SGMS.Location = new System.Drawing.Point(28, 64);
            this.SGMS.Name = "SGMS";
            this.SGMS.Size = new System.Drawing.Size(0, 30);
            this.SGMS.TabIndex = 12;
            this.SGMS.Text = "SGMS";
            this.SGMS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SGMS.Click += new System.EventHandler(this.Logo_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTransition1.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Cairo", 7F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(23, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(6, 29);
            this.label3.TabIndex = 26;
            this.label3.Text = "لإدارة الحسابات الخاصة";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuTransition1
            // 
            this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.Rotate;
            this.bunifuTransition1.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(50);
            animation1.RotateCoeff = 1F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.bunifuTransition1.DefaultAnimation = animation1;
            this.bunifuTransition1.Interval = 20;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.panel4);
            this.bunifuTransition1.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 29);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(861, 116);
            this.panel3.TabIndex = 11;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Logout);
            this.panel5.Controls.Add(this.info);
            this.panel5.Controls.Add(this.myaccount);
            this.panel5.Controls.Add(this.msg);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.bunifuTransition1.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(553, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 116);
            this.panel5.TabIndex = 13;
            // 
            // Logout
            // 
            this.Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.Logout.color = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.Logout.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.Logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.Logout, BunifuAnimatorNS.DecorationType.None);
            this.Logout.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.Logout.ForeColor = System.Drawing.Color.White;
            this.Logout.Image = global::SGMS.Properties.Resources.shutdown_52px;
            this.Logout.ImagePosition = 5;
            this.Logout.ImageZoom = 50;
            this.Logout.LabelPosition = 0;
            this.Logout.LabelText = "";
            this.Logout.Location = new System.Drawing.Point(8, 70);
            this.Logout.Margin = new System.Windows.Forms.Padding(6);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(44, 33);
            this.Logout.TabIndex = 17;
            this.Logout.Click += new System.EventHandler(this.Logout_Click);
            // 
            // info
            // 
            this.info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.info.color = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.info.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.info.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.info, BunifuAnimatorNS.DecorationType.None);
            this.info.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.info.ForeColor = System.Drawing.Color.White;
            this.info.Image = ((System.Drawing.Image)(resources.GetObject("info.Image")));
            this.info.ImagePosition = 5;
            this.info.ImageZoom = 50;
            this.info.LabelPosition = 0;
            this.info.LabelText = "";
            this.info.Location = new System.Drawing.Point(55, 70);
            this.info.Margin = new System.Windows.Forms.Padding(6);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(44, 33);
            this.info.TabIndex = 15;
            this.info.Click += new System.EventHandler(this.info_Click);
            // 
            // myaccount
            // 
            this.myaccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.myaccount.color = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.myaccount.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.myaccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.myaccount, BunifuAnimatorNS.DecorationType.None);
            this.myaccount.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.myaccount.ForeColor = System.Drawing.Color.White;
            this.myaccount.Image = ((System.Drawing.Image)(resources.GetObject("myaccount.Image")));
            this.myaccount.ImagePosition = 5;
            this.myaccount.ImageZoom = 50;
            this.myaccount.LabelPosition = 0;
            this.myaccount.LabelText = "";
            this.myaccount.Location = new System.Drawing.Point(102, 70);
            this.myaccount.Margin = new System.Windows.Forms.Padding(6);
            this.myaccount.Name = "myaccount";
            this.myaccount.Size = new System.Drawing.Size(44, 33);
            this.myaccount.TabIndex = 14;
            this.myaccount.Click += new System.EventHandler(this.myaccount_Click);
            // 
            // msg
            // 
            this.msg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.msg.color = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.msg.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.msg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTransition1.SetDecoration(this.msg, BunifuAnimatorNS.DecorationType.None);
            this.msg.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.msg.ForeColor = System.Drawing.Color.White;
            this.msg.Image = ((System.Drawing.Image)(resources.GetObject("msg.Image")));
            this.msg.ImagePosition = 5;
            this.msg.ImageZoom = 50;
            this.msg.LabelPosition = 0;
            this.msg.LabelText = "";
            this.msg.Location = new System.Drawing.Point(149, 70);
            this.msg.Margin = new System.Windows.Forms.Padding(6);
            this.msg.Name = "msg";
            this.msg.Size = new System.Drawing.Size(44, 33);
            this.msg.TabIndex = 13;
            this.msg.Click += new System.EventHandler(this.msg_Click);
            // 
            // label5
            // 
            this.bunifuTransition1.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.label5.Font = new System.Drawing.Font("Cairo", 8F);
            this.label5.ForeColor = System.Drawing.Color.Cyan;
            this.label5.Location = new System.Drawing.Point(53, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 23);
            this.label5.TabIndex = 16;
            this.label5.Text = "مرحباً بعودتك";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.bunifuTransition1.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.label4.Font = new System.Drawing.Font("Cairo", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 44);
            this.label4.TabIndex = 13;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.StoreName);
            this.bunifuTransition1.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 116);
            this.panel1.TabIndex = 16;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.StoreLogo);
            this.bunifuTransition1.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(137, 116);
            this.panel6.TabIndex = 18;
            // 
            // StoreLogo
            // 
            this.bunifuTransition1.SetDecoration(this.StoreLogo, BunifuAnimatorNS.DecorationType.None);
            this.StoreLogo.Location = new System.Drawing.Point(20, 17);
            this.StoreLogo.Name = "StoreLogo";
            this.StoreLogo.Size = new System.Drawing.Size(94, 83);
            this.StoreLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.StoreLogo.TabIndex = 17;
            this.StoreLogo.TabStop = false;
            this.StoreLogo.Tag = "";
            // 
            // StoreName
            // 
            this.StoreName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTransition1.SetDecoration(this.StoreName, BunifuAnimatorNS.DecorationType.None);
            this.StoreName.Font = new System.Drawing.Font("Cairo", 24F);
            this.StoreName.ForeColor = System.Drawing.Color.White;
            this.StoreName.Location = new System.Drawing.Point(146, 17);
            this.StoreName.Name = "StoreName";
            this.StoreName.Size = new System.Drawing.Size(397, 77);
            this.StoreName.TabIndex = 14;
            this.StoreName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel4.Controls.Add(this.dat);
            this.panel4.Controls.Add(this.tim);
            this.panel4.Controls.Add(this.day);
            this.panel4.Controls.Add(this.sec);
            this.panel4.Controls.Add(this.ProgressBar1);
            this.bunifuTransition1.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(753, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(108, 116);
            this.panel4.TabIndex = 12;
            // 
            // dat
            // 
            this.dat.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.dat, BunifuAnimatorNS.DecorationType.None);
            this.dat.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F);
            this.dat.ForeColor = System.Drawing.Color.White;
            this.dat.Location = new System.Drawing.Point(21, 73);
            this.dat.Name = "dat";
            this.dat.Size = new System.Drawing.Size(58, 10);
            this.dat.TabIndex = 17;
            this.dat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tim
            // 
            this.tim.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.tim, BunifuAnimatorNS.DecorationType.None);
            this.tim.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tim.ForeColor = System.Drawing.Color.White;
            this.tim.Location = new System.Drawing.Point(20, 36);
            this.tim.Name = "tim";
            this.tim.Size = new System.Drawing.Size(58, 23);
            this.tim.TabIndex = 15;
            this.tim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // day
            // 
            this.day.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.day, BunifuAnimatorNS.DecorationType.None);
            this.day.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.day.ForeColor = System.Drawing.Color.White;
            this.day.Location = new System.Drawing.Point(18, 59);
            this.day.Name = "day";
            this.day.Size = new System.Drawing.Size(61, 16);
            this.day.TabIndex = 16;
            this.day.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sec
            // 
            this.sec.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition1.SetDecoration(this.sec, BunifuAnimatorNS.DecorationType.None);
            this.sec.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.sec.ForeColor = System.Drawing.Color.White;
            this.sec.Location = new System.Drawing.Point(41, 25);
            this.sec.Name = "sec";
            this.sec.Size = new System.Drawing.Size(19, 12);
            this.sec.TabIndex = 14;
            this.sec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.animated = false;
            this.ProgressBar1.animationIterval = 5;
            this.ProgressBar1.animationSpeed = 300;
            this.ProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.ProgressBar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ProgressBar1.BackgroundImage")));
            this.bunifuTransition1.SetDecoration(this.ProgressBar1, BunifuAnimatorNS.DecorationType.None);
            this.ProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.ProgressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.ProgressBar1.LabelVisible = true;
            this.ProgressBar1.LineProgressThickness = 7;
            this.ProgressBar1.LineThickness = 5;
            this.ProgressBar1.Location = new System.Drawing.Point(-1, 8);
            this.ProgressBar1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ProgressBar1.MaxValue = 60;
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.ProgressBackColor = System.Drawing.Color.White;
            this.ProgressBar1.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.ProgressBar1.Size = new System.Drawing.Size(100, 100);
            this.ProgressBar1.TabIndex = 13;
            this.ProgressBar1.Value = 60;
            // 
            // ShowPanel
            // 
            this.bunifuTransition1.SetDecoration(this.ShowPanel, BunifuAnimatorNS.DecorationType.None);
            this.ShowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShowPanel.Location = new System.Drawing.Point(0, 145);
            this.ShowPanel.Name = "ShowPanel";
            this.ShowPanel.Size = new System.Drawing.Size(861, 500);
            this.ShowPanel.TabIndex = 13;
            // 
            // MouseDetect
            // 
            this.MouseDetect.Enabled = true;
            // 
            // tip
            // 
            this.tip.BackColor = System.Drawing.SystemColors.Highlight;
            this.tip.ForeColor = System.Drawing.Color.White;
            // 
            // toolTip1
            // 
            this.toolTip1.BackColor = System.Drawing.SystemColors.Highlight;
            this.toolTip1.ForeColor = System.Drawing.Color.White;
            // 
            // toolTip2
            // 
            this.toolTip2.BackColor = System.Drawing.SystemColors.Highlight;
            this.toolTip2.ForeColor = System.Drawing.Color.White;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(911, 645);
            this.Controls.Add(this.ShowPanel);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.AToolbar);
            this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "برنامج سيلفر لإدارة الحسابات الخاصة";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.AToolbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.MainMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StoreLogo)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel AToolbar;
        private Bunifu.Framework.UI.BunifuFlatButton AMin;
        private Bunifu.Framework.UI.BunifuFlatButton AMax;
        private Bunifu.Framework.UI.BunifuFlatButton AClose;
        private System.Windows.Forms.Panel MainMenu;
        private Bunifu.Framework.UI.BunifuFlatButton tab9;
        private Bunifu.Framework.UI.BunifuFlatButton tab7;
        private Bunifu.Framework.UI.BunifuFlatButton tab6;
        private Bunifu.Framework.UI.BunifuFlatButton tab5;
        private Bunifu.Framework.UI.BunifuFlatButton tab4;
        private Bunifu.Framework.UI.BunifuFlatButton tab3;
        private Bunifu.Framework.UI.BunifuFlatButton tab2;
        private Bunifu.Framework.UI.BunifuFlatButton tab1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
        private System.Windows.Forms.Timer MouseDetect;
        private Bunifu.Framework.UI.BunifuCustomLabel SGMS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label dat;
        private System.Windows.Forms.Label tim;
        private System.Windows.Forms.Label day;
        private System.Windows.Forms.Label sec;
        private Bunifu.Framework.UI.BunifuCircleProgressbar ProgressBar1;
        private System.Windows.Forms.Panel panel5;
        private Bunifu.Framework.UI.BunifuTileButton msg;
        private Bunifu.Framework.UI.BunifuTileButton info;
        private Bunifu.Framework.UI.BunifuTileButton myaccount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuFlatButton aboutBTN;
        public System.Windows.Forms.Panel ShowPanel;
        private System.Windows.Forms.ToolTip tip;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox StoreLogo;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Label StoreName;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private Bunifu.Framework.UI.BunifuTileButton Logout;
        private Bunifu.Framework.UI.BunifuFlatButton tabX;
        private System.Windows.Forms.Label AdminNotes;
        private Bunifu.Framework.UI.BunifuFlatButton tab8;
    }
}

