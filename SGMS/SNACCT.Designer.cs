﻿namespace SGMS
{
    partial class SNACCT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SNACCT));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.aboutBTN = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AMin = new Bunifu.Framework.UI.BunifuFlatButton();
            this.AClose = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SNAC = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tab3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label3 = new System.Windows.Forms.Label();
            this.UName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.aboutBTN);
            this.panel1.Controls.Add(this.AMin);
            this.panel1.Controls.Add(this.AClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(817, 32);
            this.panel1.TabIndex = 1;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AToolbar_MouseMove);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(38, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 32);
            this.label1.TabIndex = 32;
            this.label1.Text = "تفعيـــــل البرنامـــــــــج";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::SGMS.Properties.Resources.DB_01;
            this.pictureBox2.Location = new System.Drawing.Point(13, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Cairo", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 32);
            this.label2.TabIndex = 30;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // aboutBTN
            // 
            this.aboutBTN.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.aboutBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.aboutBTN.BorderRadius = 0;
            this.aboutBTN.ButtonText = "";
            this.aboutBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aboutBTN.DisabledColor = System.Drawing.Color.Gray;
            this.aboutBTN.Dock = System.Windows.Forms.DockStyle.Right;
            this.aboutBTN.Iconcolor = System.Drawing.Color.Transparent;
            this.aboutBTN.Iconimage = global::SGMS.Properties.Resources.info_512px;
            this.aboutBTN.Iconimage_right = null;
            this.aboutBTN.Iconimage_right_Selected = null;
            this.aboutBTN.Iconimage_Selected = null;
            this.aboutBTN.IconMarginLeft = 0;
            this.aboutBTN.IconMarginRight = 0;
            this.aboutBTN.IconRightVisible = true;
            this.aboutBTN.IconRightZoom = 0D;
            this.aboutBTN.IconVisible = true;
            this.aboutBTN.IconZoom = 40D;
            this.aboutBTN.IsTab = false;
            this.aboutBTN.Location = new System.Drawing.Point(706, 0);
            this.aboutBTN.Name = "aboutBTN";
            this.aboutBTN.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.aboutBTN.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.aboutBTN.OnHoverTextColor = System.Drawing.Color.White;
            this.aboutBTN.selected = false;
            this.aboutBTN.Size = new System.Drawing.Size(37, 32);
            this.aboutBTN.TabIndex = 29;
            this.aboutBTN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.aboutBTN.Textcolor = System.Drawing.Color.White;
            this.aboutBTN.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.aboutBTN.Click += new System.EventHandler(this.aboutBTN_Click);
            // 
            // AMin
            // 
            this.AMin.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AMin.BorderRadius = 0;
            this.AMin.ButtonText = "";
            this.AMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AMin.DisabledColor = System.Drawing.Color.Gray;
            this.AMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.AMin.Iconcolor = System.Drawing.Color.Transparent;
            this.AMin.Iconimage = global::SGMS.Properties.Resources._1_03;
            this.AMin.Iconimage_right = null;
            this.AMin.Iconimage_right_Selected = null;
            this.AMin.Iconimage_Selected = null;
            this.AMin.IconMarginLeft = 0;
            this.AMin.IconMarginRight = 0;
            this.AMin.IconRightVisible = true;
            this.AMin.IconRightZoom = 0D;
            this.AMin.IconVisible = true;
            this.AMin.IconZoom = 40D;
            this.AMin.IsTab = false;
            this.AMin.Location = new System.Drawing.Point(743, 0);
            this.AMin.Name = "AMin";
            this.AMin.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AMin.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.AMin.OnHoverTextColor = System.Drawing.Color.White;
            this.AMin.selected = false;
            this.AMin.Size = new System.Drawing.Size(37, 32);
            this.AMin.TabIndex = 28;
            this.AMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AMin.Textcolor = System.Drawing.Color.White;
            this.AMin.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AMin.Click += new System.EventHandler(this.AMin_Click);
            // 
            // AClose
            // 
            this.AClose.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.AClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AClose.BorderRadius = 0;
            this.AClose.ButtonText = "";
            this.AClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AClose.DisabledColor = System.Drawing.Color.Gray;
            this.AClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.AClose.Iconcolor = System.Drawing.Color.Transparent;
            this.AClose.Iconimage = global::SGMS.Properties.Resources._1_02;
            this.AClose.Iconimage_right = null;
            this.AClose.Iconimage_right_Selected = null;
            this.AClose.Iconimage_Selected = null;
            this.AClose.IconMarginLeft = 0;
            this.AClose.IconMarginRight = 0;
            this.AClose.IconRightVisible = true;
            this.AClose.IconRightZoom = 0D;
            this.AClose.IconVisible = true;
            this.AClose.IconZoom = 40D;
            this.AClose.IsTab = false;
            this.AClose.Location = new System.Drawing.Point(780, 0);
            this.AClose.Name = "AClose";
            this.AClose.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.AClose.OnHovercolor = System.Drawing.Color.Red;
            this.AClose.OnHoverTextColor = System.Drawing.Color.White;
            this.AClose.selected = false;
            this.AClose.Size = new System.Drawing.Size(37, 32);
            this.AClose.TabIndex = 27;
            this.AClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AClose.Textcolor = System.Drawing.Color.White;
            this.AClose.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.AClose.Click += new System.EventHandler(this.AClose_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Cairo", 13F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label4.Location = new System.Drawing.Point(388, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(392, 38);
            this.label4.TabIndex = 34;
            this.label4.Text = "يبدو أنها المره الأولي التي تستخدم فيها البرنامج";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Cairo", 13F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.label5.Location = new System.Drawing.Point(194, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 38);
            this.label5.TabIndex = 35;
            this.label5.Text = " مفتاح التفعيل ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SNAC
            // 
            this.SNAC.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.SNAC.Font = new System.Drawing.Font("Cairo", 9.75F);
            this.SNAC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.SNAC.HintForeColor = System.Drawing.Color.Empty;
            this.SNAC.HintText = "";
            this.SNAC.isPassword = false;
            this.SNAC.LineFocusedColor = System.Drawing.Color.Blue;
            this.SNAC.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.SNAC.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.SNAC.LineThickness = 4;
            this.SNAC.Location = new System.Drawing.Point(106, 300);
            this.SNAC.Margin = new System.Windows.Forms.Padding(4);
            this.SNAC.Name = "SNAC";
            this.SNAC.Size = new System.Drawing.Size(389, 38);
            this.SNAC.TabIndex = 36;
            this.SNAC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SGMS.Properties.Resources.SGMS_01;
            this.pictureBox1.Location = new System.Drawing.Point(692, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 32);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(817, 116);
            this.panel3.TabIndex = 39;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cairo", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(419, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(267, 29);
            this.label6.TabIndex = 41;
            this.label6.Text = "برنامج سيلفر جروب لإدارة المبيعات الخاصة";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cairo", 30F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(429, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(244, 75);
            this.label7.TabIndex = 42;
            this.label7.Text = "2020 - 2021";
            // 
            // tab3
            // 
            this.tab3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.tab3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tab3.BorderRadius = 0;
            this.tab3.ButtonText = "تفعيل البرنامج";
            this.tab3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tab3.DisabledColor = System.Drawing.Color.Gray;
            this.tab3.Iconcolor = System.Drawing.Color.Transparent;
            this.tab3.Iconimage = global::SGMS.Properties.Resources.key_30px;
            this.tab3.Iconimage_right = null;
            this.tab3.Iconimage_right_Selected = null;
            this.tab3.Iconimage_Selected = null;
            this.tab3.IconMarginLeft = 0;
            this.tab3.IconMarginRight = 0;
            this.tab3.IconRightVisible = true;
            this.tab3.IconRightZoom = 0D;
            this.tab3.IconVisible = true;
            this.tab3.IconZoom = 40D;
            this.tab3.IsTab = false;
            this.tab3.Location = new System.Drawing.Point(106, 373);
            this.tab3.Name = "tab3";
            this.tab3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.tab3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.tab3.OnHoverTextColor = System.Drawing.Color.White;
            this.tab3.selected = false;
            this.tab3.Size = new System.Drawing.Size(168, 48);
            this.tab3.TabIndex = 37;
            this.tab3.Tag = "2";
            this.tab3.Text = "تفعيل البرنامج";
            this.tab3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tab3.Textcolor = System.Drawing.Color.White;
            this.tab3.TextFont = new System.Drawing.Font("Cairo", 12F);
            this.tab3.Click += new System.EventHandler(this.tab3_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Cairo", 7F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label3.Location = new System.Drawing.Point(106, 443);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(611, 38);
            this.label3.TabIndex = 40;
            this.label3.Text = "في حالة إذا واجهتك أي مشكلة في تفعيل البرنامج برجاء الإتصال علي إدارة البرنامج : " +
    " 01115352568   ✆   01000955506";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UName
            // 
            this.UName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.UName.Font = new System.Drawing.Font("Cairo", 9.75F);
            this.UName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.UName.HintForeColor = System.Drawing.Color.Empty;
            this.UName.HintText = "";
            this.UName.isPassword = false;
            this.UName.LineFocusedColor = System.Drawing.Color.Blue;
            this.UName.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.UName.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.UName.LineThickness = 4;
            this.UName.Location = new System.Drawing.Point(517, 300);
            this.UName.Margin = new System.Windows.Forms.Padding(4);
            this.UName.Name = "UName";
            this.UName.Size = new System.Drawing.Size(200, 38);
            this.UName.TabIndex = 41;
            this.UName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Cairo", 13F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(79)))), ((int)(((byte)(124)))));
            this.label8.Location = new System.Drawing.Point(511, 258);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(212, 38);
            this.label8.TabIndex = 42;
            this.label8.Text = "الإسم";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SNACCT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(817, 502);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.UName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tab3);
            this.Controls.Add(this.SNAC);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SNACCT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SNACCT";
            this.Load += new System.EventHandler(this.SNACCT_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuFlatButton aboutBTN;
        private Bunifu.Framework.UI.BunifuFlatButton AMin;
        private Bunifu.Framework.UI.BunifuFlatButton AClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuMaterialTextbox SNAC;
        private System.Windows.Forms.Panel panel3;
        private Bunifu.Framework.UI.BunifuFlatButton tab3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuMaterialTextbox UName;
        private System.Windows.Forms.Label label8;
    }
}