﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGMS.Data;
using SGMS.Models;

namespace SGMS
{
    public partial class AccNotice : UserControl
    {
        public AccNotice()
        {
            InitializeComponent();
            LoadActive();
            LoadOld();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }
        private void LoadActive()
        {
            try
            {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", Login.cid);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM Notice where UserID =@id and r = 1", dic).ConvertNotices();
            if (check == null) return;

            foreach (Notice item in check)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(Actview);
                    row.Cells[0].Value = item.remNotice;
                    row.Cells[2].Value = item.showDate.ToString("yyyy-MM-dd");
                    row.Cells[3].Value = item.ct_time.ToString("t");
                    row.Cells[4].Value = item.ct_date.ToString("yyyy-MM-dd");
                    row.Cells[5].Value = item.id;

                    if (item.RemDate == item.showDate)
                    {
                        row.Cells[1].Value = "لا يوجد";
                    }
                    else
                    {
                        row.Cells[1].Value = item.RemDate.ToString("yyyy-MM-dd");
                    }
                    Actview.Rows.Add(row);
            }
            }
            catch { }
        }

        private void LoadOld()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", Login.cid);
                var check = DBConnection.SqliteCommand("SELECT  *  FROM Notice where UserID =@id and r = 0", dic).ConvertNotices();
                if (check == null) return;

                foreach (Notice item in check)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(Oldview);
                    row.Cells[0].Value = item.remNotice;
                    row.Cells[2].Value = item.showDate.ToString("yyyy-MM-dd");
                    row.Cells[3].Value = item.ct_time.ToString("t");
                    row.Cells[4].Value = item.ct_date.ToString("yyyy-MM-dd");
                    row.Cells[5].Value = item.id;

                    if(item.RemDate == item.showDate)
                    {
                        row.Cells[1].Value = "لا يوجد";
                    }else
                    {
                        row.Cells[1].Value = item.RemDate.ToString("yyyy-MM-dd");
                    }
                    Oldview.Rows.Add(row);
                }
            }
            catch { }
        }

        private void Actview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int RowNo;
                RowNo = e.RowIndex;

                ViewNotice1.Text = Actview.Rows[RowNo].Cells[0].Value.ToString();
            }
            catch { }
        }

        private void Oldview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int RowNo;
                RowNo = e.RowIndex;

                ViewNotice2.Text = Oldview.Rows[RowNo].Cells[0].Value.ToString();
            }
            catch { }

        }


    }
}

