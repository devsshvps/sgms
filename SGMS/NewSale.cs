﻿using SGMS.Data;
using SGMS.Models;
using SGMS.Print;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SGMS
{

    public partial class NewSale : MetroFramework.Forms.MetroForm
    {
        int NuM;
        int favBAR;
        double inunpaid12;
        double invpaid12;
        public NewSale()
        {
            InitializeComponent();
            Roles();
            InvoiceNum();
            LoadData();
            itemBox.SelectedIndex = -1;
            FavBar();
        }
        private void FavBar()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@id", 1);
            var check = DBConnection.SqliteCommand("SELECT  *  FROM FavBar where id =@id", dic).ConvertFavBar();
            if (check == null) return;

            foreach (FavBar item in check)
            {
                favBAR = item.Fav;
                if (favBAR == 1)
                {
                    isBarcode.Checked = true;
                    itemBarcode.Select();
                }
            }
        }
        private void LoadData()
        {
            Customers.Clear();
            ItemP.Clear();
            ItemP2.Clear();
            ItemsData();
            CustomersClientsData();
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                if (itemBarcode.Focused == true && itemBarcode.TextLength > 0)
                {
                    iCount.Focus();
                }
                else if (iCount.Focused == true && iCount.TextLength > 0)
                {
                    btnAdd_Click(this, new EventArgs());
                    if (isBarcode.Checked)
                    {
                        itemBarcode.Focus();
                    }
                    else
                    {
                        itemBox.Focus();
                    }
                }

            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private static List<Item> ItemP = new List<Item>();
        private static List<Item2> ItemP2 = new List<Item2>();
        private static List<Customer> Customers = new List<Customer>();
        private void CustomersClientsData()
        {
            Customers.Clear();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("@r", "1");
            var Ccl = DBConnection.SqliteCommand("SELECT *  FROM Customers where r =@r", dic);
            if (Ccl == null) return;
            if (Ccl.Rows.Count > 0)
            {
                sellerName.DataSource = Ccl;
                sellerName.DisplayMember = "Name";
                sellerName.ValueMember = "id";

                for (int i = 0; i < Ccl.Rows.Count; i++)
                {
                    var _id = int.Parse(Ccl.Rows[i]["id"].ToString());
                    var _nameC = Ccl.Rows[i]["Name"].ToString();
                    var _PhoneC = Ccl.Rows[i]["Phone"].ToString();
                    var _Cash = Ccl.Rows[i]["Cash"].ToString();
                    var C = new Customer
                    {
                        id = _id,
                        Name = _nameC,
                        Phone = _PhoneC,
                        Cash = Double.Parse(_Cash)
                    };
                    Customers.Add(C);

                }
            }
        }
        private void ItemsData()
        {
            ItemP.Clear();
            ItemP2.Clear();
            Dictionary<string, object> dics = new Dictionary<string, object>();
            dics.Add("@r", 0);
            var rIt = DBConnection.SqliteCommand("SELECT DISTINCT *  FROM Items where Count >@r", dics);
            if (rIt == null) return;
            if (rIt.Rows.Count > 0)
            {
                itemBox.DataSource = rIt;
                itemBox.DisplayMember = "Name";
                itemBox.ValueMember = "id";


                for (int i = 0; i < rIt.Rows.Count; i++)
                {
                    var _id = int.Parse(rIt.Rows[i]["id"].ToString());
                    var _name = rIt.Rows[i]["Name"].ToString();
                    var _sellprice = rIt.Rows[i]["SellPrice"].ToString();
                    var _buyprice = rIt.Rows[i]["ItemPrice"].ToString();
                    var _count = rIt.Rows[i]["Count"].ToString();
                    var _TS = rIt.Rows[i]["TotalSell"].ToString();
                    var _barc = rIt.Rows[i]["BarCode"].ToString();
                    var _earn = rIt.Rows[i]["Earn"].ToString();
                    var _NumOfSales = rIt.Rows[i]["NumOfSales"].ToString();
                    var p = new Item
                    {
                        id = _id,
                        Name = _name,
                        Count = Double.Parse(_count),
                        TotalSell = Double.Parse(_TS),
                        ItemPrice = Double.Parse(_buyprice),
                        SellPrice = Double.Parse(_sellprice),
                        BarCode = _barc,
                        Earn = Double.Parse(_earn),
                        NumOfSales = Double.Parse(_NumOfSales)
                    };
                    var p2 = new Item2
                    {
                        id = _id,
                        Name = _name,
                        Count = Double.Parse(_count),
                        TotalSell = Double.Parse(_TS),
                        ItemPrice = Double.Parse(_buyprice),
                        SellPrice = Double.Parse(_sellprice),
                        BarCode = _barc,
                        Earn = Double.Parse(_earn),
                        NumOfSales = Double.Parse(_NumOfSales)

                    };
                    ItemP.Add(p);
                    ItemP2.Add(p2);
                }
            }
        }
        private void InvoiceNum()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@id", 1);
                dic.Add("@r", 1);
                var check = DBConnection.SqliteCommand("SELECT MAX(Num) FROM Sells where r=@r", dic);
                if (check == null) return;
                if (check.Rows.Count > 0)
                {
                    NuM = int.Parse(check.Rows[0]["MAX(Num)"].ToString());
                }
                NuM += 1;
                if (NuM >= 1 && NuM < 10)
                {
                    label7.Text = "000000" + NuM.ToString();
                }
                else if (NuM >= 10 && NuM < 100)
                {
                    label7.Text = "00000" + NuM.ToString();
                }
                else if (NuM >= 100 && NuM < 1000)
                {
                    label7.Text = "0000" + NuM.ToString();
                }
                else if (NuM >= 1000 && NuM < 10000)
                {
                    label7.Text = "000" + NuM.ToString();
                }
                else if (NuM >= 10000 && NuM < 100000)
                {
                    label7.Text = "00" + NuM.ToString();
                }
                else if (NuM >= 100000 && NuM < 1000000)
                {
                    label7.Text = "0" + NuM.ToString();
                }
                else if (NuM >= 1000000)
                {
                    label7.Text = NuM.ToString();
                }

            }
            catch { }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int id;
            int uid = 0;
            try
            {
                id = int.Parse(sellerName.SelectedValue.ToString());
                var cup = Customers.Find(ur => ur.id == id);
                uid = cup.id;
            }
            catch
            {

            }

            if (string.IsNullOrWhiteSpace(itemBarcode.Text) || itemBox.SelectedIndex == -1)
            {
                MessageBox.Show("برجاء إختيار منتج بشكل صحيح");
            }
            else if (string.IsNullOrWhiteSpace(iCount.Text))
            {
                MessageBox.Show("برجاء كتابة الكمية التي تريد شرائها بشكل صحيح");
            }
            else if (string.IsNullOrWhiteSpace(Sprice.Text) || double.Parse(Sprice.Text) == 0)
            {
                MessageBox.Show("برجاء كتابة سعر البيع");
            }
            else if (uid <= 0)
            {
                MessageBox.Show("برجاء إختيار العميل بشكل صحيح");
            }
            else
            {
                sellerName.Enabled = false;
                if (uid == 1)
                {
                    paidCash.Enabled = false;
                }
                else
                {
                    paidCash.Enabled = true;
                }

                ///
                bool Found = false;
                if (dataGridView1.Rows.Count > 0)
                {
                    foreach (DataGridViewRow drow in dataGridView1.Rows)
                    {
                        if (Convert.ToString(drow.Cells[0].Value) == itemBarcode.Text)
                        {
                            drow.Cells[2].Value = Convert.ToString(Convert.ToInt16(iCount.Text) + Convert.ToInt16(drow.Cells[2].Value));
                            Found = true;

                            sumMethod();
                            ChangeCount();
                            if (isBarcode.Checked)
                            {
                                itemBarcode.Text = "";
                            }
                            else
                            {
                                itemBarcode.Text = "";
                                itemBox.SelectedIndex = -1;
                            }

                            iCount.Text = "1";
                        }
                    }
                    if (!Found)
                    {
                        DataGridViewRow row = new DataGridViewRow();
                        row.CreateCells(dataGridView1);
                        row.Cells[0].Value = itemBarcode.Text;
                        row.Cells[1].Value = this.itemBox.GetItemText(this.itemBox.SelectedItem);
                        row.Cells[2].Value = iCount.Text;
                        row.Cells[3].Value = Sprice.Text;
                        row.Cells[4].Value = "0";
                        row.Cells[5].Value = bPrice.Text;
                        row.Cells[6].Value = "0";
                        dataGridView1.Rows.Add(row);
                        dataGridView1.CurrentCell = null;
                        ChangeCount();
                        if (isBarcode.Checked)
                        {
                            itemBarcode.Text = "";
                        }
                        else
                        {
                            itemBarcode.Text = "";
                            itemBox.SelectedIndex = -1;
                        }

                        iCount.Text = "1";
                    }
                }
                else
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = itemBarcode.Text;
                    row.Cells[1].Value = this.itemBox.GetItemText(this.itemBox.SelectedItem);
                    row.Cells[2].Value = iCount.Text;
                    row.Cells[3].Value = Sprice.Text;
                    row.Cells[4].Value = "0";
                    row.Cells[5].Value = bPrice.Text;
                    row.Cells[6].Value = "0";
                    dataGridView1.Rows.Add(row);
                    dataGridView1.CurrentCell = null;
                    ChangeCount();
                    if (isBarcode.Checked)
                    {
                        itemBarcode.Text = "";
                    }
                    else
                    {
                        itemBarcode.Text = "";
                        itemBox.SelectedIndex = -1;
                    }

                    iCount.Text = "1";

                }

                ///


                if (newSeller.Text == "تعديل")
                {

                }
                else
                {
                    newSeller.Text = "تعديل";
                    newSeller.BackColor = Color.Red;
                    newSeller.Activecolor = Color.Red;
                    newSeller.Normalcolor = Color.Red;
                    newSeller.OnHovercolor = Color.Crimson;
                }

            }
        }
        private void sumMethod()
        {
            foreach (DataGridViewRow ll in dataGridView1.Rows)
            {
                double tospr; // إجمالي سعر المنتج
                double tobpr; // إجمالي سعر شراء المنتج
                double toEr; // إجمالي المكسب للمنتج

                // حساب إجمالي المنتج
                int TP = ll.Index;
                double co = Convert.ToDouble(dataGridView1.Rows[TP].Cells[2].Value); // الكمية
                double pr = Convert.ToDouble(dataGridView1.Rows[TP].Cells[3].Value); // سعر البيع
                tospr = co * pr;
                dataGridView1.Rows[TP].Cells[4].Value = tospr.ToString();

                // حساب مكسب المنتج

                double Br = Convert.ToDouble(dataGridView1.Rows[TP].Cells[5].Value); // سعر الشراء
                tobpr = co * Br;
                toEr = tospr - tobpr;
                dataGridView1.Rows[TP].Cells[6].Value = toEr.ToString();
            }



            Total.Text = "0";
            label2.Text = "0";
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                //حساب إجمالي الفاتورة
                int n = item.Index;
                var finalsum = (Double.Parse(Total.Text.ToString())
                + Double.Parse(dataGridView1.Rows[n].Cells[4].Value.ToString())).ToString();
                Total.Text = finalsum;
                paidCash.Text = finalsum;
            }

            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                // حساب إجمالي المكسب
                int n = item.Index;
                var finalsum = (Double.Parse(label2.Text.ToString())
                + Double.Parse(dataGridView1.Rows[n].Cells[6].Value.ToString())).ToString();
                label2.Text = finalsum;

            }

        }
        private void btnDel_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count >= 1)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    dataGridView1.Rows.RemoveAt(row.Index);
                    var c = row.Cells[0].Value.ToString();
                    var cup = ItemP.Find(i => i.BarCode == c);
                    var nc = cup.Count;
                    var newc = nc + Double.Parse(row.Cells[2].Value.ToString());
                    cup.Count = newc;
                    sumMethod();
                    if (dataGridView1.SelectedRows.Count <= 0)
                    {
                        paidCash.Text = Total.Text;
                    }
                    itemBarcode.Text = "";
                    itemBox.SelectedValue = -1;
                    bPrice.Text = "0";
                    Sprice.Text = "0";
                    label3.Visible = false;
                    ShowCount.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("برجاء تحديد عنصر للحذف");
            }
        }
        private void iCount_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(iCount.Text, "[^0-9]"))
            {
                MessageBox.Show("برجاء كتابة ارقام صحيحة فقط.");
                iCount.Text = iCount.Text.Remove(iCount.Text.Length - 1);
            }
            try
            {
                var brcode = itemBarcode.Text.ToString();
                var cup = ItemP.Find(i11 => i11.BarCode == brcode);
                var nc = cup.Count;
                double nec = Double.Parse(iCount.Text.ToString());
                if (nec > nc)
                {
                    MessageBox.Show(nc + " " + ":" + " " + "نأسف كمية المخزون الحالية");
                    iCount.Text = nc.ToString();
                }
            }
            catch { }

        }
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            sumMethod();
            AddIn.Enabled = true;
        }
        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            if (dataGridView1.Rows.Count >= 1)
            {
                AddIn.Enabled = true;
            }
            else
            {
                AddIn.Enabled = false;
            }
        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewTextBoxEditingControl tb)
            {
                tb.KeyDown -= dataGridView1_KeyDown;
                tb.KeyDown += dataGridView1_KeyDown;
            }
        }
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Delete))
            {
                if (dataGridView1.SelectedRows.Count >= 1)
                {
                    foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                    {
                        dataGridView1.Rows.RemoveAt(row.Index);
                        var c = row.Cells[0].Value.ToString();
                        var cup = ItemP.Find(i => i.BarCode == c);
                        var nc = cup.Count;
                        var newc = nc + Double.Parse(row.Cells[2].Value.ToString());
                        cup.Count = newc;
                        sumMethod();
                        if (dataGridView1.SelectedRows.Count <= 0)
                        {
                            paidCash.Text = Total.Text;
                        }
                        itemBarcode.Text = "";
                        itemBox.SelectedValue = -1;
                        bPrice.Text = "0";
                        Sprice.Text = "0";
                        label3.Visible = false;
                        ShowCount.Visible = false;
                    }
                }
                else
                {
                    MessageBox.Show("برجاء تحديد عنصر للحذف");
                }
            }
        }
        private void isBarcode_CheckedChanged(object sender, EventArgs e)
        {
            if (isBarcode.Checked == true)
            {
                itemBox.Enabled = false;
                itemBarcode.Enabled = true;
            }
            else
            {
                itemBox.Enabled = true;
                itemBarcode.Enabled = false;
            }
        }
        private void itemBarcode_TextChanged(object sender, EventArgs e)
        {
            if (itemBarcode.Enabled == true)
            {
                itemBox.SelectedValue = -1;
                bPrice.Text = "0";
                Sprice.Text = "0";
                label3.Visible = false;
                ShowCount.Visible = false;
                try
                {
                    var brcode = itemBarcode.Text.ToString();
                    var cup = ItemP.Find(i11 => i11.BarCode == brcode);
                    var idd = cup.id.ToString();
                    itemBox.SelectedValue = int.Parse(idd.ToString());
                    bPrice.Text = cup.ItemPrice.ToString();
                    Sprice.Text = cup.SellPrice.ToString();
                    var nc = cup.Count;
                    label3.Visible = true;
                    ShowCount.Visible = true;
                    ShowCount.Text = nc.ToString();
                    CheckCount();

                }
                catch { }
            }
            else
            {

            }

        }
        private void itemBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (itemBox.Enabled == true)
            {
                itemBarcode.Text = "";
                bPrice.Text = "0";
                Sprice.Text = "0";
                label3.Visible = false;
                ShowCount.Visible = false;
                try
                {
                    int id = 0;
                    if (int.TryParse(itemBox?.SelectedValue?.ToString(), out id))
                    {
                        var cup = ItemP.Find(i => i.id == id);
                    itemBarcode.Text = cup.BarCode.ToString();
                    bPrice.Text = cup.ItemPrice.ToString();
                    Sprice.Text = cup.SellPrice.ToString();
                    var nc = cup.Count;
                    label3.Visible = true;
                    ShowCount.Visible = true;
                    ShowCount.Text = nc.ToString();
                        CheckCount();
                    }
                }
                catch
                {

                }
            }
            else
            {

            }
        }
        private void sellerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(sellerName.SelectedValue.ToString());
                var cup = Customers.Find(i => i.id == id);
                var ccid = cup.id;
                var neE = cup.Cash;
                if (ccid == 1)
                {
                    paidCash.Enabled = false;
                    u1.Visible = false;
                    u2.Visible = false;
                }
                else if (neE > 0)
                {
                    paidCash.Enabled = true;
                    u1.Visible = true;
                    u2.Visible = true;
                    u2.Text = neE.ToString();
                }
                else
                {
                    paidCash.Enabled = true;
                }
            }
            catch { }

        }
        private void CheckCount()
        {
            try
            {
                var id = int.Parse(itemBox.SelectedValue.ToString());
                var CC = ItemP.Find(i => i.id == id);
                double cco = CC.Count;
                double cneed = double.Parse(iCount.Text);
                if (cco == 0)
                {
                    MessageBox.Show("المنتج المطلوب منتهي من المخزون");
                    btnAdd.Enabled = false;
                }
                else if (cco < cneed)
                {
                    MessageBox.Show(cco + " " + ":" + " " + "نأسف كمية المخزون الحالية");
                    btnAdd.Enabled = false;
                }
                else if (cco == 1)
                {
                    DialogResult dialogResult = MessageBox.Show("هذا المنتج متبقي منه عدد 1 قطعة فقط هل ترغب في الإستمرار", "تحذير كمية المنتج قليلة", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        btnAdd.Enabled = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        btnAdd.Enabled = false;
                    }
                }
                else if (cco == 2)
                {
                    DialogResult dialogResult = MessageBox.Show("هذا المنتج متبقي منه عدد 2 قطعة فقط هل ترغب في الإستمرار", "تحذير كمية المنتج قليلة", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        btnAdd.Enabled = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        btnAdd.Enabled = false;
                    }
                }
                else if (cco == 3)
                {
                    DialogResult dialogResult = MessageBox.Show("هذا المنتج متبقي منه عدد 3 قطعة فقط هل ترغب في الإستمرار", "تحذير كمية المنتج قليلة", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        btnAdd.Enabled = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        btnAdd.Enabled = false;
                    }
                }
                else
                {
                    btnAdd.Enabled = true;
                }

            }
            catch (Exception)
            {
            }
        }
        private void ChangeCount()
        {
            var CC = ItemP.Find(i => i.BarCode == itemBarcode.Text);
            var nc = CC.Count;
            var newc = nc - Double.Parse(iCount.Text);
            if (nc == nc)
            {
                CC.Count = newc;
            }
        }
        private void paidCash_EnabledChanged(object sender, EventArgs e)
        {
            if (paidCash.Enabled == false)
            {
                paidCash.Text = Total.Text;
            }
        }
        private void paidCash_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(paidCash.Text))
            {
                paidCash.Text = Total.Text;
            }
            try
            {
                double finalsum = Math.Round(double.Parse(Total.Text), 2, MidpointRounding.AwayFromZero);
                double paidd = Math.Round(double.Parse(paidCash.Text), 2, MidpointRounding.AwayFromZero);
                double nunpaid = finalsum - paidd;
                var f = nunpaid.ToString();
                if (nunpaid >= 0)
                {
                    unPaid.Text = f;
                }
                else
                {
                    paidCash.Text = finalsum.ToString();
                }
            }
            catch
            {

            }
        }
        private void UpdateItems()
        {
            for (int iy = 0; iy < dataGridView1.Rows.Count; iy++)
            {
                string nbarcode = dataGridView1.Rows[iy].Cells["BarCode"].Value.ToString();
                var barcode = nbarcode;
                var cou = ItemP2.Find(i => i.BarCode == barcode);
                var nowCount = cou.Count;
                var NumOfSales = cou.NumOfSales;
                double Minus = double.Parse(dataGridView1.Rows[iy].Cells["count"].Value.ToString());

                // الكمية المعدله
                double NewCount = nowCount - Minus;

                // إجمالي عدد المبيعات من المنتج
                double newNumOfSales = Minus + NumOfSales;


                double NowTS = cou.TotalSell;
                double newTS = double.Parse(dataGridView1.Rows[iy].Cells["TotalBuy"].Value.ToString());

                // إجمالي المبيعات المعدل
                double NewTotalS = NowTS + newTS;


                double NowTE = cou.Earn;
                var inE = double.Parse(dataGridView1.Rows[iy].Cells["Earn"].Value.ToString());

                // إجمالي الأرباح المعدل
                double NewTotaE = NowTE + inE;

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@Count", NewCount);
                dic.Add("@TotalSell", NewTotalS);
                dic.Add("@Earn", NewTotaE);
                dic.Add("@NumOfSales", newNumOfSales);
                dic.Add("@barcode", cou.BarCode);

                var UpdateItems = DBConnection.SqliteCommand("update Items Set  Count = @Count , TotalSell =@TotalSell , Earn =@Earn, NumOfSales =@NumOfSales  where BarCode =@barcode", dic);

            }
        }
        private void UpdateCustomers()
        {
            try
            {
                var id = int.Parse(sellerName.SelectedValue.ToString());
                var cup = Customers.Find(ic => ic.id == id);
                var nowCash = cup.Cash;
                var unpaid = Double.Parse(unPaid.Text);
                int cID = id;
                double newCash = nowCash + unpaid;

                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@cID", cID);
                addC.Add("@Cash", newCash);
                var UpdateCu = DBConnection.SqliteCommand("update Customers Set Cash = @Cash where id=@cID", addC);
            }
            catch { }
        }
        private void InsertSells()
        {
            try
            {

                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    int innum = int.Parse(label7.Text.ToString());
                    string barcoded = dataGridView1.Rows[i].Cells["BarCode"].Value.ToString();
                    string initem = dataGridView1.Rows[i].Cells["Column5"].Value.ToString();
                    var incount = dataGridView1.Rows[i].Cells["count"].Value.ToString();
                    var inSP = dataGridView1.Rows[i].Cells["BuyPrice"].Value.ToString();
                    var inBP = dataGridView1.Rows[i].Cells["SePrice"].Value.ToString();
                    var inTS = dataGridView1.Rows[i].Cells["TotalBuy"].Value.ToString();
                    var inTE = dataGridView1.Rows[i].Cells["Earn"].Value.ToString();
                    var cID = int.Parse(sellerName.SelectedValue.ToString());
                    var cName = this.sellerName.GetItemText(this.sellerName.SelectedItem);

                    Dictionary<string, object> addC = new Dictionary<string, object>();
                    addC.Add("@Num", innum);
                    addC.Add("@BarCode", barcoded);
                    addC.Add("@itemname", initem);
                    addC.Add("@count", incount);
                    addC.Add("@itemprice", inSP);
                    addC.Add("@buyprice", inBP);
                    addC.Add("@totalprice", inTS);
                    addC.Add("@sellforid", cID);
                    addC.Add("@sellfor", cName);
                    addC.Add("@Userid", Login.cid);
                    addC.Add("@User", Login.cnn);
                    addC.Add("@ddate", DateTime.Now.ToString("yyyy-MM-dd"));
                    addC.Add("@ttime", DateTime.Now.ToString("HH:mm:ss"));
                    addC.Add("@earn", inTE);
                    var addToSales = DBConnection.SqliteCommand("INSERT  INTO `Sells`(`Num`, `BarCode`, `itemname`, `count`, `itemprice`,`buyprice`,`totalprice`,`sellforid`,`sellfor`,`Userid`,`User`,`ddate`,`ttime`,`earn`) " + "values(@Num,@BarCode,@itemname,@count,@itemprice,@buyprice,@totalprice,@sellforid,@sellfor,@Userid,@User,@ddate,@ttime,@earn)", addC);
                }
            }
            catch
            {
            }
        }
        private void InsertCashInvS()
        {
            try
            {
                int invNum = int.Parse(label7.Text.ToString());
                var unpaid = unPaid.Text;
                var totalInv = Total.Text;
                var clientID = int.Parse(sellerName.SelectedValue.ToString());
                var clientName = this.sellerName.GetItemText(this.sellerName.SelectedItem);

                Dictionary<string, object> addC = new Dictionary<string, object>();
                addC.Add("@invNum", invNum);
                addC.Add("@unpaid", unpaid);
                addC.Add("@totalInv", totalInv);
                addC.Add("@clientID", clientID);
                addC.Add("@userID", Login.cid);
                addC.Add("@clientName", clientName);
                addC.Add("@userName", Login.cnn);
                addC.Add("@invDateTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                var addToPurchases = DBConnection.SqliteCommand("INSERT  INTO `CashInvS`(`invNum`, `unpaid`, `totalInv`, `clientID`, `userID`,`clientName`,`userName`,`invDateTime`) values(@invNum,@unpaid,@totalInv,@clientID,@userID,@clientName,@userName,@invDateTime)", addC);
            }
            catch
            {
            }
        }
        private void PrintSalesInvoice()
        {
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("@r", NuM);
                var check = DBConnection.SqliteCommand("SELECT * FROM Sells where Num=@r", dic);
                var invCash = DBConnection.SqliteCommand("SELECT * FROM CashInvS where invNum=@r", dic).ConvertCashSales();
                if (invCash == null) return;
                if (check == null) return;
                if (check.Rows.Count > 0)
                {
                    foreach (CashInvS item in invCash)
                    {
                        inunpaid12 = item.unpaid;
                        var invtotal12 = item.totalInv;
                        invpaid12 = invtotal12 - inunpaid12;
                    }

                    string d1 = DateTime.Now.ToString("dd");
                    string m1 = DateTime.Now.ToString("MM");
                    string y1 = DateTime.Now.ToString("yyyy");
                    string ti1 = DateTime.Now.ToString("t");
                    string PrintDate1 = DateTime.Now.ToString(Login.arNumber(d1) + " " + "-" + " " + Login.arDate(m1) + " " + "-" + " " + Login.arNumber(y1));
                    string PrintTime1 = DateTime.Now.ToString(Login.arNumber(ti1));
                    string PrintDateTime = "تمت طباعة الفاتورة يوم" + " " + ":" + " " + PrintDate1 + " " + "الساعة" + " " + ":" + " " + PrintTime1;
                    Print2S printPurch = new Print2S();
                    printPurch.SetDataSource(check);
                    printPurch.SetParameterValue("invsName", "فاتورة مبيعات");
                    printPurch.SetParameterValue("logoUrl", Dashboard.logopath);
                    printPurch.SetParameterValue("paid", invpaid12);
                    printPurch.SetParameterValue("unpaid", inunpaid12);
                    printPurch.SetParameterValue("PrintDateTime", PrintDateTime);
                    printPurch.SetParameterValue("name", Dashboard.invCoN);
                    printPurch.SetParameterValue("slug", Dashboard.invCoS);
                    printPurch.SetParameterValue("phone", Login.arNumber(Dashboard.invCoP));
                    printPurch.SetParameterValue("address", Dashboard.invCoA);
                    printPurch.PrintToPrinter(1, false, 0, 0);
                }

            }
            catch
            {
            }
        }
        private void AfterAdd()
        {
            DialogResult dialogResult = MessageBox.Show("هل تود طباعة فاتورة المبيعات ؟", "تم إضافة الفاتورة بنجاح", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                PrintSalesInvoice();
                this.Close();

            }
            else if (dialogResult == DialogResult.No)
            {
                this.Close();
            }
        }
        private void AddIn_Click(object sender, EventArgs e)
        {
            if (double.Parse(unPaid.Text) > 0)
            {
                UpdateCustomers();
                InsertSells();
                UpdateItems();
                InsertCashInvS();
                AfterAdd();
            }
            else
            {
                InsertSells();
                UpdateItems();
                InsertCashInvS();
                AfterAdd();
            }
        }
        private void newSeller_Click(object sender, EventArgs e)
        {
            if (newSeller.Text == "عميل جديد")
            {
                FastAddCustomer fastuser = new FastAddCustomer();
                fastuser.FormClosed += FastAddCustomerClosed;
                fastuser.ShowDialog();
            }
            else if (newSeller.Text == "تعديل")
            {
                sellerName.Enabled = true;
                AddIn.Enabled = false;
                newSeller.Text = "حفظ";
                newbTN.Visible = true;
                newSeller.Size = new Size(65, 25);
                newSeller.BackColor = Color.Blue;
                newSeller.Activecolor = Color.Blue;
                newSeller.Normalcolor = Color.Blue;
                newSeller.OnHovercolor = Color.DarkBlue;
            }
            else if (newSeller.Text == "حفظ")
            {
                int id;
                int uid = 0;
                try
                {
                    id = int.Parse(sellerName.SelectedValue.ToString());
                    var cup = Customers.Find(ur => ur.id == id);
                    uid = cup.id;
                }
                catch
                {

                }
                if (uid <= 0)
                {
                    MessageBox.Show("برجاء إختيار العميل بشكل صحيح");
                }
                else if (uid == 1)
                {
                    if (dataGridView1.Rows.Count > 0)
                    {
                        AddIn.Enabled = true;
                    }
                    else
                    {
                        AddIn.Enabled = false;
                    }
                    sellerName.Enabled = false;
                    paidCash.Enabled = false;
                    newSeller.Text = "تعديل";
                    newbTN.Visible = false;
                    newSeller.Size = new Size(94, 25);
                    newSeller.BackColor = Color.Red;
                    newSeller.Activecolor = Color.Red;
                    newSeller.Normalcolor = Color.Red;
                    newSeller.OnHovercolor = Color.Crimson;
                }

                else
                {
                    if (dataGridView1.Rows.Count > 0)
                    {
                        AddIn.Enabled = true;
                    }
                    else
                    {
                        AddIn.Enabled = false;
                    }
                    sellerName.Enabled = false;
                    paidCash.Enabled = true;
                    newSeller.Text = "تعديل";
                    newbTN.Visible = false;
                    newSeller.Size = new Size(94, 25);
                    newSeller.BackColor = Color.Red;
                    newSeller.Activecolor = Color.Red;
                    newSeller.Normalcolor = Color.Red;
                    newSeller.OnHovercolor = Color.Crimson;

                }

            }
        }
        void FastAddCustomerClosed(object sender, FormClosedEventArgs e)
        {
            Customers.Clear();
            CustomersClientsData();
        }
        private void newbTN_Click(object sender, EventArgs e)
        {
            FastAddCustomer fastuser = new FastAddCustomer();
            fastuser.FormClosed += FastAddCustomerClosed;
            fastuser.ShowDialog();
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }

            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }

        private void Roles() 
        {
            if (Login.r4 == "1")
            {
                Sprice.Enabled = true;
            }
        }
    }
}

