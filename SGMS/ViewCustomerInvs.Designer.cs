﻿namespace SGMS
{
    partial class ViewCustomerInvs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.Label();
            this.ViewInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PrintInvByNum = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PayBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.viUnpaid = new Bunifu.Framework.UI.BunifuTileButton();
            this.label4 = new System.Windows.Forms.Label();
            this.invClient = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.dataGridView1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.invNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invpaid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unpaidinv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalInv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cairo", 7F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label1.Location = new System.Drawing.Point(325, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 19);
            this.label1.TabIndex = 50;
            this.label1.Text = "إسم العميل";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cairo", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label2.Location = new System.Drawing.Point(324, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 19);
            this.label2.TabIndex = 51;
            this.label2.Text = "رقم الهاتف";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cairo", 25F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(115)))), ((int)(((byte)(155)))));
            this.label3.Location = new System.Drawing.Point(387, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 63);
            this.label3.TabIndex = 53;
            this.label3.Text = "فواتير العميل";
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.label9);
            this.metroPanel1.Controls.Add(this.Total);
            this.metroPanel1.Controls.Add(this.ViewInvByNum);
            this.metroPanel1.Controls.Add(this.PrintInvByNum);
            this.metroPanel1.Controls.Add(this.PayBtn);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(20, 450);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(732, 43);
            this.metroPanel1.TabIndex = 54;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cairo", 7F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.label9.Location = new System.Drawing.Point(574, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 19);
            this.label9.TabIndex = 49;
            this.label9.Text = "إجمالي الغير محصل للعميل";
            // 
            // Total
            // 
            this.Total.Font = new System.Drawing.Font("Cairo", 8F);
            this.Total.ForeColor = System.Drawing.Color.Black;
            this.Total.Location = new System.Drawing.Point(516, 10);
            this.Total.Name = "Total";
            this.Total.Size = new System.Drawing.Size(53, 24);
            this.Total.TabIndex = 50;
            this.Total.Text = "0";
            this.Total.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ViewInvByNum
            // 
            this.ViewInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ViewInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ViewInvByNum.BorderRadius = 7;
            this.ViewInvByNum.ButtonText = "عرض الفاتورة";
            this.ViewInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewInvByNum.DisabledColor = System.Drawing.Color.White;
            this.ViewInvByNum.Enabled = false;
            this.ViewInvByNum.Font = new System.Drawing.Font("Cairo", 10F);
            this.ViewInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.ViewInvByNum.Iconimage = global::SGMS.Properties.Resources.plus_480px;
            this.ViewInvByNum.Iconimage_right = null;
            this.ViewInvByNum.Iconimage_right_Selected = null;
            this.ViewInvByNum.Iconimage_Selected = null;
            this.ViewInvByNum.IconMarginLeft = 25;
            this.ViewInvByNum.IconMarginRight = 0;
            this.ViewInvByNum.IconRightVisible = false;
            this.ViewInvByNum.IconRightZoom = 0D;
            this.ViewInvByNum.IconVisible = false;
            this.ViewInvByNum.IconZoom = 30D;
            this.ViewInvByNum.IsTab = false;
            this.ViewInvByNum.Location = new System.Drawing.Point(263, 7);
            this.ViewInvByNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.ViewInvByNum.Name = "ViewInvByNum";
            this.ViewInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.ViewInvByNum.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(210)))));
            this.ViewInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.ViewInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ViewInvByNum.selected = false;
            this.ViewInvByNum.Size = new System.Drawing.Size(119, 30);
            this.ViewInvByNum.TabIndex = 48;
            this.ViewInvByNum.Text = "عرض الفاتورة";
            this.ViewInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ViewInvByNum.Textcolor = System.Drawing.Color.White;
            this.ViewInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewInvByNum.Click += new System.EventHandler(this.ViewInvByNum_Click);
            // 
            // PrintInvByNum
            // 
            this.PrintInvByNum.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PrintInvByNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PrintInvByNum.BorderRadius = 7;
            this.PrintInvByNum.ButtonText = "طباعة الفاتورة";
            this.PrintInvByNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PrintInvByNum.DisabledColor = System.Drawing.Color.White;
            this.PrintInvByNum.Enabled = false;
            this.PrintInvByNum.Font = new System.Drawing.Font("Cairo", 8F);
            this.PrintInvByNum.Iconcolor = System.Drawing.Color.Transparent;
            this.PrintInvByNum.Iconimage = null;
            this.PrintInvByNum.Iconimage_right = null;
            this.PrintInvByNum.Iconimage_right_Selected = null;
            this.PrintInvByNum.Iconimage_Selected = null;
            this.PrintInvByNum.IconMarginLeft = 0;
            this.PrintInvByNum.IconMarginRight = 0;
            this.PrintInvByNum.IconRightVisible = false;
            this.PrintInvByNum.IconRightZoom = 0D;
            this.PrintInvByNum.IconVisible = false;
            this.PrintInvByNum.IconZoom = 20D;
            this.PrintInvByNum.IsTab = false;
            this.PrintInvByNum.Location = new System.Drawing.Point(138, 7);
            this.PrintInvByNum.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PrintInvByNum.Name = "PrintInvByNum";
            this.PrintInvByNum.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PrintInvByNum.OnHovercolor = System.Drawing.Color.Green;
            this.PrintInvByNum.OnHoverTextColor = System.Drawing.Color.White;
            this.PrintInvByNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PrintInvByNum.selected = false;
            this.PrintInvByNum.Size = new System.Drawing.Size(119, 30);
            this.PrintInvByNum.TabIndex = 47;
            this.PrintInvByNum.Text = "طباعة الفاتورة";
            this.PrintInvByNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PrintInvByNum.Textcolor = System.Drawing.Color.White;
            this.PrintInvByNum.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintInvByNum.Click += new System.EventHandler(this.PrintInvByNum_Click);
            // 
            // PayBtn
            // 
            this.PayBtn.Activecolor = System.Drawing.Color.Maroon;
            this.PayBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.PayBtn.BackColor = System.Drawing.Color.Maroon;
            this.PayBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PayBtn.BorderRadius = 7;
            this.PayBtn.ButtonText = "تحصيل";
            this.PayBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PayBtn.DisabledColor = System.Drawing.Color.White;
            this.PayBtn.Enabled = false;
            this.PayBtn.Font = new System.Drawing.Font("Cairo", 8F);
            this.PayBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.PayBtn.Iconimage = null;
            this.PayBtn.Iconimage_right = null;
            this.PayBtn.Iconimage_right_Selected = null;
            this.PayBtn.Iconimage_Selected = null;
            this.PayBtn.IconMarginLeft = 0;
            this.PayBtn.IconMarginRight = 0;
            this.PayBtn.IconRightVisible = false;
            this.PayBtn.IconRightZoom = 0D;
            this.PayBtn.IconVisible = false;
            this.PayBtn.IconZoom = 20D;
            this.PayBtn.IsTab = false;
            this.PayBtn.Location = new System.Drawing.Point(13, 7);
            this.PayBtn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.PayBtn.Name = "PayBtn";
            this.PayBtn.Normalcolor = System.Drawing.Color.Maroon;
            this.PayBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PayBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.PayBtn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PayBtn.selected = false;
            this.PayBtn.Size = new System.Drawing.Size(119, 30);
            this.PayBtn.TabIndex = 46;
            this.PayBtn.Text = "تحصيل";
            this.PayBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PayBtn.Textcolor = System.Drawing.Color.White;
            this.PayBtn.TextFont = new System.Drawing.Font("Cairo SemiBold", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayBtn.Click += new System.EventHandler(this.PayBtn_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.label5);
            this.metroPanel2.Controls.Add(this.viUnpaid);
            this.metroPanel2.Controls.Add(this.label4);
            this.metroPanel2.Controls.Add(this.invClient);
            this.metroPanel2.Controls.Add(this.pictureBox1);
            this.metroPanel2.Controls.Add(this.label2);
            this.metroPanel2.Controls.Add(this.label3);
            this.metroPanel2.Controls.Add(this.label1);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(20, 30);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(732, 81);
            this.metroPanel2.TabIndex = 55;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.Font = new System.Drawing.Font("Cairo", 7F);
            this.label5.ForeColor = System.Drawing.Color.Maroon;
            this.label5.Location = new System.Drawing.Point(6, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 19);
            this.label5.TabIndex = 58;
            this.label5.Text = "غير محصل العميل فقط";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // viUnpaid
            // 
            this.viUnpaid.BackColor = System.Drawing.Color.Maroon;
            this.viUnpaid.color = System.Drawing.Color.Maroon;
            this.viUnpaid.colorActive = System.Drawing.Color.Firebrick;
            this.viUnpaid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.viUnpaid.Font = new System.Drawing.Font("Century Gothic", 15.75F);
            this.viUnpaid.ForeColor = System.Drawing.Color.White;
            this.viUnpaid.Image = global::SGMS.Properties.Resources.search_52px;
            this.viUnpaid.ImagePosition = 10;
            this.viUnpaid.ImageZoom = 25;
            this.viUnpaid.LabelPosition = 0;
            this.viUnpaid.LabelText = "";
            this.viUnpaid.Location = new System.Drawing.Point(32, 31);
            this.viUnpaid.Margin = new System.Windows.Forms.Padding(6);
            this.viUnpaid.Name = "viUnpaid";
            this.viUnpaid.Size = new System.Drawing.Size(76, 40);
            this.viUnpaid.TabIndex = 57;
            this.viUnpaid.Click += new System.EventHandler(this.viUnpaid_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Cairo", 8F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(153, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 19);
            this.label4.TabIndex = 56;
            // 
            // invClient
            // 
            this.invClient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.invClient.Font = new System.Drawing.Font("Cairo", 8F);
            this.invClient.ForeColor = System.Drawing.Color.Black;
            this.invClient.Location = new System.Drawing.Point(160, 18);
            this.invClient.Name = "invClient";
            this.invClient.Size = new System.Drawing.Size(159, 19);
            this.invClient.TabIndex = 55;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SGMS.Properties.Resources.unpaidin;
            this.pictureBox1.Location = new System.Drawing.Point(623, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 54;
            this.pictureBox1.TabStop = false;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.dataGridView1);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(20, 111);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(732, 339);
            this.metroPanel3.TabIndex = 56;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invNum,
            this.invpaid,
            this.unpaidinv,
            this.totalInv,
            this.invDate,
            this.invTime});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.DoubleBuffered = true;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderBgColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(119)))), ((int)(((byte)(165)))));
            this.dataGridView1.HeaderForeColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Cairo", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(732, 339);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // invNum
            // 
            this.invNum.FillWeight = 87.27693F;
            this.invNum.HeaderText = "فاتورة";
            this.invNum.Name = "invNum";
            this.invNum.ReadOnly = true;
            // 
            // invpaid
            // 
            this.invpaid.HeaderText = "المدفوع";
            this.invpaid.Name = "invpaid";
            this.invpaid.ReadOnly = true;
            // 
            // unpaidinv
            // 
            this.unpaidinv.HeaderText = "غير مدفوع";
            this.unpaidinv.Name = "unpaidinv";
            this.unpaidinv.ReadOnly = true;
            // 
            // totalInv
            // 
            this.totalInv.HeaderText = "إجمالي الفاتورة";
            this.totalInv.Name = "totalInv";
            this.totalInv.ReadOnly = true;
            // 
            // invDate
            // 
            this.invDate.HeaderText = "تاريخ الفاتورة";
            this.invDate.Name = "invDate";
            this.invDate.ReadOnly = true;
            // 
            // invTime
            // 
            this.invTime.HeaderText = "وقت الفاتورة";
            this.invTime.Name = "invTime";
            this.invTime.ReadOnly = true;
            // 
            // ViewCustomerInvs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 513);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.DisplayHeader = false;
            this.Name = "ViewCustomerInvs";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.metroPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private Bunifu.Framework.UI.BunifuCustomDataGrid dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn invNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn invpaid;
        private System.Windows.Forms.DataGridViewTextBoxColumn unpaidinv;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalInv;
        private System.Windows.Forms.DataGridViewTextBoxColumn invDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn invTime;
        private Bunifu.Framework.UI.BunifuFlatButton PayBtn;
        private Bunifu.Framework.UI.BunifuFlatButton PrintInvByNum;
        private Bunifu.Framework.UI.BunifuFlatButton ViewInvByNum;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label invClient;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label Total;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuTileButton viUnpaid;
    }
}